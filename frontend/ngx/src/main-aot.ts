/* tslint:disable:ordered-imports */

// this must be imported first
import '../../build/cli/src/main/app';

import {enableProdMode} from '@angular/core';
import {downgradeModule, getAngularJSGlobal} from '@angular/upgrade/static';
// @ts-ignore
import {AppModuleNgFactory} from './app/app.module.ngfactory';
import {environment} from './environments/environment';

declare var Browser: any;

if (environment.production) {
  enableProdMode();
}

/*
 * Downgrade Angular application.
 */
getAngularJSGlobal().module('ng-upgrade', [
  'coyo.app',
  downgradeModule(AppModuleNgFactory)
]);

/*
 * Manually bootstrap AngularJS application.
 */
if (!Browser.isPartiallySupported()) {
  Browser.showErrorPage();
} else {
  if (!Browser.isFullySupported()) {
    Browser.showErrorBar();
  }

  if (window['cordova']) {
    // tslint:disable-next-line:no-console
    console.log('Running in Cordova, starting the application once "deviceready" event fires.');
    document.addEventListener('deviceready', function(): void {
      // tslint:disable-next-line:no-console
      console.log('"deviceready" event has fired, starting COYO now.');
      getAngularJSGlobal().bootstrap(document.body, ['ng-upgrade'], {strictDi: true});
    }, false);
  } else {
    // tslint:disable-next-line:no-console
    console.log('Running in browser, starting COYO now.');
    getAngularJSGlobal().bootstrap(document.body, ['ng-upgrade'], {strictDi: true});
  }
}
