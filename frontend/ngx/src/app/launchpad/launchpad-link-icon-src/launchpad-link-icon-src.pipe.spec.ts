import {UrlService} from '@core/http/url/url.service';
import {LaunchpadLink} from '@domain/launchpad/launchpad-link';
import {LaunchpadLinkIconSrcPipe} from './launchpad-link-icon-src.pipe';

describe('LaunchpadLinkIconSrcPipe', () => {
  let pipe: LaunchpadLinkIconSrcPipe;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    urlService = jasmine.createSpyObj('UrlService', ['getBackendUrl']);
    urlService.getBackendUrl.and.returnValue('backendUrl');
    pipe = new LaunchpadLinkIconSrcPipe(urlService);
  });

  it('should transform null', () => {
    // when
    const result1 = pipe.transform(null);
    const result2 = pipe.transform({
      id: 'some-id',
      modified: 1234,
      icon: null
    } as LaunchpadLink);

    // then
    expect(result1).toBeNull();
    expect(result2).toBeNull();
  });

  it('should transform a link', () => {
    // when
    const result = pipe.transform({
      id: 'some-id',
      modified: 1234,
      icon: {}
    } as LaunchpadLink);

    // then
    expect(result).toEqual('backendUrl/web/launchpad/links/some-id/icon?modified=1234');
  });
});
