/**
 * The user settings of the launchpad.
 */
export interface LaunchpadSettings {
  columns: boolean;
  condensed: boolean;
}
