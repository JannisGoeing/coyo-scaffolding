import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {GeneralSettingsComponent} from '@app/admin/settings/general-settings/general-settings.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoGeneralSettings', downgradeComponent({
    component: GeneralSettingsComponent,
    propagateDigest: false
  }));
