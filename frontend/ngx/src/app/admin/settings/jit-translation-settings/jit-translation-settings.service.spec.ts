import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {JitTranslationSettings} from '@app/admin/settings/jit-translation-settings/jit-translation-settings';
import {ServiceRecognitionService} from '@core/http/service-recognition/service-recognition.service';
import {TranslateService} from '@ngx-translate/core';
import {NotificationService} from '@shared/notifications/notification/notification.service';
import {JitTranslationSettingsService} from './jit-translation-settings.service';

describe('JitTranslationSettingsService', () => {
  let httpTestingController: HttpTestingController;
  let jitTranslationSettingsService: JitTranslationSettingsService;
  let translateService: jasmine.SpyObj<TranslateService>;
  let notificationService: jasmine.SpyObj<NotificationService>;
  let serviceRecognitionService: jasmine.SpyObj<ServiceRecognitionService>;

  const DEEPL = 'DEEPL';
  const TEST_API_KEY = 'Test-key-123';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [JitTranslationSettingsService, {
        provide: NotificationService,
        useValue: jasmine.createSpyObj('notificationService', ['error'])
      }, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['instant'])
      }, {
        provide: ServiceRecognitionService,
        useValue: jasmine.createSpyObj('serviceRecognitionService', ['isServiceDisabled'])
      }]
    });
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    jitTranslationSettingsService = TestBed.get(JitTranslationSettingsService);
    translateService = TestBed.get(TranslateService);
    notificationService = TestBed.get(NotificationService);
    serviceRecognitionService = TestBed.get(ServiceRecognitionService);
  });

  it('should get jit settings', () => {
    // given

    // when
    jitTranslationSettingsService.getSettings().subscribe(jitSettings => {
      expect(jitSettings.activeProvider).toBe(DEEPL);
      expect(jitSettings.apiKey).toBe(TEST_API_KEY);
    });

    // then
    const req = httpTestingController.expectOne('/web/translation/settings');

    expect(req.request.method).toEqual('GET');
    req.flush({
      activeProvider: DEEPL,
      apiKey: TEST_API_KEY
    });
  });

  it('should put jit settings', () => {
    // given
    const jitTranslationSettings: JitTranslationSettings = {
      activeProvider: DEEPL,
      apiKey: TEST_API_KEY
    };

    // when
    jitTranslationSettingsService.putSettings(jitTranslationSettings).subscribe(jitSettings => {
      expect(jitSettings.activeProvider).toBe(DEEPL);
      expect(jitSettings.apiKey).toBe(TEST_API_KEY);
    });

    // then
    const req = httpTestingController.expectOne('/web/translation/settings');
    expect(req.request.method).toEqual('PUT');

    req.flush({
      activeProvider: DEEPL,
      apiKey: TEST_API_KEY
    });
  });

  it('should set active value', () => {
    // given
    const activeLanguages = ['DE'];

    // when
    jitTranslationSettingsService.getActiveSettings().subscribe(active => {
      expect(active.activeLanguages).toBe(activeLanguages);
    });

    // then
    const req = httpTestingController.expectOne('/web/translation/settings/activeLanguages');
    expect(req.request.method).toEqual('GET');
    req.flush({
      activeLanguages: activeLanguages
    });
  });

  it('should return inactive when service is disabled', () => {
    // given
    const activeLanguages: string[] = [];
    serviceRecognitionService.isServiceDisabled.and.returnValue(true);

    // when
    jitTranslationSettingsService.getActiveSettings().subscribe(active => {
      expect(active.activeLanguages).toEqual(activeLanguages);
    });

    // then
  });

  it('should cache active request', () => {
    // given
    const activeLanguages = ['DE'];
    jitTranslationSettingsService.getActiveSettings().subscribe();

    // when
    jitTranslationSettingsService.getActiveSettings().subscribe(active => {
      expect(active.activeLanguages).toBe(activeLanguages);
    });

    // then
    const req = httpTestingController.expectOne('/web/translation/settings/activeLanguages');
    expect(req.request.method).toEqual('GET');
    req.flush({
      activeLanguages: activeLanguages
    });
    httpTestingController.verify();
  });
});
