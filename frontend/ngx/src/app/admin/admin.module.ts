import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {generalSettingsState} from '@app/admin/settings/general-settings/general-settings.state';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {HelpModule} from '@shared/help/help.module';
import {UIRouterUpgradeModule} from '@uirouter/angular-hybrid';
import {CollapseModule} from 'ngx-bootstrap';
import {GeneralSettingsComponent} from './settings/general-settings/general-settings.component';
import './settings/general-settings/general-settings.component.downgrade';

/**
 * Module for the administration area
 */
@NgModule({
  declarations: [GeneralSettingsComponent],
  entryComponents: [GeneralSettingsComponent],
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    HelpModule,
    UIRouterUpgradeModule.forChild({
      states: [ generalSettingsState]
    }),
    ReactiveFormsModule,
    CollapseModule
  ]
})
export class AdminModule { }
