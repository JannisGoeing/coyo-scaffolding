/**
 * Verification data for teams authentication
 */
export interface TeamsVerificationData {
  verificationToken: string;
}
