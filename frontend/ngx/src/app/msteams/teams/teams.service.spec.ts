import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {TeamsService} from './teams.service';

describe('TeamsService', () => {
  let service: TeamsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TeamsService]
    });
    service = TestBed.get(TeamsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send the backend token', () => {
    // given
    const token = 'token';
    const verificationData = {
      verificationToken: 'verifyToken'
    };

    // when
    const result = service.sendToken(token);

    // then
    let called = false;
    result.subscribe(resultData => {
      called = true;
      expect(resultData).toEqual(verificationData);
    });

    const req = httpMock.expectOne('/web/teams/auth/success');
    req.flush(verificationData);

    expect(called).toBeTruthy();
  });
});
