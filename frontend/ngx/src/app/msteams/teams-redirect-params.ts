/**
 * DTO for teams redirect parameters
 */
export interface TeamsRedirectParams {
  userId: string;
}
