import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialogRef} from '@angular/material/dialog';
import {SocketConnectionLostModalComponent} from '@app/socket/socket-connection-lost-modal/socket-connection-lost-modal.component';

describe('SocketConnectionLostModalComponent', () => {
  let component: SocketConnectionLostModalComponent;
  let fixture: ComponentFixture<SocketConnectionLostModalComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<SocketConnectionLostModalComponent, boolean>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SocketConnectionLostModalComponent],
      providers: [{
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('MatDialogRef', ['close'])
      }]
    }).overrideTemplate(SocketConnectionLostModalComponent, '')
      .compileComponents();
    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketConnectionLostModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close modal with reload flag set as result', () => {
    // when
    component.onReload();

    // then
    expect(dialogRef.close).toHaveBeenCalledWith(true);
  });
});
