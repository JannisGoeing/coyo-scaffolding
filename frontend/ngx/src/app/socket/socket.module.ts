/**
 * Module that contains miscellaneous services.
 */
import {NgModule} from '@angular/core';
import {SocketConnectionLostModalComponent} from '@app/socket/socket-connection-lost-modal/socket-connection-lost-modal.component';
import {SocketConnectionMonitorService} from '@app/socket/socket-connection-monitor/socket-connection-monitor.service';
import {CoyoCommonsModule} from '@shared/commons/commons.module';

@NgModule({
  imports: [
    CoyoCommonsModule
  ],
  declarations: [
    SocketConnectionLostModalComponent
  ],
  exports: [
    SocketConnectionLostModalComponent
  ],
  entryComponents: [
    SocketConnectionLostModalComponent
  ],
  providers: [
    SocketConnectionMonitorService
  ]
})
export class SocketModule {
}
