import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {SubscriberInfoDataSource} from '@app/pages/subscriber-data-source/subscriber-info-data-source';
import {MaterialModule} from '@shared/material/material.module';
import {SubscriberModalComponent} from './subscriber-modal.component';

describe('SubscriberModalComponent', () => {
  let component: SubscriberModalComponent;
  let fixture: ComponentFixture<SubscriberModalComponent>;
  let subscriberInfoDataSource: jasmine.SpyObj<SubscriberInfoDataSource>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      declarations: [SubscriberModalComponent],
      providers: [{
        provide: MAT_DIALOG_DATA,
        useValue: jasmine.createSpyObj('SubscriberInfoDataSource', ['setTerm'])
      }, FormBuilder]
    }).overrideTemplate(SubscriberModalComponent, '<cdk-virtual-scroll-viewport [itemSize]="50"></cdk-virtual-scroll-viewport>')
      .compileComponents();
    subscriberInfoDataSource = TestBed.get(MAT_DIALOG_DATA);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriberModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should search for term', () => {
    const scrollToIndex = spyOn(component.cdkVirtualScrollViewport, 'scrollToIndex');

    // when
    component.search('term');

    // then
    expect(scrollToIndex).toHaveBeenCalledWith(0);
    expect(subscriberInfoDataSource.setTerm).toHaveBeenCalledWith('term');
  });
});
