import {ListRange} from '@angular/cdk/collections';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SubscriberInfoDataSource} from '@app/pages/subscriber-data-source/subscriber-info-data-source';
import {Page} from '@domain/pagination/page';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {User} from '@domain/user/user';
import * as _ from 'lodash';
import {of, Subject} from 'rxjs';

describe('SubscriberInfoDataSource', () => {
  let subscriberInfoDataSource: SubscriberInfoDataSource;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  const TARGET_ID = 'pageId';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: SubscriptionService,
        useValue: jasmine.createSpyObj('SubscriptionService', ['getSubscribersByTargetId'])
      }]
    });
    subscriptionService = TestBed.get(SubscriptionService);
    subscriberInfoDataSource = new SubscriberInfoDataSource(subscriptionService, TARGET_ID, 80);
  });

  it('should get the subscriber count', () => {
    // when
    const subscriberCount = subscriberInfoDataSource.subscriberCount;

    // then
    expect(subscriberCount).toBe(80);
  });

  it('should load initial page', () => {
    // given
    const data: User[][] = [];
    const users = createUsers('User', 50);

    subscriptionService.getSubscribersByTargetId.and.returnValue(of(createPage(users)));

    // when
    subscriberInfoDataSource.connect({
      viewChange: new Subject<ListRange>().asObservable()
    }).subscribe(result => data.push(result));

    // then
    expect(subscriptionService.getSubscribersByTargetId).toHaveBeenCalledTimes(1);
    expect(subscriptionService.getSubscribersByTargetId).toHaveBeenCalledWith(
      TARGET_ID, jasmine.objectContaining({page: 0, pageSize: 50}), '');

    expect(data.length).toBe(1);
    expect(data[0]).toEqual(_.concat(users, Array(30)));
  });

  it('should load first page on search', () => {
    // given
    const data: User[][] = [];
    const users = createUsers('User', 50);
    const usersForTerm = createUsers('JohnDoe', 50);

    subscriptionService.getSubscribersByTargetId.and.returnValues(
      of(createPage(users)),
      of(createPage(usersForTerm)));

    subscriberInfoDataSource.connect({
      viewChange: new Subject<ListRange>().asObservable()
    }).subscribe(result => data.push(result));

    // when
    subscriberInfoDataSource.setTerm('John');

    // then
    expect(subscriptionService.getSubscribersByTargetId).toHaveBeenCalledTimes(2);
    expect(subscriptionService.getSubscribersByTargetId.calls.argsFor(0)).toEqual(
      [TARGET_ID, jasmine.objectContaining({page: 0, pageSize: 50}), '']);
    expect(subscriptionService.getSubscribersByTargetId.calls.argsFor(1)).toEqual(
      [TARGET_ID, jasmine.objectContaining({page: 0, pageSize: 50}), 'John']);

    expect(data.length).toBe(3);
    expect(data[0]).toEqual(_.concat(users, Array(30)));
    expect(data[1]).toEqual(Array(80));
    expect(data[2]).toEqual(_.concat(usersForTerm, Array(30)));
  });

  it('should load second page on scroll', fakeAsync(() => {
    // given
    const data: User[][] = [];
    const users1 = createUsers('User', 50);
    const users2 = createUsers('User', 30, 50);

    subscriptionService.getSubscribersByTargetId.and.returnValues(
      of(createPage(users1, 0)),
      of(createPage(users2, 1)));

    const viewChange = new Subject<ListRange>();
    subscriberInfoDataSource.connect({
      viewChange: viewChange.asObservable()
    }).subscribe(result => data.push(result));

    // when
    viewChange.next({start: 50, end: 60});
    tick(250);

    // then
    expect(subscriptionService.getSubscribersByTargetId).toHaveBeenCalledTimes(2);
    expect(subscriptionService.getSubscribersByTargetId.calls.argsFor(0)).toEqual(
      [TARGET_ID, jasmine.objectContaining({page: 0, pageSize: 50}), '']);
    expect(subscriptionService.getSubscribersByTargetId.calls.argsFor(1)).toEqual(
      [TARGET_ID, jasmine.objectContaining({page: 1, pageSize: 50}), '']);

    expect(data.length).toBe(2);
    expect(data[0]).toEqual(_.concat(users1, Array(30)));
    expect(data[1]).toEqual(_.concat(users1, users2));
  }));

  function createUsers(prefix: string, length: number, offset: number = 0): User[] {
    return Array.from({length}).map((val, idx) => ({displayName: prefix + (offset + idx)} as User));
  }

  function createPage(content: User[], number: number = 0): Page<User> {
    return {content, number, totalElements: 80} as Page<User>;
  }
});
