import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {SubscriberInfoDataSource} from '@app/pages/subscriber-data-source/subscriber-info-data-source';
import {SubscriberModalComponent} from '@app/pages/subscriber-modal/subscriber-modal.component';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {SubscriberInfoComponent} from './subscriber-info.component';

describe('SubscriberInfoComponent', () => {
  let component: SubscriberInfoComponent;
  let fixture: ComponentFixture<SubscriberInfoComponent>;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubscriberInfoComponent],
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('MatDialog', ['open'])
      }, {
        provide: SubscriptionService,
        useValue: jasmine.createSpy('SubscriptionService')
      }]
    }).overrideTemplate(SubscriberInfoComponent, '')
      .compileComponents();
    dialog = TestBed.get(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriberInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open subscribers modal dialog', () => {
    // given
    component.targetId = 'pageId';
    component.subscriberCount = 4000;

    // when
    component.openSubscribers();

    // then
    expect(dialog.open).toHaveBeenCalledWith(SubscriberModalComponent,
      jasmine.objectContaining({data: jasmine.anything()}));
    const data: SubscriberInfoDataSource = dialog.open.calls.mostRecent().args[1].data;
    expect(data instanceof SubscriberInfoDataSource).toBeTrue();
    expect(data.subscriberCount).toBe(4000);
  });

  it('should close dialog', () => {
    // given
    component.targetId = 'pageId';
    component.subscriberCount = 4000;
    const dialogRef: jasmine.SpyObj<MatDialogRef<any>> = jasmine.createSpyObj('MatDialogRef', ['close']);
    dialog.open.and.returnValue(dialogRef);
    component.openSubscribers();

    // when
    component.ngOnDestroy();

    // then
    expect(dialogRef.close).toHaveBeenCalled();
  });
});
