import {NgModule} from '@angular/core';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {CoyoFormsModule} from '@shared/forms/forms.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {SocialModule} from '@shared/social/social.module';
import {UpgradeModule} from '@upgrade/upgrade.module';
import {PageVisibilityComponent} from './page-visibility/page-visibility.component';
import './page-visibility/page-visibility.component.downgrade';
import {SubscriberInfoComponent} from './subscriber-info/subscriber-info.component';
import './subscriber-info/subscriber-info.component.downgrade';
import {SubscriberModalComponent} from './subscriber-modal/subscriber-modal.component';

/**
 * Pages feature module
 */
@NgModule({
  declarations: [
    PageVisibilityComponent,
    SubscriberInfoComponent,
    SubscriberModalComponent
  ],
  imports: [
    CoyoCommonsModule,
    CoyoFormsModule,
    SenderUIModule,
    SocialModule,
    UpgradeModule
  ],
  exports: [
    PageVisibilityComponent,
    SubscriberInfoComponent
  ],
  entryComponents: [
    PageVisibilityComponent,
    SubscriberInfoComponent,
    SubscriberModalComponent
  ]
})
export class PagesModule {
}
