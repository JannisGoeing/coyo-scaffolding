import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {EventDateLabelComponent} from '@app/events/event-date-label/event-date-label.component';

getAngularJSGlobal()
  .module('coyo.events')
  .directive('coyoEventDateLabel', downgradeComponent({
    component: EventDateLabelComponent
  }));
