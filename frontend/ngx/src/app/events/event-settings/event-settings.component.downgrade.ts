import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {EventSettingsComponent} from '@app/events/event-settings/event-settings.component';

getAngularJSGlobal()
  .module('coyo.events')
  .directive('coyoEventSettings', downgradeComponent({
    component: EventSettingsComponent
  }));
