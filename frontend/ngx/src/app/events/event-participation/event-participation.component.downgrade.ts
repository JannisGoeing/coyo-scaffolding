import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {EventParticipationComponent} from '@app/events/event-participation/event-participation.component';

getAngularJSGlobal()
  .module('coyo.events')
  .directive('coyoEventParticipation', downgradeComponent({
    component: EventParticipationComponent
  }));
