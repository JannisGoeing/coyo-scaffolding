import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ScreenSize} from '@core/window-size/screen-size';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {ParticipantStatus} from '@domain/event/participant-status';
import {SenderEvent} from '@domain/event/SenderEvent';
import {of} from 'rxjs';

import {EventParticipationComponent} from './event-participation.component';

describe('EventParticipationComponent', () => {
  let component: EventParticipationComponent;
  let fixture: ComponentFixture<EventParticipationComponent>;

  let windowSizeService: jasmine.SpyObj<WindowSizeService>;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EventParticipationComponent],
      providers: [
        {provide: WindowSizeService, useValue: jasmine.createSpyObj('windowSizeService', ['isXs', 'observeScreenChange'])}
      ]
    }).overrideTemplate(EventParticipationComponent, '')
      .compileComponents();

    windowSizeService = TestBed.get(WindowSizeService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    windowSizeService.observeScreenChange.and.returnValue(of(ScreenSize.MD));

    fixture = TestBed.createComponent(EventParticipationComponent);
    component = fixture.componentInstance;
  });

  it('should initialize component options without MAYBE option', () => {
    // given
    component.event = {
      requestDefiniteAnswer: true
    } as SenderEvent;

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(component.options.length).toEqual(2);
    expect(component.options).toContain({status: ParticipantStatus.ATTENDING, icon: 'check'});
    expect(component.options).toContain({status: ParticipantStatus.NOT_ATTENDING, icon: 'close'});
  });

  it('should initialize component options with MAYBE option', () => {
    // given
    component.event = {
      requestDefiniteAnswer: false
    } as SenderEvent;

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(component.options.length).toEqual(3);
    expect(component.options).toContain({status: ParticipantStatus.ATTENDING, icon: 'check'});
    expect(component.options).toContain({status: ParticipantStatus.MAYBE_ATTENDING, icon: 'help'});
    expect(component.options).toContain({status: ParticipantStatus.NOT_ATTENDING, icon: 'close'});
  });

  it('should update status on change, emit event and adjust event', async(() => {
    // given
    const eventId = 'event-id';
    component.event = {
      requestDefiniteAnswer: false,
      status: ParticipantStatus.PENDING,
      id: eventId
    } as SenderEvent;
    spyOn(component.statusChanged, 'emit');

    // when
    fixture.detectChanges();
    component.participantStatusWasChanged({status: ParticipantStatus.ATTENDING, icon: 'check'});

    // then
    const req = httpTestingController.expectOne(request => request.url === '/web/events/' + eventId + '/status');
    req.flush({});
    expect(req.request.method).toEqual('PUT');
    expect(req.request.body).toEqual({status: ParticipantStatus.ATTENDING});
    expect(component.event.status).toEqual(ParticipantStatus.ATTENDING);
    expect(component.statusChanged.emit).toHaveBeenCalledWith({
      oldStatus: ParticipantStatus.PENDING,
      newStatus: ParticipantStatus.ATTENDING,
    });
  }));

  it('should not allow status change to pending', () => {
    // when
    const actualResult = component.isParticipantStatusSelectable({status: ParticipantStatus.PENDING, icon: ''});

    // then
    expect(actualResult).toBeFalsy();
  });

  it('should not allow attending fully booked event', () => {
    // given
    component.event = {
      requestDefiniteAnswer: false,
      limitedParticipants: {
        numberOfAttendingParticipants: 10,
        participantsLimit: 10
      },
    } as SenderEvent;

    // when
    fixture.detectChanges();
    const actualResult = component.isParticipantStatusSelectable({status: ParticipantStatus.PENDING, icon: ''});

    // then
    expect(actualResult).toBeFalsy();
  });

  it('should allow status change to may be attending', () => {
    // when
    const actualResult = component.isParticipantStatusSelectable({status: ParticipantStatus.MAYBE_ATTENDING, icon: 'help'});

    // then
    expect(actualResult).toBeTruthy();
  });

  it('should mark event as fully booked when limit is reached and not already attend', () => {
    // given
    component.event = {
      requestDefiniteAnswer: false,
      limitedParticipants: {
        numberOfAttendingParticipants: 10,
        participantsLimit: 10
      },
      status: ParticipantStatus.PENDING
    } as SenderEvent;

    // when
    fixture.detectChanges();
    const actualResult = component.isFullyBooked();

    // then
    expect(actualResult).toBeTruthy();
  });

  it('should mark event not as fully booked when limit is reached but attend', () => {
    // given
    component.event = {
      requestDefiniteAnswer: false,
      limitedParticipants: {
        numberOfAttendingParticipants: 10,
        participantsLimit: 10
      },
      status: ParticipantStatus.ATTENDING
    } as SenderEvent;

    // when
    fixture.detectChanges();
    const actualResult = component.isFullyBooked();

    // then
    expect(actualResult).toBeFalsy();
  });

  it('should disable menu when event component is used with isOngoing attribute', () => {
    // given
    component.isOngoing = true;

    // when
    const actualResult = component.isMenuDisabled();

    // then
    expect(actualResult).toBeTruthy();
  });

  it('should disable menu when event component is used with hasEnded attribute', () => {
    // given
    component.hasEnded = true;

    // when
    const actualResult = component.isMenuDisabled();

    // then
    expect(actualResult).toBeTruthy();
  });

  it('should return translation key without extension for status maybe', () => {
    // given
    const option = {
      status: ParticipantStatus.MAYBE_ATTENDING,
      icon: 'icon'
    };
    component.selectedOption = option;

    // when
    const result = component.buildStatusTranslationKey(option);

    // then
    expect(result).toEqual('EVENT.STATUS.MAYBE_ATTENDING');
  });

  it('should return translation key with extension for status attending', () => {
    // given
    const option = {
      status: ParticipantStatus.ATTENDING,
      icon: 'icon'
    };
    component.selectedOption = option;

    // when
    const result = component.buildStatusTranslationKey(option);

    // then
    expect(result).toEqual('EVENT.STATUS.ATTENDING.ACTIVE');
  });

  it('should return translation key with extension for status not attending', () => {
    // given
    const option = {
      status: ParticipantStatus.NOT_ATTENDING,
      icon: 'icon'
    };
    component.selectedOption = option;

    // when
    const result = component.buildStatusTranslationKey(option);

    // then
    expect(result).toEqual('EVENT.STATUS.NOT_ATTENDING.ACTIVE');
  });

  it('should disable menu when event component is not used with isOngoing and hasEnded attribute but limit reached and not attend', () => {
    // given
    component.isOngoing = false;
    component.hasEnded = false;
    component.event = {
      requestDefiniteAnswer: false,
      limitedParticipants: {
        numberOfAttendingParticipants: 10,
        participantsLimit: 10
      },
      status: ParticipantStatus.PENDING
    } as SenderEvent;

    // when
    fixture.detectChanges();
    const actualResult = component.isMenuDisabled();

    // then
    expect(actualResult).toBeTruthy();
  });

});
