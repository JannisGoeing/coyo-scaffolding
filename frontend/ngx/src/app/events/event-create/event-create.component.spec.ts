import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EventDateSyncService} from '@app/events/event-date-sync.service';
import {EventService} from '@domain/event/event.service';
import {GuestSelection} from '@domain/guest/GuestSelection';
import {Sender} from '@domain/sender/sender';
import {FindOptions} from '@shared/sender-ui/select-sender/select-sender-options';
import {NG1_STATE_SERVICE} from '@upgrade/upgrade.module';
import {IStateService} from 'angular-ui-router';
import * as moment from 'moment';
import {NgxPermissionsService} from 'ngx-permissions';
import {of} from 'rxjs';
import {EventCreateComponent} from './event-create.component';

describe('EventCreateComponent', () => {
  let component: EventCreateComponent;
  let fixture: ComponentFixture<EventCreateComponent>;
  let stateService: jasmine.SpyObj<IStateService>;
  let eventService: jasmine.SpyObj<EventService>;
  let permissionService: jasmine.SpyObj<NgxPermissionsService>;
  let eventDateSyncService: jasmine.SpyObj<EventDateSyncService>;
  const host = {id: 'host-id'};

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [EventCreateComponent],
        providers: [{
          provide: NG1_STATE_SERVICE,
          useValue: jasmine.createSpyObj('stateService', ['go'])
        }, {
          provide: EventDateSyncService,
          useValue: jasmine.createSpyObj('eventDateSyncService', [
            'updateWithStartDate', 'updateWithEndDate', 'updateWithStartTime', 'updateWithEndTime'
          ])
        }, {
          provide: EventService,
          useValue: jasmine.createSpyObj('eventService', ['createEvent'])
        }, {
          provide: NgxPermissionsService,
          useValue: jasmine.createSpyObj('permissionService', ['hasPermission'])
        }, FormBuilder, ChangeDetectorRef]
      })
      .overrideTemplate(EventCreateComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCreateComponent);
    component = fixture.componentInstance;
    stateService = TestBed.get(NG1_STATE_SERVICE);
    eventService =  TestBed.get(EventService);
    eventService.createEvent.and.returnValue(of({event: {id: 'event-id'}}));
    permissionService = TestBed.get(NgxPermissionsService);
    permissionService.hasPermission.and.returnValue(of(true));
    eventDateSyncService = TestBed.get(EventDateSyncService);
    component.initialSender = host as Sender;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm instanceof FormGroup).toBeTruthy();
    expect(component.eventCreateForm.get('name').value).toEqual('');
    expect(component.eventCreateForm.get('host').value).toEqual(host);
    expect(component.eventCreateForm.get('place').value).toEqual('');
    expect(component.eventCreateForm.get('description').value).toEqual('');
    expect(component.eventCreateForm.get('fullDay').value).toEqual(false);
    expect(component.eventCreateForm.get('startDate').value).toBeDefined();
    expect(component.eventCreateForm.get('endDate').value).toBeDefined();
    expect(component.eventCreateForm.get('showParticipants').value).toEqual(false);
    expect(component.eventCreateForm.get('requestDefiniteAnswer').value).toEqual(false);
    expect(component.eventCreateForm.get('limitedParticipants').value).toEqual(false);
    expect(component.eventCreateForm.get('participantsLimit').value).toEqual(0);
    expect(component.eventDates.startDate instanceof Date).toBeTruthy();
    expect(component.eventDates.startTime instanceof Date).toBeTruthy();
    expect(component.eventDates.endDate instanceof Date).toBeTruthy();
    expect(component.eventDates.endDate instanceof Date).toBeTruthy();
    expect(component.senderSelectOptions).toEqual({
      findOptions: FindOptions.MANAGED_SENDERS_ONLY,
      senderTypes: ['user', 'page', 'workspace']
    });
  });

  it('should set form to invalid if limitedParticipants is true but limit is zero', () => {
    // given
    fixture.detectChanges();
    component.eventCreateForm.patchValue({
      name: 'Test event',
      host: host,
      eventStart: new Date(),
      eventEnd: new Date(),
      limitedParticipants: true,
      participantsLimit: 0
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm.valid).toBeFalsy();
  });

  it('should set form to valid if limitedParticipants is true and limit is greater than zero', () => {
    // given
    fixture.detectChanges();
    component.eventCreateForm.patchValue({
      name: 'Test event',
      host: host,
      eventStart: new Date(),
      eventEnd: new Date(),
      limitedParticipants: true,
      participantsLimit: 1
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm.valid).toBeTruthy();
  });

  it('should set form to valid if limitedParticipants is false and limit is zero', () => {
    // given
    fixture.detectChanges();
    component.eventCreateForm.patchValue({
      name: 'Test event',
      host: host,
      eventStart: new Date(),
      eventEnd: new Date(),
      limitedParticipants: false,
      participantsLimit: 0
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.eventCreateForm.valid).toBeTruthy();
  });

  it('should call updateWithStartDate', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateStartDate();

    // then
    expect(eventDateSyncService.updateWithStartDate).toHaveBeenCalled();
  });

  it('should call updateWithEndDate', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateEndDate();

    // then
    expect(eventDateSyncService.updateWithEndDate).toHaveBeenCalled();
  });

  it('should call updateWithStartTime', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateStartTime();

    // then
    expect(eventDateSyncService.updateWithStartTime).toHaveBeenCalled();
  });

  it('should call updateWithEndTime', () => {
    // given
    fixture.detectChanges();

    // when
    component.updateEndTime();

    // then
    expect(eventDateSyncService.updateWithEndTime).toHaveBeenCalled();
  });

  it('should submit the form', () => {
    // given
    fixture.detectChanges();

    // when
    component.submitForm();

    // then
    expect(eventService.createEvent).toHaveBeenCalled();
  });

  it('should store selected guest state', () => {
    // given
    const expected = [{
      id: 'id-1',
      selected: true
    }] as GuestSelection[];
    fixture.detectChanges();
    spyOn(component, 'previousStep');

    // when
    component.saveUserChooserStateAndGoToPreviousStep(expected);

    // then
    expect(component.selectedGuestsStorage).toEqual(expected);
    expect(component.previousStep).toHaveBeenCalled();
  });

  it('should transform data before submit', () => {
    // given
    fixture.detectChanges();
    const date = new Date();
    const eventData = {
      public: false,
      name: 'name',
      host: host,
      place: 'place',
      description: 'description',
      fullDay: 'full-day',
      startDate: date,
      endDate: date,
      showParticipants: true,
      requestDefiniteAnswer: true,
      limitedParticipants: true,
      participantsLimit: 24
    };
    const expectedEventData = {
      public: false,
      name: 'name',
      place: 'place',
      description: 'description',
      fullDay: 'full-day',
      showParticipants: true,
      requestDefiniteAnswer: true,
      limitedParticipants: {
        participantsLimit: 24
      },
      adminIds: [host.id],
      memberIds: [],
      adminGroupIds: [],
      memberGroupIds: [],
      hostId: host.id,
      startDate: moment(date).format('YYYY-MM-DDTHH:mm:ss'),
      endDate: moment(date).format('YYYY-MM-DDTHH:mm:ss')
    } as any;
    component.eventCreateForm.patchValue(eventData);
    fixture.detectChanges();

    // when
    component.submitForm();

    // then
    const result = eventService.createEvent.calls.allArgs()[0][0];
    expect(result.hostId).toEqual(expectedEventData.hostId);
    expect(result.startDate).toEqual(expectedEventData.startDate);
    expect(result.endDate).toEqual(expectedEventData.endDate);
    expect(result.adminIds).toEqual(expectedEventData.adminIds);
    expect(result.limitedParticipants).toEqual(expectedEventData.limitedParticipants);
  });

  it('should cancel the form with default settings', () => {
    // given
    fixture.detectChanges();

    // when
    component.cancelForm();

    // then
    expect(stateService.go).toHaveBeenCalledWith('main.event', {});
  });

  it('should cancel the form with a previous state', () => {
    // given
    stateService['previous'] = {
      name: 'state-name',
      params: {a: 1}
    };
    fixture.detectChanges();

    // when
    component.cancelForm();

    // then
    expect(stateService.go).toHaveBeenCalledWith('state-name', {a: 1});
  });

  it('should add member to form', () => {
    // given
    const invitedUsers = {
      memberIds: ['user-1', 'user-2', 'user-3'],
      memberGroupIds: ['group-1', 'group-2', 'group-3']
    };
    fixture.detectChanges();
    spyOn(component.eventCreateForm, 'patchValue');

    // when
    component.addInvitedUsers(invitedUsers);

    // then
    expect(component.eventCreateForm.patchValue).toHaveBeenCalledWith({
      memberIds: invitedUsers.memberIds,
      memberGroupIds: invitedUsers.memberGroupIds
    });
  });

  it('should cleanup on destroy', () => {
    // given
    fixture.detectChanges();
    spyOn(component.formResult, 'complete');

    // when
    component.ngOnDestroy();

    // then
    expect(component.formResult.complete).toHaveBeenCalled();
  });

  it('should update event form if description changes', () => {
    // given
    fixture.detectChanges();
    spyOn(component.eventCreateForm, 'patchValue');

    // when
    component.descriptionChanges('new content');

    // then
    expect(component.eventCreateForm.patchValue).toHaveBeenCalledWith({description: 'new content'});
  });

});
