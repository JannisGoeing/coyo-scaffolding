import {NgModule, SimpleChanges} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TimelineItem} from '@domain/timeline-item/timeline-item';
import {PostItemComponent} from '../timeline-item-content/post-item/post-item.component';
import {TimelineItemContentOutletComponent} from './timeline-item-content-outlet.component';

@NgModule({
  declarations: [
    TimelineItemContentOutletComponent,
    PostItemComponent
  ],
  exports: [
    TimelineItemContentOutletComponent
  ],
  entryComponents: [
    PostItemComponent
  ]
})
class TimelineItemContentOutletComponentTestModule {}

describe('ItemContentContainerOutlet', () => {
  let component: TimelineItemContentOutletComponent;
  let fixture: ComponentFixture<TimelineItemContentOutletComponent>;

  const item = {
    id: 'item-1',
    itemType: 'post'
  } as TimelineItem;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TimelineItemContentOutletComponentTestModule],
    });
    TestBed.overrideTemplate(PostItemComponent, '');
    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineItemContentOutletComponent);
    component = fixture.componentInstance;
    component.item = item;
    fixture.detectChanges();
  });

  it('should create a content component on init', () => {
    expect(component.componentRef).toBeTruthy();
    expect(component.item).toBe(item);
    expect(component.componentRef.instance.item).toEqual(item);
  });

  it('should update content input parameters on change', () => {
    const item2 = {
      id: 'item-2',
      itemType: 'post'
    } as TimelineItem;
    component.item = item2;
    component.ngOnChanges({} as SimpleChanges);

    expect(component.componentRef.instance.item).toEqual(item2);
  });
});
