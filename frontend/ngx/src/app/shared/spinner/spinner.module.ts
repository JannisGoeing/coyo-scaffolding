import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {SpinnerComponent} from './spinner.component';
import './spinner.component.downgrade';

/**
 * Module exporting a spinner component.
 */
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SpinnerComponent
  ],
  exports: [
    SpinnerComponent
  ],
  entryComponents: [
    SpinnerComponent
  ]
})
export class SpinnerModule {}
