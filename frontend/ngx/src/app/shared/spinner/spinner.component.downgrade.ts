import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SpinnerComponent} from './spinner.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoSpinner', downgradeComponent({
    component: SpinnerComponent
  }));
