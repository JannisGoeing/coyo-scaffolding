import {ChangeDetectionStrategy, Component, Input, OnChanges} from '@angular/core';
import {File} from '@domain/file/file';
import {IconCssService} from '@domain/icon/icon-css.service';

/**
 * Component that handles the file icon
 */
@Component({
  selector: 'coyo-file-icon',
  templateUrl: './file-icon.component.html',
  styleUrls: ['./file-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileIconComponent implements OnChanges {

  /**
   * Attached file
   */
  @Input() file: File;
  icons: string[];

  constructor(private iconCssService: IconCssService) { }

  /**
   * Changes file icon during navigation
   */
  ngOnChanges(): void {
    this.icons = this.iconCssService.getFileIconsCss(this.file);
  }
}
