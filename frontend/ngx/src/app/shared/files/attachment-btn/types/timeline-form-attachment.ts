/**
 * Represents a timeline form attachment.
 */
export interface TimelineFormAttachment {
  uid: string;
  name: string;
  displayName: string;
  contentType: string;
  storage: string;
  visibility: string | null;
}
