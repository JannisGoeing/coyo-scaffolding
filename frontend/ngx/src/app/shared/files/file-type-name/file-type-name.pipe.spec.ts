import {FileTypeNamePipe} from './file-type-name.pipe';

describe('FileTypeNamePipe', () => {

  const types: Map<string, string> = new Map<string, string>();
  types.set('FILE_TYPE_NAME.WORD', 'officedocument/word');
  types.set('FILE_TYPE_NAME.EXCEL', 'officedocument/sheet');
  types.set('FILE_TYPE_NAME.POWERPOINT', 'officedocument/presentation');
  types.set('FILE_TYPE_NAME.GOOGLE_SPREADSHEET', 'google-apps/spreadsheet');
  types.set('FILE_TYPE_NAME.GOOGLE_DOCUMENT', 'google-apps/document');
  types.set('FILE_TYPE_NAME.GOOGLE_PRESENTATION', 'google-apps/presentation');
  types.set('FILE_TYPE_NAME.IMAGE', 'image/png');
  types.set('FILE_TYPE_NAME.VIDEO', 'video/mp4');
  types.set('FILE_TYPE_NAME.PDF', 'application/pdf');
  types.set('FILE_TYPE_NAME.PLAIN', 'text/plain');
  types.set('FILE_TYPE_NAME.DEFAULT', 'random-content-type');

  it('create an instance', () => {
    const pipe = new FileTypeNamePipe();
    expect(pipe).toBeTruthy();
  });

  it('should return expected type name', () => {
    const pipe = new FileTypeNamePipe();
    types.forEach((value, key) => {
      const typeName = pipe.transform(value);
      expect(typeName).toEqual(key);
    });
  });

  it('should not incorrectly identify strings', () => {
    const pipe = new FileTypeNamePipe();
    const typeName = pipe.transform('This is a video image/png');
    expect(typeName).toEqual('FILE_TYPE_NAME.DEFAULT');
  });
});
