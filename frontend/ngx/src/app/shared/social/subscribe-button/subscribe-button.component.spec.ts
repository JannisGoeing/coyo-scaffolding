import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {Sender} from '@domain/sender/sender';
import {Subscribable} from '@domain/subscription/subscribable';
import {Subscription} from '@domain/subscription/subscription';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {of, Subject} from 'rxjs';
import {skip} from 'rxjs/operators';
import {SubscribeButtonComponent} from './subscribe-button.component';

describe('SubscribeButtonComponent', () => {
  let component: SubscribeButtonComponent;
  let fixture: ComponentFixture<SubscribeButtonComponent>;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;

  let user$Subject: Subject<any>;

  const subscription = {
    userId: 'user-id',
    senderId: 'sender-id',
    targetId: 'target-id',
    targetType: 'target-type',
    autoSubscribe: false
  } as Subscription;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubscribeButtonComponent],
      providers: [{
        provide: SubscriptionService,
        useValue: jasmine.createSpyObj('SubscriptionService', ['subscribe', 'unsubscribe', 'getSubscriptionsById', 'onSubscriptionChange$'])
      }]
    }).overrideTemplate(SubscribeButtonComponent, '<div></div>')
      .compileComponents();

    subscriptionService = TestBed.get(SubscriptionService);
  }));

  beforeEach(() => {
    user$Subject = new Subject();
    subscriptionService.onSubscriptionChange$.and.returnValue(of(subscription));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribeButtonComponent);
    component = fixture.componentInstance;
  });

  it('should init', () => {
    // given
    component.target = {} as Subscribable;

    // when
    fixture.detectChanges();

    // then
    component.state$.subscribe(state => expect(state).toEqual({isLoading: true, isSubscribed: false}));
  });

  it('should init (active)', fakeAsync(() => {
    // given
    component.target = {} as Subscribable;
    fixture.detectChanges();
    subscriptionService.getSubscriptionsById.and.returnValue(of({}));

    // then
    component.state$
      .pipe(skip(2)) // skip initial state
      .subscribe(state => expect(state).toEqual({isLoading: false, isSubscribed: true}));

    // finally
    user$Subject.next({});
  }));

  it('should init (inactive)', () => {
    // given
    component.target = {} as Subscribable;
    fixture.detectChanges();
    subscriptionService.getSubscriptionsById.and.returnValue(of(undefined));

    // then
    component.state$
      .pipe(skip(2)) // skip initial state
      .subscribe(state => expect(state).toEqual({isLoading: false, isSubscribed: false}));

    // finally
    user$Subject.next({});
  });

  it('should subscribe', () => {
    // given
    component.target = {
      id: 'id',
      typeName: 'timeline-item',
      senderId: 'senderId'
    };
    fixture.detectChanges();
    subscriptionService.getSubscriptionsById.and.returnValue(of(undefined));
    subscriptionService.subscribe.and.returnValue(of({}));
    user$Subject.next({});

    // then
    component.state$
      .pipe(skip(3)) // skip initial & loading state
      .subscribe(state => expect(state).toEqual({isLoading: false, isSubscribed: true}));

    // when
    component.toggle({isLoading: false, isSubscribed: false});

    // then
    expect(subscriptionService.subscribe).toHaveBeenCalledWith('senderId', 'id', 'timeline-item');
  });

  it('should subscribe with author', () => {
    // given
    component.target = {
      id: 'id',
      typeName: 'timeline-item',
      author: {id: 'authorId', created: Date.now(), modified: Date.now()} as Sender
    };
    fixture.detectChanges();
    subscriptionService.getSubscriptionsById.and.returnValue(of(undefined));
    subscriptionService.subscribe.and.returnValue(of({}));
    user$Subject.next({});

    // then
    component.state$
      .pipe(skip(3)) // skip initial & loading state
      .subscribe(state => expect(state).toEqual({isLoading: false, isSubscribed: true}));

    // when
    component.toggle({isLoading: false, isSubscribed: false});

    // then
    expect(subscriptionService.subscribe).toHaveBeenCalledWith('authorId', 'id', 'timeline-item');
  });

  it('should unsubscribe', () => {
    // given
    component.target = {
      id: 'id',
      typeName: 'timeline-item',
      senderId: 'senderId'
    };
    fixture.detectChanges();
    subscriptionService.getSubscriptionsById.and.returnValue(of({}));
    subscriptionService.unsubscribe.and.returnValue(of(undefined));
    user$Subject.next({});

    // then
    component.state$
      .pipe(skip(3)) // skip initial & loading state
      .subscribe(state => expect(state).toEqual({isLoading: false, isSubscribed: false}));

    // when
    component.toggle({isLoading: false, isSubscribed: true});
  });
});
