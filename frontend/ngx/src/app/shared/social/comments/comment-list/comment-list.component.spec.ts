import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SocketService} from '@core/socket/socket.service';
import {Comment} from '@domain/comment/comment';
import {CommentService} from '@domain/comment/comment.service';
import {Sender} from '@domain/sender/sender';
import {of} from 'rxjs';
import {CommentListComponent} from './comment-list.component';

describe('CommentListComponent', () => {
  let component: CommentListComponent;
  let fixture: ComponentFixture<CommentListComponent>;
  let commentService: jasmine.SpyObj<CommentService>;
  let socketService: jasmine.SpyObj<SocketService>;

  const target = {id: 'target-id', typeName: 'timeline-item'} as Sender;
  const author = {id: 'author-id'} as Sender;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommentListComponent],
      providers: [{
        provide: CommentService,
        useValue: jasmine.createSpyObj('commentService', ['getInitialPage', 'getPage', 'get', 'delete'])
      }, {
        provide: SocketService,
        useValue: jasmine.createSpyObj('socketService', ['listenTo$'])
      }]
    }).overrideTemplate(CommentListComponent, '')
      .compileComponents();

    socketService = TestBed.get(SocketService);
    commentService = TestBed.get(CommentService);
  }));

  beforeEach(() => {
    socketService.listenTo$.and.returnValue(of({content: {}}));
    commentService.get.and.returnValue(of({}));
    commentService.getInitialPage.and.returnValue(of({
      content: [],
      numberOfElements: 0,
      totalElements: 0,
    }));

    fixture = TestBed.createComponent(CommentListComponent);
    component = fixture.componentInstance;
    component.author = author;
    component.subscriptionToken = '123';
    component.target = target;
  });

  it('should init', async(() => {
    // given
    commentService.getInitialPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10,
    }));

    // when
    fixture.detectChanges();

    // then
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/comment', 'created', 'target-id', '123');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/comment', 'updated', 'target-id', '123');
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/comment', 'deleted', 'target-id', '123');
    component.state$.subscribe(state => {
      expect(state.isLoading).toEqual(false);
      expect(state.comments[0].message).toEqual('World');
      expect(state.comments[1].message).toEqual('Hello');
      expect(state.count).toEqual(2);
      expect(state.total).toEqual(10);
      component.state$.unsubscribe();
    });
  }));

  it('should add created comments', async(() => {
    // given
    commentService.getInitialPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10,
    }));
    fixture.detectChanges();

    // when
    component.commentCreated({id: '3', message: 'Hello World'} as Comment);

    // then
    component.state$.subscribe(state => {
      expect(state.comments[0].message).toEqual('World');
      expect(state.comments[1].message).toEqual('Hello');
      expect(state.commentsNew[0].message).toEqual('Hello World');
      expect(state.count).toEqual(3);
      expect(state.total).toEqual(11);
      component.state$.unsubscribe();
    });
  }));

  it('should update edited comments', async(() => {
    // given
    commentService.getInitialPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10,
    }));
    fixture.detectChanges();

    // when
    component.commentEdited({id: '2', message: 'Hello World'} as Comment);

    // then
    component.state$.subscribe(state => {
      expect(state.comments[0].message).toEqual('Hello World');
      expect(state.comments[1].message).toEqual('Hello');
      expect(state.count).toEqual(2);
      expect(state.total).toEqual(10);
      component.state$.unsubscribe();
    });
  }));

  it('should delete comments', async(() => {
    // given
    commentService.delete.and.returnValue(of(null));
    commentService.getInitialPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10,
    }));
    fixture.detectChanges();

    // when
    component.deleteComment({id: '2', message: 'World'} as Comment);
    component.deleteComment({id: '5', message: 'New'} as Comment);

    // then
    component.state$.subscribe(state => {
      expect(state.comments[0].message).toEqual('Hello');
      expect(state.count).toEqual(1);
      expect(state.total).toEqual(8);
      component.state$.unsubscribe();
    });
  }));

  it('should start editing comments', async(() => {
    // given
    fixture.detectChanges();

    // when
    component.editComment({id: '2', message: 'World'} as Comment);

    // then
    component.state$.subscribe(state => {
      expect(state.commentEditing.message).toEqual('World');
      component.state$.unsubscribe();
    });
  }));

  it('should stop editing comments', async(() => {
    // given
    fixture.detectChanges();
    component.editComment({id: '2', message: 'World'} as Comment);

    // when
    component.cancelEditing();

    // then
    component.state$.subscribe(state => {
      expect(state.commentEditing).toEqual(null);
      component.state$.unsubscribe();
    });
  }));

  it('should load more comments', async(() => {
    // given
    commentService.getInitialPage.and.returnValue(of({
      content: [{id: '1', message: 'Hello'}, {id: '2', message: 'World'}],
      numberOfElements: 2,
      totalElements: 10,
    }));
    commentService.getPage.and.returnValue(of({
      content: [{id: '3', message: 'Foo'}, {id: '4', message: 'Bar'}],
      numberOfElements: 2,
      totalElements: 5,
    }));
    fixture.detectChanges();

    // when
    component.loadMore();

    // then
    component.state$.subscribe(state => {
      expect(state.comments[0].message).toEqual('Bar');
      expect(state.comments[1].message).toEqual('Foo');
      expect(state.comments[2].message).toEqual('World');
      expect(state.comments[3].message).toEqual('Hello');
      expect(state.count).toEqual(4);
      expect(state.total).toEqual(5);
      component.state$.unsubscribe();
    });
  }));
});
