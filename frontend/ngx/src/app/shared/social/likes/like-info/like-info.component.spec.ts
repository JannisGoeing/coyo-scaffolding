import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {LikeState} from '@domain/like/like-state';
import {Likeable} from '@domain/like/likeable';
import {Sender} from '@domain/sender/sender';
import {LikesModalComponent} from '../likes-modal/likes-modal.component';
import {LikeInfoComponent} from './like-info.component';

describe('LikeInfoComponent', () => {
  let component: LikeInfoComponent;
  let fixture: ComponentFixture<LikeInfoComponent>;
  let dialog: jasmine.SpyObj<MatDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LikeInfoComponent],
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('dialog', ['open'])
      }]
    }).overrideTemplate(LikeInfoComponent, '')
      .compileComponents();

    dialog = TestBed.get(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LikeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the likes modal', () => {
    // when
    const target: Likeable = {
      id: 'test-id',
      typeName: 'timeline-item'
    };
    component.target = target;

    // given
    component.openLikes();

    // then
    expect(dialog.open).toHaveBeenCalledWith(LikesModalComponent, {
      width: MatDialogSize.Small,
      data: target
    });
  });

  it('should update the like count info on changes', () => {
    // given
    component.state = {
      isLiked: true,
      totalCount: 100,
      othersCount: 99,
      others: [{displayName: 'John'} as Sender]
    } as LikeState;

    // when
    component.ngOnChanges({});

    // then
    expect(component.info.count).toBe(100);
    expect(component.info.isLiked).toBeTruthy();
    expect(component.info.latest).toBe('John');
    expect(component.info.othersCount).toBe(99);
  });
});
