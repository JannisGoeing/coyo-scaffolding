import {ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BaseModel} from '@domain/base-model/base-model';
import {LikeInfo} from '@domain/like/like-info';
import {LikeState} from '@domain/like/like-state';
import {LikeService} from '@domain/like/like.service';
import {Likeable} from '@domain/like/likeable';
import {Observable} from 'rxjs';

/**
 * A button to like an entity.
 */
@Component({
  selector: 'coyo-like-container',
  templateUrl: './like-container.component.html',
  styleUrls: ['./like-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LikeContainerComponent implements OnInit, OnChanges {

  /**
   * The subscription author.
   */
  @Input() author: BaseModel;

  /**
   * The subscription target.
   */
  @Input() target: Likeable;

  /**
   * Modifier to render the component in a condensed mode (no icons).
   */
  @Input() condensed: boolean;

  /**
   * Flag if the initial like request should be skipped
   */
  @Input() skipInitRequest: boolean = false;

  state$: Observable<LikeState>;

  constructor(private likeService: LikeService) {
  }

  ngOnInit(): void {
    this.initState(this.skipInitRequest);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.author && !changes.author.firstChange) {
      this.initState();
    }
  }

  private initState(skipInitialRequest: boolean = false): void {
    this.state$ =
      this.likeService.getLikeTargetState$(this.author.id, this.target.id, this.target.typeName, this.target.subscriptionInfo.token, skipInitialRequest);
  }

  /**
   * Toggles the subscription state for the component's target.
   *
   * @param state the current button state
   */
  toggle(state: LikeState): void {
    if (state.isLoading) {
      return;
    }

    const response: Observable<LikeInfo> = state.isLiked
      ? this.likeService.unlike(this.author.id, this.target.id, this.target.typeName)
      : this.likeService.like(this.author.id, this.target.id, this.target.typeName);
    response.subscribe();
  }
}
