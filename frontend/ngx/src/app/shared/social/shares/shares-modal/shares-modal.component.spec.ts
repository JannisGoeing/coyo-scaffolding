import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Share} from '@domain/share/share';
import {UIRouterModule} from '@uirouter/angular';
import {SharesModalComponent} from './shares-modal.component';

describe('SharesModalComponent', () => {
  let component: SharesModalComponent;
  let fixture: ComponentFixture<SharesModalComponent>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<SharesModalComponent>>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule.forRoot({useHash: true})],
      declarations: [SharesModalComponent],
      providers: [{
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialogRef', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {
          itemType: 'post',
          shares: [
            {id: 'share-1'} as Share,
            {id: 'share-2'} as Share
          ]
        }
      }]
    }).overrideTemplate(SharesModalComponent, '')
      .compileComponents();

    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return modal result on destroy after deleted', () => {
    // when
    component.deleteShare({id: 'share-2'} as Share);
    component.ngOnDestroy();

    // then
    expect(dialogRef.close).toHaveBeenCalledWith({
      deletedShares: [{id: 'share-2'} as Share] as Share[]
    });
  });
});
