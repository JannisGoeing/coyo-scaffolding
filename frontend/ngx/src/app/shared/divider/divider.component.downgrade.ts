import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {DividerComponent} from './divider.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoDivider', downgradeComponent({
    component: DividerComponent
  }));
