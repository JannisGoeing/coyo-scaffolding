import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {JitTranslationOutputComponent} from './jit-translation-output.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoJitTranslationOutput', downgradeComponent({
    component: JitTranslationOutputComponent
  }));
