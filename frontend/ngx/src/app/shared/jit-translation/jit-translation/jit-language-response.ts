export interface JitLanguageResponse {
  language: string;
  disabledLanguages: string[];
}
