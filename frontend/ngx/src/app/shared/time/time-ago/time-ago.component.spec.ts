import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {TranslationService} from '@core/i18n/translation-service/translation.service';
import {MaterialModule} from '@shared/material/material.module';
import {TimeAgoComponent} from './time-ago.component';

/* tslint:disable:deprecation */
describe('TimeAgoComponent', () => {
  let component: TimeAgoComponent;
  let fixture: ComponentFixture<TimeAgoComponent>;
  const translationService = jasmine.createSpyObj('translationService', ['getActiveLanguage']);
  const locale = 'it';
  translationService.getActiveLanguage.and.returnValue(locale);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MaterialModule],
      declarations: [TimeAgoComponent],
      providers: [{provide: TranslationService, useFactory: () => translationService}]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeAgoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should format the date appropriately', fakeAsync(() => {
    const subscription = component.$formattedDate.subscribe(value => {
      expect(value).toBe('a few seconds ago');
    });
    tick();
    subscription.unsubscribe();
  }));
});
