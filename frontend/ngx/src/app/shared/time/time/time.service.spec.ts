import {TestBed} from '@angular/core/testing';
import {TimeService} from './time.service';

describe('TimeService', () => {
  let service: TimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(TimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get and set the timezone offset', () => {
    expect(service.getTimezone()).toBe('UTC');
    service.setTimezone('Europe/Berlin');
    expect(service.getTimezone()).toBe('Europe/Berlin');
  });
});
