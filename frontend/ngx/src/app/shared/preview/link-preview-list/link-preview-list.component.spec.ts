import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LinkPreview} from '@domain/preview/link-preview';
import {LinkPreviewListComponent} from './link-preview-list.component';

describe('LinkPreviewListComponent', () => {
  let component: LinkPreviewListComponent;
  let fixture: ComponentFixture<LinkPreviewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LinkPreviewListComponent]
    }).overrideTemplate(LinkPreviewListComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkPreviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should track by item URL', () => {
    // given
    const item = {url: 'test-url'} as LinkPreview;

    // when
    const result = component.trackByUrl(0, item);

    // then
    expect(result).toBe(item.url);
  });
});
