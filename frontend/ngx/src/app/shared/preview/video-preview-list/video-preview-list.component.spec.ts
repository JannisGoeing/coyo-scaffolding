import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {VideoPreview} from '@domain/preview/video-preview';
import {VideoPreviewListComponent} from './video-preview-list.component';

describe('VideoPreviewListComponent', () => {
  let component: VideoPreviewListComponent;
  let fixture: ComponentFixture<VideoPreviewListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VideoPreviewListComponent]
    }).overrideTemplate(VideoPreviewListComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPreviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should sort previews by position', () => {
    const one = {position: 1} as VideoPreview;
    const two = {position: 2} as VideoPreview;
    const three = {position: 3} as VideoPreview;

    // when
    component.previews = [three, one, two];

    // then
    expect(component.sortedPreviews).toEqual([one, two, three]);
  });
});
