import {async, ComponentFixture, discardPeriodicTasks, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {WebPreviewService} from '@domain/preview/web-preview/web-preview.service';
import {of} from 'rxjs';
import {PreviewListComponent} from './preview-list.component';

describe('PreviewListComponent', () => {
  let component: PreviewListComponent;
  let fixture: ComponentFixture<PreviewListComponent>;
  let previewService: jasmine.SpyObj<WebPreviewService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreviewListComponent],
      providers: [{
        provide: WebPreviewService, useValue: jasmine.createSpyObj('webPreviewService', ['generateWebPreviews'])
      }]
    }).overrideTemplate(PreviewListComponent, '')
      .compileComponents();

    previewService = TestBed.get(WebPreviewService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.registerOnChange(() => false);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch new previews on url change', fakeAsync(() => {
    // given
    const urls = ['a', 'b'];
    previewService.generateWebPreviews.and.returnValue(of([]));
    component.urls = urls;

    // when
    component.ngOnChanges({});

    // then
    expect(previewService.generateWebPreviews).toHaveBeenCalledWith(urls);
    discardPeriodicTasks();
  }));

  it('should fetch only not fetched urls', fakeAsync(() => {
    // given
    const urls = ['a', 'b'];
    previewService.generateWebPreviews.and.returnValue(of([]));
    component.urls = ['a'];
    component.ngOnChanges({});

    tick();
    previewService.generateWebPreviews.calls.reset();
    component.urls = urls;

    // when
    component.ngOnChanges({});

    // then
    expect(previewService.generateWebPreviews).toHaveBeenCalledWith(['b']);
    discardPeriodicTasks();
  }));

  it('should call on change callback after fetched new previews', fakeAsync(() => {
    // given
    const urls = ['a', 'b'];
    const returnValue = [{type: 'LINK'}, {type: 'VIDEO'}];
    previewService.generateWebPreviews.and.returnValue(of(returnValue));
    const onChange = jasmine.createSpy('onChange');
    component.urls = urls;

    // when
    component.registerOnChange(onChange);
    component.ngOnChanges({});
    tick();

    // then
    expect(onChange).toHaveBeenCalledWith({linkPreviews: [returnValue[0]], videoPreviews: [returnValue[1]]});
  }));

  it('should calculate positions of web previews', fakeAsync(() => {
    // given
    const urls = ['a', 'b'];
    const returnValue = [{type: 'LINK', url: 'b'}, {type: 'LINK', url: 'a'}];
    previewService.generateWebPreviews.and.returnValue(of(returnValue));
    const onChange = jasmine.createSpy('onChange');
    component.urls = urls;

    // when
    component.registerOnChange(onChange);
    component.ngOnChanges({});
    tick();

    // then
    const args = onChange.calls.mostRecent().args[0];

    expect(args.linkPreviews.length).toBe(2);
    expect(args.linkPreviews[0].url).toBe('b');
    expect(args.linkPreviews[0].position).toBe(1);
    expect(args.linkPreviews[1].url).toBe('a');
    expect(args.linkPreviews[1].position).toBe(0);
  }));

  it('should reset when form value is changed', () => {
    // given
    component.linkPreviews$.subscribe(linkPreviews => {
      expect(linkPreviews).toEqual([]);
    });
    component.videoPreviews$.subscribe(videoPreviews => {
      expect(videoPreviews).toEqual([]);
    });

    // when
    component.writeValue({});

    // then
    expect(component.urls).toEqual([]);
  });
});
