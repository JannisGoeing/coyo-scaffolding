/**
 * Contains the different status of a file preview
 */
export interface  FilePreviewStatus {
  loading?: boolean;
  isProcessing?: boolean;
  previewAvailable?: boolean;
  conversionError?: boolean;
}
