import {TestBed} from '@angular/core/testing';
import {BrowserService} from '@core/browser/browser.service';
import {GSUITE, LOCAL_FILE_LIBRARY} from '@domain/attachment/storage';
import {CapabilitiesService} from '@domain/capability/capabilities/capabilities.service';
import {FilePreview} from '@domain/preview/file-preview';
import {ExternalFileHandlerService} from '@shared/files/external-file-handler/external-file-handler.service';
import {PreviewType} from '@shared/preview/file-preview/file-preview-type/preview.type';
import {of} from 'rxjs';
import {FilePreviewTypeService} from './file-preview-type.service';

describe('FilePreviewTypeService', () => {
  let service: FilePreviewTypeService;
  let browserService: jasmine.SpyObj<BrowserService>;
  let externalFileHandlerService: jasmine.SpyObj<ExternalFileHandlerService>;
  let capabilitiesService: jasmine.SpyObj<CapabilitiesService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FilePreviewTypeService, {
        provide: BrowserService,
        useValue: jasmine.createSpyObj('BrowserService', ['supportsQuicktimeVideo'])
      }, {
        provide: ExternalFileHandlerService,
        useValue: jasmine.createSpyObj('ExternalFileHandlerService', ['isGoogleMediaType'])
      }, {
        provide: CapabilitiesService,
        useValue: jasmine.createSpyObj('CapabilitiesService', ['previewImageFormat'])
      }]
    }).compileComponents();
  });

  beforeEach(() => {
    service = TestBed.get(FilePreviewTypeService);
    browserService = TestBed.get(BrowserService);
    externalFileHandlerService = TestBed.get(ExternalFileHandlerService);
    capabilitiesService = TestBed.get(CapabilitiesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should determine file as image', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'image/png', length: 1000} as FilePreview;
    capabilitiesService.previewImageFormat.and.returnValue(of('image/png'));

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.IMAGE);
  });

  it('should not determine file as image, when length is 0', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'image/png', length: 0} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });

  it('should determine file as image, when allowed by capability service', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'text/plain', length: 1000} as FilePreview;
    capabilitiesService.previewImageFormat.and.returnValue(of('image/png'));

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.IMAGE);
  });

  it('should only determine image as image, when allowed by capability service', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'image/heic', length: 1000} as FilePreview;
    capabilitiesService.previewImageFormat.and.returnValue(of(''));

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });

  it('should not determine file as image, when not allowed by capability service', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'text/plain', length: 1000} as FilePreview;
    capabilitiesService.previewImageFormat.and.returnValue(of(''));

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });

  it('should determine file as pdf', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'application/pdf'} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.PDF);
  });

  it('should determine file as video', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'video/mp4'} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.VIDEO);
  });

  it('should determine quicktime file as video, when supported by browser', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'video/quicktime'} as FilePreview;
    browserService.supportsQuicktimeVideo.and.returnValue(true);

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.VIDEO);
  });

  it('should not determine quicktime file as video, when not supported by browser', () => {
    // given
    const file = {storage: LOCAL_FILE_LIBRARY, contentType: 'video/quicktime'} as FilePreview;
    browserService.supportsQuicktimeVideo.and.returnValue(false);

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });

  it('should determine file as G Suite image', () => {
    // given
    const file = {storage: GSUITE, contentType: 'image/png', length: 1000} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.GSUITE_IMAGE);
  });

  it('should not determine file as G Suite image, when length is 0', () => {
    // given
    const file = {storage: GSUITE, contentType: 'image/png', length: 0} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });

  it('should determine file as G Suite pdf', () => {
    // given
    const file = {storage: GSUITE, contentType: 'application/pdf'} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.GSUITE_PDF);
  });

  it('should determine file as G Suite video', () => {
    // given
    const file = {storage: GSUITE, contentType: 'video/mp4'} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.GSUITE_VIDEO);
  });

  it('should determine file as G Suite exportable pdf', () => {
    // given
    const file = {
      storage: GSUITE,
      contentType: 'application/vnd.google-apps.document',
      exportLinks: {'application/pdf': 'link'}
    } as FilePreview;
    externalFileHandlerService.isGoogleMediaType.and.returnValue(true);

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.GSUITE_AS_PDF_EXPORTABLE_DOCUMENT);
  });

  it('should determine no preview, if no file was passed', () => {
    // given
    const file: FilePreview = null;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });

  it('should fall back to no preview', () => {
    // given
    const file = {} as FilePreview;

    // when
    let previewType = null;
    service.determinePreviewType(file).subscribe(type => previewType = type);

    // then
    expect(previewType).toBe(PreviewType.NO_PREVIEW);
  });
});
