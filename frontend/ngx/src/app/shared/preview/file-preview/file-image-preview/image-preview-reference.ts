import {StorageType} from '@domain/attachment/storage';

export interface ImagePreviewReference {
  id: string;
  contentType: string;
  downloadUrl?: string;
  previewUrl?: string;
  storage?: StorageType;
}
