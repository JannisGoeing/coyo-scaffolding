import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {Observable, of} from 'rxjs';
import {DurationUnit} from './duration';
import {DurationPickerComponent} from './duration-picker.component';
import {DurationPickerMode} from './duration-range-checker';

describe('DurationPickerComponent', () => {
  let component: DurationPickerComponent;
  let fixture: ComponentFixture<DurationPickerComponent>;

  let onChangeValue: string = null;
  let onChangeInvocations: number;

  class FakeLoader implements TranslateLoader {

    /**
     * Mocked translation loading
     *
     * @param lang
     * The language
     *
     * @return always an observable of an empty object
     */
    getTranslation(lang: string): Observable<any> {
      return of({});
    }
  }

  beforeEach(async(() => {
    const translateServiceSpy = jasmine.createSpyObj('TranslateService', ['get', 'instant']);
    translateServiceSpy.get.and.returnValue('translated service');
    translateServiceSpy.instant.and.returnValue('translated instant');

    const translatePipeSpy = jasmine.createSpyObj('TranslatePipe', ['transform']);
    translatePipeSpy.transform.and.returnValue('hello');

    // noinspection JSIgnoredPromiseFromCall
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        TranslateModule.forRoot({loader: {provide: TranslateLoader, useClass: FakeLoader}})
      ],
      declarations: [
        DurationPickerComponent
      ]
    })
      .compileComponents();
    onChangeInvocations = 0;
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurationPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created and display no fields by default', () => {
    // when
    component.ngOnInit();

    // then
    expect(component.months).toBeNull();
    expect(component.days).toBeNull();
    expect(component.hours).toBeNull();
    expect(component.minutes).toBeNull();
    expect(component.seconds).toBeNull();
  });

  it('should set the value and return the value in the onChange callback function', () => {
    // given
    component.registerOnChange(onChangeCallback);
    component.showUnits = [DurationUnit.DAYS, DurationUnit.HOURS, DurationUnit.MINUTES, DurationUnit.SECONDS];
    component.ngOnInit();

    // when
    component.writeValue('P2DT23H59M30S');

    // then
    expect(onChangeInvocations).toBe(2); // ngOnInit + writeValue
    expect(onChangeValue).toBe('P2DT23H59M30S');
    expect(component.months).toEqual(0);
    expect(component.days).toEqual(2);
    expect(component.hours).toEqual(23);
    expect(component.minutes).toEqual(59);
    expect(component.seconds).toEqual(30);
  });

  it('should set the fields', () => {
    // given
    component.registerOnChange(onChangeCallback);
    component.showUnits = [DurationUnit.MONTHS, DurationUnit.DAYS, DurationUnit.HOURS, DurationUnit.MINUTES,
      DurationUnit.SECONDS];
    component.mode = DurationPickerMode.NORMALIZE;
    component.ngOnInit();

    // when
    component.months = 2;
    component.days = 1;
    component.hours = 14;
    component.minutes = 34;
    component.seconds = 3;

    // then
    expect(onChangeInvocations).toBe(6);  // ngOnInit + each field
    expect(onChangeValue).toBe('P2M1DT14H34M3S');
    expect(component.months).toEqual(2);
    expect(component.days).toEqual(1);
    expect(component.hours).toEqual(14);
    expect(component.minutes).toEqual(34);
    expect(component.seconds).toEqual(3);
  });

  it('should normalize the fields', () => {
    // given
    component.registerOnChange(onChangeCallback);
    component.showUnits = [DurationUnit.MONTHS, DurationUnit.DAYS, DurationUnit.HOURS, DurationUnit.MINUTES,
      DurationUnit.SECONDS];
    component.mode = DurationPickerMode.NORMALIZE;
    component.ngOnInit();

    // when
    component.writeValue('P2DT23H59M61S');

    // then
    expect(onChangeInvocations).toBe(2); // ngOnInit + writeValue
    expect(onChangeValue).toBe('P3DT1S');
    expect(component.months).toEqual(0);
    expect(component.days).toEqual(3);
    expect(component.hours).toEqual(0);
    expect(component.minutes).toEqual(0);
    expect(component.seconds).toEqual(1);
  });

  function onChangeCallback(value: string): void {
    onChangeValue = value;
    onChangeInvocations++;
  }
});
