import {SelectUiModule} from '@shared/select-ui/select-ui.module';

describe('SenderUIModule', () => {
  let selectUiModule: SelectUiModule;

  beforeEach(() => {
    selectUiModule = new SelectUiModule();
  });

  it('should create an instance', () => {
    expect(selectUiModule).toBeTruthy();
  });
});
