import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {FroalaTranslateService} from './froala-translate.service';

describe('FroalaTranslateService', () => {
  let translateService: jasmine.SpyObj<TranslateService>;

  beforeEach(() => {
    const tranlationServiceMock = jasmine.createSpyObj('translateService', ['instant']);

    TestBed.configureTestingModule({
      providers: [
        {provide: TranslateService, useValue: tranlationServiceMock},
        FroalaTranslateService
      ]
    });

    translateService = TestBed.get(TranslateService);
  });

  it('should translate froala messages', inject([FroalaTranslateService], (service: FroalaTranslateService) => {
    // given
    const testMsg = 'Test msg';
    translateService.instant.and.returnValue(testMsg);

    // when
    const result = service.translate('test key');
    const result2 = service.translate('Really long test sentence');
    service.translate('Focus popup / toolbar');
    service.translate('The pasted content is coming from a Microsoft Word document.' +
      ' Do you want to keep the format or clean it up?');
    service.translate('Are you sure? Image will be deleted.');

    // then
    expect(result).toBe(testMsg);
    expect(result2).toBe(testMsg);
    expect(translateService.instant).toHaveBeenCalledWith('RTE.TEST_KEY');
    expect(translateService.instant).toHaveBeenCalledWith('RTE.REALLY_LONG_TEST_SENTENCE');
    expect(translateService.instant).toHaveBeenCalledWith('RTE.FOCUS_POPUP_TOOLBAR');
    expect(translateService.instant)
      .toHaveBeenCalledWith('RTE.THE_PASTED_CONTENT_IS_COMING_FROM_A_MICROSOFT_WORD_DOCUMENT.' +
        '_DO_YOU_WANT_TO_KEEP_THE_FORMAT_OR_CLEAN_IT_UP');
    expect(translateService.instant).toHaveBeenCalledWith('RTE.ARE_YOU_SURE_IMAGE_WILL_BE_DELETED.');
  }));

  it('should return key if message key is not found', inject([FroalaTranslateService], (service: FroalaTranslateService) => {
    // given
    const testKey = 'test key';
    translateService.instant.and.callFake((arg: any) => arg);

    // when
    const result = service.translate('test key');

    // then
    expect(result).toBe(testKey);
    expect(translateService.instant).toHaveBeenCalledWith('RTE.TEST_KEY');
  }));
});
