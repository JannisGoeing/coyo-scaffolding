import {DOCUMENT} from '@angular/common';
import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {SenderService} from '@domain/sender/sender/sender.service';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {RteSettingsService} from './rte-settings/rte-settings.service';
import {RteComponent} from './rte.component';

describe('RteComponent', () => {
  let component: RteComponent;
  let fixture: ComponentFixture<RteComponent>;
  let settingsService: jasmine.SpyObj<RteSettingsService>;
  let authService: jasmine.SpyObj<AuthService>;
  let document: Document;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RteComponent],
      providers: [{
        provide: TranslateService,
        useValue: jasmine.createSpyObj('TranslateService', ['instant'])
      }, {
        provide: RteSettingsService,
        useValue: jasmine.createSpyObj('RteSettingsService', ['getSettings'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('SenderService', ['getCurrentIdOrSlug'])
      }, {
        provide: AuthService,
        useValue: jasmine.createSpyObj('AuthService', ['getUser'])
      }, ChangeDetectorRef]
    }).overrideTemplate(RteComponent, '<div class="fr-view"><span coyo-download="http://test.link">Test File</span></div>')
      .compileComponents();

    settingsService = TestBed.get(RteSettingsService);
    settingsService.getSettings.and.returnValue(of({}));
    authService = TestBed.get(AuthService);
    authService.getUser.and.returnValue(of({}));
    document = TestBed.get(DOCUMENT);

    fixture = TestBed.createComponent(RteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should replace legacy coyo-download spans with anchor tags', () => {
    // when
    const callback = component.options['events']['initialized'];
    callback.bind({})();

    // then
    const view = document.querySelectorAll('.fr-view');
    expect(view.length).toEqual(1);
    expect(view.item(0).innerHTML).toEqual('<a href="http://test.link" target="_self">Test File</a>');
  });

  it('should add coyo-download to whitelist', () => {
    expect(component.options.htmlAllowedAttrs).toContain('coyo-download');
  });

  it('should register a "image.inserted" event', () => {
    // given
    const $image = jasmine.createSpyObj('$image', ['addClass', 'removeClass']);
    $image.addClass.and.returnValue($image);
    $image.removeClass.and.returnValue($image);

    // when
    const options = component['options'];
    const onImageInserted = options.events['image.inserted'];
    onImageInserted($image);

    // then
    expect($image.addClass).toHaveBeenCalledWith('fr-cdbc');
    expect($image.removeClass).toHaveBeenCalledWith('fr-dib fr-dii fr-fil fr-fir');
  });

  it('should register a "video.inserted" event', () => {
    // given
    const $video = jasmine.createSpyObj('$video', ['addClass', 'removeClass']);
    $video.addClass.and.returnValue($video);
    $video.removeClass.and.returnValue($video);

    // when
    const options = component['options'];
    const onVideoInserted = options.events['video.inserted'];
    onVideoInserted($video);

    // then
    expect($video.addClass).toHaveBeenCalledWith('fr-cdbc');
    expect($video.removeClass).toHaveBeenCalledWith('fr-dvb fr-dvi fr-fvl fr-fvr');
  });

  it('should register a "table.inserted" event', () => {
    // given
    const table = {
      classList: jasmine.createSpyObj('classList', ['add'])
    };

    // when
    const options = component['options'];
    const onTableInserted = options.events['table.inserted'];
    onTableInserted(table);

    // then
    expect(table.classList.add).toHaveBeenCalledWith('fr-table');
    expect(table.classList.add).toHaveBeenCalledWith('fr-cdbl');
    expect(table.classList.add).toHaveBeenCalledWith('rte-table-bordered');
  });

  it('should register a "table.resized" event', () => {
    // given
    const table = {
      classList: jasmine.createSpyObj('classList', ['add', 'contains']),
      style: jasmine.createSpyObj('style', ['setProperty'])
    };
    table.classList.contains.and.returnValue(true);

    // when
    const options = component['options'];
    const onTableResized = options.events['table.resized'];
    onTableResized(table);

    // then
    expect(table.classList.contains).toHaveBeenCalled();
    expect(table.style.setProperty).toHaveBeenCalledWith('margin-left', null);
    expect(table.style.setProperty).toHaveBeenCalledWith('margin-right', null);
  });

  it('should trigger word paste event when word content without rich text format is pasted', () => {
    // given
    const html = '<w:WordDocument></w:WordDocument>';
    const editor = {
      events: jasmine.createSpyObj('events', ['trigger'])
    };

    const clipboardEvent = {clipboardData: jasmine.createSpyObj('data', ['getData'])};
    clipboardEvent.clipboardData.getData.and.callFake((param: string) => param === 'text/html' ? html : '');

    // when
    const result = component.triggerWordPasteForNoRTFDocuments(editor, clipboardEvent as ClipboardEvent);

    // then
    expect(editor.events.trigger).toHaveBeenCalledWith('paste.wordPaste', [html], true);
    expect(result).toBeFalsy();
  });

  it('should not trigger word paste when rtf is defined', () => {
    // given
    const html = '<w:WordDocument></w:WordDocument>';
    const editor = {
      events: jasmine.createSpyObj('events', ['trigger'])
    };

    const clipboardEvent = {clipboardData: jasmine.createSpyObj('data', ['getData'])};
    clipboardEvent.clipboardData.getData.and.callFake((param: string) => param === 'text/html' ? html : 'rich text format');

    // when
    const result = component.triggerWordPasteForNoRTFDocuments(editor, clipboardEvent as ClipboardEvent);

    // then
    expect(editor.events.trigger).not.toHaveBeenCalledWith('paste.wordPaste', [html], true);
    expect(result).toBeTruthy();
  });
});
