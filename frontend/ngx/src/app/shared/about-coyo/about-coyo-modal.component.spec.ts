import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {SessionStorageService} from '@core/storage/session-storage/session-storage.service';
import {AboutCoyoModalComponent} from './about-coyo-modal.component';

describe('AboutCoyoModalComponent', () => {
  let component: AboutCoyoModalComponent;
  let fixture: ComponentFixture<AboutCoyoModalComponent>;
  let sessionStorageService: jasmine.SpyObj<SessionStorageService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutCoyoModalComponent],
      providers: [{
        provide: SessionStorageService,
        useValue: jasmine.createSpyObj('SessionStorageService', ['getValue', 'setValue'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: '13.3.7-SNAPSHOT'
      }]
    }).overrideTemplate(AboutCoyoModalComponent, '')
      .compileComponents();

    sessionStorageService = TestBed.get(SessionStorageService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutCoyoModalComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('#clicks() called >=3 times should toggle #isActive', () => {
    sessionStorageService.getValue.and.returnValue(false);
    fixture.detectChanges();

    expect(component.isActive).toBe(false, 'off at first');
    component.clicks.next();
    expect(component.isActive).toBe(false, 'off after one click');
    component.clicks.next();
    component.clicks.next();
    expect(component.isActive).toBe(true, 'on after third click');
    expect(sessionStorageService.setValue).toHaveBeenCalledWith('heart-feature-toggle-activated', true);
  });

  it('#isActive in component should be set to true when sessionStorage value is true', () => {
    sessionStorageService.getValue.and.returnValue(true);
    fixture.detectChanges();

    expect(component.isActive).toBe(true, 'is true when value from sessionStorage is true');
  });
});
