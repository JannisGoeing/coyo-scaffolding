import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {AboutCoyoModalService} from './about-coyo-modal.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxAboutCoyoModalService', downgradeInjectable(AboutCoyoModalService) as any);
