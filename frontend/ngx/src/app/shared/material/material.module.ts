import {DragDropModule} from '@angular/cdk/drag-drop';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIcon, MatIconModule, MatIconRegistry} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {DomSanitizer} from '@angular/platform-browser';
import {MaterialModule as MaterialUiModule} from '@coyo/ui';
import './mat-icon.component.downgrade';

export function registerIcons(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer): Function {
  return () => iconRegistry.addSvgIconSet(sanitizer.bypassSecurityTrustResourceUrl('/assets/icons/sprite.defs.svg'));
}

/**
 * Module exporting used angular modules and directives.
 */
@NgModule({
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MaterialUiModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatNativeDateModule,
    MatTabsModule,
    MatTooltipModule,
    ScrollingModule,
    DragDropModule
  ],
  exports: [
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MaterialUiModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatNativeDateModule,
    MatTabsModule,
    MatTooltipModule,
    ScrollingModule,
    DragDropModule
  ],
  entryComponents: [
    MatIcon
  ],
  providers: [{
    provide: APP_INITIALIZER, useFactory: registerIcons,
    deps: [MatIconRegistry, DomSanitizer],
    multi: true
  }]
})
export class MaterialModule {
}
