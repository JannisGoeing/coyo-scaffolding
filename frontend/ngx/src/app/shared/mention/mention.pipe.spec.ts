import {ChangeDetectorRef, WrappedValue} from '@angular/core';
import {MentionDetails} from '@domain/mention/mention-details';
import {MentionService} from '@domain/mention/mention.service';
import {Subject} from 'rxjs';
import {MentionPipe} from './mention.pipe';

describe('MentionPipe', () => {
  let pipe: MentionPipe;
  let cd: jasmine.SpyObj<ChangeDetectorRef>;
  let mentionService: jasmine.SpyObj<MentionService>;

  beforeEach(() => {
    cd = jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']);
    mentionService = jasmine.createSpyObj('MentionService', ['getDetails']);
    pipe = new MentionPipe(cd, mentionService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should immediately return text without mentions', () => {
    // given
    const text = 'Hello World';

    // when
    const result = pipe.transform(text);

    // then
    expect(result).toEqual('Hello World');
    expect(cd.markForCheck).not.toHaveBeenCalled();
    expect(mentionService.getDetails).not.toHaveBeenCalled();
  });

  it('should return text with mentions and cache the result', () => {
    // given
    const text = 'Hello @world';
    const details = {
      slug: 'slug',
      name: 'name',
      link: 'link'
    } as MentionDetails;
    const detailsSubject = new Subject<{[slug: string]: MentionDetails}>();
    mentionService.getDetails.and.returnValue(detailsSubject.asObservable());

    // when
    let result = pipe.transform(text);

    // then
    expect(result).toEqual('Hello @world');
    expect(cd.markForCheck).not.toHaveBeenCalled();
    expect(mentionService.getDetails).toHaveBeenCalledWith('world');
    mentionService.getDetails.calls.reset();

    // when - again
    detailsSubject.next({world: details});
    result = pipe.transform(text);

    // then - again
    expect(result).toEqual(WrappedValue.wrap('Hello <a href="link">name</a>'));
    expect(cd.markForCheck).toHaveBeenCalledWith();
    expect(mentionService.getDetails).not.toHaveBeenCalled();
    cd.markForCheck.calls.reset();

    // when - again
    result = pipe.transform(text);

    // then - again
    expect(result).toEqual('Hello <a href="link">name</a>');
    expect(cd.markForCheck).not.toHaveBeenCalledWith();
    expect(mentionService.getDetails).not.toHaveBeenCalled();
  });
});
