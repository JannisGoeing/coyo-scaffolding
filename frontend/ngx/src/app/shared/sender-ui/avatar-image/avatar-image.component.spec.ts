import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {WindowSizeService} from '@core/window-size/window-size.service';
import {NG1_IMAGE_MODAL_SERVICE} from '@upgrade/upgrade.module';
import * as _ from 'lodash';
import {AvatarImageComponent, AvatarSize} from './avatar-image.component';

describe('AvatarImageComponent', () => {
  const backendUrl = 'test.url';
  let component: AvatarImageComponent;
  let fixture: ComponentFixture<AvatarImageComponent>;
  let urlServiceMock: jasmine.SpyObj<UrlService>;
  let windowSizeServiceMock: jasmine.SpyObj<WindowSizeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AvatarImageComponent],
      providers: [{
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['join', 'getBackendUrl'])
      }, {
        provide: WindowSizeService,
        useValue: jasmine.createSpyObj('windowSizeService', ['isRetina'])
      }, {
        provide: NG1_IMAGE_MODAL_SERVICE,
        useValue: jasmine.createSpyObj('imageModalService', ['open'])
      }]
    }).compileComponents();

    urlServiceMock = TestBed.get(UrlService);
    windowSizeServiceMock = TestBed.get(WindowSizeService);

    urlServiceMock.join.and.callFake((...parts: string[]) => _.join(parts, '/'));
    urlServiceMock.getBackendUrl.and.returnValue(backendUrl);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarImageComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the correct img url on init for md sized avatar', () => {
    assertImageUrl(false, 'md', 'S');
  });

  it('should calculate the correct img url on init for sm sized avatar', () => {
    assertImageUrl(false, 'sm', 'XS');
  });

  it('should calculate the correct img url on init for xs sized avatar', () => {
    assertImageUrl(false, 'xs', 'XS');
  });

  it('should calculate the correct img url on init for lg sized avatar', () => {
    assertImageUrl(false, 'lg', 'M');
  });

  it('should calculate the correct img url on init for xl sized avatar', () => {
    assertImageUrl(false, 'xl', 'M');
  });

  it('should calculate the correct img url on init for md sized avatar and retina', () => {
    assertImageUrl(true, 'md', 'M');
  });

  it('should calculate the correct img url on init for sm sized avatar and retina', () => {
    assertImageUrl(true, 'sm', 'S');
  });

  it('should calculate the correct img url on init for xs sized avatar and retina', () => {
    assertImageUrl(true, 'xs', 'S');
  });

  it('should calculate the correct img url on init for lg sized avatar and retina', () => {
    assertImageUrl(true, 'lg', 'L');
  });

  it('should calculate the correct img url on init for xl sized avatar and retina', () => {
    assertImageUrl(true, 'xl', 'L');
  });

  it('should calculate the correct img url on init for modal image', () => {
    // when
    component.imageUrl = 'test/url';
    component.ngOnChanges({});

    // then
    expect(component.imageSrcXL).toBe(backendUrl + '/test/url?imageSize=XL');
  });

  it('should append the imageSize parameter to a given parameter list', () => {
    // when
    component.imageUrl = 'test/url?withParm=1';
    component.ngOnChanges({});

    // then
    expect(component.imageSrc).toBe(backendUrl + '/test/url?withParm=1&imageSize=S');
  });

  it('should have an empty img src when no image url is given', () => {
    // when
    component.imageUrl = null;
    component.ngOnChanges({});

    // then
    expect(component.imageSrc).toBe('');
  });

  it('should have an empty XL img src when no image url is given', () => {
    // given
    component.imageUrl = null;

    // then
    expect(component.imageSrcXL).toBe('');
  });

  it('should enable modal, if url is set and allowModal is true', () => {
    // given
    component.imageUrl = 'test/url';
    component.allowModal = true;
    component.ngOnChanges({});

    // when
    const canShowModal = component.canShowModal();

    // then
    expect(canShowModal).toBe(true);
  });

  it('should disable modal, if url is not set', () => {
    // given
    component.imageUrl = null;
    component.allowModal = true;

    // when
    const canShowModal = component.canShowModal();

    // then
    expect(canShowModal).toBe(false);
  });

  it('should disable modal, if allowModal is false', () => {
    // given
    component.imageUrl = 'test/url';
    component.allowModal = false;

    // when
    const canShowModal = component.canShowModal();

    // then
    expect(canShowModal).toBe(false);
  });

  function assertImageUrl(retina: boolean, avatarSize: AvatarSize, resultSize: string): void {
    // given
    component.avatarSize = avatarSize;
    windowSizeServiceMock.isRetina.and.returnValue(retina);

    // when
    component.imageUrl = 'test/url';
    component.ngOnChanges({});

    // then
    expect(component.imageSrc).toBe(backendUrl + '/test/url?imageSize=' + resultSize);
  }
});
