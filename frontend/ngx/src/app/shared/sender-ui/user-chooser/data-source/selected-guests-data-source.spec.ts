import {CollectionViewer, ListRange} from '@angular/cdk/collections';
import {TestBed} from '@angular/core/testing';
import {GuestSelection} from '@domain/guest/GuestSelection';
import {SelectedGuestsDataSource} from '@shared/sender-ui/user-chooser/data-source/selected-guests-data-source';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';

describe('SelectedGuestsDataSource', () => {

  let selectedGuestsDataSource: SelectedGuestsDataSource;
  let selectedGuests$: BehaviorSubject<GuestSelection[]>;
  const range = {start: 0, end: 5};
  const collectionViewerMock: CollectionViewer = {
    viewChange: new Observable<ListRange>(subscriber => subscriber.next(range))
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    selectedGuests$ = new BehaviorSubject<GuestSelection[]>([]);
    selectedGuestsDataSource = new SelectedGuestsDataSource(selectedGuests$);
  });

  it('should use initial list value', done => {
    // given

    // when
    selectedGuestsDataSource.connect(collectionViewerMock).subscribe({
      next: result => {
        // then
        expect(result).toEqual([]);
        done();
      }
    });
  });

  it('should use list with elements', done => {
    // given
    const selectedUser = [
      {id: 'id-1'},
      {id: 'id-2'},
      {id: 'id-3'}
    ] as GuestSelection[];
    const selectedGuestsDataSource$ = selectedGuestsDataSource.connect(collectionViewerMock);
    selectedGuests$.next(selectedUser);

    // when
    selectedGuestsDataSource$.subscribe({
      next: result => {
        // then
        expect(result).toEqual(selectedUser);
        done();
      }
    });
  });

  it('should unsubscribe on disconnect', () => {
    // given
    const selectedGuestsSubscription: Subscription = new Subscription();
    spyOn(selectedGuestsSubscription, 'unsubscribe').and.callThrough();
    spyOn(selectedGuests$, 'subscribe').and.callFake((fn: Function): Subscription => {
      fn([]);
      return selectedGuestsSubscription;
    });
    selectedGuestsDataSource.connect(collectionViewerMock);

    // when
    selectedGuestsDataSource.disconnect();

    // then
    expect(selectedGuestsSubscription.unsubscribe).toHaveBeenCalled();
  });

  it('should indicate a empty data source', done => {
    // given
    selectedGuestsDataSource.isEmpty$.subscribe(isEmpty => {
      // then
      expect(isEmpty).toBeTruthy();
      done();
    });

    // when
    selectedGuestsDataSource.connect(collectionViewerMock);
  });

  it('should indicate a not empty data source', done => {
    // given
    selectedGuestsDataSource.connect(collectionViewerMock);

    selectedGuestsDataSource.isEmpty$.subscribe(isEmpty => {
      // then
      expect(isEmpty).toBeFalsy();
      done();
    });

    // when
    selectedGuests$.next([{}, {}] as GuestSelection[]);
  });
});
