import {CollectionViewer, ListRange} from '@angular/cdk/collections';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Guest} from '@domain/guest/Guest';
import {GuestSelection} from '@domain/guest/GuestSelection';
import {UserChooserService} from '@domain/guest/user-chooser.service';
import {UserChooserSelectionConfig} from '@domain/guest/UserChooserSelectionConfig';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {UserChooserDataSource} from '@shared/sender-ui/user-chooser/data-source/user-chooser-data-source';
import * as _ from 'lodash';
import {BehaviorSubject, Observable, of, Subscription} from 'rxjs';

describe('UserChooserDataSource', () => {

  const userCount = 100;
  const pageSize = 20;
  const range = {start: 0, end: 10};
  let userChooserDataSource: UserChooserDataSource;
  let selectedGuests$: BehaviorSubject<GuestSelection[]>;
  let userChooserService: jasmine.SpyObj<UserChooserService>;
  let range$: BehaviorSubject<ListRange>;
  let collectionViewerMock: CollectionViewer;
  let users: Guest[];
  let page: Page<Guest>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: UserChooserService,
        useValue: jasmine.createSpyObj('userChooserService', ['searchGuests'])
      }]
    });

    users = [];
    for (let i = 1; i <= userCount; i++) {
      users.push({id: 'id-' + i, displayName: 'guest-' + i} as Guest);
    }

    userChooserService = TestBed.get(UserChooserService);
    userChooserService.searchGuests.and.callFake((searchTerm: string, types: UserChooserSelectionConfig, pageable: Pageable) => {
      const pages = _.chunk(users, pageSize);
      page.content = pages[pageable.page];
      page.number = pageable.page;
      return of(page);
    });
    selectedGuests$ = new BehaviorSubject<GuestSelection[]>([]);
    userChooserDataSource = new UserChooserDataSource(userChooserService, 'A', {} as UserChooserSelectionConfig, selectedGuests$);
    range$ = new BehaviorSubject<ListRange>(range);
    collectionViewerMock = {
      viewChange: new Observable<ListRange>(subscriber => range$.subscribe(r => subscriber.next(r)))
    };
    page = {
      content: users,
      size: pageSize,
      first: true,
      last: false,
      number: 0,
      numberOfElements: pageSize,
      totalElements: userCount,
      totalPages: Math.ceil(userCount / pageSize),
      empty: false
    } as Page<Guest>;
  });

  it('should load first page on connect', done => {
    // given
    const source$ = userChooserDataSource.connect(collectionViewerMock);
    const expected = _.concat(_.chunk(users, pageSize)[0] as GuestSelection[], new Array(80).fill(null));

    // when
    source$.subscribe(result => {

      // then
      expect(result).toEqual(expected);
      expect(userChooserService.searchGuests).toHaveBeenCalledTimes(1);
      done();
    });
  });

  it('should load more pages on view change', fakeAsync(() => {
    const source$ = userChooserDataSource.connect(collectionViewerMock);
    const expected = _.concat(_.chunk(users, 60)[0] as GuestSelection[], new Array(40).fill(null));

    // when
    range$.next({start: 0, end: 20});
    range$.next({start: 20, end: 40});
    range$.complete();
    tick(250);
    source$.subscribe({
      next: result => {
        // then
        expect(result).toEqual(expected);
        expect(userChooserService.searchGuests).toHaveBeenCalledTimes(3);
      }
    });
  }));

  it('should mark selected guests', done => {
    // given
    const source$ = userChooserDataSource.connect(collectionViewerMock);
    const expected = _.concat(_.chunk(users, pageSize)[0] as GuestSelection[], new Array(80).fill(null));
    expected[3] = {id: 'id-' + 4, displayName: 'guest-' + 4, selected: true} as GuestSelection;
    expected[5] = {id: 'id-' + 6, displayName: 'guest-' + 6, selected: true} as GuestSelection;
    const selectedGuests = [{id: 'id-' + 4}, {id: 'id-' + 6}] as GuestSelection[];
    selectedGuests$.next(selectedGuests);

    // when
    source$.subscribe({
      next: result => {
        // then
        expect(result).toEqual(expected);
        done();
      }
    });
  });

  it('should unsubscribe on disconnect', () => {
    // given
    const selectedGuestsSubscription: Subscription = new Subscription();
    spyOn(selectedGuestsSubscription, 'unsubscribe').and.callThrough();
    spyOn(selectedGuests$, 'subscribe').and.callFake((fn: Function): Subscription => {
      fn([]);
      return selectedGuestsSubscription;
    });
    userChooserDataSource.connect(collectionViewerMock);

    // when
    userChooserDataSource.disconnect();

    // then
    expect(selectedGuestsSubscription.unsubscribe).toHaveBeenCalled();
  });

  it('should indicate a empty data source', done => {
    // given
    page.empty = true;
    userChooserDataSource.isEmpty$.subscribe(isEmpty => {
      // then
      expect(isEmpty).toBeTruthy();
      done();
    });

    // when
    userChooserDataSource.connect(collectionViewerMock);
  });

  it('should indicate a empty data source', done => {
    // given
    userChooserDataSource.isEmpty$.subscribe(isEmpty => {
      // then
      expect(isEmpty).toBeFalsy();
      done();
    });

    // when
    userChooserDataSource.connect(collectionViewerMock);
  });

});
