import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {GuestSelection} from '@domain/guest/GuestSelection';
import {UserChooserService} from '@domain/guest/user-chooser.service';
import {UserChooserSelectionConfig} from '@domain/guest/UserChooserSelectionConfig';
import {SelectedGuestsDataSource} from '@shared/sender-ui/user-chooser/data-source/selected-guests-data-source';
import {UserChooserDataSource} from '@shared/sender-ui/user-chooser/data-source/user-chooser-data-source';
import {BehaviorSubject} from 'rxjs';

import {GuestListComponent} from './guest-list.component';

describe('GuestListComponent', () => {
  let component: GuestListComponent;
  let fixture: ComponentFixture<GuestListComponent>;
  let userChooserService: jasmine.SpyObj<UserChooserService>;
  let selectedGuestsDataSource: SelectedGuestsDataSource;
  let userChooserDataSource: UserChooserDataSource;
  const guests$: BehaviorSubject<GuestSelection[]> = new BehaviorSubject([]);
  const event = {
    stopPropagation: () => {},
    preventDefault: () => {}
  } as Event;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [GuestListComponent],
        providers: [{
            provide: UserChooserService,
            useValue: jasmine.createSpyObj('userChooserService', ['searchGuests'])
          }, ChangeDetectorRef]
      })
      .overrideTemplate(GuestListComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuestListComponent);
    userChooserService = TestBed.get(UserChooserService);
    userChooserService.searchGuests.and.callThrough();
    component = fixture.componentInstance;
    selectedGuestsDataSource = new SelectedGuestsDataSource(guests$);
    userChooserDataSource = new UserChooserDataSource(userChooserService, 'A', {} as UserChooserSelectionConfig, guests$);
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init with scroll settings', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component.scrollSettings).toBe(0);
  });

  it('should init with scroll settings for list without checkboxes', () => {
    // given
    component.renderCheckbox = false;

    // when
    fixture.detectChanges();

    // then
    expect(component.scrollSettings).toBe(1);
  });

  it('should init with selected guests data source', done => {
    // given
    component.guestDataSource = new BehaviorSubject<SelectedGuestsDataSource>(selectedGuestsDataSource);

    // when
    fixture.detectChanges();

    // then
    component.guestDataSource.subscribe(dataSource => {
      expect(dataSource instanceof SelectedGuestsDataSource).toBeTruthy();
      done();
    });
  });

  it('should init with user chooser data source', done => {
    // given
    component.guestDataSource = new BehaviorSubject<UserChooserDataSource>(userChooserDataSource);

    // when
    fixture.detectChanges();

    // then
    component.guestDataSource.subscribe(dataSource => {
      expect(dataSource instanceof UserChooserDataSource).toBeTruthy();
      done();
    });
  });

  it('should emit on change', () => {
    // given
    fixture.detectChanges();
    spyOn(component.changes, 'emit');

    // when
    component.emitChange({} as GuestSelection);

    // then
    expect(component.changes.emit).toHaveBeenCalled();
  });

  it('should emit on change', () => {
    // given
    const guest = {
      selected: true
    } as GuestSelection;
    fixture.detectChanges();
    spyOn(component.changes, 'emit');

    // when
    component.change(event, guest);

    // then
    expect(guest.selected).toBeFalsy();
    expect(component.changes.emit).toHaveBeenCalled();
  });

  it('should process translation params', () => {
    // given
    const translationParams = ['paramA', 'paramB'];
    const expected = {
      count: 2,
      param0: 'paramA',
      param1: 'paramB'
    };
    fixture.detectChanges();

    // when
    const result = component.processParams(translationParams);

    // then
    expect(result).toEqual(expected);
  });
});
