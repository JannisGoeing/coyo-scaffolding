import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {GuestListComponent} from '@shared/sender-ui/guest-list/guest-list.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoGuestList', downgradeComponent({
    component: GuestListComponent,
    propagateDigest: false
  }));
