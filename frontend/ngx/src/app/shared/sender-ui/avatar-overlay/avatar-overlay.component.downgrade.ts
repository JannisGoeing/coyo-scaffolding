import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {AvatarOverlayComponent} from '@shared/sender-ui/avatar-overlay/avatar-overlay.component';

getAngularJSGlobal()
  .module('commons.sender')
  .directive('coyoAvatarOverlay', downgradeComponent({
    component: AvatarOverlayComponent,
    propagateDigest: false
  }));
