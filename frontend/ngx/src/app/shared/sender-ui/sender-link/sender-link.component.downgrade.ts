import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SenderLinkComponent} from './sender-link.component';

getAngularJSGlobal()
  .module('coyo.domain')
  .directive('coyoSenderLink', downgradeComponent({
    component: SenderLinkComponent,
    propagateDigest: false
  }));
