import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Target} from '@domain/sender/target';
import {TargetService} from '@domain/sender/target/target.service';
import {User} from '@domain/user/user';
import {of} from 'rxjs';
import {UserAvatarComponent} from './user-avatar.component';

describe('UserAvatarComponent', () => {
  let component: UserAvatarComponent;
  let fixture: ComponentFixture<UserAvatarComponent>;
  let targetService: jasmine.SpyObj<TargetService>;

  const user = {
    id: 'user-id',
    target: {
      name: 'user',
      params: {
        id: 'user-id',
        slug: 'bob-ross'
      }
    } as Target
  } as User;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAvatarComponent ],
      providers: [{provide: TargetService, useValue: jasmine.createSpyObj('targetService', ['canLinkTo'])}
    ]
    }).overrideTemplate(UserAvatarComponent, '')
      .compileComponents();

    targetService = TestBed.get(TargetService);
    targetService.canLinkTo.and.returnValue(of(false));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAvatarComponent);
    component = fixture.componentInstance;
    component.user = user;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be link', () => {
    // given
    component.noLink = false;
    targetService.canLinkTo.and.returnValue(of(true));
    // when
    component.ngOnInit();
    // then
    component.state$.subscribe(state => expect(state.showLink).toBe(true));
  });

  it('should not be link', () => {
    // given
    component.noLink = true;
    // when
    component.ngOnInit();
    // then
    component.state$.subscribe(state => expect(state.showLink).toBe(false));
  });
});
