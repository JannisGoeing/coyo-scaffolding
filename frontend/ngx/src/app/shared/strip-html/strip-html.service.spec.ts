import {DOCUMENT} from '@angular/common';
import {inject, TestBed} from '@angular/core/testing';
import {StripHtmlService} from './strip-html.service';

describe('StripHtmlService', () => {
  let document: Document;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StripHtmlService]
    });
    document = TestBed.get(DOCUMENT);

    spyOn(document, 'createElement').and.callFake((tagName: string) => {
      const tag = '<' + tagName + '></' + tagName + '>';
      const parser = new DOMParser();
      const html = parser.parseFromString(tag, 'text/html');
      return html.body.firstChild;
    });
  });

  it('should be created', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service).toBeTruthy();
  }));

  it('should do nothing with null value', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip(null)).toEqual('');
  }));

  it('should do nothing with undefined value', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip(undefined)).toEqual('');
  }));

  it('should do nothing with empty string', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('')).toEqual('');
  }));

  it('should strip headline', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<h1>headline text</h1>')).toEqual('headline text');
  }));

  it('should strip paragraph', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<p>paragraph text</p>')).toEqual('paragraph text');
  }));

  it('should strip boldness', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<strong>strong text</strong>')).toEqual('strong text');
  }));

  it('should strip italic', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<em>italic text</em>')).toEqual('italic text');
  }));

  it('should strip underlined', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<u>underlined text</u>')).toEqual('underlined text');
  }));

  it('should strip strikethrough', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<s>strikethrough text</s>')).toEqual('strikethrough text');
  }));

  it('should strip link', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<a href="https://www.coyoapp.com">COYO</a>')).toEqual('COYO');
  }));

  it('should strip ordered list', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<ol><li>some random list entry</li></ol>')).toEqual('some random list entry');
  }));

  it('should strip unordered list', inject([StripHtmlService], (service: StripHtmlService) => {
    expect(service.strip('<ul><li>some&nbsp;random&nbsp;list&nbsp;entry</li></ul>')).toEqual('some random list entry');
  }));
});
