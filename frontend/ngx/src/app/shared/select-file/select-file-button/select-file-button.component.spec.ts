import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material';
import {App} from '@domain/apps/app';
import {Document} from '@domain/file/document';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {SelectFileDialogComponent} from '@shared/select-file/select-file-dialog/select-file-dialog.component';
import {EMPTY, of, throwError} from 'rxjs';
import {SelectFileButtonComponent} from './select-file-button.component';

describe('SelectFileButtonComponent', () => {
  const defaultOptions = {
    selectMode: 'multiple',
    uploadMultiple: true, initialFolder: {}
  };
  let component: SelectFileButtonComponent;
  let fixture: ComponentFixture<SelectFileButtonComponent>;
  let matDialog: jasmine.SpyObj<MatDialog>;
  let senderService: jasmine.SpyObj<SenderService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectFileButtonComponent],
      providers: [{
        provide: MatDialog,
        useValue: jasmine.createSpyObj('matDialog', ['open'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('senderService', ['getCurrentApp'])
      }]
    }).overrideTemplate(SelectFileButtonComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    matDialog = TestBed.get(MatDialog);
    senderService = TestBed.get(SenderService);
    fixture = TestBed.createComponent(SelectFileButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open the file library modal', done => {
    // given
    const fileArray = [{id: 'file-id'}] as Document[];
    component.sender = {id: 'sender-id'} as Sender;
    senderService.getCurrentApp.and.returnValue(of(EMPTY));
    matDialog.open.and.returnValue({afterClosed: () => of(fileArray)});
    fixture.detectChanges();

    component.filesSelected.asObservable().subscribe(array => {
      expect(array).toEqual(fileArray);
      done();
    });

    // when
    component.selectFiles();

    // then
    expect(matDialog.open)
      .toHaveBeenCalledWith(SelectFileDialogComponent, {
        width: '768px',
        data: {
          sender: component.sender,
          options: {
            selectMode: 'multiple',
            uploadMultiple: true,
            initialFolder: {}
          },
          selectedFiles: [],
          cropSettings: {},
        }
      });
  });

  it('should set the initialFolder', () => {
    // given
    component.sender = {id: 'sender-id'} as Sender;
    const rootFolderId = 'some-id';
    senderService.getCurrentApp.and.returnValue(of({rootFolderId: rootFolderId} as App));
    matDialog.open.and.returnValue({afterClosed: () => of(null)});
    fixture.detectChanges();

    // when
    component.selectFiles();

    // then
    expect(matDialog.open)
      .toHaveBeenCalledWith(SelectFileDialogComponent, {
        width: '768px',
        data: {
          sender: component.sender,
          options: {
            selectMode: 'multiple',
            uploadMultiple: true,
            initialFolder: {id: rootFolderId},
          },
          cropSettings: {},
          selectedFiles: []
        }
      });
  });

  it('should open the file library without setting the initial folder when request fails', () => {
    // given
    component.sender = {id: 'sender-id'} as Sender;
    senderService.getCurrentApp.and.returnValue(throwError({status: 404}));
    matDialog.open.and.returnValue({afterClosed: () => of(null)});
    fixture.detectChanges();

    // when
    component.selectFiles();

    // then
    expect(matDialog.open)
      .toHaveBeenCalledWith(SelectFileDialogComponent, {
        width: '768px',
        data: {
          sender: component.sender,
          options: {
            selectMode: 'multiple',
            uploadMultiple: true,
            initialFolder: {}
          },
          cropSettings: {},
          selectedFiles: []
        }
      });
  });
});
