/**
 * Contains all the options for the select file component
 */
export interface SelectFileOptions {
  selectMode?: 'single' | 'multiple' | 'folder';
  uploadMultiple?: boolean;
  showAuthors?: boolean;
  initialFolder?: object;
  filterContentType?: string;
}

export interface CropSettings {
  cropImage: boolean;
}
