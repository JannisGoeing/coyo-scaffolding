import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {File} from '@domain/file/file';
import {FileService} from '@domain/file/file/file.service';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {Ng1FileLibraryModalService} from '@root/typings';
import {SelectFileOptions} from '@shared/select-file/select-file-options';
import {of} from 'rxjs';
import {skip} from 'rxjs/operators';
import {SelectFileComponent} from './select-file.component';
import SpyObj = jasmine.SpyObj;

describe('SelectFileComponent', () => {
  let component: SelectFileComponent;
  let fixture: ComponentFixture<SelectFileComponent>;
  let senderService: SpyObj<SenderService>;
  let dialog: jasmine.SpyObj<Ng1FileLibraryModalService>;
  let onChangeSpyFn: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectFileComponent],
      providers: [
        {
          provide: MatDialog,
          useValue: jasmine.createSpyObj('dialog', ['open'])
        },
        {
          provide: FileService,
          useValue: jasmine.createSpyObj('fileService', ['getFile'])
        },
        {
          provide: SenderService,
          useValue: jasmine.createSpyObj('senderService', ['getCurrentApp'])
        }
      ]
    })
      .overrideTemplate(SelectFileComponent, '')
      .compileComponents();

    senderService = TestBed.get(SenderService);
    dialog = TestBed.get(MatDialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectFileComponent);
    component = fixture.componentInstance;
    onChangeSpyFn = jasmine.createSpy('onChangeSpyFn');
    component.registerOnChange(onChangeSpyFn);
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should use default options', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component.options.uploadMultiple).toBeTruthy();
    expect(component.options.showAuthors).toBeFalsy();
    expect(component.options.selectMode).toEqual('multiple');
  });

  it('should add value and sort it', () => {
    // given
    const file1 = {id: 'first file', name: 'A'} as File;
    const file2 = {id: 'second file', name: 'B'} as File;
    const file3 = {id: 'third file', name: 'C'} as File;
    const file4 = {id: 'fourth file', name: 'D'} as File;
    component.sender = {id: 'sender-id'} as Sender;
    fixture.detectChanges();

    // when
    component.writeValue([file2, file4, file1, file3]);

    // then
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.selectedFiles).toEqual([file1, file2, file3, file4]);
    }, error => expect(error).toBeFalsy());
  });

  it('should sort correctly', () => {
    // given
    const file1 = {id: 'first file', name: '.'} as File;
    const file2 = {id: 'second file', name: 'B'} as File;
    const file3 = {id: 'third file', name: 'C'} as File;
    const file4 = {id: 'fourth file', name: ''} as File;
    const file5 = {id: 'fifth file', name: 'C'} as File;
    component.sender = {id: 'sender-id'} as Sender;
    fixture.detectChanges();

    // when
    component.writeValue([file2, file4, file1, file3, file5]);

    // then
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.selectedFiles).toEqual([file4, file1, file2, file3, file5]);
    }, error => expect(error).toBeFalsy());
  });

  it('should use default crop settings', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component.cropSettings.cropImage).toBeFalsy();
  });

  it('should remove the file when select mode is single', done => {
    // given
    const file = {id: 'first file'} as File;
    const files = [file];
    component.state$ = of({selectedFiles: files});
    fixture.detectChanges();

    // when
    component.removeFile(file, files);

    // then
    component.state$.pipe(skip(1)).subscribe(
      state => {
        expect(state.selectedFiles.length).toBe(0);
        expect(onChangeSpyFn).toHaveBeenCalledWith([]);
        done();
      });
  });

  it('should remove particular file of selectedFiles when select mode is multiple', done => {
    // given
    const file1 = {id: 'first file'} as File;
    const file2 = {id: 'second file'} as File;
    const files = [file1, file2];
    component.state$ = of({selectedFiles: files});
    fixture.detectChanges();

    // when
    component.removeFile(file2, files);

    // then
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.selectedFiles.length).toBe(1);
      expect(state.selectedFiles[0].id).toBe(file1.id);
      expect(onChangeSpyFn).toHaveBeenCalledWith([file1]);
      done();
    });
  });

  it('should replace the selected file when select mode is single', done => {
    // given
    component.sender = {id: 'sender-id'} as Sender;
    const file1 = {id: 'first file'} as File;
    const file2 = {id: 'second file'} as File;
    component.options = {selectMode: 'single'} as SelectFileOptions;
    fixture.detectChanges();
    component.writeValue(file1);

    // when
    component.selectFile([file2], [file1]);

    // then
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.selectedFiles).toEqual([file2]);
      expect(onChangeSpyFn).toHaveBeenCalledWith(file2);
      done();
    });
  });

  it('should add the selected file to the already selected files', done => {
    // given
    component.sender = {id: 'sender-id'} as Sender;
    const file1 = {id: 'first file', name: 'A'} as File;
    const file2 = {id: 'second file', name: 'B'} as File;
    const file3 = {id: 'third file', name: 'c'} as File;
    const files = [file1, file3];
    component.options = {selectMode: 'multiple'} as SelectFileOptions;
    component.state$ = of({selectedFiles: files});
    fixture.detectChanges();

    // when
    component.selectFile([file2], files);

    // then
    component.state$.pipe(skip(1)).subscribe(state => {
      expect(state.selectedFiles).toEqual([file1, file2, file3]);
      expect(onChangeSpyFn).toHaveBeenCalledWith([file1, file2, file3]);
      done();
    });
  });
});
