import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {App} from '@domain/apps/app';
import {GlobalAppService} from '@domain/apps/global-app.service';
import {Pageable} from '@domain/pagination/pageable';
import {Permissions} from '@domain/permissions/permissions';
import {SelectUiComponent} from '@shared/select-ui/select-ui.component';
import {SelectAppComponent} from './select-app.component';

describe('SelectAppComponent', () => {
  let component: SelectAppComponent;
  let fixture: ComponentFixture<SelectAppComponent>;
  let globalAppService: jasmine.SpyObj<GlobalAppService>;
  const select: jasmine.SpyObj<SelectUiComponent<any>> = jasmine.createSpyObj('SelectUiComponent',
    ['registerOnChange', 'registerOnTouched', 'setDisabledState', 'writeValue']);
  const setting_key = 'test-key';
  const setting_placeholder = 'test-placeholder';
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SelectAppComponent],
        providers: [{
          provide: GlobalAppService,
          useValue: jasmine.createSpyObj('GlobalAppService', ['get'])
        }]
      })
      .overrideTemplate(SelectAppComponent, '<div></div>')
      .compileComponents().then(() => {
      globalAppService = TestBed.get(GlobalAppService);
      fixture = TestBed.createComponent(SelectAppComponent);
      component = fixture.componentInstance;
      component.key = setting_key;
      component.placeholder = setting_placeholder;
    });
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should pass through ControlValueAccessor functions to the child component', fakeAsync(() => {
    // given

    // when
    fixture.detectChanges();
    component.select = select;
    component.registerOnChange('a');
    component.registerOnTouched('b');
    component.writeValue('c');
    component.setDisabledState(true);
    tick();
    // then
    expect(select.registerOnChange).toHaveBeenCalledWith('a');
    expect(select.registerOnTouched).toHaveBeenCalledWith('b');
    expect(select.writeValue).toHaveBeenCalledWith('c');
    expect(select.setDisabledState).toHaveBeenCalledWith(true);
  }));

  it('should correctly init settings', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component.settings).toEqual({
      closeOnSelect: component.closeOnSelect,
      multiselect: component.multiselect,
      searchFn: component.settings.searchFn,
      compareFn: component.settings.compareFn,
      clearable: component.clearable,
      placeholder: component.placeholder,
      debounceTime: component.typeaheadDebounceTime,
      pageSize: component.scrollPageSize,
      scrollOffsetTrigger: component.scrollPageTriggerOffset
    });
  });

  it('should correctly compare apps', () => {
    // given
    const app1: App = {
      id: 'app1',
      created: 0,
      modified: 1,
      rootFolderId: 'test-rootFolderId',
      settings: {a: 4, b: 'test'},
      slug: 'test-slug1',
      key: 'test-key1',
      senderType: 'test-senderType',
      senderId: 'test-senderId',
      name: 'test-name1',
      _permissions: {a: true, b: false} as Permissions
    };
    const app2: App = {
      id: 'app1',
      created: 0,
      modified: 1,
      rootFolderId: 'test-rootFolderId',
      settings: {a: 4, b: 'test'},
      slug: 'test-slug1',
      key: 'test-key1',
      senderType: 'test-senderType',
      senderId: 'test-senderId',
      name: 'test-name1',
      _permissions: {a: true, b: false} as Permissions
    };
    const app3: App = {
      id: 'app3',
      created: 0,
      modified: 1,
      rootFolderId: 'test-rootFolderId',
      settings: {a: 4, b: 'test'},
      slug: 'test-slug2',
      key: 'test-key2',
      senderType: 'test-senderType',
      senderId: 'test-senderId',
      name: 'test-name2',
      _permissions: {a: true, b: false} as Permissions
    };

    // when
    fixture.detectChanges();

    // then

    expect(component.settings.compareFn(app1, app1)).toBeTruthy();
    expect(component.settings.compareFn(app1, app2)).toBeTruthy();
    expect(component.settings.compareFn(app1, app3)).toBeFalsy();
  });

  it('should use the global app service to find apps', () => {
    // given

    // when
    fixture.detectChanges();
    component.settings.searchFn(new Pageable(0, 10), 'test-term');

    // then
    expect(globalAppService.get).toHaveBeenCalledWith(new Pageable(0, 10), component.key, 'test-term');
  });

  it('should correctly update settings', () => {
    // given
    component.typeaheadDebounceTime = 1;

    // when
    fixture.detectChanges();

    // then
    expect(component.settings).toEqual({
      closeOnSelect: component.closeOnSelect,
      multiselect: component.multiselect,
      searchFn: component.settings.searchFn,
      compareFn: component.settings.compareFn,
      clearable: component.clearable,
      placeholder: component.placeholder,
      debounceTime: component.typeaheadDebounceTime,
      pageSize: component.scrollPageSize,
      scrollOffsetTrigger: component.scrollPageTriggerOffset
    });
  });
});
