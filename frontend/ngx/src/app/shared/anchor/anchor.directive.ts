import {Directive, HostListener, Inject} from '@angular/core';
import {WINDOW} from '@root/injection-tokens';

/**
 * Directive to make anchor tags clickable every time.
 */
@Directive({
  selector: 'a[coyoAnchor]'
})
export class AnchorDirective {

  constructor(@Inject(WINDOW) private window: Window) {
  }

  /**
   * Handles click events on this directive.
   *
   * @param $event the click event
   */
  @HostListener('click', ['$event'])
  onClick($event: MouseEvent): void {
    const target = ($event.target as HTMLAnchorElement);
    // reset to empty hash before setting it to the real value so scrolling works multiple times instead of just once
    this.window.location.hash = '';
    this.window.location.hash = target.hash;
    // reset base URL so anchor links can be opened in a new tab
    target.href = this.window.location.href;
  }
}
