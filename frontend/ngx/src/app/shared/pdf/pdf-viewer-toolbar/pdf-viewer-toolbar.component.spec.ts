import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SettingPreset} from '../setting-preset';
import {settingPresets} from '../setting-presets';
import {PdfViewerToolbarComponent} from './pdf-viewer-toolbar.component';

describe('PdfViewerToolbarComponent', () => {
  let component: PdfViewerToolbarComponent;
  let fixture: ComponentFixture<PdfViewerToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PdfViewerToolbarComponent]
    }).overrideTemplate(PdfViewerToolbarComponent, '<div></div>')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfViewerToolbarComponent);
    component = fixture.componentInstance;
    component.settingPresets = settingPresets;
    component.activeSetting = settingPresets[0];
    fixture.detectChanges();
  });

  it('should init', () => {
    let newSetting: SettingPreset = null;
    component.settingSelected.subscribe((setting: SettingPreset) => newSetting = setting);
    component.ngOnInit();
    expect(component.presets.length).toBe(settingPresets.length);
    expect(component.activeSetting).toEqual(settingPresets[0]);
    expect(component.activeSetting).toEqual(newSetting);
  });

  it('should change page on enter', () => {
    let page: number;
    component.maxPages = 3;
    component.pageChange.subscribe((pageChange: number) => page = pageChange);
    component.onEnter(2);
    expect(page).toBe(2);
  });

  it('should navigate to a page', () => {
    let newPage: number;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.changePage(2);
    expect(newPage).toBe(2);
  });

  it('should not navigate to a page before first page', () => {
    let newPage: number;
    component.currentPage = 1;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.changePage(0);
    expect(newPage).toBe(undefined);
  });

  it('should not navigate to a page after last page', () => {
    let newPage: number;
    component.currentPage = 1;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.changePage(4);
    expect(newPage).toBe(undefined);
  });

  it('should navigate to previous page', () => {
    let newPage: number;
    component.currentPage = 2;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.previousPage();
    expect(newPage).toBe(1);
  });

  it('should not navigate to previous page on first page', () => {
    let newPage: number;
    component.currentPage = 1;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.previousPage();
    expect(newPage).toBe(undefined);
  });

  it('should navigate to next page', () => {
    let newPage: number;
    component.currentPage = 2;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.nextPage();
    expect(newPage).toBe(3);
  });

  it('should not navigate to next page on last page', () => {
    let newPage: number;
    component.currentPage = 3;
    component.maxPages = 3;
    component.pageChange.subscribe((page: number) => newPage = page);
    component.nextPage();
    expect(newPage).toBe(undefined);
  });

  it('should zoom out', () => {
    let newZoom = 0;
    component.settingSelected.subscribe((setting: SettingPreset) => newZoom = setting.zoom);
    component.zoomOut();
    expect(newZoom).toBe(0.8);
  });

  it('should not zoom out under 0.1', () => {
    let newZoom = 0;
    component.zoom(0.1);
    component.settingSelected.subscribe((setting: SettingPreset) => newZoom = setting.zoom);
    component.zoomOut();
    expect(newZoom).toBe(0.1);
  });

  it('should zoom in', () => {
    let newZoom = 0;
    component.settingSelected.subscribe((setting: SettingPreset) => newZoom = setting.zoom);
    component.zoomIn();
    expect(newZoom).toBe(1.2);
  });

  it('should not zoom in over 10', () => {
    let newZoom = 0;
    component.zoom(10);
    component.settingSelected.subscribe((setting: SettingPreset) => newZoom = setting.zoom);
    component.zoomIn();
    expect(newZoom).toBe(10);
  });

  it('should rotate clockwise', () => {
    let newRotation = 0;
    component.rotation = 0;
    component.updateRotation.subscribe((rotation: number) => newRotation = rotation);
    component.rotateRight();
    expect(newRotation).toBe(90);
  });

  it('should rotate counter-clockwise', () => {
    let newRotation = 0;
    component.rotation = 90;
    component.updateRotation.subscribe((rotation: number) => newRotation = rotation);
    component.rotateLeft();
    expect(newRotation).toBe(0);
  });

  it('should set active setting', () => {
    const newSetting = {} as SettingPreset;
    component.updateActiveSetting(newSetting);
    expect(component.activeSetting).toBe(newSetting);
  });
});
