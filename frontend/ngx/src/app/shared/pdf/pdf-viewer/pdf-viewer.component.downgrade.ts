import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {PdfViewerComponent} from './pdf-viewer.component';

getAngularJSGlobal()
  .module('commons.ui')
  .directive('coyoPdfViewer', downgradeComponent({
    component: PdfViewerComponent
  }));
