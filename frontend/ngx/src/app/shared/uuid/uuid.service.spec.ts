import {TestBed} from '@angular/core/testing';
import {UuidService} from '@shared/uuid/uuid.service';

describe('UuidService', () => {
  let service: UuidService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.get(UuidService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should generate different UUID\'s', () => {
    const a = service.getUuid();
    const b = service.getUuid();
    const c = service.getUuid();
    expect(a).not.toEqual(b);
    expect(a).not.toEqual(c);
    expect(b).not.toEqual(c);
  });

  it('should generate valid UUID\'s', () => {
    // given
    // https://gist.github.com/johnelliott/cf77003f72f889abbc3f32785fa3df8d
    // https://stackoverflow.com/questions/19989481/how-to-determine-if-a-string-is-a-valid-v4-uuid
    // https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_(random)
    const uuid_v4_regexp = new RegExp(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);

    // when
    const a = service.getUuid();
    const b = service.getUuid();
    const c = service.getUuid();

    // then
    expect(a.match(uuid_v4_regexp)).toBeTruthy();
    expect(b.match(uuid_v4_regexp)).toBeTruthy();
    expect(c.match(uuid_v4_regexp)).toBeTruthy();
  });
});
