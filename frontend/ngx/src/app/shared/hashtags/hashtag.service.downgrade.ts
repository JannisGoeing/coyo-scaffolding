import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {HashtagService} from './hashtag.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxHashtagService', downgradeInjectable(HashtagService));
