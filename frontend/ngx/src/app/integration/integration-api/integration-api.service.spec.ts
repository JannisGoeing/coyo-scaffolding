import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {getTestBed, inject, TestBed} from '@angular/core/testing';
import {AccessToken} from '@app/integration/gsuite/google-api/access-token';
import {AuthService} from '@core/auth/auth.service';
import {SettingsService} from '@domain/settings/settings.service';
import {User} from '@domain/user/user';
import {Observable, of} from 'rxjs';
import {IntegrationApiService} from './integration-api.service';

describe('IntegrationApiService', () => {
  let injector: TestBed;
  let httpMock: HttpTestingController;
  let user: User;
  const ssoTokenResponse: AccessToken = {
    token: 'any-valid-access-token',
    expiresIn: 5000
  };
  const authService: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['getUser']);
  const settingsService: jasmine.SpyObj<SettingsService> = jasmine.createSpyObj('settingsService', ['retrieveByKey']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [IntegrationApiService, {
        provide: SettingsService,
        useValue: settingsService
      }, {
        provide: AuthService,
        useValue: authService
      }]
    });

    injector = getTestBed();

    user = {globalPermissions: ['NO_PERMISSION']} as User;
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([IntegrationApiService], (service: IntegrationApiService) => {
    // then
    expect(service).toBeTruthy();
  }));

  it('should get true for updateAndGetActiveState', inject([IntegrationApiService], (service: IntegrationApiService) => {
    // given
    settingsService.retrieveByKey.and.returnValue(of('G_SUITE'));
    user.globalPermissions.push('ACCESS_G_SUITE_INTEGRATION');
    authService.getUser.and.returnValue(of(user));

    // when
    const actualResponse: Observable<boolean> = service.updateAndGetActiveState('G_SUITE');

    // then
    actualResponse.subscribe((isActive: boolean) => {
      expect(isActive).toBeTruthy();
    });

    const req = httpMock.expectOne(request => request.url === '/web/sso/token');
    req.flush(ssoTokenResponse);
  }));

  it('should get false for updateAndGetActiveState when integration setting not active',
    inject([IntegrationApiService], (service: IntegrationApiService) => {
      // given
      settingsService.retrieveByKey.and.returnValue(of('NONE'));

      // when
      const actualResponse: Observable<boolean> = service.updateAndGetActiveState('G_SUITE');

      // then
      actualResponse.subscribe((isActive: boolean) => {
        expect(isActive).toBeFalsy();
      });
    }));

  it('should get false for updateAndGetActiveState when user have insufficient permission',
    inject([IntegrationApiService], (service: IntegrationApiService) => {
      // given
      settingsService.retrieveByKey.and.returnValue(of('G_SUITE'));
      authService.getUser.and.returnValue(of(user));

      // when
      const actualResponse: Observable<boolean> = service.updateAndGetActiveState('G_SUITE');

      // then
      actualResponse.subscribe((isActive: boolean) => {
        expect(isActive).toBeFalsy();
      });
    }));

  it('should get false for updateAndGetActiveState when user not logged in with oauth',
    inject([IntegrationApiService], (service: IntegrationApiService) => {
      // given
      settingsService.retrieveByKey.and.returnValue(of('G_SUITE'));
      user.globalPermissions.push('ACCESS_G_SUITE_INTEGRATION');
      authService.getUser.and.returnValue(of(user));

      // when
      const actualResponse: Observable<boolean> = service.updateAndGetActiveState('G_SUITE');

      // then
      actualResponse.subscribe((isActive: boolean) => {
        expect(isActive).toBeFalsy();
      });

      const req = httpMock.expectOne(request => request.url === '/web/sso/token');
      req.error(new ErrorEvent('any error'));
    }));

  it('should call the token endpoint correctly', inject([IntegrationApiService], (service: IntegrationApiService) => {
    // when
    const actualResponse: Observable<AccessToken> = service.getToken();

    // then
    actualResponse.subscribe((result: AccessToken) => {
      expect(result).toEqual(ssoTokenResponse);
    });

    const req = httpMock.expectOne(request =>
      request.url === '/web/sso/token' &&
      request.headers.has('handleErrors') &&
      request.headers.get('handleErrors') === 'false');
    req.flush(ssoTokenResponse);
  }));

  it('should execute updateAndGetActiveState for isIntegrationApiActiveFor call',
    inject([IntegrationApiService], (service: IntegrationApiService) => {
      // when
      service.isIntegrationApiActiveFor('ANY_INTEGRATION_TYPE');

      // then
      // -- verify that only one of the updateAndGetActiveState method executions is triggered should be enough
      expect(settingsService.retrieveByKey).toHaveBeenCalledWith('integrationType');
    }));

});
