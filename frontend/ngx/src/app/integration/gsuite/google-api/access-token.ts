/**
 * Transfer structure for an OAuth token that is provided by COYO Core.
 */
export interface AccessToken {
  token: string;
  expiresIn: number;
}
