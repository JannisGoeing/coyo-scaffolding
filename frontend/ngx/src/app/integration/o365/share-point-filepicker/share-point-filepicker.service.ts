import {Injectable} from '@angular/core';
import {FilepickerItem} from '@app/filepicker/filepicker-modal/filepicker-item';
import {FilepickerSearch} from '@app/filepicker/filepicker-modal/filepicker-search/filepicker-search';
import {FilepickerService} from '@app/filepicker/filepicker.service';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {DriveFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-filepicker-item';
import {DriveItemFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/drive-item-filepicker-item';
import {SiteFilepickerItem} from '@app/integration/o365/share-point-filepicker/filepicker-items/site-filepicker-item';
import {TranslateService} from '@ngx-translate/core';
import {forkJoin, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export type FetchSites = () => Observable<SiteFilepickerItem[]>;
export type FetchDrives = (siteId: string) => Observable<DriveFilepickerItem[]>;
export type FetchDriveItems = (driveId: string, driveItemId?: string) => Observable<DriveItemFilepickerItem[]>;
export type FetchRecentDriveItems = () => Observable<DriveItemFilepickerItem[]>;
export type SearchSites = (query: string) => Observable<SiteFilepickerItem[]>;
export type SearchDriveItems = (query: string) => Observable<DriveItemFilepickerItem[]>;

/**
 * Service that handles the SharePoint Filepicker.
 */
@Injectable({
  providedIn: 'root'
})
export class SharePointFilepickerService {
  constructor(private filepickerService: FilepickerService, private o365ApiService: O365ApiService, private translateService: TranslateService) {
  }

  private search: FilepickerSearch = {
    inputPlaceholder: this.translateService.instant('INTEGRATION.O365.FILEPICKER.SEARCH_FOR_FILES'),
    execute: query => forkJoin([this.searchForSites(query), this.searchForFiles(query)])
      .pipe(map(searchResults => [...searchResults[0], ...searchResults[1]]))
  };

  /**
   * Opens the SharePoint filepicker
   * @return Observable of selected items
   */
  openFilepicker(): Observable<FilepickerItem[]> {
    return this.filepickerService.openFilepicker({
      rootFolder : this.getRootFolderFilepickerItem(),
      search: this.search,
      firstOpenFolder: this.getRecentFilesFilepickerItem()
    });
  }

  private getRootFolderFilepickerItem(): FilepickerItem {
    return this.createFolder('SharePoint', this.fetchSites);
  }

  private getRecentFilesFilepickerItem(): FilepickerItem {
    const recentFolderName = this.translateService.instant('RECENT_FOLDER.NAME');
    return this.createFolder(recentFolderName, this.fetchRecentFiles);
  }

  private createFolder(folderName: string, getChildren: () => Observable<FilepickerItem[]>): FilepickerItem {
    return {
      lastModified: null,
      mimeType: null,
      sizeInBytes: null,
      storageType: 'OFFICE_365',
      name: folderName,
      isFolder: true,
      getChildren: getChildren
    };
  }

  private fetchSites: FetchSites = () =>
    this.o365ApiService
      .getSites()
      .pipe(map(sites => SiteFilepickerItem.fromArray(sites, this.fetchDrives)));

  private fetchDrives: FetchDrives = siteId =>
    this.o365ApiService
      .getDrivesBySiteId(siteId)
      .pipe(map(drives => DriveFilepickerItem.fromArray(drives, this.fetchDriveItems)));

  private fetchRecentFiles: FetchRecentDriveItems = () =>
    this.o365ApiService
      .getRecentFiles()
      .pipe(map(files => DriveItemFilepickerItem.fromArray(files, this.fetchDriveItems)));

  private fetchDriveItems: FetchDriveItems = (driveId, driveItemId = 'root') =>
    this.o365ApiService
      .getDriveItems(driveId, driveItemId)
      .pipe(map(driveItems => DriveItemFilepickerItem.fromArray(driveItems, this.fetchDriveItems)));

  private searchForSites: SearchSites = (query: string) =>
    this.o365ApiService
      .searchForSites(query)
      .pipe(map(sites => SiteFilepickerItem.fromArray(sites, this.fetchDrives)));

  private searchForFiles: SearchDriveItems = (query: string) =>
    this.o365ApiService
      .searchForDriveItems(query)
      .pipe(map(driveItems => DriveItemFilepickerItem.fromArray(driveItems, this.fetchDriveItems)));
}
