export interface Insight {
    id: string;
    resourceReference: {
        id: string;
    };
    resourceVisualization: {
        title: string;
        mediaType: string;
        type: string;
    };
    lastUsed: {
        lastModifiedDateTime: string;
    };
}
