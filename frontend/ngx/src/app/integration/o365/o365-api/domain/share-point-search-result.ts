import {SharePointSearchRow} from '@app/integration/o365/o365-api/domain/share-point-search-row';

export interface SharePointSearchResult {
  totalResultCount: number;
  resultRows: SharePointSearchRow[];
}
