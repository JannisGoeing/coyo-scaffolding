import {FileDetail} from '@app/integration/o365/o365-api/domain/file-detail';

export interface DriveItemDetail {
  totalResultCount: number;
  driveItems: FileDetail[];
}
