import {HttpClient} from '@angular/common/http';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {of} from 'rxjs';
import {GraphApiService} from './graph-api.service';

describe('GraphApiService', () => {
  let service: GraphApiService;
  let integrationApiService: jasmine.SpyObj<IntegrationApiService>;
  let httpClient: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: IntegrationApiService,
        useValue: jasmine.createSpyObj('IntegrationApiService', ['updateAndGetActiveState', 'getToken'])
      }, {
        provide: HttpClient,
        useValue: jasmine.createSpyObj('HttpClient', ['get', 'post'])
      }]
    });
    integrationApiService = TestBed.get(IntegrationApiService);
    httpClient = TestBed.get(HttpClient);

    service = TestBed.get(GraphApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add Authorization header in GET request', fakeAsync(() => {
    // given
    const expectedToken = 'TOKEN';
    integrationApiService.getToken.and.returnValue(of({token: expectedToken}));
    let actualToken = null;
    httpClient.get.and.callFake((url: string, opts: { headers?: { 'Authorization': string } }) => {
      actualToken = opts.headers.Authorization;
      return of({value: []});
    });

    // when
    service.get<any>('/endpoint').subscribe();

    // then
    expect(integrationApiService.getToken).toHaveBeenCalled();
    expect(actualToken).toEqual('Bearer ' + expectedToken);
  }));

  it('should use the endpoints array index as id in batch request', fakeAsync(() => {
    // given
    integrationApiService.getToken.and.returnValue(of({token: 'TOKEN'}));
    httpClient.post.and.returnValue(of({responses: []}));

    // when
    service.getBatch<string>(['/endpoint-1', '/endpoint-2']).subscribe();
    tick();

    // then
    expect(httpClient.post).toHaveBeenCalledWith(jasmine.any(String), {
      requests: [
        {
          id: 0,
          method: 'GET',
          url: '/endpoint-1'
        }, {
          id: 1,
          method: 'GET',
          url: '/endpoint-2'
        }
      ]
    }, jasmine.any(Object));
  }));

  it('should sort batch requests responses by id', fakeAsync(() => {
    // given
    integrationApiService.getToken.and.returnValue(of({token: 'TOKEN'}));
    httpClient.post.and.returnValue(of({
      responses: [
        {id: 2, status: 200, body: 'response-3'},
        {id: 0, status: 200, body: 'response-1'},
        {id: 1, status: 200, body: 'response-2'}
      ]
    }));

    // when
    let responses: string[] = [];
    service.getBatch<string>(['/endpoint-1', '/endpoint-2', '/endpoint-3']).subscribe(r => responses = r);
    tick();

    // then
    expect(responses.length).toBe(3);
    expect(responses[0]).toBe('response-1');
    expect(responses[1]).toBe('response-2');
    expect(responses[2]).toBe('response-3');
  }));

  it('should use null as response for failed requests in batch request', fakeAsync(() => {
    // given
    integrationApiService.getToken.and.returnValue(of({token: 'TOKEN'}));
    httpClient.post.and.returnValue(of({
      responses: [{id: 1, status: 400, body: 'failing'}, {id: 2, status: 200, body: 'successful'}]
    }));

    // when
    let responses: string[] = [];
    service.getBatch<string>(['/endpoint-1', '/endpoint-2']).subscribe(r => responses = r);
    tick();

    // then
    expect(responses.length).toBe(2);
    expect(responses[0]).toBe(null);
    expect(responses[1]).toBe('successful');
  }));

  it('should not execute batch request if there are no given endpoints', fakeAsync(() => {
    // given
    integrationApiService.getToken.and.returnValue(of({token: 'TOKEN'}));

    // when
    service.getBatch<string>([]).subscribe();
    tick();

    // then
    expect(httpClient.post).toHaveBeenCalledTimes(0);
  }));
});
