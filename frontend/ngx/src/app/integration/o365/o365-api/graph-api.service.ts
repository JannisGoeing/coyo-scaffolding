import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {Observable, of} from 'rxjs';
import {flatMap, map} from 'rxjs/operators';

export type apiVersion = 'v1.0' | 'beta';

export const V1: apiVersion = 'v1.0';
export const BETA: apiVersion = 'beta';

/**
 * This service makes calls to the Microsoft GraphAPI
 */
@Injectable({
  providedIn: 'root'
})
export class GraphApiService {
  private static readonly GRAPH_URL: string = 'https://graph.microsoft.com/';

  constructor(private integrationApiService: IntegrationApiService, private httpClient: HttpClient) {
  }

  /**
   * Creates a GET request to the Microsoft GraphAPI and adds the needed headers
   * @param endpoint the endpoint of the GraphAPI (with leading slash)
   * @param version the version of the GraphAPI to query against
   * @param completeResult the flag to get complete response from Api
   * @returns an Observable with the data in the defined type
   */
  get<T>(endpoint: string, version: apiVersion = V1, completeResult: boolean = false): Observable<T> {
    return this.integrationApiService.getToken().pipe(
      flatMap(token => {
        const url = GraphApiService.GRAPH_URL + version + endpoint;
        const options = {headers: {Authorization: 'Bearer ' + token.token}};
        return this.httpClient.get<{ value: T }>(url, options);
      }),
      map((response: any) => completeResult ? response : (response.value ? response.value : response))
    );
  }

  /**
   * Creates batch request including multiple GET request to the Microsoft GraphAPI and adds the needed headers
   * @param endpoints the endpoints of the GraphAPI (with leading slash)
   * @param version the version of the GraphAPI to query against
   * @returns an Observable with the data in the defined type.
   *          Each result has the same index as the corresponding endpoint.
   *          If one request failed, its response will be null.
   */
  getBatch<T>(endpoints: string[], version: apiVersion = V1): Observable<T[]> {
    if (endpoints.length === 0) {
      return of([]);
    }

    return this.integrationApiService.getToken().pipe(
      flatMap(token => {
        const url = GraphApiService.GRAPH_URL + version + '/$batch';
        const options = {headers: {Authorization: 'Bearer ' + token.token}};
        return this.httpClient.post<BatchResponse<T>>(url, {
          requests: endpoints.map((endpoint, index) => ({
            id: index,
            method: 'GET',
            url: endpoint
          }))
        }, options);
      }),
      map(batchResponse => this.sortResponsesById(batchResponse.responses)),
      map(responses => responses.map(response => this.isSuccessful(response) ? response.body : null))
    );
  }

  private sortResponsesById<T>(responses: Response<T>[]): Response<T>[] {
    return responses.sort((a, b) => parseInt(a.id, 10) - parseInt(b.id, 10));
  }

  private isSuccessful<T>(response: Response<T>): boolean {
    return response.status >= 200 && response.status < 400;
  }
}

interface BatchResponse<T> {
  responses: Response<T>[];
}

interface Response<T> {
  id: string;
  status: number;
  body: T;
}
