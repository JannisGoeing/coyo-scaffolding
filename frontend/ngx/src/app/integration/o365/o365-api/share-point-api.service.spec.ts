import {HttpClient} from '@angular/common/http';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SharePointSearchRow} from '@app/integration/o365/o365-api/domain/share-point-search-row';
import {SharePointApiService} from '@app/integration/o365/o365-api/share-point-api.service';
import {of} from 'rxjs';

describe('SharePointApiService', () => {
  let service: SharePointApiService;
  let httpClient: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: HttpClient,
        useValue: jasmine.createSpyObj('HttpClient', ['get'])
      }]
    });
    service = TestBed.get(SharePointApiService);
    httpClient = TestBed.get(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add Authorization header in GET request', fakeAsync(() => {
    // given
    const expectedToken = 'TOKEN';
    let actualToken = null;
    httpClient.get.and.callFake((url: string, opts: { headers?: { 'Authorization': string } }) => {
      if (url === SharePointApiService.TOKEN_ENDPOINT) {
        return of({token: expectedToken});
      } else {
        actualToken = opts.headers.Authorization;
        return of({});
      }
    });

    // when
    service.get('https://xxx.sharepoint.com', '/endpoint').subscribe();

    // then
    expect(actualToken).toEqual('Bearer ' + expectedToken);
  }));

  it('should extract rows from response', fakeAsync(() => {
    // given
    const expectedRows: SharePointSearchRow[] = [{Cells: [{Key: 'key', Value: 'value'}]}];
    httpClient.get.and.callFake((url: string, opts: { headers?: { 'Authorization': string } }) => {
      if (url === SharePointApiService.TOKEN_ENDPOINT) {
        return of({token: 'TOKEN'});
      } else {
        return of({PrimaryQueryResult: {RelevantResults: {Table: {Rows: expectedRows}}}});
      }
    });

    // when
    let actualRows: SharePointSearchRow[] = [];
    service.get('https://xxx.sharepoint.com', '/endpoint').subscribe(r => actualRows = r);
    tick();

    // then
    expect(actualRows).toEqual(expectedRows);
  }));
});
