import {ComponentFixture, TestBed} from '@angular/core/testing';
import {TermsViewComponent} from '@app/engage/terms/terms-view/terms-view.component';
import {Terms} from '@domain/terms/terms';
import {TermsService} from '@domain/terms/terms.service';
import {of} from 'rxjs';

describe('TermsViewComponent', () => {
  let component: TermsViewComponent;
  let fixture: ComponentFixture<TermsViewComponent>;
  let termsService: jasmine.SpyObj<TermsService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TermsViewComponent],
      providers: [{
        provide: TermsService,
        useValue: jasmine.createSpyObj('TermsService', ['getBestSuitable'])
      }]
    }).overrideTemplate(TermsViewComponent, '')
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsViewComponent);
    component = fixture.componentInstance;
    termsService = TestBed.get(TermsService);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should load best suitable terms of use', () => {
    const terms: Terms = {
      title: 'Thou shall not pass!',
      text: '...until you sold your soul to me'
    };
    termsService.getBestSuitable.and.returnValue(of(terms));

    component.ngOnInit();

    component.terms$.subscribe(actualTerms => expect(actualTerms).toBe(terms));
  });
});
