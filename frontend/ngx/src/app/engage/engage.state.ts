import {LocalStorageService} from '@core/storage/local-storage/local-storage.service';
import {NgHybridStateDeclaration} from '@uirouter/angular-hybrid';
import {Transition} from '@uirouter/core';

// Do not change the state names
export const STATE_NAME_ENGAGE = 'engage';
export const STATE_NAME_ENGAGE_INIT = STATE_NAME_ENGAGE + '.init';

setLocalStorage.$inject = ['$transition$'];

/**
 * Setting up the local storage because the ios webview sometimes loses it on new instances.
 *
 * @param $transition$ the transition information
 */
export function setLocalStorage($transition$: Transition): void {
  const localStorageService: LocalStorageService = $transition$.injector().get(LocalStorageService);
  if ($transition$.params().userId && $transition$.params().clientId && $transition$.params().backendUrl) {
    localStorageService.setValue('userId', $transition$.params().userId);
    localStorageService.setValue('clientId', $transition$.params().clientId);
    localStorageService.setValue('isAuthenticated', true);
    localStorageService.setValue('backendUrl', decodeURIComponent($transition$.params().backendUrl));
  }
}

export const engageState: NgHybridStateDeclaration = {
  name: STATE_NAME_ENGAGE,
  url: '/engage?clientId&userId&backendUrl', // Do not change the URL
  data: {},
  params: {
    clientId: {type: 'query'},
    userId: {type: 'query'},
    backendUrl: {type: 'query'}
  },
  onEnter: setLocalStorage
};

export const engageStateInit: NgHybridStateDeclaration = {
  name: STATE_NAME_ENGAGE_INIT,
  url: '/init' // Do not change the URL
};
