import {Widget} from '@domain/widget/widget';
import {UpcomingEventsWidgetSettings} from './upcoming-events-widget-settings.model';

export interface UpcomingEventsWidget extends Widget<UpcomingEventsWidgetSettings> {
}
