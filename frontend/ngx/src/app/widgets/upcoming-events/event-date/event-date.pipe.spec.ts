import {SenderEvent} from '@domain/event/SenderEvent';
import {EventDatePipe} from './event-date.pipe';

describe('EventDatePipe', () => {
  let pipe: EventDatePipe;

  beforeEach(() => {
    pipe = new EventDatePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform event (sameDay)', () => {
    const event = buildEvent('2020-01-27T09:00:00', '2020-01-27T11:00:00');
    expect(pipe.transform(event)).toEqual('Mo. 27.1.20');
  });

  it('transform event (not sameDay)', () => {
    const event = buildEvent('2020-01-27T09:00:00', '2020-01-28T11:00:00');
    expect(pipe.transform(event)).toEqual('Mo. 27.1.20 – Tu. 28.1.20');
  });

  function buildEvent(startDate: string, endDate: string): SenderEvent {
    return {startDate, endDate} as unknown as SenderEvent;
  }
});
