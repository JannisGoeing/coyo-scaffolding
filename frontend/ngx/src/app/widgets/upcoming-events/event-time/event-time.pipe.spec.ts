import {SenderEvent} from '@domain/event/SenderEvent';
import {EventTimePipe} from './event-time.pipe';

describe('EventTimePipe', () => {
  let pipe: EventTimePipe;

  beforeEach(() => {
    pipe = new EventTimePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform event (sameDay)', () => {
    const event = buildEvent('2020-01-27T09:00:00', '2020-01-27T11:00:00');
    expect(pipe.transform(event)).toEqual('9:00 AM – 11:00 AM');
  });

  it('transform event (sameDay, sameTime)', () => {
    const event = buildEvent('2020-01-27T09:00:00', '2020-01-27T09:00:00');
    expect(pipe.transform(event)).toEqual('9:00 AM');
  });

  it('transform event (not sameDay, sameTime)', () => {
    const event = buildEvent('2020-01-27T09:00:00', '2020-01-28T09:00:00');
    expect(pipe.transform(event)).toEqual('9:00 AM – 9:00 AM');
  });

  it('transform event (not sameDay, not sameTime)', () => {
    const event = buildEvent('2020-01-27T09:00:00', '2020-01-28T11:00:00');
    expect(pipe.transform(event)).toEqual('9:00 AM – 11:00 AM');
  });

  function buildEvent(startDate: string, endDate: string): SenderEvent {
    return {startDate, endDate} as unknown as SenderEvent;
  }
});
