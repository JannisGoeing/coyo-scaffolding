import {SimpleChange} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TargetService} from '@domain/sender/target/target.service';
import {Store} from '@ngxs/store';
import {WikiArticleWidget} from '@widgets/wiki-article/wiki-article-widget';
import {Init} from '@widgets/wiki-article/wiki-article-widget/wiki-article-widget.actions';
import {WikiArticleWidgetComponent} from './wiki-article-widget.component';

describe('WikiArticleWidgetComponent', () => {
  let component: WikiArticleWidgetComponent;
  let fixture: ComponentFixture<WikiArticleWidgetComponent>;
  let store: jasmine.SpyObj<Store>;
  let targetService: jasmine.SpyObj<TargetService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WikiArticleWidgetComponent],
      providers: [{
        provide: Store, useValue: jasmine.createSpyObj('store', ['select', 'dispatch'])
      }, {
        provide: TargetService, useValue: jasmine.createSpyObj('targetService', ['getLinkTo'])
      }]
    }).overrideTemplate(WikiArticleWidgetComponent, '')
      .compileComponents();

    targetService = TestBed.get(TargetService);
    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WikiArticleWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'id',
      settings: {
        _articleId: 'articleId'
      }
    } as WikiArticleWidget;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(store.dispatch).toHaveBeenCalledWith(new Init(component.widget.id, component.widget.settings._articleId));
    expect(store.select).toHaveBeenCalled();
  });

  it('should reinit on changed widget', () => {
    // given
    store.dispatch.calls.reset();
    const oldWidget = component.widget;
    const newWidget = {id: 'id', settings: {_articleId: 'new'}} as WikiArticleWidget;
    component.widget = newWidget;

    // when
    component.ngOnChanges({widget: {currentValue: newWidget, previousValue: oldWidget} as SimpleChange});

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new Init(component.widget.id, component.widget.settings._articleId));
  });

  it('should not reinit when article id stays the same', () => {
    // given
    store.dispatch.calls.reset();
    const newWidget = {id: 'id', settings: {_articleId: 'articleId'}} as WikiArticleWidget;
    component.widget = newWidget;

    // when
    fixture.detectChanges();

    // then
    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it('should get link to target', () => {
    // given
    const target = {
      name: 'test-target',
      params: {}
    };

    const link = 'link';

    targetService.getLinkTo.and.returnValue(link);

    // when
    const result = component.getArticleLink(target);

    // then
    expect(result).toBe(link);
  });
});
