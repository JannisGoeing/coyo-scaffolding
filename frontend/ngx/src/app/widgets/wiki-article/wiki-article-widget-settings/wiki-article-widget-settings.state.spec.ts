import {async, TestBed} from '@angular/core/testing';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {NgxsModule, Store} from '@ngxs/store';
import {WidgetWikiArticleResponse} from '@widgets/wiki-article/widget-wiki-article-response';
import {
  Init,
  LoadMore,
  Search
} from '@widgets/wiki-article/wiki-article-widget-settings/wiki-article-widget-settings.actions';
import {
  WikiArticleWidgetSettingsState,
  WikiArticleWidgetSettingsStateModel
} from '@widgets/wiki-article/wiki-article-widget-settings/wiki-article-widget-settings.state';
import {WikiArticleWidgetService} from '@widgets/wiki-article/wiki-article-widget/wiki-article-widget.service';
import {of} from 'rxjs';

describe('WikiArticleWidgetSettingsStore', () => {
  let store: Store;
  let wikiService: jasmine.SpyObj<WikiArticleWidgetService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([WikiArticleWidgetSettingsState])],
      providers: [{
        provide: WikiArticleWidgetService, useValue: jasmine.createSpyObj('wikiArticleWidgetService', ['getWikiArticle',
          'getWikiArticlePage'])
      }]
    }).compileComponents();

    store = TestBed.get(Store);
    wikiService = TestBed.get(WikiArticleWidgetService);
  }));

  it('should init the settings with the used article', () => {
    // given
    const id = 'article-id';
    const article = {
      title: 'test'
    } as WidgetWikiArticleResponse;

    wikiService.getWikiArticle.and.returnValue(of(article));

    // when
    store.dispatch(new Init(id));

    // then
    const state: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state.initialized).toBeTruthy();
    expect(state.articles.length).toBe(1);
    expect(state.articles[0]).toBe(article);
    expect(wikiService.getWikiArticle).toHaveBeenCalledWith(id);
  });

  it('should load more wiki articles', () => {
    // given
    const articlePage = {
      content: [{id: 'id1'}, {id: 'id2'}],
      number: 0
    } as unknown as Page<WidgetWikiArticleResponse>;

    const articlePage2 = {
      content: [{id: 'id3'}, {id: 'id4'}],
      number: 1
    } as unknown as Page<WidgetWikiArticleResponse>;

    wikiService.getWikiArticlePage.and.returnValues(of(articlePage), of(articlePage2));

    store.reset({wikiArticleWidgetSettingsState: {initialized: true}});

    // when
    store.dispatch(new LoadMore());

    // then
    const state: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state.page).toBe(articlePage);
    expect(state.articles).toEqual(articlePage.content);
    expect(state.loading).toBeFalsy();

    // when again
    store.dispatch(new LoadMore());

    // then again
    const state2: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state2.page).toBe(articlePage2);
    expect(state2.articles).toEqual([...articlePage.content, ...articlePage2.content]);
    expect(state2.loading).toBeFalsy();
  });

  it('should not load more articles if not initialized', () => {
    // when
    store.dispatch(new LoadMore());

    // then
    const state: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state).toEqual({
      articles: null,
      page: null,
      searchTerm: '',
      loading: false,
      initialized: false
    });
    expect(wikiService.getWikiArticlePage).not.toHaveBeenCalled();
  });

  it('should not load more articles if loading', () => {
    // given
    store.reset({wikiArticleWidgetSettingsState: {loading: true}});

    // when
    store.dispatch(new LoadMore());

    // then
    const state: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state.loading).toBeTruthy();
    expect(wikiService.getWikiArticlePage).not.toHaveBeenCalled();
  });

  it('should search for articles', () => {
    // given
    const term = 'term';
    const articlePage = {
      content: [{id: 'id1'}, {id: 'id2'}],
      number: 0
    } as unknown as Page<WidgetWikiArticleResponse>;

    wikiService.getWikiArticlePage.and.returnValues(of(articlePage));

    store.reset({wikiArticleWidgetSettingsState: {initialized: true}});

    // when
    store.dispatch(new Search(term));

    // then
    const state: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state.page).toBe(articlePage);
    expect(state.articles).toEqual(articlePage.content);
    expect(state.loading).toBeFalsy();
    expect(state.searchTerm).toBe(term);
    expect(wikiService.getWikiArticlePage).toHaveBeenCalledWith(new Pageable(), term);
  });

  it('should not search for articles if not initialized', () => {
    // when
    store.dispatch(new Search('term'));

    // then
    const state: WikiArticleWidgetSettingsStateModel =
      store.selectSnapshot(snapshot => snapshot.wikiArticleWidgetSettingsState);
    expect(state).toEqual({
      articles: null,
      page: null,
      searchTerm: '',
      loading: false,
      initialized: false
    });
    expect(wikiService.getWikiArticlePage).not.toHaveBeenCalled();
  });
});
