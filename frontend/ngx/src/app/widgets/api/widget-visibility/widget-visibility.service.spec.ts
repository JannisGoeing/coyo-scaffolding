import {TestBed} from '@angular/core/testing';
import {skip} from 'rxjs/operators';
import {WidgetVisibilityService} from './widget-visibility.service';

describe('WidgetVisibilityService', () => {
  let service: WidgetVisibilityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetVisibilityService]
    });

    service = TestBed.get(WidgetVisibilityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get and set IDs', done => {
    // given
    service.setHidden('widget-id1', true);
    service.getHiddenIds$()
      .pipe(skip(1))
      .subscribe(ids => {
        expect(ids).toEqual(['widget-id1', 'widget-id2']);
        done();
      });

    // when
    service.setHidden('widget-id2', true);
  });
});
