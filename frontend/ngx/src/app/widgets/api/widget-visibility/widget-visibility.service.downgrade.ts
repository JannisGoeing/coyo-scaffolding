import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {WidgetVisibilityService} from '@widgets/api/widget-visibility/widget-visibility.service';

getAngularJSGlobal()
  .module('coyo.widgets.api')
  .factory('ngxWidgetVisibilityService', downgradeInjectable(WidgetVisibilityService) as any);
