import {ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {WidgetComponent} from '../widget-component';
import {WidgetCategory} from '../widget-config';
import {WidgetContainerComponent} from './widget-container.component';

class DummyWidgetComponent extends WidgetComponent<Widget<WidgetSettings>> {
}

describe('WidgetContainerComponent', () => {
  let component: WidgetContainerComponent;
  let fixture: ComponentFixture<WidgetContainerComponent>;
  let componentFactoryResolver: jasmine.SpyObj<ComponentFactoryResolver>;
  let viewContainerRef: jasmine.SpyObj<ViewContainerRef>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetContainerComponent],
      providers: [{
        provide: ComponentFactoryResolver,
        useValue: jasmine.createSpyObj('ComponentFactoryResolver', ['resolveComponentFactory'])
      }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetContainerComponent);
    component = fixture.componentInstance;
    componentFactoryResolver = fixture.debugElement.injector.get(ComponentFactoryResolver); // tslint:disable-line:deprecation
    viewContainerRef = fixture.debugElement.injector.get(ViewContainerRef); // tslint:disable-line:deprecation
  });

  it('should dynamically create a component', () => {
    spyOn(viewContainerRef, 'createComponent');

    // given
    const componentFactory = {};
    componentFactoryResolver.resolveComponentFactory.and.returnValue(componentFactory);
    const componentRef = {instance: jasmine.createSpyObj('instance', ['detectChanges'])};
    viewContainerRef.createComponent.and.returnValue(componentRef);

    component.config = {
      key: 'dummy',
      component: DummyWidgetComponent,
      icon: '',
      name: '',
      description: '',
      categories: WidgetCategory.STATIC
    };
    component.widget = {};
    component.editMode = false;
    fixture.detectChanges();

    // when
    component.ngOnInit();

    // then
    expect(componentFactoryResolver.resolveComponentFactory).toHaveBeenCalledWith(component.config.component);
    expect(viewContainerRef.createComponent).toHaveBeenCalledWith(componentFactory);
    expect(component['componentRef'].instance.config).toEqual(component.config);
    expect(component['componentRef'].instance.widget).toEqual(component.widget);
    expect(component['componentRef'].instance.editMode).toEqual(component.editMode);
  });
});
