import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Ng1ScrollBehaviourService} from '@root/typings';
import {UIRouter} from '@uirouter/core';
import {NG1_SCROLL_BEHAVIOR_SERVICE} from '@upgrade/upgrade.module';
import {of} from 'rxjs';
import {WidgetSettingsModalComponent} from './widget-settings-modal.component';

describe('WidgetSettingsModalComponent', () => {
  let component: WidgetSettingsModalComponent;
  let fixture: ComponentFixture<WidgetSettingsModalComponent>;
  let scrollBehaviourService: jasmine.SpyObj<Ng1ScrollBehaviourService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WidgetSettingsModalComponent],
      providers: [{
        provide: MatDialogRef,
        useValue: jasmine.createSpyObj('dialogRef', ['close'])
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }, {
        provide: UIRouter, useValue: {globals: {start$: of()}}
      }, {
        provide: NG1_SCROLL_BEHAVIOR_SERVICE, useValue: jasmine.createSpyObj('scrollBehaviourService',
          ['enableBodyScrolling', 'disableBodyScrolling', 'disableBodyScrollingOnXsScreen'])
      }]
    }).overrideTemplate(WidgetSettingsModalComponent, '<div></div>')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WidgetSettingsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    scrollBehaviourService = TestBed.get(NG1_SCROLL_BEHAVIOR_SERVICE);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
