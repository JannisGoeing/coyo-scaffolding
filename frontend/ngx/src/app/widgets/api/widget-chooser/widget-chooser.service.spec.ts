import {Overlay, ScrollStrategyOptions} from '@angular/cdk/overlay';
import {async, inject, TestBed} from '@angular/core/testing';
import {MatDialog} from '@angular/material/dialog';
import {MatDialogSize} from '@coyo/ui';
import {Subject} from 'rxjs';
import {WidgetChooserComponent} from './widget-chooser.component';
import {WidgetChooserService} from './widget-chooser.service';

describe('WidgetChooserService', () => {
  let dialogService: jasmine.SpyObj<MatDialog>;
  let scrollStategies: jasmine.SpyObj<ScrollStrategyOptions>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WidgetChooserService, {
        provide: MatDialog,
        useValue: jasmine.createSpyObj('MatDialog', ['open'])
      }, {
        provide: Overlay,
        useValue: {
          scrollStrategies: jasmine.createSpyObj('scrollStrategies', ['noop'])
        }
      }]
    });

    dialogService = TestBed.get(MatDialog);
    scrollStategies = TestBed.get(Overlay).scrollStrategies;
  });

  it('should open a modal', async((done: () => void) =>
    inject([WidgetChooserService], (service: WidgetChooserService) => {
      // given
      const subject = new Subject<any>();
      const dialogRef = jasmine.createSpyObj('dialogRef', ['afterClosed']);
      dialogRef.afterClosed.and.returnValue(subject);
      dialogService.open.and.returnValue(dialogRef);

      // when
      service.open().then(result => {
        // then
        expect(result).toEqual('result');
        expect(JSON.stringify(dialogService.open.calls.mostRecent().args)).toEqual(JSON.stringify([WidgetChooserComponent, {
          width: MatDialogSize.Medium,
          data: {
            widget: {
              isNew: () => true, settings: {}
            }
          }
        }]));
        expect(scrollStategies.noop).toHaveBeenCalled();
        done();
      });
      subject.next('result');
    })));
});
