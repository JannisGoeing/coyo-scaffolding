import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {App} from '@domain/apps/app';
import {Document} from '@domain/file/document';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {CropSettings, Ng1FileLibraryModalOptions, Ng1FileLibraryModalService} from '@root/typings';
import {NG1_FILE_LIBRARY_MODAL_SERVICE} from '@upgrade/upgrade.module';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {ImageWidget} from '@widgets/image/image-widget';
import {of} from 'rxjs';
import {ImageWidgetSettingsComponent} from './image-widget-settings.component';

describe('ImageWidgetSettingsComponent', () => {
  let component: ImageWidgetSettingsComponent;
  let fixture: ComponentFixture<ImageWidgetSettingsComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let senderService: jasmine.SpyObj<SenderService>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  let fileLibraryModalService: jasmine.SpyObj<Ng1FileLibraryModalService>;
  const user = {id: 'user-id'} as Sender;
  const sender = {id: 'sender-id'} as Sender;
  const senderIdOrSlug = 'sender-slug';
  const app = {rootFolderId: 'root-folder-id'} as App;
  const fileLibraryResult = {id: 'document-id', senderId: 'sender-id'} as Document;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [ImageWidgetSettingsComponent],
        providers: [{
          provide: AuthService,
          useValue: jasmine.createSpyObj('AuthService', ['getUser'])
        }, {
          provide: SenderService,
          useValue: jasmine.createSpyObj('SenderService', ['getCurrentIdOrSlug', 'get', 'getCurrentApp'])
        }, {
          provide: WidgetSettingsService,
          useValue: jasmine.createSpyObj('WidgetSettingsService', ['propagateChanges'])
        }, {
          provide: NG1_FILE_LIBRARY_MODAL_SERVICE,
          useValue: jasmine.createSpyObj('Ng1FileLibraryModalService', ['open'])
        }]
      })
      .overrideTemplate(ImageWidgetSettingsComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    senderService = TestBed.get(SenderService);
    widgetSettingsService = TestBed.get(WidgetSettingsService);
    fileLibraryModalService = TestBed.get(NG1_FILE_LIBRARY_MODAL_SERVICE);
  }));

  beforeEach(() => {
    authService.getUser.and.returnValue(of(user));
    senderService.getCurrentIdOrSlug.and.returnValue(senderIdOrSlug);
    senderService.get.and.returnValue(of(sender));
    senderService.getCurrentApp.and.returnValue(of(app));
    fileLibraryModalService.open.and.returnValue(Promise.resolve(fileLibraryResult));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id'
    } as ImageWidget;
  });

  afterEach( () => {
    component.ngOnDestroy();
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', fakeAsync(() => {
    // given
    const options: Ng1FileLibraryModalOptions = {
      uploadMultiple: false,
      selectMode: 'single',
      filterContentType: 'image',
      initialFolder: {id: app.rootFolderId}
    };
    const cropSettings: CropSettings = {
      cropImage: true
    };

    // when
    fixture.detectChanges();
    tick();

    // then
    component.state$.subscribe(state => {
      expect(state.sender).toBe(sender);
      expect(state.app).toBe(app);
    });
    expect(senderService.get).toHaveBeenCalledWith(senderIdOrSlug, {permissions: ['createFile']});
    expect(fileLibraryModalService.open).toHaveBeenCalledWith(sender, options, cropSettings);
    expect(widgetSettingsService.propagateChanges).toHaveBeenCalled();
  }));

  it('should call the file library modal service', () => {
    // given
    const options: Ng1FileLibraryModalOptions = {
      uploadMultiple: false,
      selectMode: 'single',
      filterContentType: 'image',
      initialFolder: {id: app.rootFolderId}
    };
    const cropSettings: CropSettings = {
      cropImage: true
    };
    fixture.detectChanges();

    // when
    component.selectImage();

    // then
    expect(fileLibraryModalService.open).toHaveBeenCalledTimes(2);
    expect(fileLibraryModalService.open).toHaveBeenCalledWith(sender, options, cropSettings);
  });

  it('should not open file library if image already set', () => {
    // given
    component.widget = {
      id: 'widget-id',
      settings: {
        _image: fileLibraryResult
      }
    } as ImageWidget;

    // when
    fixture.detectChanges();

    // then
    expect(fileLibraryModalService.open).toHaveBeenCalledTimes(0);
  });

  it('should open file library without app', () => {
    // given
    senderService.getCurrentApp.and.returnValue(of());

    // when
    fixture.detectChanges();

    // then
    expect(fileLibraryModalService.open).toHaveBeenCalled();
  });

});
