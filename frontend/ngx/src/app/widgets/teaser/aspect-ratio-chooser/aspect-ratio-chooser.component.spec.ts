import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {AspectRatioChooserComponent} from './aspect-ratio-chooser.component';

describe('AspectRatioChooserComponent', () => {
  let component: AspectRatioChooserComponent;
  let fixture: ComponentFixture<AspectRatioChooserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AspectRatioChooserComponent],
      providers: [{
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }]
    })
      .overrideTemplate(AspectRatioChooserComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AspectRatioChooserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should write value', () => {
    // given
    const newValue = 'newValue';
    component.value = 'oldValue';

    // when
    component.writeValue(newValue);

    // then
    expect(component.value).toBe(newValue);
  });

  it('should select an option', () => {
    // given
    const newValue = 'newValue';
    component.value = 'oldValue';
    const spy = jasmine.createSpy();
    component.registerOnChange(spy);

    // when
    component.selectOption(newValue);

    // then
    expect(component.value).toBe(newValue);
    expect(spy).toHaveBeenCalledWith(newValue);
  });
});
