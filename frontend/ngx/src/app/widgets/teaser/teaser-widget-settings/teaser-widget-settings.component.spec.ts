import {CdkDragSortEvent} from '@angular/cdk/drag-drop';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormArray, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from '@core/auth/auth.service';
import {MatDialogSize} from '@coyo/ui';
import {App} from '@domain/apps/app';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {TranslateService} from '@ngx-translate/core';
import {WidgetSettingsService} from '@widgets/api/widget-settings/widget-settings.service';
import {NewSlideDialogComponent} from '@widgets/teaser/new-slide-dialog/new-slide-dialog.component';
import {TeaserWidget} from '@widgets/teaser/teaser-widget';
import * as _ from 'lodash';
import {of} from 'rxjs';
import {TeaserWidgetSettingsComponent} from './teaser-widget-settings.component';

describe('TeaserWidgetSettingsComponent', () => {
  let component: TeaserWidgetSettingsComponent;
  let fixture: ComponentFixture<TeaserWidgetSettingsComponent>;
  let authService: jasmine.SpyObj<AuthService>;
  let senderService: jasmine.SpyObj<SenderService>;
  let widgetSettingsService: jasmine.SpyObj<WidgetSettingsService>;
  let dialog: jasmine.SpyObj<MatDialog>;
  let dialogRef: jasmine.SpyObj<MatDialogRef<TeaserWidgetSettingsComponent>>;
  const user = {id: 'user-id'} as Sender;
  const sender = {id: 'sender-id'} as Sender;
  const senderIdOrSlug = 'sender-slug';
  const app = {rootFolderId: 'root-folder-id'} as App;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [TeaserWidgetSettingsComponent],
        providers: [{
          provide: AuthService,
          useValue: jasmine.createSpyObj('AuthService', ['getUser'])
        }, {
          provide: SenderService,
          useValue: jasmine.createSpyObj('SenderService', ['getCurrentIdOrSlug', 'get', 'getCurrentApp'])
        }, {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }, {
          provide: MatDialogRef,
          useValue: jasmine.createSpyObj('ref', ['addPanelClass', 'removePanelClass'])
        }, {
          provide: MatDialog,
          useValue: jasmine.createSpyObj('MatDialog', ['open'])
        }, {
          provide: WidgetSettingsService,
          useValue: jasmine.createSpyObj('WidgetSettingsService', ['propagateChanges'])
        }, {
          provide: TranslateService,
          useValue: jasmine.createSpyObj('TranslateService', ['instant'])
        }]
      })
      .overrideTemplate(TeaserWidgetSettingsComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    senderService = TestBed.get(SenderService);
    widgetSettingsService = TestBed.get(WidgetSettingsService);
    dialog = TestBed.get(MatDialog);
    dialogRef = TestBed.get(MatDialogRef);
  }));

  beforeEach(() => {
    authService.getUser.and.returnValue(of(user));
    senderService.getCurrentIdOrSlug.and.returnValue(senderIdOrSlug);
    senderService.get.and.returnValue(of(sender));
    senderService.getCurrentApp.and.returnValue(of(app));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeaserWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({});
    component.widget = {
      id: 'widget-id',
      settings: {}
    } as TeaserWidget;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect((component.parentForm.get('slides') as FormArray).length).toBe(3);
  });

  it('should add a slide', () => {
    // given
    const localDialogRef = jasmine.createSpyObj('dialogRef', ['afterClosed']);
    dialog.open.and.returnValue(localDialogRef);
    const newSlide = {
      _image: '',
      _url: '',
      headline: '',
      subheadline: '',
      _newTab: ''
    };
    localDialogRef.afterClosed.and.returnValue(of(newSlide));
    fixture.detectChanges();

    // when
    component.addSlide();

    // then
    expect((component.parentForm.get('slides') as FormArray).length).toBe(4);
    expect(dialog.open).toHaveBeenCalled();
    expect(dialog.open.calls.argsFor(0)).toEqual([NewSlideDialogComponent, {
      width: MatDialogSize.Medium,
      data: {}
    }]);
    expect(localDialogRef.afterClosed).toHaveBeenCalled();
    expect(dialogRef.addPanelClass).toHaveBeenCalledWith('ng-hide');
    expect(dialogRef.removePanelClass).toHaveBeenCalledWith('ng-hide');
  });

  it('should edit a slide', () => {
    // given
    const editIndex = 1;
    const localDialogRef = jasmine.createSpyObj('dialogRef', ['afterClosed']);
    dialog.open.and.returnValue(localDialogRef);
    const newSlide = {
      _image: '',
      _url: '',
      headline: '',
      subheadline: '',
      _newTab: ''
    };
    localDialogRef.afterClosed.and.returnValue(of(newSlide));
    fixture.detectChanges();
    const oldData = _.cloneDeep((component.parentForm.get('slides') as FormArray).at(editIndex).value);

    // when
    component.editSlide(editIndex);

    // then
    expect((component.parentForm.get('slides') as FormArray).length).toBe(3);
    expect(dialog.open).toHaveBeenCalled();
    expect(dialog.open.calls.argsFor(0)).toEqual([NewSlideDialogComponent, {
      width: MatDialogSize.Medium,
      hasBackdrop: false,
      data: oldData
    }]);
    expect(localDialogRef.afterClosed).toHaveBeenCalled();
    expect(dialogRef.addPanelClass).toHaveBeenCalledWith('ng-hide');
    expect(dialogRef.removePanelClass).toHaveBeenCalledWith('ng-hide');
    expect((component.parentForm.get('slides') as FormArray).at(editIndex).value).toEqual(newSlide);
  });

  it('should delete a slide', () => {
    // given
    fixture.detectChanges();

    // when
    component.deleteSlide(0);

    // then
    expect((component.parentForm.get('slides') as FormArray).length).toBe(2);
  });

  it('should insert correctly when dropping a slide', () => {
    // given
    fixture.detectChanges();
    const firstSlideTitle = component.getSlides().at(0).value.headline;

    // when
    component.dropped({
      previousIndex: 0,
      currentIndex: 1
    } as CdkDragSortEvent);

    // then
    expect(component.getSlides().at(1).value.headline).toEqual(firstSlideTitle);
  });
});
