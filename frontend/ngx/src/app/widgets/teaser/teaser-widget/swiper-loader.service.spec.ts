import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {SwiperLoaderService} from './swiper-loader.service';

describe('SwiperLoaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SwiperLoaderService = TestBed.get(SwiperLoaderService);
    expect(service).toBeTruthy();
  });
});
