import {Widget} from '@domain/widget/widget';
import {RssWidgetSettings} from '@widgets/rss/rss-widget-settings.model';

export interface RssWidget extends Widget<RssWidgetSettings> {
}
