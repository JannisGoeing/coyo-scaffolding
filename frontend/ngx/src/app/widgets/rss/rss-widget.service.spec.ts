import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {RssEntry} from '@widgets/rss/rss-entry.model';
import {RssWidgetVerifyUrlResponse} from '@widgets/rss/rss-widget-verify-url-response.model';
import {RssWidgetService} from '@widgets/rss/rss-widget.service';
import * as _ from 'lodash';

describe('RsssWidgetService', () => {
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [RssWidgetService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }]
    });
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', inject([RssWidgetService], (service: RssWidgetService) => {
    expect(service).toBeTruthy();
  }));

  it('should get RSS feed entries', inject([RssWidgetService], (service: RssWidgetService) => {
    // Given
    const widgetId = 'wiget-id';
    const rssEntry1: RssEntry = {
      title: 'title-1', description: 'description-1', publishedDate: new Date('2019-11-12T12:00:00'),
      feedUri: 'http://rssfeed.uri', imageUrl: 'http://some.site/image1.jpg'
    };
    const rssEntry2: RssEntry = {
      title: 'title-2', description: 'description-2', publishedDate: new Date('2019-11-11T12:00:00'),
      feedUri: 'http://rssfeed.uri', imageUrl: 'http://some.site/image2.jpg'
    };
    service.getEntries(widgetId)
      .subscribe(response => expect(response).toEqual([rssEntry1, rssEntry2]));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'GET'
        && req.url === '/web/widgets/rss/' + widgetId + '/entries'
    );

    // Finally
    request.flush([rssEntry1, rssEntry2]);
    httpTestingController.verify();
  }));

  it('should verify Url', inject([RssWidgetService], (service: RssWidgetService) => {
    // Given
    const rssUrl = 'http://some-test/rss';
    const userName = 'testUserName';
    const password = 'secret';

    const urlValidationResponse: RssWidgetVerifyUrlResponse = {
      validationMessage: 'validUrl',
      valid: true
    };

    service.verifyUrl(rssUrl, userName, password)
      .subscribe(response => expect(response).toEqual(urlValidationResponse));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'POST'
        && req.url === '/web/widgets/rss/verify'
        && req.body['rssUrl'] === rssUrl
        && req.body['userName'] === userName
        && req.body['password'] === password
    );

    request.flush(urlValidationResponse);
    httpTestingController.verify();
  }));

  it('should get entries from preview', inject([RssWidgetService], (service: RssWidgetService) => {
    // Given
    const rssUrl = 'http://some-test/rss';
    const withImages = true;
    const size = 2;
    const userName = 'testUserName';
    const password = 'secret';

    const rssEntry1: RssEntry = {
      title: 'title-2', description: 'description-2', publishedDate: new Date('2019-11-11T12:00:00'),
      feedUri: 'http://rssfeed.uri', imageUrl: 'http://some.site/image2.jpg'
    };

    service.getEntriesForPreview(rssUrl, withImages, size, userName, password)
      .subscribe(response => expect(response).toEqual([rssEntry1]));

    // Then
    const request = httpTestingController.expectOne(
      req => req.method === 'GET'
        && req.url === '/web/widgets/rss/entries'
        && req.params.get('url') === rssUrl
        && req.params.get('images') === 'true'
        && req.params.get('size') === '2'
        && req.params.get('userName') === userName
        && req.params.get('password') === password
    );

    request.flush([rssEntry1]);
    httpTestingController.verify();
  }));
});
