import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {RssEntry} from '@widgets/rss/rss-entry.model';
import {RssWidget} from '@widgets/rss/rss-widget';
import {RssWidgetService} from '@widgets/rss/rss-widget.service';
import {RssWidgetComponent} from '@widgets/rss/rss-widget/rss-widget.component';
import {of} from 'rxjs';

describe('RssWidgetComponent', () => {

  const rssEntry: RssEntry = {
    title: 'title-1',
    description: 'description-1',
    publishedDate: new Date('2019-11-12T12:00:00'),
    feedUri: 'http://rssfeed.uri',
    imageUrl: 'http://some.site/image1.jpg'
  };
  const rssEntries = [rssEntry];

  let component: RssWidgetComponent;
  let fixture: ComponentFixture<RssWidgetComponent>;
  let rssWidgetService: jasmine.SpyObj<RssWidgetService>;

  let widget: RssWidget;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [RssWidgetComponent],
        providers: [ChangeDetectorRef, {
          provide: RssWidgetService,
          useValue: jasmine.createSpyObj('rssWidgetService', ['getEntries', 'getEntriesForPreview'])
        }]
      })
      .overrideTemplate(RssWidgetComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    widget = {
      id: 'widget-id',
      settings: {
        rssUrl: 'https://some.site/rss/feed',
        userName: 'John Doe',
        _encrypted_password: 'secret',
        maxCount: 3,
        displayImage: true
      }
    } as RssWidget;

    fixture = TestBed.createComponent(RssWidgetComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    rssWidgetService = TestBed.get(RssWidgetService);
    rssWidgetService.getEntries.and.returnValue(of(rssEntries));
    rssWidgetService.getEntriesForPreview.and.returnValue(of(rssEntries));
    fixture.detectChanges();
  });

  it('should create', () => {
    // given

    // when

    // then
    expect(component).toBeTruthy();
  });

  it('should initially load the RSS entries', done => {
    // given

    // when
    component.entries$.subscribe(entries => {
      // then
      expect(entries).toBe(rssEntries);
      expect(rssWidgetService.getEntries).toHaveBeenCalledWith('widget-id');
      done();
    });
  });

  it('should load the preview RSS entries if no widget Id is given', done => {
    // given
    widget.id = undefined;

    // when
    component.entries$.subscribe(entries => {
      // then
      expect(entries).toBe(rssEntries);
      expectGetEntriesForPreviewCalled();
      done();
    });
  });

  it('should load the preview RSS entries if in edit mode', done => {
    // given
    component.editMode = true;

    // when
    component.entries$.subscribe(entries => {
      // then
      expect(entries).toBe(rssEntries);
      expectGetEntriesForPreviewCalled();
      done();
    });
  });

  it('should load entries on change', fakeAsync(() => {
    // given

    // when
    component.entries$.subscribe();
    component.ngOnChanges({});
    tick();

    // then
    expect(rssWidgetService.getEntries).toHaveBeenCalledTimes(2);
  }));

  function expectGetEntriesForPreviewCalled(): void {
    expect(rssWidgetService.getEntriesForPreview).toHaveBeenCalledWith(
      'https://some.site/rss/feed', true, 3, 'John Doe', 'secret');
    expect(rssWidgetService.getEntries).not.toHaveBeenCalled();
  }
});
