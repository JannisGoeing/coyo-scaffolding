import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormControl, FormGroup} from '@angular/forms';
import {AppService} from '@domain/apps/app.service';
import {LatestBlogArticlesWidget} from '@widgets/latest-blog-articles/latest-blog-articles-widget';
import {of, Subject} from 'rxjs';
import {LatestBlogArticlesWidgetSettingsComponent} from './latest-blog-articles-widget-settings.component';

describe('LatestBlogArticlesWidgetSettingsComponent', () => {
  let component: LatestBlogArticlesWidgetSettingsComponent;
  let fixture: ComponentFixture<LatestBlogArticlesWidgetSettingsComponent>;
  let appService: jasmine.SpyObj<AppService>;
  let formGroup: jasmine.SpyObj<FormGroup>;
  let onSourceSelection: Subject<string>;
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [LatestBlogArticlesWidgetSettingsComponent],
        providers: [{
          provide: AppService,
          useValue: jasmine.createSpyObj('AppService', ['get'])
        }]
      })
      .overrideTemplate(LatestBlogArticlesWidgetSettingsComponent, '')
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(LatestBlogArticlesWidgetSettingsComponent);
        component = fixture.componentInstance;
        component.widget = {
          id: 'widget-id',
          settings: {}
        } as LatestBlogArticlesWidget;
        onSourceSelection = new Subject<string>();
        formGroup = jasmine.createSpyObj('FormGroup', ['addControl', 'patchValue', 'get']);
        formGroup.get.and.returnValue({valueChanges: onSourceSelection});
        component.parentForm = formGroup;
        appService = TestBed.get(AppService);
        appService.get.and.returnValue(of('app'));
      });
  }));

  it('should init without settings', () => {
    // given

    // when
    fixture.detectChanges();
    // then
    expect(appService.get).not.toHaveBeenCalled();
    expect(formGroup.addControl).toHaveBeenCalledTimes(4);
  });

  it('should correctly set form values', () => {
    // given
    component.widget.settings = {
      _app: [{id: 'a', senderId: 'a'}, {id: 'b', senderId: 'b'}, {id: 'c', senderId: 'c'}],
      _sourceSelection: 'ALL',
      _articleCount: 3,
      _showTeaserImage: true
    };
    const addControlCalls = {};

    appService.get.and.callFake((id: string) => id);
    formGroup.addControl.and.callFake((name: string, formControl: FormControl) => {
      addControlCalls[name] = formControl.value;
    });
    formGroup.patchValue.and.callFake((value: { [name: string]: FormControl }) => {
      const keys = Object.keys(value);
      keys.forEach(key => {
        if (!!addControlCalls[key]) {
          addControlCalls[key] = value[key];
        }
      });
    });

    // when
    fixture.detectChanges();

    // then
    expect(component.parentForm.addControl).toHaveBeenCalledTimes(4);
    expect(component.parentForm.patchValue).toHaveBeenCalledTimes(1);
    expect(addControlCalls['apps']).toEqual(['a', 'b', 'c']);
    expect(addControlCalls['showTeaserImage']).toEqual(true);
    expect(addControlCalls['sourceSelection']).toEqual('ALL');
    expect(addControlCalls['articleCount']).toEqual(3);

  });

  it('should correctly save values', () => {
    // given
    const settings = {
      apps: [{id: 'a', senderId: 'a'}, {id: 'b', senderId: 'b'}, {id: 'c', senderId: 'c'}],
      sourceSelection: 'ALL',
      articleCount: 3,
      showTeaserImage: true
    };
    const settingsResult = component.widget.settings = {
      _app: [{id: 'a', senderId: 'a'}, {id: 'b', senderId: 'b'}, {id: 'c', senderId: 'c'}],
      _sourceSelection: 'ALL',
      _articleCount: 3,
      _showTeaserImage: true
    };
    // when
    fixture.detectChanges();
    component.onBeforeSave(settings).subscribe(result => {
      // then
      expect(result).toEqual(settingsResult);
    });
  });
});
