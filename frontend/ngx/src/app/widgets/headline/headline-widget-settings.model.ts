import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';
import {SizeOption} from '@widgets/headline/size-option';

/**
 * The entity model for the settings of a headline widget
 */
export interface HeadlineWidgetSettings extends WidgetSettings {
  text: string;
  _headline: SizeOption;
}
