import {Widget} from '@domain/widget/widget';
import {HeadlineWidgetSettings} from '@widgets/headline/headline-widget-settings.model';

export interface HeadlineWidget extends Widget<HeadlineWidgetSettings> {
}
