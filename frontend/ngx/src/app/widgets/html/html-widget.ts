import {Widget} from '@domain/widget/widget';
import {HtmlWidgetSettings} from '@widgets/html/html-widget-settings.model';

export interface HtmlWidget extends Widget<HtmlWidgetSettings> {
}
