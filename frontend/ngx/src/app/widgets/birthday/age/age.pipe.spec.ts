import {BirthdayService} from '@widgets/birthday/birthday/birthday.service';
import {AgePipe} from './age.pipe';

describe('AgePipe', () => {
  let pipe: AgePipe;
  let birthdayService: jasmine.SpyObj<BirthdayService>;

  beforeEach(() => {
    birthdayService = jasmine.createSpyObj('birthdayService', ['hasYear']);
    pipe = new AgePipe(birthdayService);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should transform a date to an age', () => {
    // given
    const birthDate = '2000-01-01';
    const refDate = new Date('2019-01-02');
    birthdayService.hasYear.and.returnValue(true);

    // when
    const result = pipe.transform(birthDate, refDate);

    // then
    expect(result).toBe(20);
    expect(birthdayService.hasYear).toHaveBeenCalledWith(birthDate);
  });

  it('should take month and day into account', () => {
    // given
    const birthDate = '2000-01-02';
    const refDate = new Date('2019-01-02');
    birthdayService.hasYear.and.returnValue(true);

    // when
    const result = pipe.transform(birthDate, refDate);

    // then
    expect(result).toBe(19);
    expect(birthdayService.hasYear).toHaveBeenCalledWith(birthDate);
  });

  it('should return null if no year is given', () => {
    // given
    const birthDate = '01-02';
    const refDate = new Date('2019-01-01');
    birthdayService.hasYear.and.returnValue(false);

    // when
    const result = pipe.transform(birthDate, refDate);

    // then
    expect(result).toBeNull();
    expect(birthdayService.hasYear).toHaveBeenCalledWith(birthDate);
  });
});
