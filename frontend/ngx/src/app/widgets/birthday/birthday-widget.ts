import {Widget} from '@domain/widget/widget';
import {BirthdayWidgetSettings} from '@widgets/birthday/birthday-widget-settings.model';

export interface BirthdayWidget extends Widget<BirthdayWidgetSettings> {
}
