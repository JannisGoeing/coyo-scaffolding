import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SettingsService} from '@domain/settings/settings.service';
import {ButtonWidget} from '@widgets/button/button-widget';
import {of, Subject} from 'rxjs';
import {ButtonWidgetSettingsComponent} from './button-widget-settings.component';

describe('ButtonWidgetSettingsComponent', () => {
  let component: ButtonWidgetSettingsComponent;
  let fixture: ComponentFixture<ButtonWidgetSettingsComponent>;
  let settingsService: jasmine.SpyObj<SettingsService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ButtonWidgetSettingsComponent],
      providers: [FormBuilder, {
        provide: SettingsService,
        useValue: jasmine.createSpyObj('SettingsService', ['retrieveByKey'])
      }]
    }).overrideTemplate(ButtonWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    settingsService = TestBed.get(SettingsService);
    settingsService.retrieveByKey.and.returnValue(of('https?:\\/\\/\\S+'));
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {settings: {}} as ButtonWidget;
    component.parentForm = new FormGroup({});
    component.onSubmit = new Subject<any>();
    fixture.detectChanges();
  });

  it('should validate settings', () => {
    component.parentForm.patchValue({
      text: 'Button',
      _url: 'http://example.com/'
    });

    expect(component.parentForm.valid).toBeTruthy();
    expect(component.parentForm.get('_linkTarget').value).toEqual('_blank');
    expect(component.parentForm.get('_button').value).toEqual({btnClass: 'btn-default'});
  });

  it('should use the configured link pattern', fakeAsync(() => {
    component.ngOnInit();
    tick();

    component.parentForm.patchValue({
      text: 'Button',
      _url: 'www.example.com'
    });

    expect(component.parentForm.valid).toBeFalsy();
    expect(component.parentForm.get('_url').errors).toEqual(jasmine.objectContaining({
      pattern: {
        requiredPattern: '^https?:\\/\\/\\S+$',
        actualValue: 'www.example.com'
      }
    }));
  }));

  it('should use long links', () => {
    component.ngOnInit();

    component.parentForm.patchValue({
      text: 'Long link',
      _url: 'https://www.example.com?value=ozarebberovbrtbnaerboerbuaerozgrakhgbarevrglbaerbglaerbglerg' +
        'eabrlgbeargbalerbglaekshrbrvaervaberluvafaqfwbvxcvdnbftnojlpzurtegernvkdrhvberuvniwfuwvuwbvwie' +
        'vnthoihwezfvqbjwrnbbnaerbneaornboerbitnboraehsjrztkuziliujhaervbartsztklt9lk6agfargrtjhtszhaer' +
        'gsrzjtgaethhajsk79lk7z4567994352gahsrjynmduzksthath4542tzahthrthaethjhrthsrhrhsrthsrthsrthrsth'
    });
    fixture.detectChanges();

    expect(component.parentForm.valid).toBeTruthy();
  });
});
