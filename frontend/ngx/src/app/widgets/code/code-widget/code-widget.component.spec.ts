import {DOCUMENT} from '@angular/common';
import {ChangeDetectorRef, Pipe, PipeTransform} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CodeWidget} from '@widgets/code/code-widget';
import {CodeWidgetComponent} from './code-widget.component';

@Pipe({name: 'translate'})
class TranslatePipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) => value;
}

describe('CodeWidgetComponent', () => {
  let component: CodeWidgetComponent;
  let fixture: ComponentFixture<CodeWidgetComponent>;
  let document: Document;
  let cd: jasmine.SpyObj<ChangeDetectorRef>;

  let widget: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CodeWidgetComponent, TranslatePipeMock],
      providers: [{
        provide: ChangeDetectorRef,
        useValue: jasmine.createSpyObj('ChangeDetectorRef', ['detectChanges'])
      }]
    });

    document = TestBed.get(DOCUMENT);
    cd = TestBed.get(ChangeDetectorRef);

    spyOn(document, 'createElement').and.callThrough();
    spyOn(document.head, 'appendChild').and.callThrough();
    spyOn(document.head, 'removeChild').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeWidgetComponent);
    component = fixture.componentInstance;
    widget = {
      id: '123', settings: {
        _html_content: '', _js_content: '', _css_content: ''
      }} as CodeWidget;
    component.widget = widget;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have content on changed html', () => {
    // given
    const htmlContent = '<span>hi</span><script src="therewasonceabugwherethiswasfilteredout.js"></script>';
    widget.settings._html_content = htmlContent;

    // when
    fixture.detectChanges();

    // then
    expect(document.body.innerHTML.includes(htmlContent)).toBeTruthy();
  });

  it('should have content and update on changed js code', () => {
    // given
    const jsContent = 'console.log(\'huhu\')';
    widget.settings._js_content = jsContent;

    // when
    fixture.detectChanges();

    // then
    // tslint:disable-next-line:deprecation
    expect(document.createElement).toHaveBeenCalled();
    expect(document.head.appendChild).toHaveBeenCalled();
    expect(document.head.innerHTML.includes(jsContent)).toBeTruthy();
  });

  it('should have content and update on changed css code', () => {
    // given
    const cssContent = '.lufthansa { color: #fab400; }}';
    widget.settings._css_content = cssContent;

    // when
    fixture.detectChanges();

    // then
    // tslint:disable-next-line:deprecation
    expect(document.createElement).toHaveBeenCalled();
    expect(document.head.appendChild).toHaveBeenCalled();
    expect(document.head.innerHTML.includes(cssContent)).toBeTruthy();
  });
});
