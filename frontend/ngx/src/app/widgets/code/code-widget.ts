import {Widget} from '@domain/widget/widget';
import {CodeWidgetSettings} from '@widgets/code/code-widget-settings.model';

export interface CodeWidget extends Widget<CodeWidgetSettings> {
}
