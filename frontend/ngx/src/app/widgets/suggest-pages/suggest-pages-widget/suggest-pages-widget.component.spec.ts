import {ChangeDetectorRef} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Store} from '@ngxs/store';
import {SuggestPagesWidget} from '@widgets/suggest-pages/suggest-pages-widget';
import {Load} from '@widgets/suggest-pages/suggest-pages-widget/suggest-pages-widget.actions';
import {SuggestPagesWidgetComponent} from './suggest-pages-widget.component';

describe('SuggestPagesWidgetComponent', () => {
  let component: SuggestPagesWidgetComponent;
  let fixture: ComponentFixture<SuggestPagesWidgetComponent>;
  let store: jasmine.SpyObj<Store>;
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SuggestPagesWidgetComponent],
        providers: [{
          provide: Store, useValue: jasmine.createSpyObj('store', ['dispatch', 'select'])
        }, ChangeDetectorRef]
      })
      .overrideTemplate(SuggestPagesWidgetComponent, '')
      .compileComponents();
    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestPagesWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {
        _pageIds: []
      }
    } as SuggestPagesWidget;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });

  it('should init', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(store.select).toHaveBeenCalled();
    expect(store.select.calls.mostRecent().args[0]({suggestPagesWidget: {[component.widget.id]: 'test-id'}})).toBe('test-id');
    expect(store.dispatch)
      .toHaveBeenCalledWith(new Load(component.widget.settings, component.widget.id));
  });

  it('should re-init pages on change', () => {
    // when
    fixture.detectChanges();
    component.ngOnChanges({
      widget: {
        firstChange: false,
        isFirstChange: () => false,
        currentValue: {settings: {_pageIds: ['a', 'b']}},
        previousValue: {settings: {_pageIds: ['b']}}
      }
    });

    // then
    expect(store.dispatch)
      .toHaveBeenCalledWith(new Load(component.widget.settings, component.widget.id));
  });

  it('should not re-init pages on no changes to the widget', () => {
    // when
    component.ngOnChanges({
      widget: {
        firstChange: false,
        isFirstChange: () => false,
        currentValue: {settings: {_pageIds: ['a', 'b']}},
        previousValue: {settings: {_pageIds: ['a', 'b']}},
      }
    });

    // then
    expect(store.dispatch).not.toHaveBeenCalled();
  });

  it('should correctly return skeleton parameters without any pages', () => {
    // given
    component.widget.settings._pageIds = [];

    // when
    const result = component.getNeededSkeletons();

    // then
    expect(result).toEqual([]);
  });

  it('should correctly return skeleton parameters with 1 page', () => {
    // given
    component.widget.settings._pageIds = ['a'];

    // when
    const result = component.getNeededSkeletons();

    // then
    expect(result).toEqual([0]);
  });

  it('should correctly return skeleton parameters with 3 pages', () => {
    // given
    component.widget.settings._pageIds = ['a', 'b', 'c'];

    // when
    const result = component.getNeededSkeletons();

    // then
    expect(result).toEqual([0, 1, 2]);
  });

  it('should correctly return skeleton parameters with more than 3 pages', () => {
    // given
    component.widget.settings._pageIds = ['a', 'b', 'c', 'd'];

    // when
    const result = component.getNeededSkeletons();

    // then
    expect(result).toEqual([0, 1, 2]);
  });
});
