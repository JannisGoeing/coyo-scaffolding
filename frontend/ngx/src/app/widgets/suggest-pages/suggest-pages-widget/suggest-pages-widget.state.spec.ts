import {async, TestBed} from '@angular/core/testing';
import {PageService} from '@domain/page/page.service';
import {SubscriptionService} from '@domain/subscription/subscription.service';
import {NgxsModule, Store} from '@ngxs/store';
import {SuggestPagesWidgetSettings} from '@widgets/suggest-pages/suggest-pages-widget-settings';
import {Load} from '@widgets/suggest-pages/suggest-pages-widget/suggest-pages-widget.actions';
import {SuggestPagesWidgetState} from '@widgets/suggest-pages/suggest-pages-widget/suggest-pages-widget.state';
import {of, Subject} from 'rxjs';

describe('SuggestPagesWidgetStore', () => {
  let store: Store;
  let settings: SuggestPagesWidgetSettings;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let pageService: jasmine.SpyObj<PageService>;
  const ID = 'ID';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([SuggestPagesWidgetState])],
      providers: [{
        provide: SubscriptionService, useValue: jasmine.createSpyObj('SubscriptionService', ['getSubscriptionsByType'])
      }, {
        provide: PageService, useValue: jasmine.createSpyObj('PageService', ['getBulk'])
      }]
    }).compileComponents();
    store = TestBed.get(Store);
    subscriptionService = TestBed.get(SubscriptionService);
    pageService = TestBed.get(PageService);

    settings = {
      _pageIds: ['c', 'b', 'a']
    } as SuggestPagesWidgetSettings;
  }));

  it('should correctly init the state with pages', () => {

    // given
    const subscriptionSubject = new Subject();
    store.reset({suggestPagesWidget: {[ID]: {loading: false, hasPages: true, pages: ['f', 'g']}}});
    subscriptionService.getSubscriptionsByType.and.returnValue(subscriptionSubject);
    /*tslint:disable:arrow-return-shorthand*/
    pageService.getBulk.and.callFake((ids: string[]) => of(ids.map(id => {
      return {name: 'page-' + id};
    })));
    // when
    store.dispatch(new Load(settings, ID));

    // then
    let state = store.selectSnapshot<SuggestPagesWidgetState>(s => s.suggestPagesWidget);
    expect(state[ID]).toBeDefined();
    expect(state[ID].pages).toBeUndefined();
    expect(state[ID].hasPages).toBeFalsy();
    expect(state[ID].loading).toBeTruthy();

    subscriptionSubject.next([{senderId: 'c'}]);
    subscriptionSubject.complete();
    state = store.selectSnapshot<SuggestPagesWidgetState>(s => s.suggestPagesWidget);

    expect(state[ID]).toBeDefined();
    expect(state[ID].loading).toBeFalsy();
    expect(state[ID].hasPages).toBeTruthy();
    expect(state[ID].pages).toEqual([{name: 'page-a'}, {name: 'page-b'}]);
  });

  it('should correctly init the state without pages', () => {

    // given
    const subscriptionSubject = new Subject();
    store.reset({suggestPagesWidget: {[ID]: {loading: false, hasPages: true, pages: ['f', 'g']}}});
    subscriptionService.getSubscriptionsByType.and.returnValue(subscriptionSubject);
    /*tslint:disable:arrow-return-shorthand*/
    pageService.getBulk.and.callFake((ids: string[]) => of(ids.map(id => {
      return {name: 'page-' + id};
    })));
    // when
    store.dispatch(new Load(settings, ID));

    // then
    let state = store.selectSnapshot<SuggestPagesWidgetState>(s => s.suggestPagesWidget);
    expect(state[ID]).toBeDefined();
    expect(state[ID].pages).toBeUndefined();
    expect(state[ID].hasPages).toBeFalsy();
    expect(state[ID].loading).toBeTruthy();

    subscriptionSubject.next([{senderId: 'c'}, {senderId: 'b'}, {senderId: 'a'}]);
    subscriptionSubject.complete();
    state = store.selectSnapshot<SuggestPagesWidgetState>(s => s.suggestPagesWidget);

    expect(state[ID]).toBeDefined();
    expect(state[ID].loading).toBeFalsy();
    expect(state[ID].hasPages).toBeFalsy();
    expect(state[ID].pages).toEqual([]);
  });
});
