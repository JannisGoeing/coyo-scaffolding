import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

/**
 * The entity model for the settings of a suggest-pages widget
 */
export interface SuggestPagesWidgetSettings extends WidgetSettings {
  _pageIds: string[];
}
