import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {PageService} from '@domain/page/page.service';
import {SuggestPagesWidget} from '@widgets/suggest-pages/suggest-pages-widget';
import {of} from 'rxjs';
import {SuggestPagesWidgetSettingsComponent} from './suggest-pages-widget-settings.component';

describe('SuggestPagesWidgetSettingsComponent', () => {
  let component: SuggestPagesWidgetSettingsComponent;
  let fixture: ComponentFixture<SuggestPagesWidgetSettingsComponent>;
  let pageService: jasmine.SpyObj<PageService>;
  let parentForm: FormGroup;
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [SuggestPagesWidgetSettingsComponent],
        providers: [{
          provide: PageService,
          useValue: jasmine.createSpyObj('PageService', ['getBulk'])
        }]
      })
      .overrideTemplate(SuggestPagesWidgetSettingsComponent, '')
      .compileComponents();
    pageService = TestBed.get(PageService);

  }));

  beforeEach(() => {
    pageService.getBulk.and.returnValue(of([]));
    fixture = TestBed.createComponent(SuggestPagesWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id'
    } as SuggestPagesWidget;
    parentForm = new FormGroup({});
    component.parentForm = parentForm;
  });

  it('should create', () => {
    // given

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
  });
  it('should store id\'s, not entire pages', () => {
    // given
    // when
    component.onBeforeSave({pages: [{id: 'test1'}, {id: 'test2'}]}).subscribe(settings => {
      // then
      expect(settings).toEqual({_pageIds: ['test1', 'test2']});
    });
  });
});
