import {Widget} from '@domain/widget/widget';
import {TextWidgetSettings} from '@widgets/text/text-widget-settings.model';

export interface TextWidget extends Widget<TextWidgetSettings> {
}
