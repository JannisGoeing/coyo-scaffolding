/**
 * Defines the slots for the different layouts offered in the widget layout selection.
 */
export interface LayoutType {
  slots: number[];
}
