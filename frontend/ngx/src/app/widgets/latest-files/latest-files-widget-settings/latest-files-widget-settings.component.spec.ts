import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {AppService} from '@domain/apps/app.service';
import {LatestFilesWidget} from '@widgets/latest-files/latest-files-widget';
import {of} from 'rxjs';
import {LatestFilesWidgetSettingsComponent} from './latest-files-widget-settings.component';

describe('LatestFilesWidgetSettingsComponent', () => {
  let component: LatestFilesWidgetSettingsComponent;
  let fixture: ComponentFixture<LatestFilesWidgetSettingsComponent>;
  let appService: jasmine.SpyObj<AppService>;
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [LatestFilesWidgetSettingsComponent],
        providers: [{
          provide: AppService,
          useValue: jasmine.createSpyObj('AppService', ['get'])
        }]
      })
      .overrideTemplate(LatestFilesWidgetSettingsComponent, '')
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(LatestFilesWidgetSettingsComponent);
      component = fixture.componentInstance;
      component.widget = {
        id: 'widget-id',
        tempId: 'test-temp-id',
        settings: {
          _app: {id: 'test-app-id', senderId: 'test-app-sender-id'},
          _fileCount: 5
        }
      } as LatestFilesWidget;
      component.parentForm = new FormGroup({});
      appService = TestBed.get(AppService);
    });
  }));

  it('should create', () => {
    // given
    appService.get.and.returnValue(of({id: 'test-id'}));

    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(appService.get).toHaveBeenCalledWith(component.widget.settings._app.id, {context: {senderId: component.widget.settings._app.senderId}});
  });

  it('should transform settings on save', () => {
    // given
    let called = false;
    // when
    const result = component.onBeforeSave({
      app: component.widget.settings._app,
      fileCount: component.widget.settings._fileCount
    });

    // then
    result.subscribe(data => {
      expect(data).toEqual(component.widget.settings);
      called = true;
    });
    expect(called).toBeTruthy();
  });

});
