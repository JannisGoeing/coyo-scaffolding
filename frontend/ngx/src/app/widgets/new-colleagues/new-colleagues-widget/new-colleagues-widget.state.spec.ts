import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {NewColleaguesWidgetState} from '@widgets/new-colleagues/new-colleagues-widget/new-colleagues-widget.state';
import {Init, LoadMore, Reset, SetLoading} from '@widgets/new-colleagues/new-colleagues-widget/new-colleagues.actions';
import {NewColleaguesService} from '@widgets/new-colleagues/new-colleagues.service';
import {of} from 'rxjs';

describe('NewColleaguesWidgetState', () => {
  let store: Store;
  let colleaguesService: jasmine.SpyObj<NewColleaguesService>;
  let users: any;
  const id = 'id';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([NewColleaguesWidgetState])],
      providers: [{
        provide: NewColleaguesService,
        useValue: jasmine.createSpyObj('colleaguesService', ['getNewColleaguesList'])
      }]
    });

    store = TestBed.get(Store);
    colleaguesService = TestBed.get(NewColleaguesService);

    users = { content: [{ id: 1 }, { id: 2 }], pages: 1, last: true};

    colleaguesService.getNewColleaguesList.and.returnValue(of(users));
  }));

  it('should init new colleagues state', () => {
    // when
    store.dispatch(new Init(id));

    // then
    const actual = store.selectSnapshot((state => state.newColleagues));
    expect(actual[id]).toBeDefined();
    expect(actual[id].users.length).toBe(2);
    expect(actual[id].pages).toBe(1);
    expect(actual[id].last).toBeTruthy();
    expect(actual[id].loading).toBeFalsy();
  });

  it('should load more users for new colleagues state', () => {
    // given
    store.dispatch(new Init(id));

    const moreUsers: any = { content: [{ id: 3 }, { id: 4 } , { id: 5} ], pages: 2, last: true};

    colleaguesService.getNewColleaguesList.and.returnValue(of(moreUsers));
    // when
    store.dispatch(new LoadMore(id));

    // then
    const actual = store.selectSnapshot((state => state.newColleagues));
    expect(actual[id]).toBeDefined();
    expect(actual[id].users.length).toBe(5);
    expect(actual[id].pages).toBe(2);
    expect(actual[id].last).toBeTruthy();
    expect(actual[id].loading).toBeFalsy();
  });

  it('should doesnt load more again when the state is loading', () => {
    // given
    store.dispatch(new Init(id));

    store.dispatch(new SetLoading(true, id ));

    /* Is just there to recognize that this tests fails when more data is loaded */
    const moreUsers: any = { content: [{ id: 3 }, { id: 4 }, { id: 5 }], pages: 2, last: true};
    colleaguesService.getNewColleaguesList.and.returnValue(of(moreUsers));

    // when
    store.dispatch(new LoadMore(id));

    // then
    const actual = store.selectSnapshot((state => state.newColleagues));
    expect(actual[id]).toBeDefined();
    expect(actual[id].users.length).toBe(2);
    expect(actual[id].pages).toBe(1);
    expect(actual[id].last).toBeTruthy();
    expect(actual[id].loading).toBeTruthy();
  });

  it('should set the loading flag to the new colleagues state to false', () => {
    // when
    store.dispatch(new SetLoading(false, id ));

    // then
    const actual = store.selectSnapshot((state => state.newColleagues));
    expect(actual[id]).toBeDefined();
    expect(actual[id].loading).toBeFalsy();
  });

  it('should set the loading flag to the new colleagues state to true', () => {
    // when
    store.dispatch(new SetLoading(true, id ));

    // then
    const actual = store.selectSnapshot((state => state.newColleagues));
    expect(actual[id]).toBeDefined();
    expect(actual[id].loading).toBeTruthy();
  });

  it('should reset the new colleagues state', () => {
    // given
    store.dispatch(new Reset(id));

    // then
    const actual = store.selectSnapshot((state => state.newColleagues));
    expect(actual[id]).toBeUndefined();
  });
});
