import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';
import {User} from '@domain/user/user';
import {Store} from '@ngxs/store';
import {WidgetVisibilityService} from '@widgets/api/widget-visibility/widget-visibility.service';
import {Init, LoadMore, Reset} from '@widgets/new-colleagues/new-colleagues-widget/new-colleagues.actions';
import {NewColleaguesService} from '@widgets/new-colleagues/new-colleagues.service';
import * as _ from 'lodash';
import {of} from 'rxjs';

import {NewColleaguesWidgetComponent} from './new-colleagues-widget.component';

describe('NewColleaguesWidgetComponent', () => {
  let component: NewColleaguesWidgetComponent;
  let fixture: ComponentFixture<NewColleaguesWidgetComponent>;
  let widgetVisibilityService: jasmine.SpyObj<WidgetVisibilityService>;
  let store: jasmine.SpyObj<Store>;
  let colleaguesService: jasmine.SpyObj<NewColleaguesService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewColleaguesWidgetComponent ],
      providers: [{
        provide: WidgetVisibilityService,
        useValue: jasmine.createSpyObj('widgetVisibilityService', ['setHidden'])
      }, {
        provide: Store,
        useValue: jasmine.createSpyObj('store', ['dispatch', 'select'])
      }, {
        provide: NewColleaguesService,
        useValue: jasmine.createSpyObj('colleaguesService', ['getNewColleaguesList'])
      }]
    }).overrideTemplate(NewColleaguesWidgetComponent, '')
    .compileComponents();

    colleaguesService = TestBed.get(NewColleaguesService);
    widgetVisibilityService = TestBed.get(WidgetVisibilityService);
    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewColleaguesWidgetComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'id',
      tempId: 'tempId',
      settings: {
        _titles: ['title']
      }
    };
  });

  it('should init newColleaguesWidget when new colleagues available', () => {
    // given
    const body = { loading: false, users: [{loginName: 'Dummy'}, {loginName: 'Fredy'}]};

    const state = {newColleagues: {id: body}};

    store.select.and.returnValue(of(body));

    // when
    fixture.detectChanges();
    component.state$.subscribe();

    // then
    expect(store.select).toHaveBeenCalled();
    expect(store.select.calls.mostRecent().args[0](state)).toBe(state.newColleagues.id);
    expect(store.dispatch).toHaveBeenCalledWith( new Init(component.widget.id));
    expect(widgetVisibilityService.setHidden).toHaveBeenCalledWith(component.widget.id, false);
  });

 it('should init newColleaguesWidget when no new colleagues available', () => {
   // given
   const body = { loading: false, users: [] as User[]};

   const state = {newColleagues: {id: body}};

   store.select.and.returnValue(of(body));

   // when
   fixture.detectChanges();
   component.state$.subscribe();

   // then
   expect(store.select).toHaveBeenCalled();
   expect(store.select.calls.mostRecent().args[0](state)).toBe(state.newColleagues.id);
   expect(store.dispatch).toHaveBeenCalledWith( new Init(component.widget.id));
   expect(widgetVisibilityService.setHidden).toHaveBeenCalledWith(component.widget.id, true);
  });

  it('should load more colleagues', () => {
    // given

    // when
    component.onLoadMore();

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new LoadMore(component.widget.id));
  });

  it('should destroy the current state of the newColleaguesWidget', () => {
    // given

    // when
    component.ngOnDestroy();

    // then
    expect(store.dispatch).toHaveBeenCalledWith(new Reset(component.widget.id));
  });

  it('should set a new titel to the widget', () => {
    // given
    const newTitle = 'my colleagues';
    _.set(component.widget, 'settings._titles[0]', newTitle);

    // when
    const result = component.getTitle();

    // then
    expect(result).toBe(newTitle);
  });
});
