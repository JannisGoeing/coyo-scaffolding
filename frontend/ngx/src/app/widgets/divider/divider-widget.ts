import {Widget} from '@domain/widget/widget';
import {DividerWidgetSettings} from '@widgets/divider/divider-widget-settings.model';

export interface DividerWidget extends Widget<DividerWidgetSettings> {
}
