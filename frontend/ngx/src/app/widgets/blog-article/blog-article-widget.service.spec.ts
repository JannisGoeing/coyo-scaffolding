import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {fakeAsync, inject, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {SingleBlogArticle} from '@widgets/blog-article/blog-article-widget';
import * as _ from 'lodash';
import {BlogArticleWidgetService} from './blog-article-widget.service';

describe('BlogArticleWidgetService', () => {
  let blogArticleWidgetService: BlogArticleWidgetService;
  let httpMock: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BlogArticleWidgetService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }]
    });
    blogArticleWidgetService = TestBed.get(BlogArticleWidgetService);
    httpMock = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
        parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  it('should be created', () => {
    expect(blogArticleWidgetService).toBeTruthy();
  });

  it('should request blog article by id', fakeAsync(() => {
    inject([BlogArticleWidgetService], (service: BlogArticleWidgetService) => {
      // when
      service.getArticleById('blog-id').subscribe(article => {
        expect(article.id).toBe('blog-id');
        expect(article.title).toBe('blog-title');
      });
      tick();

      // then
      const request = httpMock.expectOne('/web/widgets/blogarticle/article/blog-id');
      request.flush({id: 'blog-id', title: 'blog-title'});
    });
  }));

  it('should request page of blog articles', fakeAsync(() => {
    inject([BlogArticleWidgetService], (service: BlogArticleWidgetService) => {
      // given
      const singleBlogArticle: SingleBlogArticle = new SingleBlogArticle();
      singleBlogArticle.id = 'id';
      singleBlogArticle.title = 'title';
      const singleBlogArticle1: SingleBlogArticle = new SingleBlogArticle();
      singleBlogArticle1.id = 'id1';
      singleBlogArticle1.title = 'title1';
      const singleBlogArticle2: SingleBlogArticle = new SingleBlogArticle();
      singleBlogArticle2.id = 'id2';
      singleBlogArticle2.title = 'title2';
      const articles: SingleBlogArticle[] = [singleBlogArticle, singleBlogArticle1, singleBlogArticle2];

      // when
      service.getArticles(new Pageable(0, 20), '').subscribe((response: Page<SingleBlogArticle>) => {
        expect(response.content[0].id).toBe(articles[0].id);
        expect(response.content[0].title).toBe(articles[0].title);
        expect(response.content[1].id).toBe(articles[1].id);
        expect(response.content[1].title).toBe(articles[1].title);
        expect(response.content[2].id).toBe(articles[2].id);
        expect(response.content[2].title).toBe(articles[2].title);
      });
      tick();

      // then
      const request = httpMock.expectOne(req =>
          req.url === '/web/widgets/blogarticle' &&
          req.params.get('title') === '' &&
          req.params.get('_page') === '0' &&
          req.params.get('_pageSize') === '20' &&
          req.params.get('_orderBy') === 'title,DESC');
      request.flush({
        content: [{id: 'id', title: 'title'},
          {id: 'id1', title: 'title1'},
          {id: 'id2', title: 'title2'}]
      });
    });
  }));
});
