import {Pipe, PipeTransform} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {Page} from '@test/util/page';
import {CalloutWidgetComponent} from './callout-widget.component';

class ComponentPage extends Page<CalloutWidgetComponent> {
  get alert(): HTMLElement {
    return this.query<HTMLElement>('.alert');
  }
}

@Pipe({name: 'markdown'})
class MarkdownPipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) => value;
}

@Pipe({name: 'hashtags'})
class HashtagsPipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) => value;
}

@Pipe({name: 'mention'})
class MentionPipeMock implements PipeTransform {
  transform = (value: string, ...args: any[]) => value;
}

describe('CalloutWidgetComponent', () => {
  let component: CalloutWidgetComponent;
  let fixture: ComponentFixture<CalloutWidgetComponent>;
  let page: ComponentPage;

  let widget: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalloutWidgetComponent, MarkdownPipeMock, HashtagsPipeMock, MentionPipeMock]
    }).compileComponents();
  }));

  beforeEach(() => {
    widget = {settings: {}};
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalloutWidgetComponent);
    page = new ComponentPage(fixture);
    component = fixture.componentInstance;
    component.widget = widget;
    fixture.detectChanges();
  });

  it('should use the configured text', () => {
    widget.settings['md_text'] = 'Callout text';
    fixture.detectChanges();

    expect(page.alert.textContent).toContain(widget.settings['md_text']);
  });
});
