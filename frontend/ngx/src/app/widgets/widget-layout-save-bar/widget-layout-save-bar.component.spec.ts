import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LocalStorageService} from '@core/storage/local-storage/local-storage.service';
import {Ng1WidgetLayoutService} from '@root/typings';
import {NG1_ROOTSCOPE, NG1_WIDGET_LAYOUT_SERVICE} from '@upgrade/upgrade.module';
import {WidgetLayoutSaveBarComponent} from '@widgets/widget-layout-save-bar/widget-layout-save-bar.component';
import {IRootScopeService} from 'angular';

describe('WidgetLayoutSaveBarComponent', () => {
  let component: WidgetLayoutSaveBarComponent;
  let fixture: ComponentFixture<WidgetLayoutSaveBarComponent>;
  let localStorageService: jasmine.SpyObj<LocalStorageService>;
  let widgetLayoutService: jasmine.SpyObj<Ng1WidgetLayoutService>;
  let rootScopeService: jasmine.SpyObj<IRootScopeService>;
  let rootScopeCallback: (newValue: any, oldValue: any) => void;
  beforeEach(async(() => {
    rootScopeCallback = null;

    TestBed
      .configureTestingModule({
        declarations: [WidgetLayoutSaveBarComponent],
        providers: [
          {
            provide: LocalStorageService,
            useValue: jasmine.createSpyObj('localStorageService', ['getValue'])
          },
          {
            provide: NG1_WIDGET_LAYOUT_SERVICE,
            useValue: jasmine.createSpyObj('widgetLayoutService', ['save', 'cancel'])
          },
          {
            provide: NG1_ROOTSCOPE,
            useValue: jasmine.createSpyObj('rootScopeService', ['$watch'])
          }
        ]
      }).overrideTemplate(WidgetLayoutSaveBarComponent, '')
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(WidgetLayoutSaveBarComponent);
      component = fixture.componentInstance;
      localStorageService = TestBed.get(LocalStorageService);
      widgetLayoutService = TestBed.get(NG1_WIDGET_LAYOUT_SERVICE);
      rootScopeService = TestBed.get(NG1_ROOTSCOPE);
      rootScopeService.$watch.and.callFake((key: string, callback: any) => {
        rootScopeCallback = callback;
      });
      localStorageService.getValue.and.returnValue(false);
    });
  }));

  it('should create', () => {
    // given
    let done = false;
    let globalEditMode = null;

    // when
    fixture.detectChanges();

    // then
    component.globalEditMode.subscribe(v => {
      done = true;
      globalEditMode = v;
    });
    expect(component.compact).toBeFalsy();
    expect(done).toBeTruthy();
    expect(globalEditMode).toBeFalsy();
  });

  it('should watch the global edit mode', () => {
    // given
    let done = false;
    let globalEditMode = null;

    // when
    fixture.detectChanges();
    rootScopeCallback(true, false);
    // then
    component.globalEditMode.subscribe(v => {
      done = true;
      globalEditMode = v;
    });
    expect(done).toBeTruthy();
    expect(globalEditMode).toBe(true);
  });

  it('should correctly change to compact view', () => {
    // given
    localStorageService.getValue.and.callFake((key: string) => key === 'messagingSidebar.compact');

    // when
    fixture.detectChanges();

    // then
    expect(component.compact).toBeTruthy();
  });

  it('should save', () => {
    // given

    // when
    fixture.detectChanges();
    component.save();

    // then
    expect(widgetLayoutService.save).toHaveBeenCalledWith(rootScopeService, true);
  });

  it('should cancel', () => {
    // given

    // when
    fixture.detectChanges();
    component.cancel();

    // then
    expect(widgetLayoutService.cancel).toHaveBeenCalledWith(rootScopeService, true);
  });
});
