import {Widget} from '@domain/widget/widget';
import {PollWidgetSettings} from '@widgets/poll/poll-widget-settings';

export interface PollWidget extends Widget<PollWidgetSettings> {
}
