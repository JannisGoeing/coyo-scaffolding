import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from '@ngxs/store';
import {PollWidgetSettingsOption} from '@widgets/poll/poll-widget-settings';
import {HydratePollWidget, RemoveVote, Vote} from '@widgets/poll/poll-widget.actions';
import {
  PollWidgetOptionVoteResponse,
  PollWidgetSelectedAnswerResponse,
  PollWidgetService
} from '@widgets/poll/poll-widget/poll-widget.service';
import {of} from 'rxjs';
import {PollWidgetsState, PollWidgetsStateModel, PollWidgetStateModel} from './poll-widget.state';

describe('PollWidget state', () => {
  let store: Store;
  let service: jasmine.SpyObj<PollWidgetService>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([PollWidgetsState])],
      providers: [
        {
          provide: PollWidgetService,
          useValue: jasmine.createSpyObj('pollWidgetService', ['getVotes',
            'getSelectedAnswers',
            'selectAnswer',
            'deleteAnswer'])
        }
      ]
    }).compileComponents();
    store = TestBed.get(Store);
    service = TestBed.get(PollWidgetService);
  }));

  it('should create an empty state', () => {
    const actual = store.selectSnapshot(PollWidgetsState.getState);
    const expected: PollWidgetsStateModel = {};
    expect(actual).toEqual(expected);
  });

  it('should hydrate poll widget without results', () => {
    // given
    const widgetId = 'widget-id';
    const options = [{
      id: '0',
      answer: 'Option A'
    }, {
      id: '1',
      answer: 'Option B'
    }] as PollWidgetSettingsOption[];
    service.getVotes.and.returnValue(of([]));
    service.getSelectedAnswers.and.returnValue(of([]));

    // when
    store.dispatch(new HydratePollWidget(widgetId, options, false));

    // then
    const actual = store.selectSnapshot(PollWidgetsState.getState)[widgetId];
    const expected: PollWidgetStateModel = {
      totalVotes: 0,
      options: [{
        id: options[0].id,
        votes: 0,
        answer: options[0].answer,
        answerId: undefined
      }, {
        id: options[1].id,
        votes: 0,
        answer: options[1].answer,
        answerId: undefined
      }]
    };

    expect(actual).toEqual(expected);
  });

  it('should hydrate poll widget with results', () => {
    // given
    const widgetId = 'widget-id';
    const options = [{
      id: '0',
      answer: 'Option A'
    }, {
      id: '1',
      answer: 'Option B'
    }] as PollWidgetSettingsOption[];

    const voteResponse: PollWidgetOptionVoteResponse[] = [{
      optionId: '0',
      count: 12
    }, {
      optionId: '1',
      count: 3
    }];

    const selectedAnswerResponse: PollWidgetSelectedAnswerResponse[] = [{
      optionId: '0',
      id: 'answer-id'
    }];
    service.getVotes.and.returnValue(of(voteResponse));
    service.getSelectedAnswers.and.returnValue(of(selectedAnswerResponse));

    // when
    store.dispatch(new HydratePollWidget(widgetId, options, true));

    // then
    const actual = store.selectSnapshot(PollWidgetsState.getState)[widgetId];
    const expected: PollWidgetStateModel = {
      totalVotes: 15,
      options: [{
        id: options[0].id,
        votes: 12,
        answer: options[0].answer,
        answerId: 'answer-id'
      }, {
        id: options[1].id,
        votes: 3,
        answer: options[1].answer,
        answerId: undefined
      }]
    };

    expect(actual).toEqual(expected);
  });

  it('should vote', () => {
    // given
    const widgetId = 'widget-id';
    const options = [{
      id: '0',
      answer: 'Option A'
    }, {
      id: '1',
      answer: 'Option B'
    }] as PollWidgetSettingsOption[];

    const voteResponse: PollWidgetOptionVoteResponse[] = [{
      optionId: '0',
      count: 0
    }, {
      optionId: '1',
      count: 0
    }];

    const response: PollWidgetSelectedAnswerResponse = {id: 'answer-id', optionId: '1'};
    service.selectAnswer.and.returnValue(of(response));
    service.getVotes.and.returnValue(of(voteResponse));
    service.getSelectedAnswers.and.returnValue(of([]));
    store.dispatch(new HydratePollWidget(widgetId, options, true));

    // when
    store.dispatch(new Vote(widgetId, store.selectSnapshot(PollWidgetsState.getState)[widgetId].options[1]));

    // then
    const actual = store.selectSnapshot(PollWidgetsState.getState)[widgetId];
    const expected: PollWidgetStateModel = {
      totalVotes: 1,
      options: [{
        id: options[0].id,
        votes: 0,
        answer: options[0].answer,
        answerId: undefined
      }, {
        id: options[1].id,
        votes: 1,
        answer: options[1].answer,
        answerId: 'answer-id',
        optimistic: false
      }]
    };

    expect(actual).toEqual(expected);
  });

  it('should remove vote', () => {
    // given
    const widgetId = 'widget-id';
    const options = [{
      id: '0',
      answer: 'Option A'
    }, {
      id: '1',
      answer: 'Option B'
    }] as PollWidgetSettingsOption[];

    const voteResponse: PollWidgetOptionVoteResponse[] = [{
      optionId: '0',
      count: 0
    }, {
      optionId: '1',
      count: 1
    }];

    service.getVotes.and.returnValue(of(voteResponse));
    service.getSelectedAnswers.and.returnValue(of([{
      optionId: '1',
      id: 'answer-id'
    } as PollWidgetSelectedAnswerResponse]));
    service.deleteAnswer.and.returnValue(of({}));
    store.dispatch(new HydratePollWidget(widgetId, options, true));

    // when
    store.dispatch(new RemoveVote(widgetId, '1', 'answer-id'));

    // then
    const actual = store.selectSnapshot(PollWidgetsState.getState)[widgetId];
    const expected: PollWidgetStateModel = {
      totalVotes: 0,
      options: [{
        id: options[0].id,
        votes: 0,
        answer: options[0].answer,
        answerId: undefined
      }, {
        id: options[1].id,
        votes: 0,
        answer: options[1].answer,
        optimistic: false
      }]
    };

    expect(actual).toEqual(expected);
  });
});
