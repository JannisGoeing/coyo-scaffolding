import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxsModule} from '@ngxs/store';
import {CoyoCommonsModule} from '@shared/commons/commons.module';
import {HelpModule} from '@shared/help/help.module';
import {SenderUIModule} from '@shared/sender-ui/sender-ui.module';
import {WIDGET_CONFIGS} from '@widgets/api/widget-config';
import {POLL_WIDGET} from '@widgets/poll/poll-widget-config';
import {PollWidgetsState} from '@widgets/poll/poll-widget.state';
import {VotersModalState} from '@widgets/poll/voters-modal/voters-modal.state';
import {TabsModule} from 'ngx-bootstrap';
import {PollWidgetSettingsComponent} from './poll-widget-settings/poll-widget-settings.component';
import {PollWidgetComponent} from './poll-widget/poll-widget.component';
import {VotersModalComponent} from './voters-modal/voters-modal.component';

@NgModule({
  declarations: [PollWidgetComponent, PollWidgetSettingsComponent, VotersModalComponent],
  entryComponents: [PollWidgetComponent, PollWidgetSettingsComponent, VotersModalComponent],
  providers: [
    {provide: WIDGET_CONFIGS, useValue: POLL_WIDGET, multi: true}
  ],
  imports: [
    NgxsModule.forFeature([PollWidgetsState, VotersModalState]),
    ReactiveFormsModule,
    CoyoCommonsModule,
    TabsModule,
    HelpModule,
    SenderUIModule
  ]
})
export class PollWidgetModule {
}
