import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {NgxsModule} from '@ngxs/store';
import {PollWidget} from '@widgets/poll/poll-widget';
import {PollWidgetSettings} from '@widgets/poll/poll-widget-settings';
import {PollWidgetsState} from '@widgets/poll/poll-widget.state';
import {PollWidgetService} from '@widgets/poll/poll-widget/poll-widget.service';

import {PollWidgetSettingsComponent} from './poll-widget-settings.component';

describe('PollWidgetSettingsComponent', () => {
  let component: PollWidgetSettingsComponent;
  let fixture: ComponentFixture<PollWidgetSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PollWidgetSettingsComponent],
      imports: [NgxsModule.forRoot([PollWidgetsState])],
      providers: [
        {
          provide: PollWidgetService,
          useValue: jasmine.createSpyObj('pollWidgetService', [''])
        }
      ]
    })
      .overrideTemplate(PollWidgetSettingsComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PollWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.parentForm = new FormGroup({});
    component.widget = {settings: {}} as PollWidget;
  });

  it('should init the form with default values', () => {
    // when
    fixture.detectChanges();

    // then
    expect(component).toBeTruthy();
    expect(component.parentForm.getRawValue()).toEqual({
      question: null,
      description: null,
      _maxAnswers: 1,
      _frozen: false,
      _anonymous: false,
      _showResults: true,
      _options: [{
        id: 0,
        answer: ''
      }]
    });
    expect(component.widget.settings._nextOptionId).toBe(1);
  });

  it('should init the form with value of the settings', () => {
    // given
    const settings = {
      question: 'Ís this the question?',
      description: 'I describe this question',
      _nextOptionId: 3,
      _maxAnswers: 1,
      _frozen: true,
      _anonymous: true,
      _showResults: false,
      _options: [{
        id: '0',
        answer: 'Option A'
      }, {
        id: '1',
        answer: 'Option B'
      }]
    } as PollWidgetSettings;
    component.widget.settings = settings;

    // when
    fixture.detectChanges();

    // then
    expect(component.parentForm.getRawValue()).toEqual({
      question: 'Ís this the question?',
      description: 'I describe this question',
      _maxAnswers: 1,
      _frozen: true,
      _anonymous: true,
      _showResults: false,
      _options: [{
        id: '0',
        answer: 'Option A'
      }, {
        id: '1',
        answer: 'Option B'
      }]
    });
    expect(component.widget.settings._nextOptionId).toBe(3);
  });

  it('should init the form with value of the settings', () => {
    // given
    const settings = {
      question: 'Ís this the question?',
      description: 'I describe this question',
      _nextOptionId: 3,
      _maxAnswers: 1,
      _frozen: true,
      _anonymous: true,
      _showResults: false,
      _options: [{
        id: '0',
        answer: 'Option A'
      }, {
        id: '1',
        answer: 'Option B'
      }]
    } as PollWidgetSettings;
    component.widget.settings = settings;

    // when
    fixture.detectChanges();
    component.deleteOption(1);

    // then
    expect(component.parentForm.getRawValue()).toEqual({
      question: 'Ís this the question?',
      description: 'I describe this question',
      _maxAnswers: 1,
      _frozen: true,
      _anonymous: true,
      _showResults: false,
      _options: [{
        id: '0',
        answer: 'Option A'
      }]
    });
    expect(component.widget.settings._nextOptionId).toBe(3);
  });

  it('should add option on enter', () => {
    // given
    spyOn(component, 'addOption');
    const event = jasmine.createSpyObj('event', ['preventDefault']);

    // when
    component.onInputEnter(event, 'some answer');

    // then
    expect(component.addOption).toHaveBeenCalled();
  });

  it('should not add option on enter', () => {
    // given
    spyOn(component, 'addOption');
    const event = jasmine.createSpyObj('event', ['preventDefault']);

    // when
    component.onInputEnter(event, '');

    // then
    expect(component.addOption).not.toHaveBeenCalled();
  });

  it('should remove option on backspace', () => {
    // given
    spyOn(component, 'deleteOption');
    const event = jasmine.createSpyObj('event', ['preventDefault']);

    // when
    component.onInputBackspace(event, '', 1);

    // then
    expect(component.deleteOption).toHaveBeenCalled();
  });

  it('should not remove option on backspace', () => {
    // given
    spyOn(component, 'deleteOption');
    const event = jasmine.createSpyObj('event', ['preventDefault']);

    // when
    component.onInputBackspace(event, 'some answer', 1);

    // then
    expect(component.deleteOption).not.toHaveBeenCalled();
  });
});
