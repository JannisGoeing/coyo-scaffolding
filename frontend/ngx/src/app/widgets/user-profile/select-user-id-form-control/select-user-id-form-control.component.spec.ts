import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SenderService} from '@domain/sender/sender/sender.service';
import {of} from 'rxjs';
import {SelectUserIdFormControlComponent} from './select-user-id-form-control.component';

describe('SelectUserIdFormControlComponent', () => {
  let component: SelectUserIdFormControlComponent;
  let fixture: ComponentFixture<SelectUserIdFormControlComponent>;
  let changeFnMock: jasmine.Spy;
  let touchedFnMock: jasmine.Spy;
  let senderService: jasmine.SpyObj<SenderService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SelectUserIdFormControlComponent],
      providers: [{
        provide: SenderService, useValue: jasmine.createSpyObj('senderService', ['get'])
      }]
    })
      .overrideTemplate(SelectUserIdFormControlComponent, '')
      .compileComponents();

    changeFnMock = jasmine.createSpy();
    touchedFnMock = jasmine.createSpy();
    senderService = TestBed.get(SenderService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectUserIdFormControlComponent);
    component = fixture.componentInstance;
    component.registerOnChange(changeFnMock);
    component.registerOnTouched(touchedFnMock);
    fixture.detectChanges();
  });

  it('should call functions on change', () => {
    // when
    component.selectControl.patchValue('123');

    // then
    expect(touchedFnMock).toHaveBeenCalled();
    expect(changeFnMock).toHaveBeenCalled();
  });

  it('should reset control when form value is null', () => {
    // when
    component.writeValue(null);

    // then
    expect(component.selectControl.value).toEqual(null);
  });

  it('should write form value', () => {
    // given
    const user = {};
    senderService.get.and.returnValue(of(user));

    // when
    component.writeValue('user-id');

    // then
    expect(component.selectControl.value).toBe(user);
  });
});
