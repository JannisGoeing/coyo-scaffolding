import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface UserProfileWidgetSettings extends WidgetSettings {
  _selectedUser: string;
  _showInfo: boolean;
}
