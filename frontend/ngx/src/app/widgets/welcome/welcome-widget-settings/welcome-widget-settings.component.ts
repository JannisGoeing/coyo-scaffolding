import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {WidgetSettingsComponent} from '@widgets/api/widget-settings-component';
import {WelcomeWidget} from '@widgets/welcome/welcome-widget';
import * as _ from 'lodash';

/**
 * The welcome widget's settings component
 */
@Component({
  selector: 'coyo-welcome-widget-settings',
  templateUrl: './welcome-widget-settings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WelcomeWidgetSettingsComponent extends WidgetSettingsComponent<WelcomeWidget> implements OnInit {

  constructor(private translateService: TranslateService) {
    super();
   }

  ngOnInit(): void {
    const cover = _.get(this.widget.settings, '_showCover', false);
    this.translateService.get('WIDGET.WELCOME.TEXT').subscribe(translation => {
      if (this.widget.settings.welcomeText === undefined) {
        this.widget.settings.welcomeText = translation;
      }
    });
    this.parentForm.addControl('welcomeText', new FormControl(this.widget.settings.welcomeText, Validators.maxLength(40)));
    this.parentForm.addControl('_showCover', new FormControl(cover));
  }

}
