import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ContentTypeService} from '@widgets/media/content-type/content-type.service';
import {MediaWidgetFile} from '@widgets/media/media-widget-settings/media-widget-settings.component';

import {MediaWidgetSettingsRowComponent} from './media-widget-settings-row.component';

describe('MediaWidgetSettingsRowComponent', () => {
  let component: MediaWidgetSettingsRowComponent;
  let fixture: ComponentFixture<MediaWidgetSettingsRowComponent>;
  let contentTypeService: jasmine.SpyObj<ContentTypeService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaWidgetSettingsRowComponent ],
      providers: [{
        provide: ContentTypeService,
        useValue: jasmine.createSpyObj('contentTypeService', ['isVideo', 'isImage'])
      }]
    }).overrideTemplate(MediaWidgetSettingsRowComponent, '')
    .compileComponents();

    contentTypeService = TestBed.get(ContentTypeService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaWidgetSettingsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check video type', () => {
    // given
    const contentType = 'video/mp4';
    contentTypeService.isVideo.and.returnValue(true);

    // when
    const result = component.isVideo({contentType} as MediaWidgetFile);

    // then
    expect(result).toBeTruthy();
    expect(contentTypeService.isVideo).toHaveBeenCalledWith(contentType);
  });

  it('should check image type', () => {
    // given
    const contentType = 'image/jpeg';
    contentTypeService.isImage.and.returnValue(true);

    // when
    const result = component.isImage({contentType} as MediaWidgetFile);

    // then
    expect(result).toBeTruthy();
    expect(contentTypeService.isImage).toHaveBeenCalledWith(contentType);
  });
});
