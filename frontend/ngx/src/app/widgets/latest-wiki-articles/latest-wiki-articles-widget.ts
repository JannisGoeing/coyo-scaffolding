import {Widget} from '@domain/widget/widget';
import {LatestWikiArticlesWidgetSettings} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget-settings';

export interface LatestWikiArticlesWidget extends Widget<LatestWikiArticlesWidgetSettings> {
}
