import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {AppService} from '@domain/apps/app.service';
import {LatestWikiArticlesWidget} from '@widgets/latest-wiki-articles/latest-wiki-articles-widget';
import {of} from 'rxjs';
import {LatestWikiArticlesWidgetSettingsComponent} from './latest-wiki-articles-widget-settings.component';

describe('LatestWikiArticlesWidgetSettingsComponent', () => {
  let component: LatestWikiArticlesWidgetSettingsComponent;
  let fixture: ComponentFixture<LatestWikiArticlesWidgetSettingsComponent>;
  let appService: jasmine.SpyObj<AppService>;
  let formGroup: jasmine.SpyObj<FormGroup>;
  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [LatestWikiArticlesWidgetSettingsComponent],
        providers: [{
          provide: AppService,
          useValue: jasmine.createSpyObj('AppService', ['get'])
        }]
      })
      .overrideTemplate(LatestWikiArticlesWidgetSettingsComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(LatestWikiArticlesWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'widget-id',
      settings: {}
    } as LatestWikiArticlesWidget;
    formGroup = jasmine.createSpyObj('FormGroup', ['addControl', 'patchValue']);
    component.parentForm = formGroup;
    appService = TestBed.get(AppService);
    appService.get.and.returnValue(of('app'));
  }));

  it('should init without settings', () => {
    // given

    // when
    component.ngOnInit();
    // then
    expect(appService.get).not.toHaveBeenCalled();
    expect(formGroup.addControl).toHaveBeenCalledTimes(2);
  });

  it('should init with settings', () => {
    // given
    component.widget.settings = {
      _appId: 'appId',
      _senderId: 'senderId',
      _articleCount: 3
    };
    // when
    component.ngOnInit();
    // then
    expect(appService.get).toHaveBeenCalledWith('appId', {context: {senderId: 'senderId'}});
    expect(formGroup.addControl).toHaveBeenCalledTimes(2);
    expect(formGroup.patchValue).toHaveBeenCalledWith({selectedApp: 'app'});
  });

});
