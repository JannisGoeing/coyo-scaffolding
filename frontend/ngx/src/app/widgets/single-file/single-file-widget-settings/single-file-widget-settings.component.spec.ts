import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {FormGroup} from '@angular/forms';
import {AuthService} from '@core/auth/auth.service';
import {Sender} from '@domain/sender/sender';
import {SenderService} from '@domain/sender/sender/sender.service';
import {SingleFileWidget} from '@widgets/single-file/single-file-widget';
import {of} from 'rxjs';
import {SingleFileWidgetSettingsComponent} from './single-file-widget-settings.component';
import SpyObj = jasmine.SpyObj;

describe('SingleFileWidgetSettingsComponent', () => {
  let component: SingleFileWidgetSettingsComponent;
  let fixture: ComponentFixture<SingleFileWidgetSettingsComponent>;
  let authService: SpyObj<AuthService>;
  let senderService: SpyObj<SenderService>;
  let parentForm: FormGroup;
  let defaultSender: Sender;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleFileWidgetSettingsComponent],
      providers: [{
        provide: AuthService,
        useValue: jasmine.createSpyObj('authService', ['getCurrentUserId'])
      }, {
        provide: SenderService,
        useValue: jasmine.createSpyObj('senderService', ['get', 'getCurrentIdOrSlug'])
      },
      ]
    })
      .overrideTemplate(SingleFileWidgetSettingsComponent, '')
      .compileComponents();

    authService = TestBed.get(AuthService);
    authService.getCurrentUserId.and.returnValue('');
    senderService = TestBed.get(SenderService);
    defaultSender = {id: 'sender-id'} as Sender;
    senderService.get.and.returnValue(of(defaultSender));
    senderService.getCurrentIdOrSlug.and.returnValue(undefined);
    parentForm = new FormGroup({});
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleFileWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = {
      id: 'some-id',
      tempId: '',
      settings: {
        title: undefined,
        _fileId: undefined,
        _senderId: undefined,
        _hideDate: false,
        _hideDownload: false,
        _hidePreview: false,
        _hideSender: false
      }
    } as SingleFileWidget;
    component.parentForm = parentForm;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should set the senderId to the one of the current user', done => {
    // given
    authService.getCurrentUserId.and.returnValue(defaultSender.id);
    fixture.detectChanges();

    // when
    component.sender$.subscribe(sender => {
      // then
      expect(authService.getCurrentUserId).toHaveBeenCalled();
      expect(senderService.get).toHaveBeenCalledWith(defaultSender.id, {permissions: ['createFile']});
      expect(component.widget.settings._senderId).toEqual(defaultSender.id);
      expect(sender).toEqual(defaultSender);
      done();
    });
  });

  it('should use the sender of the current context (e.g. of an app)', done => {
    // given
    senderService.getCurrentIdOrSlug.and.returnValue(defaultSender.id);

    fixture.detectChanges();

    // when
    component.sender$.subscribe(sender => {
      // then
      expect(authService.getCurrentUserId).not.toHaveBeenCalled();
      expect(senderService.get).toHaveBeenCalledWith(defaultSender.id, {permissions: ['createFile']});
      expect(component.widget.settings._senderId).toEqual(defaultSender.id);
      expect(sender).toEqual(defaultSender);
      done();
    });
  });

  it('should set default values to parentForm', fakeAsync(() => {
    // when
    fixture.detectChanges();
    tick();

    // then
    expect(component.parentForm.get('_fileId').value).toEqual('');
    expect(component.parentForm.get('_hidePreview').value).toBeFalsy();
    expect(component.parentForm.get('_hideDate').value).toBeFalsy();
    expect(component.parentForm.get('_hideSender').value).toBeFalsy();
    expect(component.parentForm.get('_hideDownload').value).toBeFalsy();
  }));

  it('should set default values retrieved from setting sto parentForm', fakeAsync(() => {
    // given
    const fileId = 'file-ID';
    component.widget.settings._fileId = fileId;
    component.widget.settings._hidePreview = true;
    component.widget.settings._hideDate = true;
    component.widget.settings._hideSender = true;
    component.widget.settings._hideDownload = true;

    // when
    fixture.detectChanges();
    tick();

    // then
    expect(component.parentForm.get('_fileId').value).toEqual(fileId);
    expect(component.parentForm.get('_hidePreview').value).toBeTruthy();
    expect(component.parentForm.get('_hideDate').value).toBeTruthy();
    expect(component.parentForm.get('_hideSender').value).toBeTruthy();
    expect(component.parentForm.get('_hideDownload').value).toBeTruthy();
  }));
});
