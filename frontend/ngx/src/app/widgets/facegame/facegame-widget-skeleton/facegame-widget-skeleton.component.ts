import {ChangeDetectionStrategy, Component} from '@angular/core';

/**
 * Renders the skeleton for the facegame widget.
 */
@Component({
  selector: 'coyo-facegame-widget-skeleton',
  template: '<div class="container"><div class="skeleton"></div></div>',
  styleUrls: ['./facegame-widget-skeleton.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FacegameWidgetSkeletonComponent {
}
