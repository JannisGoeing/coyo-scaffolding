import {Facegame, RankingEntry} from '@widgets/facegame/facegame-widget';

export interface FacegameState {
  isRunning: boolean;
  isLoading: boolean;
  isNoMoreColleagues: boolean;
  game: Facegame;
  avatarTeaserUserIds: Set<string>;
  ranking: RankingEntry[];
}
