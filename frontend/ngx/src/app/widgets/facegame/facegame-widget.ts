import {User} from '@domain/user/user';
import {Widget} from '@domain/widget/widget';
import {WidgetSettings} from '@widgets/api/widget-settings/widget-settings';

export interface Facegame {
  answers: FacegameUser[];
  currentAnswer: number;
  endDate: number;
  facegameId: string;
  imageUrl: string;
  lastAnswerCorrect: boolean;
  multiplier: number;
  percentage: number;
  points: number;
  remainingMilliSeconds: number;
  startDate: number;
  started: boolean;
  totalMilliSeconds: number;
}

export interface FacegameUser {
  id: string;
  displayName: string;
}

export interface RankingEntry {
  user: User;
  points: number;
}

export interface FacegameWidgetSettings extends WidgetSettings {
  _senderId: string;
  _time: number;
  _includeExternal: boolean;
}

export interface FacegameWidget extends Widget<FacegameWidgetSettings> {
}
