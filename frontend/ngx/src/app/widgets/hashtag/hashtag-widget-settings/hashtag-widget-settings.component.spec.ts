import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {HashtagWidgetSettingsComponent} from './hashtag-widget-settings.component';

describe('HashtagWidgetSettingsComponent', () => {
  let component: HashtagWidgetSettingsComponent;
  let fixture: ComponentFixture<HashtagWidgetSettingsComponent>;
  let translateService: jasmine.SpyObj<any>;

  let widget: any;
  let parentForm: FormGroup;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HashtagWidgetSettingsComponent],
      providers: [FormBuilder, {
        provide: TranslateService,
        useValue: jasmine.createSpyObj('translateService', ['get'])
      }]
    }).overrideTemplate(HashtagWidgetSettingsComponent, '')
      .compileComponents();

    translateService = TestBed.get(TranslateService);
  }));

  beforeEach(() => {
    translateService.get.and.returnValue(of({'WIDGET.HASHTAG.SETTINGS.PERIOD.ONE_MONTH': '1 Monat'}));
    widget = {settings: {}};
    parentForm = new FormGroup({});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HashtagWidgetSettingsComponent);
    component = fixture.componentInstance;
    component.widget = widget;
    component.parentForm = parentForm;
    fixture.detectChanges();
  });

  it('should validate settings', () => {
    component.ngOnInit();
    expect(component.parentForm.valid).toBeTruthy();
    expect(component.parentForm.value._period.id).toEqual('ONE_MONTH');
  });
});
