import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {CompleteProfileWidgetService} from './complete-profile-widget.service';
import {ProfileInfo} from './profileInfo.model';

describe('CompleteProfileWidgetService', () => {
  let service: CompleteProfileWidgetService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CompleteProfileWidgetService],
      imports: [HttpClientTestingModule]
    });

    service = TestBed.get(CompleteProfileWidgetService);
    http = TestBed.get(HttpTestingController);
   });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should make http request', done => {
    // given
    const resp = {
      profileFields: true,
      avatar: false,
      cover: true,
      followingUser: false,
      pageMember: true,
      createdPost: false
    } as ProfileInfo;

    // when
    const result = service.getCompleteProfileTasksValue();

    // then
    result.subscribe(response => {
      expect(response).toBe(resp);
      done();
    });

    const req = http.expectOne(request => request.method === 'GET' && request.url === '/web/widgets/complete-profile');
    req.flush(resp);
  });
});
