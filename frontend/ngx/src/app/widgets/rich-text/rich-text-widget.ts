import {Widget} from '@domain/widget/widget';
import {RichTextWidgetSettings} from '@widgets/rich-text/rich-text-widget-settings';

export interface RichTextWidget extends Widget<RichTextWidgetSettings> {

}
