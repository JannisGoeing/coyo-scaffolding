import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SettingsService} from '@domain/settings/settings.service';
import {of} from 'rxjs';
import {BookmarkingEditFormComponent} from './bookmarking-edit-form.component';

describe('BookmarkingEditFormComponent', () => {
  let component: BookmarkingEditFormComponent;
  let fixture: ComponentFixture<BookmarkingEditFormComponent>;

  let settingsService: jasmine.SpyObj<SettingsService>;
  const bookmark = {url: 'http://www.coyoapp.com', title: 'COYO'};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: SettingsService,
        useValue: jasmine.createSpyObj('SettingsService', ['retrieveByKey'])
      }],
      declarations: [BookmarkingEditFormComponent]
    }).overrideTemplate(BookmarkingEditFormComponent, '')
      .compileComponents();
  }));

  beforeEach(() => {
    settingsService = TestBed.get(SettingsService);
    settingsService.retrieveByKey.and.returnValue(of('https?:\\/\\/\\S+'));

    fixture = TestBed.createComponent(BookmarkingEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set URL validator on init', () => {
    // given
    spyOn(component.url, 'setValidators');

    // when
    component.ngOnInit();

    // then
    expect(component.url.setValidators).toHaveBeenCalled();
  });

  it('should toggle from title to URL', () => {
    // given
    spyOn(component.bookmarkChange, 'emit');

    // when
    component.toggleStep();

    // then
    expect(component.bookmarkChange.emit).not.toHaveBeenCalled();
  });

  it('should toggle from URL to title', () => {
    // given
    spyOn(component.bookmarkChange, 'emit');
    component.toggleStep();

    // when
    component.toggleStep();

    // then
    expect(component.bookmarkChange.emit).not.toHaveBeenCalled();
  });

  it('should remove the bookmark', () => {
    // given
    spyOn(component.bookmarkRemove, 'emit');

    // when
    component.removeBookmark();

    // then
    expect(component.bookmarkRemove.emit).toHaveBeenCalled();
  });

  it('should save the bookmark', () => {
    // given
    spyOn(component.bookmarkChange, 'emit');
    component.bookmarkForm.setValue(bookmark);
    fixture.detectChanges();

    // when
    component.saveBookmark();

    // then
    expect(component.bookmarkChange.emit).toHaveBeenCalledWith(bookmark);
    expect(component.bookmarkForm.pristine).toBeTruthy();
  });
});
