import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {Ng1AuthService} from '@root/typings';
import {NG1_AUTH_SERVICE, NG1_LOCAL_STORAGE} from '@upgrade/upgrade.module';
import {UrlService} from '../url/url.service';
import {SecurityInterceptor} from './security-interceptor';

describe('SecurityInterceptor', () => {
  const backendUrl = 'http://host';
  let mockedAuthService: jasmine.SpyObj<Ng1AuthService>;
  let mockedLocalStorage: any;

  beforeEach(() => {
    const mockedUrlService = jasmine.createSpyObj('urlService', ['getBackendUrl', 'isBackendUrlSet',
      'isAbsoluteBackendUrl']);
    mockedUrlService.getBackendUrl.and.returnValue(backendUrl);
    mockedUrlService.isBackendUrlSet.and.returnValue(true);
    mockedUrlService.isAbsoluteBackendUrl.and.returnValue(true);

    mockedAuthService = jasmine.createSpyObj('authService', ['getCurrentUserId', 'validateUserId']);
    mockedLocalStorage = {};

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: UrlService, useValue: mockedUrlService},
        {provide: NG1_AUTH_SERVICE, useValue: mockedAuthService},
        {provide: NG1_LOCAL_STORAGE, useValue: mockedLocalStorage},
        {provide: HTTP_INTERCEPTORS, useClass: SecurityInterceptor, multi: true}
      ]
    });
  });

  it('should add "security" headers', inject([HttpClient, HttpTestingController, NG1_LOCAL_STORAGE],
    (http: HttpClient, mock: HttpTestingController, $localStorage: any) => {
      // given
      $localStorage.clientId = 'client-id';
      mockedAuthService.getCurrentUserId.and.returnValue('user-id');

      // when
      http.get(backendUrl + '/test').subscribe();

      // then
      const request = mock.expectOne(req =>
        req.headers.get('X-Coyo-Client-ID') === 'client-id'
        && req.headers.get('X-Coyo-Current-User') === 'user-id', 'add security header to request');
      request.flush({}, {headers: {'X-Coyo-Current-User': 'new-id'}});
      expect(mockedAuthService.validateUserId).toHaveBeenCalled();
      mock.verify();
    }));
});
