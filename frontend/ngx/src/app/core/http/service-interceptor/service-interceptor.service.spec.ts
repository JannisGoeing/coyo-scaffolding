import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {ServiceRecognitionService} from '@core/http/service-recognition/service-recognition.service';
import {UrlService} from '@core/http/url/url.service';
import {TokenService} from '@core/token/token.service';
import {of} from 'rxjs';
import {ServiceInterceptor} from './service-interceptor.service';

describe('ServiceInterceptorService', () => {
  let tokenService: jasmine.SpyObj<TokenService>;
  let serviceRecognitionService: jasmine.SpyObj<ServiceRecognitionService>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: TokenService, useValue: jasmine.createSpyObj('tokenService', ['getToken'])
      }, {
        provide: ServiceRecognitionService, useValue: jasmine.createSpyObj('serviceRecognitionService', ['getTargetService', 'isServiceDisabled'])
      }, {
        provide: HTTP_INTERCEPTORS, useClass: ServiceInterceptor, multi: true
      }, {
        provide: UrlService, useValue: jasmine.createSpyObj('urlService', ['isBackendUrlSet'])
      }]
    });
    tokenService = TestBed.get(TokenService);
    serviceRecognitionService = TestBed.get(ServiceRecognitionService);
    httpClient = TestBed.get(HttpClient);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should add a token to service requests headers', () => {
    // given
    const url = '/web/service';
    const token = 'token-value';
    serviceRecognitionService.getTargetService.and.returnValue('service');
    serviceRecognitionService.isServiceDisabled.and.returnValue(false);
    tokenService.getToken.and.returnValue(of(token));

    // when
    httpClient.get(url).subscribe();

    // then
    const request = httpTestingController.expectOne(url);
    expect(request.request.headers.get('x-coyo-token')).toBe(token);
    expect(tokenService.getToken).toHaveBeenCalled();
    expect(serviceRecognitionService.getTargetService).toHaveBeenCalledWith(url);
    httpTestingController.verify();
  });
});
