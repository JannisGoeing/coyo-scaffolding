import {HTTP_INTERCEPTORS, HttpClient, HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {inject, TestBed} from '@angular/core/testing';
import {Observable} from 'rxjs';
import {UrlService} from '../url/url.service';
import {BackendInterceptor} from './backend-interceptor';

describe('BackendInterceptor', () => {
  const urlAfterInterceptorExecution = 'INTERCEPTOR_EXECUTED';
  const backendUrl = 'http://testBackend:8080';

  class BackendInterceptorImpl extends BackendInterceptor {
    interceptBackendRequest(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const modifiedRequest = req.clone({url: urlAfterInterceptorExecution});
      return next.handle(modifiedRequest);
    }
  }

  beforeEach(() => {
    const mockedUrlService = jasmine.createSpyObj('urlService', ['getBackendUrl', 'isBackendUrlSet']);
    mockedUrlService.getBackendUrl.and.returnValue(backendUrl);
    mockedUrlService.isBackendUrlSet.and.returnValue(true);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {provide: UrlService, useValue: mockedUrlService},
        {provide: HTTP_INTERCEPTORS, useValue: new BackendInterceptorImpl(mockedUrlService), multi: true}
      ]
    });
  });

  it('should intercept for all requests to /web', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // when
      http.get('/web/some-url').subscribe(() => undefined);
      http.post('/web/some-url', {}).subscribe(() => undefined);
      http.put('/web/some-url', {}).subscribe(() => undefined);
      http.delete('/web/some-url').subscribe(() => undefined);

      // then
      expectInterceptorExecution(mock, 'GET');
      expectInterceptorExecution(mock, 'POST');
      expectInterceptorExecution(mock, 'PUT');
      expectInterceptorExecution(mock, 'DELETE');
      mock.verify();
    }));

  it('should intercept for all requests to configured backendUrl', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // when
      http.get(backendUrl + '/some-url').subscribe(() => undefined);
      http.post(backendUrl + '/some-url', {}).subscribe(() => undefined);
      http.put(backendUrl + '/some-url', {}).subscribe(() => undefined);
      http.delete(backendUrl + '/some-url').subscribe(() => undefined);

      // then
      expectInterceptorExecution(mock, 'GET');
      expectInterceptorExecution(mock, 'POST');
      expectInterceptorExecution(mock, 'PUT');
      expectInterceptorExecution(mock, 'DELETE');
      mock.verify();
    }));

  function expectInterceptorExecution(mock: HttpTestingController, httpMethod: string): void {
    mock.expectOne(req => req.method === httpMethod && req.url === urlAfterInterceptorExecution,
      'Interceptor executed for ' + httpMethod);
  }

  it('should not intercept for all requests not pointing to /web or to the backendUrl', inject([HttpClient, HttpTestingController],
    (http: HttpClient, mock: HttpTestingController) => {
      // when
      http.get('/1').subscribe(() => undefined);
      http.post('/2', {}).subscribe(() => undefined);
      http.put('/3', {}).subscribe(() => undefined);
      http.delete('/4').subscribe(() => undefined);

      // then
      mock.expectOne('/1');
      mock.expectOne('/2');
      mock.expectOne('/3');
      mock.expectOne('/4');
      mock.verify();
    }));
});
