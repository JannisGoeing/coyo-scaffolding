import {TestBed} from '@angular/core/testing';
import {AuthService} from '@core/auth/auth.service';
import {LocalStorageService} from '@core/storage/local-storage/local-storage.service';
import {Subject} from 'rxjs';
import {UserStorageService} from './user-storage.service';

describe('UserStorageService', () => {

  let localStorageService: jasmine.SpyObj<LocalStorageService>;
  let authService: jasmine.SpyObj<AuthService>;
  let authenticatedSubject: Subject<boolean>;
  let service: UserStorageService;

  beforeEach(() => {
    authService = jasmine.createSpyObj('authService', ['isAuthenticated$']);
    localStorageService = jasmine.createSpyObj('localStorageService', ['getValue', 'setValue', 'deleteEntry']);

    TestBed.configureTestingModule({
      providers: [UserStorageService, {
        provide: LocalStorageService,
        useValue: localStorageService
      }, {
        provide: AuthService,
        useValue: authService
      }]
    });

    authenticatedSubject = new Subject<boolean>();
    authService.isAuthenticated$.and.returnValue(authenticatedSubject);

    service = TestBed.get(UserStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should init', () => {
    // when
    service.init();

    // then
    expect(localStorageService.getValue).toHaveBeenCalledWith(UserStorageService.USER_STORAGE_KEY);
  });

  it('should clear the user storage when not authenticated', () => {
    // when
    service.init();
    authenticatedSubject.next(false);

    // then
    expect(localStorageService.deleteEntry).toHaveBeenCalledWith(UserStorageService.USER_STORAGE_KEY);
  });

  it('should not clear the user storage when authenticated', () => {
    // when
    service.init();
    authenticatedSubject.next(true);

    // then
    expect(localStorageService.deleteEntry).not.toHaveBeenCalledWith(UserStorageService.USER_STORAGE_KEY);
  });

  it('should delete entries', () => {
    // given
    localStorageService.getValue.and.returnValue({
      test: 'test',
      anotherKey: 'another value'
    });
    service.init();

    // when
    service.deleteEntry('test');

    // then
    expect(localStorageService.setValue)
      .toHaveBeenCalledWith(UserStorageService.USER_STORAGE_KEY, {anotherKey: 'another value'});
    expect(service.getValue('anotherKey')).toEqual('another value');
    expect(service.getValue('test')).toBeUndefined();
  });

  it('should set value', () => {
    // given
    service.init();

    // when
    service.setValue('test', 'test');

    // then
    expect(localStorageService.setValue)
      .toHaveBeenCalledWith(UserStorageService.USER_STORAGE_KEY, {test: 'test'});
    expect(service.getValue('test')).toEqual('test');
  });
});
