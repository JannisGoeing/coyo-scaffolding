import {TestBed} from '@angular/core/testing';
import {WINDOW} from '@root/injection-tokens';
import {SessionStorageService} from './session-storage.service';

describe('SessionStorageService', () => {
  let service: SessionStorageService;
  let sessionStorage: jasmine.SpyObj<Storage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionStorageService, {
        provide: WINDOW,
        useValue: {sessionStorage: jasmine.createSpyObj('sessionStorage', ['setItem', 'getItem'])}
      }]
    });

    service = TestBed.get(SessionStorageService);

    sessionStorage = TestBed.get(WINDOW).sessionStorage;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set an item to the session storage', () => {
    // when
    service.setValue('test', 'test-value');

    // then
    expect(sessionStorage.setItem).toHaveBeenCalledWith('ngStorage-test', '"test-value"');
  });

  it('should get an item from the session storage', () => {
    sessionStorage.getItem.and.returnValue('"test-value"');

    // when
    const result = service.getValue<string>('test', 'test-fallback');

    // then
    expect(result).toBe('test-value');
    expect(sessionStorage.getItem).toHaveBeenCalledWith('ngStorage-test');
  });

  it('should get the fallback value for a missing item', () => {
    sessionStorage.getItem.and.returnValue(null);

    // when
    const result = service.getValue<string>('test', 'test-fallback');

    // then
    expect(result).toBe('test-fallback');
    expect(sessionStorage.getItem).toHaveBeenCalledWith('ngStorage-test');
  });
});
