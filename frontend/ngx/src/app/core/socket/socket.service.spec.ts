import {async, TestBed} from '@angular/core/testing';
import {Ng1SocketService} from '@root/typings';
import {NG1_SOCKET_SERVICE} from '@upgrade/upgrade.module';
import {SocketService} from './socket.service';

describe('SocketService', () => {

  let subscriptionCallbackSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: NG1_SOCKET_SERVICE, useValue: jasmine.createSpyObj('socketService', ['subscribe'])
      }]
    });

    const socketService = TestBed.get(NG1_SOCKET_SERVICE);
    subscriptionCallbackSpy = jasmine.createSpy();
    socketService.subscribe.and.returnValue(subscriptionCallbackSpy);
  });

  it('should subscribe', async(() => {
    const service: SocketService = TestBed.get(SocketService);
    const ng1Service: jasmine.SpyObj<Ng1SocketService> = TestBed.get(NG1_SOCKET_SERVICE);
    service.listenTo$('some-topic').subscribe(() => {
      expect(ng1Service.subscribe).toHaveBeenCalledWith('some-topic');
    });
  }));

  it('should unsubscribe', async(() => {
    const service: SocketService = TestBed.get(SocketService);
    const ng1Service: jasmine.SpyObj<Ng1SocketService> = TestBed.get(NG1_SOCKET_SERVICE);
    const listen$ = service.listenTo$('some-topic').subscribe(() => {
      expect(ng1Service.subscribe).toHaveBeenCalledWith('some-topic');
    });

    listen$.unsubscribe();
    expect(subscriptionCallbackSpy).toHaveBeenCalled();
  }));
});
