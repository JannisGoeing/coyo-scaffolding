import {TestBed} from '@angular/core/testing';

import {TimeProviderService} from './time-provider.service';

describe('TimeProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TimeProviderService = TestBed.get(TimeProviderService);
    expect(service).toBeTruthy();
  });
});
