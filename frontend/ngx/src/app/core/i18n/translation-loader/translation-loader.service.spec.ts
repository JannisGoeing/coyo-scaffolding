import {inject, TestBed} from '@angular/core/testing';
import {NG1_COYO_TRANSLATION_LOADER} from '@upgrade/upgrade.module';
import {TranslationLoaderService} from './translation-loader.service';

describe('TranslationLoaderService', () => {
  let translationLoaderService: TranslationLoaderService;
  let coyoTranslationLoader: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TranslationLoaderService,
        {provide: NG1_COYO_TRANSLATION_LOADER, useValue: jasmine.createSpy('coyoTranslationLoader')}
      ]
    });
    translationLoaderService = TestBed.get(TranslationLoaderService);
    coyoTranslationLoader = TestBed.get(NG1_COYO_TRANSLATION_LOADER);
  });

  it('should be created', inject([TranslationLoaderService], (service: TranslationLoaderService) => {
    // then
    expect(service).toBeTruthy();
  }));

  it('should get translations', (done: DoneFn) => {
    // given
    const ng1Message = 'ng1 message';
    coyoTranslationLoader.and.returnValue(Promise.resolve({TEST: ng1Message}));

    // when
    const result = translationLoaderService.getTranslation('de');

    // then
    result.subscribe(data => {
      expect(data).toBeTruthy();
      expect(data['TEST']).toBe(ng1Message);
      done();
    });
  });
});
