import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Page} from '@domain/page/page';
import {PageService} from '@domain/page/page.service';

describe('PageService', () => {
  let pageService: PageService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PageService,
        {
          provide: UrlService,
          useValue: jasmine.createSpyObj('UrlService', ['join'])
        }]
    });

    pageService = TestBed.get(PageService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should perform a bulk page request', fakeAsync(() => {
    // given
    const ids = ['a', 'b', 'c'];

    // when
    pageService.getBulk(ids)
      .subscribe();
    tick();

    // then
    httpMock.expectOne(`/web/pages?pageIds=a&pageIds=b&pageIds=c`);
    discardPeriodicTasks();
  }));

  it('should produce and empty array output for an empty array parameter', fakeAsync(() => {
    // given
    const ids: string[] = [];
    let result: Page[] = null;
    // when
    pageService.getBulk(ids)
      .subscribe(res => result = res);
    tick();

    // then
    httpMock.expectNone(`/web/pages`);
    httpMock.expectNone(`/web/pages?`);
    expect(result).toEqual([]);
    discardPeriodicTasks();
  }));
});
