import {App} from '@domain/apps/app';
import {BaseModel} from '@domain/base-model/base-model';
import {Permissions} from '@domain/permissions/permissions';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';

/**
 * A shared article models the data of a shared blog or wiki article in a timeline item.
 */
export interface SharedArticle extends BaseModel {
  author: Sender;
  typeName: string;
  articleTarget: Target;
  app: App;
  sender: Sender;
  title: string;
  deleted: boolean;
  _permissions?: Permissions;
}
