import {File} from './file';

/**
 * A document is a file with actual content (for example an image).
 */
export interface Document extends File {
  id: string;
  senderId: string;
  contentType: string;
}
