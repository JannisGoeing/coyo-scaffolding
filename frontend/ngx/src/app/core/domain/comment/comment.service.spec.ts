import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Comment} from '@domain/comment/comment';
import {Page} from '@domain/pagination/page';
import * as _ from 'lodash';
import {CommentService} from './comment.service';

describe('CommentService', () => {
  let commentService: CommentService;
  let httpMock: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CommentService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('urlService', ['join'])
      }]
    });

    commentService = TestBed.get(CommentService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);

    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should return the correct URL', () => {
    // when
    const url = commentService.getUrl();

    // then
    expect(url).toEqual('/web/comments');
  });

  it('should delay the like info request for ' + CommentService.THROTTLE + 'ms', fakeAsync(() => {
    // given
    const targetId = 'target-id';
    const targetType = 'test-type';
    let emitted = false;
    // when
    const observable = commentService.getInitialPage(targetId, targetType);

    // then
    const subscription = observable.subscribe(
      () => {
        emitted = true;
      }
    );
    tick();
    // => There should be no request because the debounce time is not yet over
    httpMock.verify();
    subscription.unsubscribe();
    discardPeriodicTasks();
    expect(emitted).toBeFalsy();
  }));

  it('should send a bulked request after waiting ' + CommentService.THROTTLE + 'ms', fakeAsync(() => {
    // given
    const targetId = 'target-id';
    const targetId2 = 'target-id2';
    const targetType = 'test-type';
    const responseBody: { [key: string]: Page<Comment> } = {};
    responseBody[targetId] = {content: [{message: 'comment1'}]} as Page<Comment>;
    responseBody[targetId2] = {content: [{message: 'comment2'}]} as Page<Comment>;

    // when
    const observable = commentService.getInitialPage(targetId, targetType);
    const subscription = observable.subscribe(result => expect(result.content[0].message).toBe('comment1'));
    const observable2 = commentService.getInitialPage(targetId2, targetType);
    const subscription2 = observable2.subscribe(result => expect(result.content[0].message).toBe('comment2'));
    tick(CommentService.THROTTLE);

    // then
    const request = httpMock.expectOne(req => req.url === `/web/comments/${targetType}`
      && req.params.getAll('ids').indexOf(targetId) > -1
      && req.params.getAll('ids').indexOf(targetId2) > -1
      && req.params.get('_permissions') === '*'
    );
    request.flush(responseBody);
    subscription.unsubscribe();
    subscription2.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should request the original author', () => {
    // when
    commentService.getOriginalAuthor('comment-id').subscribe(author => {
      expect(author.id).toBe('author-id');
    });

    // then
    const req = httpMock.expectOne('/web/comments/comment-id/original-author');
    req.flush({id: 'author-id'});
  });
});
