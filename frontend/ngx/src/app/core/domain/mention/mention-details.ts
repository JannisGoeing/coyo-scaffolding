/**
 * A mentioned item.
 */
export interface MentionDetails {
  slug: string;
  name: string;
  link: string | null;
  cached: number | null;
}
