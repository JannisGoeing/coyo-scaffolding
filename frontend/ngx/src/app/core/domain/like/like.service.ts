import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {SocketService} from '@core/socket/socket.service';
import {DomainService} from '@domain/domain/domain.service';
import {EntityId} from '@domain/entity-id/entity-id';
import {Sender} from '@domain/sender/sender';
import * as _ from 'lodash';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';
import {bufferTime, debounceTime, map, tap} from 'rxjs/operators';
import {Like} from './like';
import {LikeInfo} from './like-info';
import {LikeState} from './like-state';

interface LikeInfoRequest {
  senderId: string;
  targetType: string;
  requestSubject: Subject<string>;
  subscriptions: { [key: string]: BehaviorSubject<LikeState> };
  socketSubscriptions: { [key: string]: Observable<any>};
}

interface LikeInfoEvent {
  sender: Sender;
  target: EntityId;
  count: number;
  created: boolean;
}

/**
 * Service to retrieve and manage likes.
 */
@Injectable({
  providedIn: 'root'
})
export class LikeService extends DomainService<Like, Like> {

  static readonly INITIAL_STATE: LikeState = {
    isLoading: false,
    isLiked: false,
    total: [],
    totalCount: 0,
    others: [],
    othersCount: 0
  };

  /**
   * The number of milliseconds to throttle GET requests of this service.
   */
  static readonly THROTTLE: number = 50;

  /*
   * This array of possible info requests will not be cleared when the requests
   * are completed. They are reused when another request with the same key comes
   * in. We expect a rather small number of different throttled requests and thus
   * take the risk of a minimal memory leak here.
   */
  private subjects: { [key: string]: LikeInfoRequest } = {};

  constructor(http: HttpClient, urlService: UrlService, private socketService: SocketService) {
    super(http, urlService);
  }

  /**
   * Likes the given target as the given sender.
   *
   * @param senderId the ID of the acting sender
   * @param targetId the ID of the target
   * @param targetType the type of the target
   * @return an `Observable` holding the the latest like information
   */
  like(senderId: string, targetId: string, targetType: string): Observable<LikeInfo> {
    const key = this.getKey(senderId, targetType);
    const subject = this.subjects[key];

    if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
      const value = subject.subscriptions[targetId].getValue();
      subject.subscriptions[targetId].next({...value, isLoading: true});
    }

    return this.http.post<LikeInfo>(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`, null)
      .pipe(tap(likeInfo => {
        if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
          subject.subscriptions[targetId].next(this.toLikeState(likeInfo, senderId));
        }
      }));
  }

  /**
   * Unlikes the given target as the given sender.
   *
   * @param senderId the ID of the acting sender
   * @param targetId the ID of the target
   * @param targetType the type of the target
   * @return an `Observable` holding the the latest like information
   */
  unlike(senderId: string, targetId: string, targetType: string): Observable<LikeInfo> {
    const key = this.getKey(senderId, targetType);
    const subject = this.subjects[key];

    if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
      const value = subject.subscriptions[targetId].getValue();
      subject.subscriptions[targetId].next({...value, isLoading: true});
    }

    return this.http.delete<LikeInfo>(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`)
      .pipe(tap(likeInfo => {
        if (subject && subject.subscriptions && subject.subscriptions[targetId]) {
          subject.subscriptions[targetId].next(this.toLikeState(likeInfo, senderId));
        }
      }));
  }

  /**
   * Retrieves the like state for the given target as the given sender. Like information is
   * retrieved initially and updated via socket. If the target was liked or unliked by the sender
   * the state change will be emitted immediately.
   *
   * You need to unsubscribe to prevent memory leaks.
   *
   * @param senderId the ID of the acting sender
   * @param targetId the ID of the target
   * @param targetType the type of the target
   * @param token The websocket subscription token for the target
   * @param skipInitRequest flag for skipping the initial request which is done immediately
   * @return an `Observable` holding the the like state
   */
  getLikeTargetState$(senderId: string,
                      targetId: string,
                      targetType: string,
                      token: string,
                      skipInitRequest: boolean = false): Observable<LikeState> {
    const key = this.getKey(senderId, targetType);
    const subject = this.subjects[key] || this.buildRequest(senderId, targetType);

    if (!subject.subscriptions[targetId]) {
      subject.subscriptions[targetId] = new BehaviorSubject({...LikeService.INITIAL_STATE, ...{isLoading: true}});
    }

    if (!skipInitRequest) {
      subject.requestSubject.next(targetId);
    } else {
      subject.subscriptions[targetId].next({
        ...subject.subscriptions[targetId].getValue(),
        ...{isLoading: false}
      });
    }

    if (!subject.socketSubscriptions[targetId]) {
      subject.socketSubscriptions[targetId] = this.setupSocket(targetId, token, subject);
    }

    return new Observable((subscriber: Subscriber<LikeState>) => {
      const targetSubject = subject.subscriptions[targetId];
      targetSubject.subscribe(subscriber);
      const socketSub = subject.socketSubscriptions[targetId].subscribe();
      return () => {
        socketSub.unsubscribe();
        if (targetSubject.observers.length === 0) {
          subject.subscriptions[targetId] = undefined;
          subject.socketSubscriptions[targetId] = undefined;
        }
      };
    });
  }

  private setupSocket(targetId: string, token: string, subject: LikeInfoRequest): Observable<any> {
    return this.socketService.listenTo$('/topic/like.' + subject.targetType, null, targetId, token)
      .pipe(
        map(event => event.content as LikeInfoEvent),
        bufferTime(1000),
        tap((infos: LikeInfoEvent[]) => {
          const subscription = subject.subscriptions[targetId];
          const likeState = {
            ...subscription.getValue()
          };
          infos.forEach(info => {
            if (info.created) {
              likeState.isLiked = likeState.isLiked || subject.senderId === info.sender.id;
              likeState.total = [...likeState.total, info.sender];
            } else {
              if (likeState.isLiked && subject.senderId === info.sender.id) {
                likeState.isLiked = false;
              }
              likeState.total = _.reject(likeState.total, {id: info.sender.id});
            }

            likeState.totalCount = info.count;
          });
          likeState.others = _.reject(likeState.total, {id: subject.senderId});
          likeState.othersCount = likeState.totalCount - (likeState.isLiked ? 1 : 0);
          subject.subscriptions[targetId].next(likeState);
        }));
  }

  protected getBaseUrl(): string {
    return '/web/like-targets/{targetType}/{targetId}/likes';
  }

  private setup(request: LikeInfoRequest): void {
    const buffer: Set<string> = new Set();
    request.requestSubject
      .pipe(map(value => this.collect(buffer, value)))
      .pipe(debounceTime(LikeService.THROTTLE))
      .pipe(tap(() => buffer.clear()))
      .subscribe(ids => this.request(request.senderId, request.targetType, ids)
        .subscribe(
          likeInfos => this.sendResponses(request.senderId, request.targetType, likeInfos),
          () => this.revertLoading(request.senderId, request.targetType, ids)));
  }

  private revertLoading(senderId: string, targetType: string, ids: string[]): void {
    ids.forEach(targetId => {
      const key = this.getKey(senderId, targetType);
      const subject = this.subjects[key];
      if (subject && subject.subscriptions[targetId]) {
        const subscription = subject.subscriptions[targetId];
        subscription.next({
          ...subscription.getValue(),
          ...{isLoading: false}
        });
      }
    });
  }

  private sendResponses(senderId: string, targetType: string, likeInfos: { [key: string]: LikeInfo }): void {
    _.keys(likeInfos).forEach(targetId => {
      const key = this.getKey(senderId, targetType);
      const subject = this.subjects[key];
      if (subject && subject.subscriptions[targetId]) {
        subject.subscriptions[targetId].next(this.toLikeState(likeInfos[targetId], senderId));
      }
    });
  }

  private toLikeState(likeInfo: LikeInfo, senderId: string): LikeState {
    return {
      isLoading: false,
      isLiked: likeInfo.likedBySender,
      total: likeInfo.latest,
      totalCount: likeInfo.count,
      others: _.reject(likeInfo.latest, {id: senderId}),
      othersCount: likeInfo.count - (likeInfo.likedBySender ? 1 : 0)
    };
  }

  private buildRequest(senderId: string, targetType: string): LikeInfoRequest {
    const subject: LikeInfoRequest = {
      senderId,
      targetType,
      requestSubject: new Subject(),
      subscriptions: {},
      socketSubscriptions: {}
    };
    const key = this.getKey(senderId, targetType);
    this.subjects[key] = subject;
    this.setup(subject);
    return subject;
  }

  private collect(set: Set<string>, value: string): string[] {
    set.add(value);
    return Array.from(set);
  }

  private request(senderId: string, targetType: string, targetIds: string[]): Observable<{ [key: string]: LikeInfo }> {
    return this.http.get<{ [key: string]: LikeInfo }>(`/web/like-targets/${targetType}`, {
      headers: {
        etagBulkId: 'ids'
      },
      params: {
        senderId,
        ids: targetIds
      }
    });
  }

  private getKey(senderId: string, targetType: string): string {
    return _.join([targetType, senderId], '-');
  }
}
