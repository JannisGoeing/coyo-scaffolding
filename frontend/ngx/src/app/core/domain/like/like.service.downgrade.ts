import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {LikeService} from './like.service';

getAngularJSGlobal()
  .module('commons.ui')
  .factory('ngxLikeService', downgradeInjectable(LikeService) as any);
