import {Sender} from './../sender/sender';
import {Target} from './../sender/target';

/**
 * A single like of a sender on a likeable entity.
 */
export interface Like {
  sender: Sender;
  target: Target;
}
