import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {discardPeriodicTasks, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {Sender} from '@core/domain/sender/sender';
import {UrlService} from '@core/http/url/url.service';
import {SocketService} from '@core/socket/socket.service';
import * as _ from 'lodash';
import {Subject} from 'rxjs';
import {skip} from 'rxjs/operators';
import {LikeInfo} from './like-info';
import {LikeState} from './like-state';
import {LikeService} from './like.service';

describe('LikeService', () => {
  let likeService: LikeService;
  let httpMock: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;
  let socketService: jasmine.SpyObj<SocketService>;
  const createdSubject: Subject<any> = new Subject();

  const senderId = 'sender-id';
  const targetId = 'target-id';
  const targetType = 'test-type';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LikeService, {
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }, {
        provide: SocketService,
        useValue: jasmine.createSpyObj('SocketService', ['listenTo$'])
      }]
    });

    likeService = TestBed.get(LikeService);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpMock = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
    parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
    socketService = TestBed.get(SocketService);
    socketService.listenTo$.and.returnValue(createdSubject);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should like a target', () => {
    // given
    const likeInfo = {
      count: 12,
      likedBySender: true
    } as LikeInfo;

    // when
    const observable = likeService.like(senderId, targetId, targetType);

    // then
    observable.subscribe(result => {
      expect(result).toBe(likeInfo);
    });
    const req = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);
    expect(req.request.method).toBe('POST');
    req.flush(likeInfo);
  });

  it('should unlike a target', () => {
    // when
    const observable = likeService.unlike(senderId, targetId, targetType);

    // then
    observable.subscribe();
    const req = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(null);
  });

  it('should delay the like info request for ' + LikeService.THROTTLE + 'ms', fakeAsync(() => {
    // when
    const observable = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token');

    // then
    const subscription = observable.subscribe();
    // => There should be no request because the debounce time is not yet over
    httpMock.verify();
    subscription.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should send a bulked request after waiting ' + LikeService.THROTTLE + 'ms', fakeAsync(() => {
    // given
    const targetId2 = 'target-id2';
    const responseBody: {[key: string]: LikeInfo} = {};
    responseBody[targetId] = {
      count: 5
    } as LikeInfo;
    responseBody[targetId2] = {
      count: 2
    } as LikeInfo;

    // when
    const observable = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token').pipe(skip(1));
    const subscription = observable.subscribe(result => expect(result.totalCount).toBe(5));
    const observable2 = likeService.getLikeTargetState$(senderId, targetId2, targetType, 'token').pipe(skip(1));
    const subscription2 = observable2.subscribe(result => expect(result.totalCount).toBe(2));
    tick(LikeService.THROTTLE);

    // then
    const request = httpMock.expectOne(req => req.url === `/web/like-targets/${targetType}` &&
      req.params.getAll('ids').indexOf(targetId) > -1 &&
      req.params.getAll('ids').indexOf(targetId2) > -1
    );
    request.flush(responseBody);

    tick();

    subscription.unsubscribe();
    subscription2.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should create a second request when a new target id comes in while requesting', fakeAsync(() => {
    // given
    const targetId2 = 'target-id2';
    const responseBody: {[key: string]: LikeInfo} = {};
    responseBody[targetId] = {
      count: 5
    } as LikeInfo;

    const responseBody2: {[key: string]: LikeInfo} = {};
    responseBody2[targetId2] = {
      count: 2
    } as LikeInfo;

    // when
    const observable = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token');

    const subscription = observable.pipe(skip(1)).subscribe(result =>
      expect(result.totalCount).toBe(5)
    );
    tick(LikeService.THROTTLE);

    const observable2 = likeService.getLikeTargetState$(senderId, targetId2, targetType, 'token');

    const subscription2 = observable2.pipe(skip(1)).subscribe(result =>
      expect(result.totalCount).toBe(2)
    );

    const request = httpMock.expectOne(req => req.url === `/web/like-targets/${targetType}` &&
      req.params.getAll('ids').indexOf(targetId) > -1 &&
      req.params.getAll('ids').indexOf(targetId2) === -1
    );

    request.flush(responseBody);

    tick(LikeService.THROTTLE);

    // then
    const request2 = httpMock.expectOne(req => req.url === `/web/like-targets/${targetType}` &&
      req.params.getAll('ids').indexOf(targetId) === -1 &&
      req.params.getAll('ids').indexOf(targetId2) > -1
    );

    request2.flush(responseBody2);

    subscription.unsubscribe();
    subscription2.unsubscribe();
    discardPeriodicTasks();
  }));

  it('should emit a loading state on subscribing', fakeAsync(() => {
    // given
    let stateEmitted = false;

    // when
    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token')
      .subscribe(state => {
        stateEmitted = true;
        expect(state.isLoading).toBeTruthy();
    });
    tick();

    // then
    subscription.unsubscribe();
    expect(stateEmitted).toBeTruthy();
    discardPeriodicTasks();
  }));

  it('should emit changes to observers when liking', fakeAsync(() => {
    // given
    const states: LikeState[] = [];
    const likeInfo = {
      likedBySender: true
    } as LikeInfo;

    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token').pipe(skip(1))
      .subscribe(state => states.push(state));

    // when
    likeService.like(senderId, targetId, targetType).subscribe();

    const req = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);

    req.flush(likeInfo);

    // then
    subscription.unsubscribe();
    expect(states.length).toBe(2);
    expect(states[0].isLoading).toBeTruthy();
    expect(states[1].isLiked).toBeTruthy();
    expect(states[1].isLoading).toBeFalsy();
    discardPeriodicTasks();
  }));

  it('should emit changes to observers when unliking', fakeAsync(() => {
    // given
    const states: LikeState[] = [];
    const likeInfo = {
      likedBySender: false
    } as LikeInfo;

    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token').pipe(skip(2))
      .subscribe(state => states.push(state));

    tick(LikeService.THROTTLE);
    const request = httpMock.expectOne(req =>
        req.url === `/web/like-targets/${targetType}` && req.params.getAll('ids').indexOf(targetId) > -1
    );

    request.flush({'target-id': {likedBySender: true}});

    // when
    likeService.unlike(senderId, targetId, targetType).subscribe();

    const request2 = httpMock.expectOne(`/web/like-targets/${targetType}/${targetId}/likes/${senderId}`);

    request2.flush(likeInfo);

    // then
    subscription.unsubscribe();
    expect(states.length).toBe(2);
    expect(states[0].isLoading).toBeTruthy();
    expect(states[0].isLiked).toBeTruthy();
    expect(states[1].isLiked).toBeFalsy();
    expect(states[1].isLoading).toBeFalsy();
    discardPeriodicTasks();
  }));

  it('should revert the loading state when error occurs', fakeAsync(() => {
    // given
    let stateEmitted = false;

    // when
    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token').pipe(skip(1))
      .subscribe(state => {
        stateEmitted = true;
        expect(state.isLoading).toBeFalsy();
      });

    tick(LikeService.THROTTLE);
    const request = httpMock.expectOne(req =>
      req.url === `/web/like-targets/${targetType}` && req.params.getAll('ids').indexOf(targetId) > -1
    );

    request.error(new ErrorEvent('test error'));

    // then
    subscription.unsubscribe();
    expect(stateEmitted).toBeTruthy();
  }));

  it('should call the right endpoint', () => {
    // when
    const result = likeService.getUrl({
      targetId,
      targetType
    });

    // then
    expect(result).toBe(`/web/like-targets/${targetType}/${targetId}/likes`);
  });

  it('should skip initial request when flag is given', fakeAsync(() => {
    // given

    // when
    likeService.getLikeTargetState$('sender-id', 'target-id', 'target-type', 'token', true);
    tick(LikeService.THROTTLE);

    // then
    httpMock.verify();
    discardPeriodicTasks();
  }));

  it('should subscribe websocket', fakeAsync(() => {
    // given
    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token').pipe(skip(1))
      .subscribe();

    // when
    subscription.unsubscribe();
    expect(socketService.listenTo$).toHaveBeenCalledWith('/topic/like.' + targetType, null, targetId, 'token');
    discardPeriodicTasks();
  }));

  it('should emit changes to observable based on websocket', fakeAsync(() => {
    // given
    const states: LikeState[] = [];

    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token', true).pipe(skip(1))
      .subscribe(state => states.push(state));

    // when
    createdSubject.next({content: {sender: {id: senderId}, created: true, count: 1, target: {id: targetId, typeName: targetType}}});

    // then
    tick(1000);
    subscription.unsubscribe();
    expect(states.length).toBe(1);
    expect(states[0].isLiked).toBeTruthy();
    expect(states[0].isLoading).toBeFalsy();
    expect(states[0].totalCount).toBe(1);
    expect(states[0].othersCount).toBe(0);
    discardPeriodicTasks();
  }));

  it('should buffer websocket changes', fakeAsync(() => {
    // given
    const states: LikeState[] = [];

    const subscription = likeService.getLikeTargetState$(senderId, targetId, targetType, 'token', true).pipe(skip(1))
      .subscribe(state => states.push(state));

    const sender = {id: senderId} as Sender;
    const otherSender = {id: 'sender2'} as Sender;

    // when
    createdSubject.next({content: {sender, created: true, count: 1, target: {id: targetId, typeName: targetType}}});
    createdSubject.next({content: {sender: otherSender, created: true, count: 2, target: {id: targetId, typeName: targetType}}});
    createdSubject.next({content: {sender, created: false, count: 1, target: {id: targetId, typeName: targetType}}});
    tick(1000);

    // then
    expect(states.length).toBe(1);
    expect(states[0].isLiked).toBeFalsy();
    expect(states[0].isLoading).toBeFalsy();
    expect(states[0].totalCount).toBe(1);
    expect(states[0].othersCount).toBe(1);
    expect(states[0].others).toContain(otherSender);

    // when
    createdSubject.next({content: {sender, created: true, count: 2, target: {id: targetId, typeName: targetType}}});
    tick(1000);

    // then
    discardPeriodicTasks();
    subscription.unsubscribe();
    expect(states.length).toBe(2);
    expect(states[1].isLiked).toBeTruthy();
    expect(states[1].isLoading).toBeFalsy();
    expect(states[1].totalCount).toBe(2);
    expect(states[1].othersCount).toBe(1);
    expect(states[1].others).toContain(otherSender);
    expect(states[1].total).toContain(sender, otherSender);
  }));
});
