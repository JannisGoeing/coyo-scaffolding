/**
 * The request data to create a new timeline item.
 */
export interface TimelineItemRequest {
  type: 'post';
  authorId: string;
  recipientIds: string[];
  data: {
    message: string,
    edited: boolean
  };
}
