import {Page} from '@domain/pagination/page';
import {TimelineItem} from '@domain/timeline-item/timeline-item';

/**
 * Response interface for new timeline items which includes the refresh dates of the timeline
 */
export interface NewTimelineItemsResponse {
  data: {
    lastRefreshDate: number;
    newRefreshDate: number;
  };
  page: Page<TimelineItem>;
}
