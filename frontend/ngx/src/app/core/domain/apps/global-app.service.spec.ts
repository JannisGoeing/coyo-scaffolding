import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {DefaultUrlSerializer} from '@angular/router';
import {GlobalAppService} from '@domain/apps/global-app.service';
import {Pageable} from '@domain/pagination/pageable';

describe('GlobalAppService', () => {
  let service: GlobalAppService;
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;
  const deserializer = new DefaultUrlSerializer();
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GlobalAppService]
    });
    service = TestBed.get(GlobalAppService);
    httpTestingController = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);
  });
  afterEach(() => {
    httpTestingController.verify();
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should execute a request by key and name', fakeAsync(() => {
    // given

    // when
    service.get(new Pageable(0, 5), 'key1', 'name1').subscribe();
    tick();
    // then
    httpTestingController.expectOne(req => {
      const urlTree = deserializer.parse(req.urlWithParams);
      return urlTree.queryParams['key'] === 'key1' &&
        urlTree.queryParams['name'] === 'name1' &&
        req.url === '/web/apps';
    });
  }));

  it('should execute a request by key', fakeAsync(() => {
    // given

    // when
    service.getByKey(new Pageable(0, 5), 'key1').subscribe();
    tick();
    // then
    httpTestingController.expectOne(req => {
      const urlTree = deserializer.parse(req.urlWithParams);
      return urlTree.queryParams['key'] === 'key1' &&
        req.url === '/web/apps';
    });
  }));

  it('should execute a request by name', fakeAsync(() => {
    // given

    // when
    service.getByName(new Pageable(0, 5), 'name1').subscribe();
    tick();
    // then
    httpTestingController.expectOne(req => {
      const urlTree = deserializer.parse(req.urlWithParams);
      return urlTree.queryParams['name'] === 'name1' &&
        req.url === '/web/apps';
    });
  }));
});
