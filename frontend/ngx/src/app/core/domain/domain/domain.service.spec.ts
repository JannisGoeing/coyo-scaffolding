import {HttpClient} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Pageable} from '@domain//pagination/pageable';
import {Direction} from '@domain/pagination/direction.enum';
import {Order} from '@domain/pagination/order';
import {Page} from '@domain/pagination/page';
import * as _ from 'lodash';
import {DomainService} from './domain.service';

describe('DomainService', () => {
  let baseUrl: string;
  let service: DomainService<string, string>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  class TestDomainService extends DomainService<string, string> {
    protected getBaseUrl(): string {
      return baseUrl;
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: UrlService,
        useValue: jasmine.createSpyObj('UrlService', ['join'])
      }]
    });

    httpClient = TestBed.get(HttpClient);
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
    urlService = TestBed.get(UrlService);
    urlService.join.and.callFake((...parts: string[]) =>
      parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
  });

  beforeEach(() => {
    baseUrl = '/domain';
    service = new TestDomainService(httpClient, TestBed.get(UrlService));
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should return a contextual URL', () => {
    // given
    baseUrl = '/domain/path1/{part1}/path2/{part2}/path3';

    // when
    const url = service.getUrl({
      part1: 'test',
      part2: '42',
      part3: 'more',
    });

    // then
    expect(url).toEqual('/domain/path1/test/path2/42/path3');
  });

  it('should return a contextual URL with a custom path', () => {
    // given
    baseUrl = '/domain/path1/{part1}/path2/{part2}/path3';

    // when
    const url = service.getUrl({
      part1: 'test',
      part2: '42',
      part3: 'more',
    }, 'custom/path/{part3}');

    // then
    expect(url).toEqual('/domain/path1/test/path2/42/path3/custom/path/more');
  });

  it('should GET all data', () => {
    // given
    const options = {
      context: {part: 'test'},
      path: '/custom/{part}',
      headers: {header: 'value'},
      params: {param: 'value'},
      permissions: ['p1', 'p2'],
      handleErrors: false
    };

    // when
    const response = ['Hello', 'World'];
    const result$ = service.getAll(options);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/domain/custom/test');
    expect(req.request.method).toEqual('GET');
    expect(req.request.headers.get('header')).toEqual('value');
    expect(req.request.params.get('param')).toEqual('value');
    expect(req.request.params.get('_permissions')).toEqual('p1,p2');

    // finally
    req.flush(response);
  });

  it('should GET paged data', () => {
    // given
    const pageable = new Pageable(1, 10, null, new Order('prop1'), new Order('prop2', Direction.Desc));

    const options = {
      context: {part: 'test'},
      path: '/custom/{part}',
      headers: {header: 'value'},
      params: {param: 'value'},
      permissions: ['p1', 'p2'],
      handleErrors: false
    };

    // when
    const response = {content: ['Hello', 'World']} as Page<string>;
    const result$ = service.getPage(pageable, options);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/domain/custom/test');
    expect(req.request.method).toEqual('GET');
    expect(req.request.headers.get('header')).toEqual('value');
    expect(req.request.params.get('param')).toEqual('value');
    expect(req.request.params.get('_permissions')).toEqual('p1,p2');
    expect(req.request.params.get('_page')).toEqual('1');
    expect(req.request.params.get('_pageSize')).toEqual('10');
    expect(req.request.params.get('_orderBy')).toEqual('prop1,ASC,prop2,DESC');

    // finally
    req.flush(response);
  });

  it('should GET data', () => {
    // given
    const id = 'ID';
    const options = {
      context: {part: 'test'},
      path: '/custom/{part}',
      headers: {header: 'value'},
      params: {param: 'value'},
      permissions: ['p1', 'p2'],
      handleErrors: false
    };

    // when
    const response = 'Hello';
    const result$ = service.get(id, options);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/domain/custom/test/' + id);
    expect(req.request.method).toEqual('GET');
    expect(req.request.headers.get('header')).toEqual('value');
    expect(req.request.params.get('param')).toEqual('value');
    expect(req.request.params.get('_permissions')).toEqual('p1,p2');

    // finally
    req.flush(response);
  });

  it('should POST data', () => {
    // given
    const options = {
      context: {part: 'test'},
      path: '/custom/{part}',
      headers: {header: 'value'},
      params: {param: 'value'},
      permissions: ['p1', 'p2'],
      handleErrors: false
    };

    // when
    const response = 'Hello';
    const result$ = service.post('test', options);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/domain/custom/test');
    expect(req.request.method).toEqual('POST');
    expect(req.request.headers.get('header')).toEqual('value');
    expect(req.request.params.get('param')).toEqual('value');
    expect(req.request.params.get('_permissions')).toEqual('p1,p2');

    // finally
    req.flush(response);
  });

  it('should PUT data', () => {
    // given
    const id = 'ID';
    const options = {
      context: {part: 'test'},
      path: '/custom/{part}',
      headers: {header: 'value'},
      params: {param: 'value'},
      permissions: ['p1', 'p2'],
      handleErrors: false
    };

    // when
    const response = 'Hello';
    const result$ = service.put(id, 'test', options);

    // then
    result$.subscribe(data => expect(data).toEqual(response));
    const req = httpTestingController.expectOne(request => request.url === '/domain/custom/test/' + id);
    expect(req.request.method).toEqual('PUT');
    expect(req.request.headers.get('header')).toEqual('value');
    expect(req.request.params.get('param')).toEqual('value');
    expect(req.request.params.get('_permissions')).toEqual('p1,p2');

    // finally
    req.flush(response);
  });

  it('should DELETE data', () => {
    // given
    const id = 'ID';
    const options = {
      context: {part: 'test'},
      path: '/custom/{part}',
      headers: {header: 'value'},
      params: {param: 'value'},
      permissions: ['p1', 'p2'],
      handleErrors: false
    };

    // when
    const response = 'Hello';
    const result$ = service.delete(id, options);

    // then
    result$.subscribe(data => expect(data).toBeNull());
    const req = httpTestingController.expectOne(request => request.url === '/domain/custom/test/' + id);
    expect(req.request.method).toEqual('DELETE');
    expect(req.request.headers.get('header')).toEqual('value');
    expect(req.request.params.get('param')).toEqual('value');
    expect(req.request.params.get('_permissions')).toEqual('p1,p2');

    // finally
    req.flush(response);
  });
});
