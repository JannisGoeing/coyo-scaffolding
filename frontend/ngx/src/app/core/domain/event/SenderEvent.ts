import {ParticipantStatus} from '@domain/event/participant-status';
import {ParticipantsLimit} from '@domain/event/participants-limit';
import {Sender} from '@domain/sender/sender';
import {ShareableSender} from '@domain/sender/shareable-sender';

/**
 * Domain modal for Events
 */
export interface SenderEvent extends ShareableSender {
  attendingCount: number;
  host: Sender;
  endDate: Date;
  fullDay: boolean;
  limitedParticipants: ParticipantsLimit;
  place: string;
  requestDefiniteAnswer: boolean;
  showParticipants: boolean;
  startDate: Date;
  status?: ParticipantStatus;
  name?: string;
}
