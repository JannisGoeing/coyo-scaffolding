import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {UrlService} from '@core/http/url/url.service';
import {DomainService} from '@domain/domain/domain.service';
import {Guest} from '@domain/guest/Guest';
import {UserChooserSelectionConfig} from '@domain/guest/UserChooserSelectionConfig';
import {Page} from '@domain/pagination/page';
import {Pageable} from '@domain/pagination/pageable';
import {Observable} from 'rxjs';

/**
 * Service to search for invites.
 */
@Injectable({
  providedIn: 'root'
})
export class UserChooserService extends DomainService<Guest, Guest> {

  constructor(
    @Inject(HttpClient) protected http: HttpClient,
    @Inject(UrlService) protected urlService: UrlService) {
    super(http, urlService);
  }

  protected getBaseUrl(): string {
    return '/web/users/chooser';
  }

  /**
   * Returns a page of searched guests.
   *
   * @param searchTerm      a search term for guest search
   * @param selectionTypes  the types that the endpoint should return
   * @param pageable        the pagination information
   * @returns an observable with the search result
   */
  searchGuests(searchTerm: string, selectionTypes: UserChooserSelectionConfig, pageable: Pageable): Observable<Page<Guest>> {
    const types = [];
    for (const key in selectionTypes) {
      if (selectionTypes.hasOwnProperty(key) && !!selectionTypes[key]) {
        types.push(this.removeLastCharacter(key));
      }
    }
    return this.getPage(pageable, {path: '/search?term=' + searchTerm + '&types=' + types.join(',')});
  }

  private removeLastCharacter(term: string): string {
    return term.toLowerCase().slice(0, -1);
  }
}
