export interface UserChooserSelectionConfig {
  groups: boolean;
  pages: boolean;
  users: boolean;
  workspaces: boolean;
}
