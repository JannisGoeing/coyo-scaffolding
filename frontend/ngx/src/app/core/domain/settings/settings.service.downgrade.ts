import {downgradeInjectable, getAngularJSGlobal} from '@angular/upgrade/static';
import {SettingsService} from './settings.service';

getAngularJSGlobal()
  .module('coyo.domain')
  .factory('ngxSettingsService', downgradeInjectable(SettingsService));
