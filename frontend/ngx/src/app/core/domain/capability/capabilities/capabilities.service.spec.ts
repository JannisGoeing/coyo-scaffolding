import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {CapabilitiesService} from './capabilities.service';

describe('CapabilitiesService', () => {
  let capabilitiesService: CapabilitiesService;
  let urlServiceMock: jasmine.SpyObj<UrlService>;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CapabilitiesService, {
        provide: UrlService, useValue: jasmine.createSpyObj('urlService', ['join'])
      }]
    });

    capabilitiesService = TestBed.get(CapabilitiesService);
    urlServiceMock = TestBed.get(UrlService);
    urlServiceMock.join.and.callFake((parts: string) => parts.slice(1));
    // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(capabilitiesService).toBeTruthy();
  });

  it('should preview jpeg format', () => {
    // given
    const type = 'image/jpeg';
    // when
    const result = capabilitiesService.previewImageFormat(type);
    // then
    result.subscribe(contentType => {
      expect(contentType).toBe(type);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush(
      [{'image/jpeg': [{details: 'image/jpeg'}]}]
    );
  });

  it('should not preview jpeg format', () => {
    // given
    const type = 'image/jpeg';
    // when
    const result = capabilitiesService.previewImageFormat('noJpegType');
    // then
    result.subscribe(contentType => {
      expect(contentType).not.toBe(type);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush(
      [{'image/jpeg': [{details: 'image/jpeg'}]}]
    );
  });

  it('should preview png format', () => {
    // given
    const type = 'image/png';
    // when
    const result = capabilitiesService.previewImageFormat(type);
    // then
    result.subscribe(contentType => {
      expect(contentType).toBe(type);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush(
      [{'image/png': [{details: 'image/png'}]}]
    );
  });

  it('should not preview png format', () => {
    // given
    const type = 'image/png';
    // when
    const result = capabilitiesService.previewImageFormat('noPngType');
    // then
    result.subscribe(contentType => {
      expect(contentType).not.toBe(type);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush(
      [{'image/png': [{details: 'image/png'}]}]
    );
  });

  it('should pdf be available', () => {
    // given
    const type = 'application/pdf';
    // when
    const result = capabilitiesService.pdfAvailable(type);
    // then
    result.subscribe(available => {
      expect(available).toBe(true);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush([{'application/pdf': [{details: 'application/pdf'}]}]);
  });

  it('should pdf not be available', () => {
    // given
    const type = '';
    // when
    const result = capabilitiesService.pdfAvailable(type);
    // then
    result.subscribe(available => {
      expect(available).not.toBe(true);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush([{'application/pdf': [{details: 'application/pdf'}]}]);
  });

  it('should img be available', () => {
    // given
    const type = 'image/png';
    // when
    const result = capabilitiesService.imgAvailable(type);
    // then
    result.subscribe(available => {
      expect(available).toBe(true);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush([{'image/png': [{details: 'image/png'}]}]);
  });

  it('should img not be available', () => {
    // given
    const type = '';
    // when
    const result = capabilitiesService.imgAvailable(type);
    // then
    result.subscribe(available => {
      expect(available).not.toBe(true);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush([{'image/png': [{details: 'image/png'}]}]);
  });

  it('should img available be false, diferent contentType', () => {
    // given
    const type = 'text/plain';
    // when
    const result = capabilitiesService.imgAvailable(type);
    // then
    result.subscribe(available => {
      expect(available).toBe(false);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush([{'text/plain': [{details: 'text/plain'}]}]);
  });

  it('should pdf available be false, diferent contentType', () => {
    // given
    const type = 'text/plain';
    // when
    const result = capabilitiesService.pdfAvailable(type);
    // then
    result.subscribe(available => {
      expect(available).toBe(false);
    });
    const request = httpTestingController.expectOne(req =>
      req.url === '/web/capabilities/preview'
    );
    request.flush([{'text/plain': [{details: 'text/plain'}]}]);
  });

  it('should return empty string for undefined content type', () => {
    // when
    const result = capabilitiesService.previewImageFormat(null);
    // then
    result.subscribe(available => {
      expect(available).toBe('');
    });
  });
});
