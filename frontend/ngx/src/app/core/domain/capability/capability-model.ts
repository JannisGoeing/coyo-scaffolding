/**
 * Allowed contentTypes to preview
 */
export interface CapabilityModel {
  [key: string]: {details: string}[];
}
