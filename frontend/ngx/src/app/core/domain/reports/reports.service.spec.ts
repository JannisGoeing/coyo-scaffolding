import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {UrlService} from '@core/http/url/url.service';
import {Report} from '@domain/reports/report';
import {ReportsService} from '@domain/reports/reports.service';
import * as _ from 'lodash';

describe('ReportsService', () => {

  let httpMock: HttpTestingController;
  let urlService: jasmine.SpyObj<UrlService>;

  beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [
          {provide: UrlService, useValue: jasmine.createSpyObj('urlService', ['join'])}
        ]
      });
      // tslint:disable-next-line:deprecation Fixme: COYOFOUR-10816 remove with Angular 9 & RxJs 7 update
      httpMock = TestBed.get(HttpTestingController);
      urlService = TestBed.get(UrlService);
      urlService.join.and.callFake((...parts: string[]) =>
        parts.map(p => _.trim(p, '/')).filter(p => !!p).join('/'));
    }
  );

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    const service: ReportsService = TestBed.get(ReportsService);
    expect(service).toBeTruthy();
  });

  it('should resolve a report', () => {
    const service: ReportsService = TestBed.get(ReportsService);
    const report = {id: 'some-id'} as Report;

    service.resolve(report).subscribe(() => {
    });

    const req = httpMock.expectOne(request => request.url === '/web/reports/some-id');
    expect(req.request.method).toEqual('DELETE');

    // finally
    req.flush({});
  });
});
