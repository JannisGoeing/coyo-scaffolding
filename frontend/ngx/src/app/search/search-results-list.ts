import {SearchResult} from './search-result';

/**
 * List of search results to be displayed in the external search results panel.
 */
export interface SearchResultsList {
  searchProvider: string;
  searchResults: SearchResult[];
  searchResultCountString: string;
}
