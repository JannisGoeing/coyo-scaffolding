import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {GoogleApiService} from '@app/integration/gsuite/google-api/google-api.service';
import {IntegrationApiService} from '@app/integration/integration-api/integration-api.service';
import {O365ApiService} from '@app/integration/o365/o365-api/o365-api.service';
import {Observable, zip} from 'rxjs';
import {map} from 'rxjs/operators';
import {ExternalSearchResultsService} from '../external-search-results.service';
import {SearchResultsList} from '../search-results-list';

@Component({
  selector: 'coyo-search-results-external-wrapper',
  templateUrl: './search-results-external-wrapper.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsExternalWrapperComponent implements OnInit {

  private readonly INTEGRATION_TYPES: string[] = [
    O365ApiService.INTEGRATION_TYPE,
    GoogleApiService.INTEGRATION_TYPE
  ];
  private readonly LIMIT: number = 3;

  /**
   * The search term, the user has entered.
   */
  @Input() searchQuery: string;

  providers$: Observable<string[]>;
  searches$: Observable<SearchResultsList[]>;

  constructor(private externalSearchResultsService: ExternalSearchResultsService, private integrationApiService: IntegrationApiService) { }

  ngOnInit(): void {
    this.providers$ = zip(
      ...this.INTEGRATION_TYPES.map(type => this.isIntegrationActive(type))
    ).pipe(map(integrations => integrations
      .filter(integration => integration.isActive)
      .map(integration => integration.type)
    ));

    const params = {term: this.searchQuery, limit: this.LIMIT.toString()};
    this.searches$ = this.externalSearchResultsService.getExternalSearchesResults(params);
  }

  private isIntegrationActive(type: string): Observable<{type: string, isActive: boolean}> {
    return this.integrationApiService.updateAndGetActiveState(type)
      .pipe(map(isActive => ({type, isActive})));
  }
}
