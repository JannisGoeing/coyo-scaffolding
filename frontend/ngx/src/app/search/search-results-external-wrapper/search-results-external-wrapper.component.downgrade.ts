import {downgradeComponent, getAngularJSGlobal} from '@angular/upgrade/static';
import {SearchResultsExternalWrapperComponent} from '@app/search/search-results-external-wrapper/search-results-external-wrapper.component';

getAngularJSGlobal()
  .module('coyo.search')
  .directive('coyoSearchResultsExternalWrapper', downgradeComponent({
    component: SearchResultsExternalWrapperComponent
  }));
