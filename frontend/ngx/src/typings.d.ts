import {HttpHeaders, HttpParams} from '@angular/common/http';
import {Document} from '@domain/file/document';
import {FilePreview} from '@domain/preview/file-preview';
import {Sender} from '@domain/sender/sender';
import {Target} from '@domain/sender/target';
import {User} from '@domain/user/user';
import {MediaWidgetSettings} from '@widgets/media/media-widget-settings';
import {IRootScopeService, IScope} from 'angular';
import {Observable} from 'rxjs';

/* SystemJS module definition */
declare var module: NodeModule;

interface NodeModule {
  id: string;
}

interface Ng1CoyoConfig {
  backendUrl: string;
  backendUrlStrategy: string;
  applicationName: string;
  version: {
    major: number;
    minor: number;
    patch: number;
    qualifier: string;
  };
  entityTypes: { [key: string]: EntityType };

  versionString(): string;
}

interface EntityType {
  label: string;
  icon: string;
  color: string;
  plural: string;
}

interface Ng1DefaultThemeColors {
  'color-primary': string;
  'color-secondary': string;
  'color-navbar-border': string;
  'coyo-navbar-text': string;
  'coyo-navbar': string;
  'coyo-navbar-active': string;
  'btn-primary-color': string;
  'btn-primary-bg': string;
  'color-background-main': string;
  'text-color': string;
  'link-color': string;
}

interface Ng1AdminThemeConfig {
  imageConfigs: Ng1AdminThemeImageConfig[];
}

interface Ng1AdminThemeImageConfig {
  key: string;
  retinaKey: string;
  variables: { [key: string]: (originalFile: any, resizedFile: any, Upload: any) => void };
}

interface Ng1FileLibraryModalService {
  open(sender: Sender | { [id: string]: string }, options: Ng1FileLibraryModalOptions, cropSettings?: CropSettings): Promise<Document | Document[]>;
}

type Ng1CoyoTranslationLoader = (input: { key: string, from: string }) => Promise<Map<any>>;

interface Ng1TranslationRegistry {
  getTranslationTable(language: string): (language: string) => string;
  getTranslationTables(): any[];
  addTranslations(language: string, translation: {[key: string]: string}): (language: string, translation: string) => string;
}

interface Ng1BackendUrlService {
  getUrl(): string | null;
  isSet(): boolean;
}

interface Ng1CsrfService {
  getToken(): Promise<any>;
  clearToken(): void;
}

interface Ng1MobileEventsService {
  propagate(eventName: string, payload?: any): void;
  evaluate(jsonString: string): void;
}

/* We have two authServices, this authService type represent the AngularJS authService */
interface Ng1AuthService {
  userUpdated$: Observable<[any, User, User]>;
  userLoggedIn$: Observable<[any]>;
  userLoggedOut$: Observable<[any]>;
  clearSession(): Promise<any>;
  isAuthenticated(): boolean;
  canUseHashtags(): boolean;
  getCurrentUserId(): string | null;

  validateUserId(userId: string, content: () => T | string): void;
  getUser(forceRefresh: boolean = false): Promise<User> | null;
}

interface Ng1TargetService {
  go(target: Target): Promise;
  getLink(target: Target): string;
}

interface Ng1ErrorService {
  showErrorPage(message: string, status: number, buttons: object[] | string[]): void;
}

interface Ng1AppService {
  getCurrentAppIdOrSlug(): string | null;
}

interface Ng1WidgetRegistry {
  getAll(): any[];
  get(key: string): object;
  getEnabled(): object;
}

interface Options {
  context?: { [key: string]: string };
  path?: string;
  headers?: HttpHeaders | { [header: string]: string | string[] };
  params?: HttpParams | { [param: string]: string | string[] };
  permissions?: string[];
  handleErrors?: boolean;
}

interface Ng1FileLibraryModalOptions {
  uploadMultiple?: boolean;
  selectMode?: string;
  filterContentType?: string | string[];
  initialFolder?: object;
}

interface CropSettings {
  cropImage: boolean;
}

interface Ng1ModalAlerts {
  level: string;
  title: string;
  text: string;
}

interface Ng1ModalOptions {
  size?: string;
  title?: string;
  text?: string;
  translationContext?: object;
  alerts?: Ng1ModalAlerts[];
  close?: object;
  dismiss?: object;
}

interface Ng1ModalService {
  open(options: Ng1ModalOptions): Observable<any>;
  confirm(options: Ng1ModalOptions): Observable<any>;
  confirmDelete(options: Ng1ModalOptions): Observable<any>;
}

interface CoyoConfig {
  entityTypes: {[key: string]: {icon: string}};
}

interface Ng1SublineService {
  getSublineForUser(user: Sender): Observable<string>;
  getSublineForSender(sender: Sender): Observable<string>;
}

interface Ng1StateLockService {
  ignoreLock(callback: () => Promise<void>): void;
}

interface Ng1ScrollBehaviourService {
  enableBodyScrolling(): void;
  disableBodyScrolling(): void;
  disableBodyScrollingOnXsScreen(): void;
}

interface Ng1SocketService {
  webSocketOffline$: Observable<any>;
  webSocketDisconnected$: Observable<[Event, boolean]>;
  webSocketReconnected$: Observable<any>;

  subscribe(destination: string, callback: Function, eventFilter?: string | object | Function, selectorValue?: string, jwtToken?: string): Function;
}

interface Ng1fileDetailsModalService {
  open(files: FilePreview[],
       currentIndex?: number,
       linkToFileLibrary?: boolean,
       showAuthors?: boolean,
       fileAuthors?: { [key: string]: any },
       appIdOrSlug?: string,
       filesTotal?: number,
       loadNext?: () => File[]): void;
}

interface Ng1ImageModalService {
  open(imageUrl: string, title: string): void;
}

export interface Ng1WidgetLayoutService {
  edit($scope: IRootScopeService, global: boolean, keepUnlocked?: boolean): void;
  save($scope: IRootScopeService, global: boolean, keepUnlocked?: boolean): void;
  collect($scope: IRootScopeService, name: string): Promise<void>;
  fill($scope: IRootScopeService, type: string, name: string, data: any): Promise<void>;
  cancel($scope: IRootScopeService, global: boolean, keepUnlocked?: boolean): void;
  onload($scope: IRootScopeService): Promise<void>;
}

export interface Ng1MobileStorageHandler {
  getStorageValue(key: string): any;
  setStorageValue(args: {key: string, value: any}): void;
}

export interface Ng1DeviceRegistrationHandler {
  registerPushDevice(args: {device: string}): void;
}

export interface Ng1AppRegistry {
  getDefaultStateName(key: any, senderType: any): string;
}

export interface Ng1MobileEventService {
  propagate(eventName: string, payload: any): void;
  evaluate(json: string): void;
}

export interface Ng1MessagingService {
  start(partnerId: string): Promise<any>;
}

export interface Ng1SocketReconnectDelays {
  TIMELINE_ITEMS_RELOAD_DELAY: number;
}

export interface Ng1FileAuthorService {
  loadFileAuthors(senderId: string, id: string, ids: string[], showAuthors: boolean): Promise<{ [id: string]: Sender }>;
}

export interface Ng1LightBoxModalService {
  open(model: MediaWidgetSettings, initialMediaId: string, initialSortOrderId: number): Promise<void>;
}
