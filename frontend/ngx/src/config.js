window.Config = {
  applicationName: 'Coyo',
  debug: true,
  maxReconnectDelaySeconds: 30,
  minReconnectDelaySeconds: 10,
  enabledServices: []
};
