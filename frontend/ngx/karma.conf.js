// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

process.env.CHROME_BIN = require('puppeteer').executablePath();

const isDocker = require('is-docker')();
const path = require('path');
const path_ng1_test = '../src/test';

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', 'jasmine-matchers', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-webpack'),
      require('karma-sourcemap-loader'),
      require('karma-jasmine'),
      require('karma-jasmine-matchers'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-junit-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, 'coverage'), reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    junitReporter: {
      outputDir: '../test-results/',
      suite: 'com.mindsmash.coyo.frontend',
      useBrowserName: true
    },
    files: [
      path.join(path_ng1_test, '/test.js'),
      path.join(path_ng1_test, '/**/*.spec.js')
    ],
    preprocessors: {
      '../src/test/test.js': ['webpack', 'sourcemap']
    },
    reporters: ['progress', 'junit'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['ChromeCustom'],
    singleRun: true,
    webpack: {
      mode: 'development',
      devtool: 'inline-source-map'
    },
    browserNoActivityTimeout: 60000,
    customLaunchers: {
      ChromeCustom: {
        base: 'ChromeHeadless',
        flags: isDocker ? ['--no-sandbox'] : [],
        captureConsole: false
      }
    }
  });
};
