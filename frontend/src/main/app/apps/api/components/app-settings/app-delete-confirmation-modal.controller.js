(function (angular) {
  'use strict';

  angular.module('coyo.apps.api')
      .controller('AppDeleteConfirmationModalController', AppDeleteConfirmationModalController);

  function AppDeleteConfirmationModalController(FolderModel, app) {
    var vm = this;
    vm.$onInit = onInit;
    vm.deleteAppFiles = true;

    function onInit() {
      if (app.rootFolderId) {
        vm.loading = true;
        FolderModel.hasChildren(app.senderId, app.rootFolderId).then(function (appFolderHasFiles) {
          vm.showDeleteFilesHint = appFolderHasFiles;
        }).finally(function () {
          vm.loading = false;
        });
      }
    }
  }

})(angular);
