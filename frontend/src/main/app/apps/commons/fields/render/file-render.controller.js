(function (angular) {
  'use strict';

  angular
      .module('coyo.apps.commons.fields')
      .controller('FileFieldRenderController', OptionsFieldRenderController);

  function OptionsFieldRenderController(fileDetailsModalService) {
    var vm = this;
    var detailsOpen = false;

    vm.openDetails = openDetails;

    function openDetails(file) {
      if (detailsOpen) {
        return;
      }
      fileDetailsModalService.open(file).result.finally(function () {
        detailsOpen = false;
      });
    }
  }

})(angular);
