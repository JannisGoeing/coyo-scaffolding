(function (angular) {
  'use strict';

  LinkPreviewModel.$inject = ['$http', 'restResourceFactory', 'coyoEndpoints'];
  angular
      .module('coyo.domain')
      .factory('LinkPreviewModel', LinkPreviewModel);

  /**
   * @ngdoc service
   * @name coyo.domain.LinkPreviewModel
   *
   * @description
   * Provides the Coyo link preview model.
   *
   * @requires $http
   * @requires restResourceFactory
   * @requires commons.config.coyoEndpoints
   */
  function LinkPreviewModel($http, restResourceFactory, coyoEndpoints) {
    var LinkPreviewModel = restResourceFactory({
      url: coyoEndpoints.webPreviews.generate
    });

    // class members
    angular.extend(LinkPreviewModel, {

      /**
       * @ngdoc function
       * @name coyo.domain.LinkPreviewModel#generate
       * @methodOf coyo.domain.LinkPreviewModel
       *
       * @description
       * Find urls and generate link previews for them.
       *
       * @params {string} urls The urls found in a post
       *
       * @returns {promise} A $http promise
       */
      generateWebPreviews: function (urls) {
        return this.$post(coyoEndpoints.webPreviews.generate, {
          urls: urls
        });
      }
    });

    return LinkPreviewModel;
  }

})(angular);
