(function (angular) {
  'use strict';

  angular
      .module('coyo.domain')
      .factory('I18nModel', I18nModel);

  /**
   * @ngdoc service
   * @name coyo.domain.I18nModel
   *
   * @description
   * Provides the Coyo i18n model.
   *
   * @requires $http
   * @requires commons.config.coyoEndpoints
   * @requires $q
   */
  function I18nModel($http, coyoEndpoints, $q) {
    var I18nModel = {};
    //class members
    angular.extend(I18nModel, {

      /**
       * @ngdoc function
       * @name coyo.domain.I18nModel#getAvailableLanguages
       * @methodOf coyo.domain.I18nModel
       *
       * @description
       * Get the available languages
       *
       * @returns {promise} An $http promise
       */
      getAvailableLanguages: function () {
        return $http({
          method: 'GET',
          url: coyoEndpoints.i18n.availableLanguages
        });
      },

      /**
       * @ngdoc function
       * @name coyo.domain.I18nModel#getTranslations
       * @methodOf coyo.domain.I18nModel
       *
       * @description
       * Get the translations for the given language key
       *
       * @param {string} lang The language key
       *
       * @returns {promise} An $http promise
       */
      getTranslations: function (lang) {
        var deferred = $q.defer();

        var xmlRequest = new XMLHttpRequest();
        xmlRequest.addEventListener('load', function () {
          deferred.resolve({data: angular.fromJson(this.responseText)});
        });
        xmlRequest.open('GET', coyoEndpoints.i18n.translations.replace('{language}', lang));
        xmlRequest.send();

        return deferred.promise;
      }

    });


    return I18nModel;
  }

})(angular);
