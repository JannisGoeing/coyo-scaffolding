(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .component('oyocLicenseIndicator', licenseIndicator())
      .controller('LicenseIndicatorController', LicenseIndicatorController);

  /**
   * @ngdoc directive
   * @name commons.ui.oyocLicenseIndicator:oyocLicenseIndicator
   * @element OWN
   * @restrict E
   * @scope
   *
   * @description
   * Renders the warning indicator on admin navigation tab when license is invalid or about to expire.
   *
   * @requires $rootScope
   * @requires $scope
   * @requires coyo.domain.LicenseModel
   */
  function licenseIndicator() {
    return {
      templateUrl: 'app/commons/ui/components/license-indicator/license-indicator.html',
      controller: 'LicenseIndicatorController'
    };
  }

  function LicenseIndicatorController($rootScope, $scope, LicenseModel) {
    var vm = this;

    vm.$onInit = _init;

    function _warn(license) {
      if (license.currentUserSize >= (license.maxUserSize * .9)) {
        return true;
      }
      if (license.currentStorageSize && license.currentStorageSize >= (license.maxStorageSize * .9)) {
        return true;
      }
      return license.validDays !== null && license.validDays < 30;
    }

    function _init() {
      var unsubscribe = $rootScope.$on('license:loaded', function (event, license) {
        if (!license.status) {
          vm.status = _warn(license) ? 'warning' : undefined;
        } else {
          vm.status = 'error';
        }
      });
      $scope.$on('$destroy', unsubscribe);
      LicenseModel.load(true);
    }
  }

})(angular);
