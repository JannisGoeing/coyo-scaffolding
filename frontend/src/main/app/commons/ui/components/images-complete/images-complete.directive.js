(function (angular) {
  'use strict';

  angular
      .module('commons.ui')
      .directive('coyoImagesComplete', imagesComplete);

  /**
   * @ngdoc directive
   * @name commons.ui.imagesComplete:imagesComplete
   * @restrict 'A'
   * @element ANY
   *
   * @description
   * Check all image elements inside this element and triggers a callback function
   * when all images have a complete state i.e. have been successfully loaded or loading finished with an error.
   *
   * @param {function} onCompleteHandler the function to call when all images are complete
   */
  function imagesComplete() {
    return {
      restrict: 'A',
      scope: {
        onCompleteHandler: '&coyoImagesComplete'
      },
      link: function (scope, element) {
        var pendingImages = [];

        function _onComplete(event) {
          var image = event.target;
          angular.element(image).off('load error');
          _.pull(pendingImages, image);
          if (pendingImages.length <= 0) {
            scope.$apply(function () {
              scope.onCompleteHandler();
            });
          }
        }

        element.ready(function () {
          pendingImages = Array.prototype.filter.call(element.querySelectorAll('img'), function (img) {
            return !img.complete;
          });
          if (pendingImages.length === 0) {
            scope.$apply(function () {
              scope.onCompleteHandler();
            });
          } else {
            pendingImages.forEach(function (img) {
              angular.element(img).on('load error', _onComplete);
            });
          }
        });
      }
    };
  }

})(angular);
