(function () {
  'use strict';

  var moduleName = 'coyo.widgets.api';
  var targetName = 'WidgetLayoutController';

  describe('module: ' + moduleName, function () {
    var $controller, $httpBackend, $scope, $rootScope, backendUrlService, WidgetModel;

    var name = 'app-content-123';
    var renderStyle = 'PLAIN';

    var layout = {
      name: name,
      settings: {
        rows: [
          {slots: [{name: 'slot1', cols: 12}]},
          {slots: [{name: 'slot2', cols: 12}], $deleted: true}
        ]
      },
      widgets: {
        'layout-app-content-123-slot-slot2': [{id: 'widget1-id', slot: 'layout-app-content-123-slot-slot2'}]
      },
      create: angular.noop,
      update: angular.noop,
      snapshot: angular.noop,
      rollback: angular.noop
    };

    beforeEach(function () {
      module(moduleName);

      inject(function ($q, _$controller_, _$httpBackend_, _$rootScope_, _backendUrlService_,
                       WidgetLayoutModel) {
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        backendUrlService = _backendUrlService_;

        WidgetLayoutModel.prototype.getWithWidgets = jasmine.createSpy().and.returnValue($q.resolve(layout));

        $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
      });

      WidgetModel = function WidgetModel() {
      };
      WidgetModel.prototype.remove = function () {
      };
      WidgetModel.removeCachedWidgets = function () {
      };
      spyOn(WidgetModel.prototype, 'remove');
      spyOn(WidgetModel, 'removeCachedWidgets');
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl(createMode) {
        var controller = $controller(targetName, {
          $scope: $scope,
          WidgetModel: WidgetModel
        }, {
          layoutName: name,
          renderStyle: renderStyle,
          createMode: createMode
        });
        controller.$onInit();
        return controller;
      }

      it('should init controller as user', function () {
        // given
        var ctrl = buildCtrl();

        // when
        $scope.$apply();

        // then
        expect(ctrl.renderStyle).toBe(renderStyle);
        expect(ctrl.editMode).toBe(false);
        expect(ctrl.layout.name).toEqual(layout.name);
      });

      it('should activate edit mode and preserve state on event', function () {
        // given
        var ctrl = buildCtrl();
        spyOn(layout, 'snapshot');

        // when
        $rootScope.$broadcast('widget-slot:edit');
        $scope.$apply();

        // then
        expect(ctrl.editMode).toBe(true);
        expect(layout.snapshot).toHaveBeenCalled();
      });

      it('should not activate edit mode on global event if listing to local events only', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.globalEvents = false;
        ctrl.editMode = false;
        spyOn(layout, 'snapshot');

        // when
        $rootScope.$broadcast('widget-slot:edit', true);
        $scope.$apply();

        // then
        expect(ctrl.editMode).toBe(false);
        expect(layout.snapshot).not.toHaveBeenCalled();
      });

      it('should deactivate edit mode and rollback on event', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = layout;
        ctrl.editMode = true;
        spyOn(layout, 'rollback');

        // when
        $scope.$broadcast('widget-slot:cancel');

        // then
        expect(ctrl.editMode).toBe(false);
        expect(layout.rollback).toHaveBeenCalled();
      });

      it('should not cancel on global event if listing to local events only', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.globalEvents = false;
        ctrl.editMode = true;
        spyOn(layout, 'rollback');

        // when
        $rootScope.$broadcast('widget-slot:cancel', true);
        $scope.$apply();

        // then
        expect(ctrl.editMode).toBe(true);
        expect(layout.rollback).not.toHaveBeenCalled();
      });

      it('should not put already existing widget to the layout', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = layout;
        var newAddedWidget = {
          id: 'widget1-id',
          slot: 'layout-app-content-123-slot-slot2'
        };

        // when
        $scope.$broadcast('widget:saved', newAddedWidget);

        expect(layout.widgets['layout-app-content-123-slot-slot2'].length).toBe(1);
      });

      it('should put new added widget to existing slot entries of the layout', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = layout;
        var newAddedWidget = {
          id: 'widget2-id',
          slot: 'layout-app-content-123-slot-slot2'
        };

        // when
        $scope.$broadcast('widget:saved', newAddedWidget);

        expect(layout.widgets['layout-app-content-123-slot-slot2'].length).toBe(2);
      });

      it('should put new added widget to a new slot entry of the layout', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = layout;
        var newAddedWidget = {
          id: 'widget3-id',
          slot: 'layout-app-content-123-slot-slot3'
        };

        // when
        $scope.$broadcast('widget:saved', newAddedWidget);

        expect(layout.widgets['layout-app-content-123-slot-slot3'].length).toBe(1);
      });

      it('should update on event', function () {
        // given
        var ctrl = buildCtrl(),
            promises = [];
        ctrl.layout = layout;
        ctrl.editMode = true;
        spyOn(layout, 'update');

        // when
        $scope.$broadcast('widget-slot:save', promises);

        // then
        expect(promises.length).toBe(1);
        expect(ctrl.editMode).toBe(false);
        expect(WidgetModel.prototype.remove).toHaveBeenCalled();
        expect(WidgetModel.removeCachedWidgets).toHaveBeenCalledWith('layout-app-content-123-slot-slot2');
        expect(layout.settings.rows.length).toBe(1);
        expect(layout.update).toHaveBeenCalled();
      });

      it('should update on event widget paste event', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = {
          name: name,
          widgets: {
            'layout-app-content-123-slot-slot2': [{id: 'widget1-id', slot: 'layout-app-content-123-slot-slot2'}]
          }
        };
        ctrl.editMode = true;
        var widget = {id: 'widget1-id', slot: 'layout-app-content-123-slot-slot3'};
        var slot = {name: 'layout-app-content-123-slot-slot2'};

        // when
        $scope.$broadcast('widget:paste', widget, slot);

        // then
        expect(ctrl.layout.widgets['layout-app-content-123-slot-slot2'].length).toBe(0);
        expect(ctrl.layout.widgets['layout-app-content-123-slot-slot3'].length).toBe(1);
        expect(ctrl.layout.widgets['layout-app-content-123-slot-slot3'][0]).toBe(widget);
      });

      it('should create on event in create mode', function () {
        // given
        var ctrl = buildCtrl(true),
            promises = [];
        ctrl.layout = layout;
        ctrl.editMode = true;
        spyOn(layout, 'create');

        // when
        $scope.$broadcast('widget-slot:save', promises);

        // then
        expect(promises.length).toBe(1);
        expect(ctrl.editMode).toBe(false);
        expect(layout.create).toHaveBeenCalled();
      });

      it('should not save on global event if listing to local events only', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.globalEvents = false;
        ctrl.editMode = true;
        spyOn(layout, 'update');

        // when
        $rootScope.$broadcast('widget-slot:save', [], true);
        $scope.$apply();

        // then
        expect(ctrl.editMode).toBe(true);
        expect(layout.update).not.toHaveBeenCalled();
      });

      it('should hide deleted row', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = layout;
        ctrl.layout.settings.rows.push({$deleted: true, slots: [{name: 'slot2', cols: 12}]});

        // when
        var visible = ctrl.isRowVisible(ctrl.layout.settings.rows[1]);

        // then
        expect(visible).toBe(false);
      });

      it('should not hide non-deleted row', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.layout = layout;

        // when
        var visible = ctrl.isRowVisible(ctrl.layout.settings.rows[0]);

        // then
        expect(visible).toBe(true);
      });
    });
  });
})();
