(function () {
  'use strict';

  var moduleName = 'commons.layout';

  describe('module: ' + moduleName, function () {

    var $rootScope, $controller, sidebarService;

    beforeEach(
        module(moduleName, function ($provide) {
          $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        })
    );

    beforeEach(inject(function (_$rootScope_, _$controller_) {
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      sidebarService = jasmine.createSpyObj('sidebarService', ['open']);
    }));

    describe('directive: oyoc-admin-sidebar', function () {

      var controllerName = 'AdminNavigationController';

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          sidebarService: sidebarService
        });
      }

      describe('controller: ' + controllerName, function () {

        it('should open admin sidebar', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.openMenu();

          // then
          expect(sidebarService.open).toHaveBeenCalledWith('admin-menu');
        });

      });
    });
  });
})();
