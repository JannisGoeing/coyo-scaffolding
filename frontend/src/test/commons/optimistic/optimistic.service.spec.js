(function () {
  'use strict';

  var moduleName = 'commons.optimistic';
  var targetName = 'optimisticService';

  describe('module: ' + moduleName, function () {

    var optimisticService, utilService, $localStorage, optimisticStatus, $timeout;
    var optimisticTypeMessage = 'OPTIMISTIC_TYPE_MESSAGE';

    beforeEach(function () {
      module(moduleName, function ($provide) {
        utilService = jasmine.createSpyObj('utilService', ['uuid']);
        utilService.uuid = function () {
          return 'some-uuid';
        };
        $provide.value('utilService', utilService);
        $localStorage = {
          optimistic: [],
          removeItem: jasmine.createSpy()
        };
        $provide.value('$localStorage', $localStorage);
        optimisticStatus = {
          NEW: 'NEW',
          PENDING: 'PENDING',
          ERROR: 'ERROR'
        };
        $provide.value('optimisticStatus', optimisticStatus);
      });

      inject(function (_optimisticService_, _$timeout_) {
        optimisticService = _optimisticService_;
        $timeout = _$timeout_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should save new entity to local storage', function () {
        // given
        var entity = {property: 'value'};
        var entityType = optimisticTypeMessage;
        spyOn(utilService, 'uuid').and.callThrough();

        // when
        optimisticService.saveEntity(entity, entityType);
        $timeout.flush();

        //then
        expect(utilService.uuid).toHaveBeenCalledTimes(1);
        expect($localStorage.optimistic.length).toEqual(1);
        expect($localStorage.optimistic[0].entityId).toEqual('some-uuid');
        expect($localStorage.optimistic[0].status).toEqual(optimisticStatus.NEW);
        expect($localStorage.optimistic[0].entityType).toEqual(optimisticTypeMessage);
        expect($localStorage.optimistic[0].entity.property).toEqual(entity.property);
      });

      it('should override existing entity in local storage', function () {
        // given
        for (var i = 1; i <= 3; i++) {
          var entity = {
            entityId: 'uuid-' + i,
            entityType: optimisticTypeMessage,
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'channel-id-' + i
            }
          };
          $localStorage.optimistic.push(entity);
        }
        var expectedEntity = {
          key: 'updated',
          entityId: 'uuid-2',
          entityType: optimisticTypeMessage,
          status: optimisticStatus.NEW,
          entity: {
            channelId: 'channel-id-2'
          }
        };

        // when
        optimisticService.saveEntity(expectedEntity, optimisticTypeMessage);
        $timeout.flush();

        //then
        expect($localStorage.optimistic.length).toEqual(3);
        var result = _.find($localStorage.optimistic, {entityId: expectedEntity.entityId});
        expect(result).toEqual(expectedEntity);
      });

      it('should cleanup local storage for channel before save', function () {
        // given
        for (var i = 1; i <= 3; i++) {
          var entity = {
            entityId: 'uuid-' + i,
            entityType: optimisticTypeMessage,
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'channel-id'
            }
          };
          $localStorage.optimistic.push(entity);
        }
        var expectedEntity = {
          key: 'updated',
          entityId: 'uuid-2',
          entityType: optimisticTypeMessage,
          status: optimisticStatus.NEW,
          entity: {
            channelId: 'channel-id'
          }
        };

        // when
        optimisticService.saveEntity(expectedEntity, optimisticTypeMessage);
        $timeout.flush();

        //then
        expect($localStorage.optimistic.length).toEqual(1);
        expect($localStorage.optimistic[0]).toEqual(expectedEntity);
      });

      it('should find an entity by its id', function () {
        // given
        for (var i = 1; i <= 3; i++) {
          var entity = {
            entityId: 'uuid-' + i,
            entityType: optimisticTypeMessage,
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'channel-id-' + i
            }
          };
          $localStorage.optimistic.push(entity);
        }
        var expectedEntity = {
          entityId: 'uuid-2',
          entityType: optimisticTypeMessage,
          status: optimisticStatus.NEW,
          entity: {
            channelId: 'channel-id-2'
          }
        };

        // when
        var result = optimisticService.find(expectedEntity.entityId);

        // then
        expect(result).toEqual(expectedEntity);
      });

      it('should find an entity of a given entityType', function () {
        // given
        for (var i = 1; i <= 10; i++) {
          var entity = {
            entityId: 'uuid-' + i,
            entityType: (i % 2 === 0) ? optimisticTypeMessage : 'OTHER-ENTITY-TYPE',
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'channel-id-' + i
            }
          };
          $localStorage.optimistic.push(entity);
        }
        expect($localStorage.optimistic.length).toEqual(10);

        // when
        var result = optimisticService.findAll(optimisticTypeMessage);

        // then
        expect(result.length).toEqual(5);
        expect(result[0].entityType).toEqual(optimisticTypeMessage);
        expect(result[1].entityType).toEqual(optimisticTypeMessage);
        expect(result[2].entityType).toEqual(optimisticTypeMessage);
        expect(result[3].entityType).toEqual(optimisticTypeMessage);
        expect(result[4].entityType).toEqual(optimisticTypeMessage);
      });

      it('should remove entities by their ids', function () {
        // given
        for (var i = 1; i <= 10; i++) {
          var entity = {
            entityId: 'uuid-' + i,
            entityType: optimisticTypeMessage,
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'channel-id-' + i
            }
          };
          $localStorage.optimistic.push(entity);
        }
        expect($localStorage.optimistic.length).toEqual(10);

        // when
        optimisticService.remove(['uuid-1', 'uuid-2', 'uuid-8']);

        // then
        expect($localStorage.optimistic.length).toEqual(7);
        $localStorage.optimistic.forEach(function (element) {
          expect(element.entityId).not.toEqual('uuid-1');
          expect(element.entityId).not.toEqual('uuid-2');
          expect(element.entityId).not.toEqual('uuid-8');
        });
      });

      it('should remove all entities with clear', function () {
        // given
        for (var i = 1; i <= 10; i++) {
          var entity = {
            entityId: 'uuid-' + i,
            entityType: optimisticTypeMessage,
            status: optimisticStatus.NEW,
            entity: {
              channelId: 'channel-id-' + i
            }
          };
          $localStorage.optimistic.push(entity);
        }
        expect($localStorage.optimistic.length).toEqual(10);

        // when
        optimisticService.clear();

        // then
        expect($localStorage.optimistic).toBeNull();
        expect($localStorage.removeItem).toHaveBeenCalledWith('optimistic');
      });
    });
  });
})();
