(function () {
  'use strict';

  var moduleName = 'commons.i18n.custom';

  describe('module: ' + moduleName, function () {
    var $scope, LanguagesModel, translationRegistry, languageDeterminer, $q, errorService, I18nModel;
    var coyoTranslationLoader, originalNavigatorLanguages;

    beforeAll(function () {
      originalNavigatorLanguages = navigator.languages;
    });

    afterAll(function () {
      Object.defineProperty(navigator, 'languages', {
        get: function () {
          return originalNavigatorLanguages;
        }
      });
    });

    beforeEach(function () {
      LanguagesModel = jasmine.createSpyObj('LanguagesModel', ['getTranslations', 'getDefaultLanguage']);
      I18nModel = jasmine.createSpyObj('I18nModel', ['getAvailableLanguages', 'getTranslations']);
      languageDeterminer = jasmine.createSpyObj('LanguageDeterminer', ['getLanguageKeys']);
      translationRegistry = jasmine.createSpyObj('translationRegistry', ['addTranslations']);
      errorService = jasmine.createSpyObj('errorService', ['suppressNotification']);

      module(moduleName, function ($provide) {
        $provide.value('I18nModel', I18nModel);
        $provide.value('LanguagesModel', LanguagesModel);
        $provide.value('languageDeterminer', languageDeterminer);
        $provide.value('translationRegistry', translationRegistry);
        $provide.value('errorService', errorService);
      });
    });

    beforeEach(inject(function (_$rootScope_, _coyoTranslationLoader_, _$localStorage_, _$q_) {
      $scope = _$rootScope_.$new();
      coyoTranslationLoader = _coyoTranslationLoader_;
      $q = _$q_;

      LanguagesModel.getDefaultLanguage.and.returnValue($q.resolve({language: 'en'}));
      languageDeterminer.getLanguageKeys.and.returnValue($q.resolve(
          [languageKey('en'), languageKey('de')]
      ));
      I18nModel.getTranslations.and.returnValue($q.resolve({
        data: {key2: 'i18n_translation2', key4: 'i18n_translation4'}
      }));
      LanguagesModel.getTranslations.and.returnValue($q.resolve({}));
    }));

    describe('service: coyoTranslationLoader', function () {
      it('should initialize', function () {
        // then
        expect(coyoTranslationLoader).toBeDefined();
      });

      it('should make calls for translations and overrides correctly', function () {
        // given
        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve(
            [languageKey('en'), languageKey('de'), languageKey('nl')]
        ));

        // when
        coyoTranslationLoader({key: 'nl'});
        $scope.$apply();

        // then
        expect(languageDeterminer.getLanguageKeys).toHaveBeenCalledWith('nl');
        expect(I18nModel.getTranslations.calls.allArgs()).toEqual([['en'], ['de'], ['nl']]);
        expect(LanguagesModel.getTranslations.calls.allArgs()).toEqual([['en'], ['de'], ['nl']]);
      });

      it('should call TranslationRegistry correctly', function () {
        // given
        var expectedTranslationMap = {
          key2: 'i18n_translation2',
          key4: 'i18n_translation4'
        };
        var overrides = [
          {key: 'key2', translation: 'override2'},
          {key: 'key4', translation: 'override4'}
        ];
        LanguagesModel.getTranslations.and.returnValue($q.resolve(overrides));

        // when
        coyoTranslationLoader({key: 'en'});
        $scope.$apply();

        // then
        expect(translationRegistry.addTranslations.calls.allArgs())
            .toEqual([['en', expectedTranslationMap], ['de', expectedTranslationMap]]);
      });

      it('should merge fallback, default and user language correctly', function () {
        // given
        var translations = {};
        var i18nTranslations = {
          nl: {key2: 'i18n_translation2_nl'},
          de: {key2: 'i18n_translation2_de', key3: 'i18n_translation3_de'},
          en: {
            key1: 'i18n_translation1_en',
            key2: 'i18n_translation2_en',
            key3: 'i18n_translation3_en',
            key4: 'i18n_translation4_en'
          }
        };

        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve(
            [languageKey('en'), languageKey('de'), languageKey('nl')]
        ));
        I18nModel.getTranslations.and.callFake(function (language) {
          return $q.resolve({data: i18nTranslations[language]});
        });

        // when
        coyoTranslationLoader({key: 'nl'}).then(function (data) {
          translations = data;
        });
        $scope.$apply();

        // then
        expect(translations).toEqual({
          key1: 'i18n_translation1_en',
          key2: 'i18n_translation2_nl',
          key3: 'i18n_translation3_de',
          key4: 'i18n_translation4_en'
        });
      });

      it('should merge override translations', function () {
        // given
        var translations = {};
        var i18nTranslations = {
          data: {
            key1: 'i18n_translation1_en',
            key2: 'i18n_translation2_en',
            key3: 'i18n_translation3_en',
            key4: 'i18n_translation4_en'
          }
        };
        var overrides = [
          {key: 'key1', translation: 'override1_en'},
          {key: 'key3', translation: 'override1_en'}
        ];

        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve([languageKey('en')]));
        I18nModel.getTranslations.and.returnValue($q.resolve(i18nTranslations));
        LanguagesModel.getTranslations.and.returnValue($q.resolve(overrides));

        // when
        coyoTranslationLoader({key: 'en'}).then(function (data) {
          translations = data;
        });
        $scope.$apply();

        // then
        expect(translations).toEqual({
          key1: 'override1_en',
          key2: 'i18n_translation2_en',
          key3: 'override1_en',
          key4: 'i18n_translation4_en'
        });
      });

      it('should get override for language that i18n model doesn\'t have', function () {
        // given
        var translations = {};
        LanguagesModel.getTranslations.and.callFake(function (language) {
          return language === 'cn' ? $q.resolve([{key: 'key2', translation: 'override_2_cn'}]) : $q.resolve({});
        });
        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve(
            [languageKey('en'), languageKey('de'), languageKey('cn', true)]
        ));

        // when
        coyoTranslationLoader({key: 'cn'}).then(function (data) {
          translations = data;
        });
        $scope.$apply();

        // then
        expect(translationRegistry.addTranslations.calls.allArgs())
            .not.toContain(['cn', jasmine.any(Object)]);
        expect(translations).toEqual({
          key2: 'override_2_cn',
          key4: 'i18n_translation4'
        });
      });

      it('should not call i18n model for a language that is marked onlyOverrides', function () {
        // given
        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve(
            [languageKey('en'), languageKey('de'), languageKey('cn', true)]
        ));

        // when
        coyoTranslationLoader({key: 'cn'});
        $scope.$apply();

        // then
        expect(I18nModel.getTranslations.calls.allArgs())
            .not.toContain(['cn']);
        expect(LanguagesModel.getTranslations.calls.allArgs())
            .toContain(['cn']);
      });

      it('should get translations from cache', function () {
        // given
        var translations1 = {}, translations2 = {};
        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve([languageKey('en')]));

        // when
        coyoTranslationLoader({key: 'en'}).then(function (data) {
          translations1 = data;
        });
        coyoTranslationLoader({key: 'en'}).then(function (data) {
          translations2 = data;
        });
        $scope.$apply();

        // then
        expect(LanguagesModel.getTranslations.calls.count()).toBe(1);
        expect(I18nModel.getTranslations.calls.count()).toBe(1);
        expect(translationRegistry.addTranslations.calls.count()).toBe(1);
        expect(languageDeterminer.getLanguageKeys.calls.count()).toBe(1);
        expect(translations1).toBe(translations2);
      });

      it('should not get translations from cache if different language key is given', function () {
        // given
        var translations1 = {}, translations2 = {};
        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve([languageKey('en')]));
        coyoTranslationLoader({key: 'en'}).then(function (data) {
          translations1 = data;
        });
        languageDeterminer.getLanguageKeys.and.returnValue($q.resolve([languageKey('de')]));

        // when
        coyoTranslationLoader({key: 'de'}).then(function (data) {
          translations2 = data;
        });
        $scope.$apply();

        // then
        expect(LanguagesModel.getTranslations.calls.count()).toBe(2);
        expect(I18nModel.getTranslations.calls.count()).toBe(2);
        expect(translationRegistry.addTranslations.calls.count()).toBe(2);
        expect(languageDeterminer.getLanguageKeys.calls.count()).toBe(2);
        expect(translations2).toEqual(translations1);
      });

    });

    function languageKey(lang, override) {
      override = override || false;
      return {languageKey: lang, overrideOnly: override};
    }

  });
})();
