(function () {
  'use strict';

  var moduleName = 'commons.mobile';
  var targetName = 'mobileEventsService';

  describe('module: ' + moduleName, function () {

    var mobileEventsService, mobileRequestHandlerRegistry, $window, $log;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        mobileRequestHandlerRegistry = jasmine.createSpyObj(mobileRequestHandlerRegistry, ['getHandler']);
        $provide.value('mobileRequestHandlerRegistry', mobileRequestHandlerRegistry);
        $log = jasmine.createSpyObj('$log', ['error', 'info']);
        $provide.value('$log', $log);
      });

      inject(function (_mobileEventsService_, _$window_) {
        mobileEventsService = _mobileEventsService_;
        $window = _$window_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should provide a global function in window object that calls the evaluate method', function () {
        var evaluateSpy = spyOn(mobileEventsService, 'evaluate');
        expect(_.isFunction($window.invokeMethod)).toBe(true);

        $window.invokeMethod('arg');

        expect(evaluateSpy).toHaveBeenCalledWith('arg');
      });

      it('should not invoke handler if json is invalid', function () {
        mobileEventsService.evaluate('{1]');

        expect($log.error).toHaveBeenCalled();
        expect(mobileRequestHandlerRegistry.getHandler).not.toHaveBeenCalled();
      });

      it('should not invoke handler if method is missing', function () {
        mobileEventsService.evaluate('{}');

        expect($log.error).toHaveBeenCalledWith('[MobileEventsService] No handler method given in invocation: {}');
        expect(mobileRequestHandlerRegistry.getHandler).not.toHaveBeenCalled();
      });

      it('should not invoke handler if registry has no handler with that name', function () {
        mobileEventsService.evaluate('{"method": "foo"}');

        expect($log.error)
            .toHaveBeenCalledWith('[MobileEventsService] No handler found for method foo, argument was: undefined');
        expect(mobileRequestHandlerRegistry.getHandler).toHaveBeenCalledWith('foo');
      });

      it('should not invoke handler if registry does not return a function', function () {
        mobileRequestHandlerRegistry.getHandler.and.returnValue(undefined);

        mobileEventsService.evaluate('{"method": "foo"}');

        expect($log.error)
            .toHaveBeenCalledWith('[MobileEventsService] No handler found for method foo, argument was: undefined');
        expect(mobileRequestHandlerRegistry.getHandler).toHaveBeenCalledWith('foo');
      });

      it('should invoke handler', function () {
        var actualArg = null;
        mobileRequestHandlerRegistry.getHandler.and.returnValue(function (arg) {
          actualArg = arg;
        });

        mobileEventsService.evaluate('{"method": "foo", "argument": "bar"}');

        expect(mobileRequestHandlerRegistry.getHandler).toHaveBeenCalledWith('foo');
        expect(actualArg).toEqual('bar');
      });
    });
  });
})();
