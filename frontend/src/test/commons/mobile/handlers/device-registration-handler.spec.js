(function () {
  'use strict';

  var moduleName = 'commons.mobile';
  var targetName = 'deviceRegistrationHandler';

  describe('module: ' + moduleName, function () {

    var deviceRegistrationHandler, authService;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        authService = jasmine.createSpyObj('authService', ['isAuthenticated', 'getUser']);
        authService.isAuthenticated.and.returnValue(true);
        authService.getUser.and.returnValue({
          then: function (callback) {
            callback({
              id: 'user-id',
              addPushDevice: function () {
                return {
                  then: function (callback) {
                    callback();
                    return {
                      catch: angular.noop
                    };
                  }
                };
              }
            });
          }
        });
        $provide.value('authService', authService);
      });

      inject(function (_deviceRegistrationHandler_) {
        deviceRegistrationHandler = _deviceRegistrationHandler_;
      });
    });

    describe('service: ' + targetName, function () {

      it('should proceed if device is present', function () {
        // given
        var data = {device: {token: 'device-token'}};

        // when
        deviceRegistrationHandler.registerPushDevice(data);

        // then
        expect(authService.getUser).toHaveBeenCalledTimes(1);
      });

      it('should abort if device is missing', function () {
        // given
        var device = undefined;

        // when
        deviceRegistrationHandler.registerPushDevice(device);

        // then
        expect(authService.getUser).toHaveBeenCalledTimes(0);
      });

    });
  });
})();
