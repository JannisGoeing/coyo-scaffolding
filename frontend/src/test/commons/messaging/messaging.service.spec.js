(function () {
  'use strict';

  var moduleName = 'commons.messaging';

  describe('module: ' + moduleName, function () {
    var messagingService;
    var $rootScope, $scope, $q;
    var authService, MessageChannelModel, browserNotificationsService;

    var user = {
      id: 'userId',
      hasGlobalPermissions: function () {
        return true;
      }
    };

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
        authService = jasmine.createSpyObj('authService', ['isAuthenticated', 'getUser', 'logout', 'subscribeToUserUpdate']);
        authService.getUser = function () {
          return {
            then: function (callback) {
              callback(user);
            }
          };
        };
        authService.isAuthenticated = function () {
          return true;
        };
        authService.getCurrentUserId = function () {
          return 'user-id';
        };

        $provide.value('authService', authService);
        MessageChannelModel = jasmine.createSpyObj('MessageChannelModel', ['save', 'get']);
        $provide.value('MessageChannelModel', function () {
          return MessageChannelModel;
        });
        browserNotificationsService =
            jasmine.createSpyObj('browserNotificationsService', ['notifyPost', 'writeNotificationSettingsToLocalStorage']);
        $provide.value('browserNotificationsService', browserNotificationsService);
      });

      inject(function (_messagingService_, _$rootScope_, _$q_) {
        messagingService = _messagingService_;
        $rootScope = _$rootScope_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
      });
    });

    describe('service: messagingService', function () {

      it('should toggle the messaging sidebar', function () {
        // given
        spyOn($rootScope, '$emit');

        // when
        messagingService.toggle(true);
        $scope.$apply();

        // then
        expect($rootScope.$emit).toHaveBeenCalled();
        expect($rootScope.$emit.calls.argsFor(0)[0]).toBe('messaging:toggle');
        expect($rootScope.$emit.calls.argsFor(0)[1]).toBeTrue();
      });

      it('should start a new channel', function () {
        // given
        MessageChannelModel.save.and.returnValue($q.resolve('channel'));
        spyOn($rootScope, '$emit');

        // when
        messagingService.start('partnerId');
        $scope.$apply();

        // then
        expect($rootScope.$emit).toHaveBeenCalled();
        expect($rootScope.$emit.calls.argsFor(0)[0]).toBe('messaging:start');
        expect($rootScope.$emit.calls.argsFor(0)[1]).toBe('channel');
      });

      it('should open a channel', function () {
        // given
        MessageChannelModel.get.and.returnValue($q.resolve('channel'));
        spyOn($rootScope, '$emit');

        // when
        messagingService.open('channelId');
        $scope.$apply();

        // then
        expect($rootScope.$emit).toHaveBeenCalled();
        expect($rootScope.$emit.calls.argsFor(0)[0]).toBe('messaging:start');
        expect($rootScope.$emit.calls.argsFor(0)[1]).toBe('channel');
      });
    });
  });

})();
