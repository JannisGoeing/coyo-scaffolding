(function () {
  'use strict';

  var moduleName = 'commons.auth';
  var directiveName = 'coyo-global-permissions';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, template, authService;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function ($rootScope, _$compile_, _authService_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        authService = _authService_;
        template = '<div coyo-global-permissions="P1">...</div>';
      }));

      it('should hide element on permission mismatch', function () {
        // given
        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(permissionNames === 'P2');
        });

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$apply();

        // then
        expect(element.attr('class')).toContain('global-permissions-hidden');
      });

      it('should not hide element on permission match', function () {
        // given
        spyOn(authService, 'onGlobalPermissions').and.callFake(function (permissionNames, callback) {
          callback(permissionNames === 'P1');
        });

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$apply();

        // then
        expect(element.attr('class')).not.toContain('global-permissions-hidden');
      });

    });

  });
})();
