(function () {
  'use strict';

  var moduleName = 'commons.resource';

  describe('module: ' + moduleName, function () {

    var etagCacheService, $interval, $sessionStorage = {};

    beforeEach(function () {

      module(moduleName, function ($provide) {
        $provide.value('$sessionStorage', $sessionStorage);
      });

      inject(function (_etagCacheService_, _$interval_) {
        etagCacheService = _etagCacheService_;
        $interval = _$interval_;
      });

    });

    describe('Service: etagCacheService', function () {

      afterEach(function () {
        if ($sessionStorage.etagCache) {
          Object.keys($sessionStorage.etagCache).forEach(function (key) {
            delete $sessionStorage.etagCache[key];
          });
        }
        if ($sessionStorage.etagBulkCache) {
          Object.keys($sessionStorage.etagBulkCache).forEach(function (key) {
            delete $sessionStorage.etagBulkCache[key];
          });
        }
      });

      describe('Method: buildCacheKey', function () {

        it('should return url when no parameters are present', function () {
          // given
          var url = '/web/foo';

          // when
          var result = etagCacheService.buildCacheKey(url);

          // then
          expect(result).toBe(url);
        });

        it('should include request parameter', function () {
          // given
          var url = '/web/foo';

          // when
          var result = etagCacheService.buildCacheKey(url, {p1: 'v1', p2: 'v2'});

          // then
          expect(result).toBe(url + '?p1=v1&p2=v2');
        });

        it('should not include bulk request parameter', function () {
          // given
          var url = '/web/foo';

          // when
          var result = etagCacheService.buildCacheKey(url, {
            p1: 'v1',
            p2: ['v2.1', 'v2.2'],
            p3: 'v3'
          }, {
            bulkParameter: 'p2'
          });

          // then
          expect(result).toBe(url + '?p1=v1&p3=v3');
        });

      });

      describe('Method: _cleanupCache', function () {

        it('should expire outdated regular entries', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key1 = 'cache-key-1';
          var key2 = 'cache-key-2';
          $sessionStorage.etagCache[key1] = {data: 'v1', cacheAccess: new Date().getTime() - expiry - 10};
          $sessionStorage.etagCache[key2] = {data: 'v2', cacheAccess: new Date().getTime() - expiry + 10};

          // when
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagCache[key1]).not.toBeDefined();
          expect($sessionStorage.etagCache[key2]).toBeDefined();
          expect($sessionStorage.etagCache[key2].data).toBe('v2');
        });

        it('should cache read reset expiry', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key = 'cache-key';
          $sessionStorage.etagCache[key] = {data: 'v1', cacheAccess: new Date().getTime() - expiry - 10};

          // when
          etagCacheService.get(key);
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagCache[key]).toBeDefined();
        });

        it('should cache check reset expiry', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key = 'cache-key';
          $sessionStorage.etagCache[key] = {data: 'v1', cacheAccess: new Date().getTime() - expiry - 10};

          // when
          etagCacheService.isCached(key);
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagCache[key]).toBeDefined();
        });

        it('should expire outdated bulk url', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key1 = 'cache-key-1';
          var key2 = 'cache-key-2';
          $sessionStorage.etagBulkCache[key1] = {cacheAccess: new Date().getTime() - expiry - 10};
          $sessionStorage.etagBulkCache[key2] = {cacheAccess: new Date().getTime() - expiry + 10};

          // when
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagBulkCache[key1]).not.toBeDefined();
          expect($sessionStorage.etagBulkCache[key2]).toBeDefined();
        });

        it('should expire outdated bulk ID entries', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key = 'cache-key';
          var id1 = 'id-1';
          var id2 = 'id-2';
          $sessionStorage.etagBulkCache[key] = {cacheAccess: new Date().getTime() - expiry + 10};
          $sessionStorage.etagBulkCache[key][id1] = {cacheAccess: new Date().getTime() - expiry + 10};
          $sessionStorage.etagBulkCache[key][id2] = {cacheAccess: new Date().getTime() - expiry - 10};

          // when
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagBulkCache[key]).toBeDefined();
          expect($sessionStorage.etagBulkCache[key][id1]).toBeDefined();
          expect($sessionStorage.etagBulkCache[key][id2]).not.toBeDefined();
        });

        it('should bulk cache read reset group expiry', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key = 'cache-key';
          var id = 'some-id';
          $sessionStorage.etagBulkCache[key] = {cacheAccess: new Date().getTime() - expiry - 10};
          $sessionStorage.etagBulkCache[key][id] = {cacheAccess: new Date().getTime() - expiry - 10};

          // when
          etagCacheService.getBulk(key, 'some-id');
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagBulkCache[key]).toBeDefined();
          expect($sessionStorage.etagBulkCache[key][id]).toBeDefined();
        });

        it('should bulk cache check reset group expiry but not nested entry', function () {
          // given
          var expiry = 1000 * 60 * 60;
          var key = 'cache-key';
          var id = 'some-id';
          $sessionStorage.etagBulkCache[key] = {cacheAccess: new Date().getTime() - expiry - 10};
          $sessionStorage.etagBulkCache[key][id] = {cacheAccess: new Date().getTime() - expiry - 10};

          // when
          etagCacheService.isBulkCached(key);
          $interval.flush(60000);

          // then
          expect($sessionStorage.etagBulkCache[key]).toBeDefined();
          expect($sessionStorage.etagBulkCache[key][id]).not.toBeDefined();
        });

      });

      describe('Regular request cache', function () {

        it('should store request data in session storage', function () {
          // given
          var url = '/web/foo';
          var payload = 'request-payload';
          var etag = 'abc';
          var status = 200;

          // when
          etagCacheService.store(url, payload, etag, status);

          // then
          var cacheElement = $sessionStorage.etagCache[url];
          expect(cacheElement).toBeDefined();
          expect(cacheElement.etag).toBe(etag);
          expect(cacheElement.data).toBe(payload);
          expect(cacheElement.status).toBe(status);
        });

        it('should retrieve stored request data', function () {
          // given
          var url = '/web/foo';
          var payload = 'request-payload';
          var etag = 'abc';
          var status = 200;
          etagCacheService.store(url, payload, etag, status);

          // when
          var cacheElement = etagCacheService.get(url);

          // then
          expect(cacheElement).toBeDefined();
          expect(cacheElement.etag).toBe(etag);
          expect(cacheElement.data).toBe(payload);
          expect(cacheElement.status).toBe(status);
        });

        it('should find stored request data', function () {
          // given
          var url = '/web/foo';
          var payload = 'request-payload';
          var etag = 'abc';
          var status = 200;
          expect(etagCacheService.isCached(url)).toBe(false);
          etagCacheService.store(url, payload, etag, status);

          // when
          var isCached = etagCacheService.isCached(url);

          // then
          expect(isCached).toBe(true);
        });

      });

      describe('Bulk request cache', function () {

        it('should store request data in session storage', function () {
          // given
          var url = '/web/foo';
          var id = 'some-id';
          var payload = 'request-payload';
          var etag = 'abc';

          // when
          etagCacheService.storeBulk(url, id, payload, etag);

          // then
          var cacheGroup = $sessionStorage.etagBulkCache[url];
          expect(cacheGroup).toBeDefined();
          var cacheElement = cacheGroup[id];
          expect(cacheElement.etag).toBe(etag);
          expect(cacheElement.data).toBe(payload);
        });

        it('should retrieve stored request data', function () {
          // given
          var url = '/web/foo';
          var id = 'some-id';
          var payload = 'request-payload';
          var etag = 'abc';
          etagCacheService.storeBulk(url, id, payload, etag);

          // when
          var cacheElement = etagCacheService.getBulk(url, id);

          // then
          expect(cacheElement).toBeDefined();
          expect(cacheElement.etag).toBe(etag);
          expect(cacheElement.data).toBe(payload);
        });

        it('should find stored request data', function () {
          // given
          var url = '/web/foo';
          var id = 'some-id';
          var payload = 'request-payload';
          var etag = 'abc';
          expect(etagCacheService.isBulkCached(url)).toBe(false);
          etagCacheService.storeBulk(url, id, payload, etag);

          // when
          var isCached = etagCacheService.isBulkCached(url);

          // then
          expect(isCached).toBe(true);
        });
      });
    });

  });
})();
