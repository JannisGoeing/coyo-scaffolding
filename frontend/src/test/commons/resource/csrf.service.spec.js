(function () {
  'use strict';

  var moduleName = 'commons.resource';
  var targetName = 'csrfService';

  describe('module: ' + moduleName, function () {
    var csrfService;
    var backendUrlService;
    var $rootScope;
    var $httpBackend;

    beforeEach(function () {
      module(moduleName);

      inject(function (_csrfService_, _$rootScope_, _$httpBackend_, _backendUrlService_) {
        csrfService = _csrfService_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        backendUrlService = _backendUrlService_;
      });
    });

    it('should be registered', function () {
      expect(csrfService).not.toBeUndefined();
    });

    describe('service: ' + targetName, function () {

      it('should clear token', function () {
        // given
        $rootScope.csrfToken = 'foo';

        // when
        csrfService.clearToken();

        // then
        expect($rootScope.csrfToken).toBeUndefined();
      });

      it('should check if token is set', function () {
        // case is set
        $rootScope.csrfToken = 'foo';
        expect(csrfService.isSet()).toBeTruthy();

        // case is not set
        $rootScope.csrfToken = null;
        expect(csrfService.isSet()).toBeFalsy();
      });

      it('should resolve token when already set', function () {
        // given
        $rootScope.csrfToken = 'foo';

        // when
        var promise = csrfService.getToken();

        // then
        promise.then(function (token) {
          expect(token).toEqual('foo');
        }).catch(function () {
          throw Error('Unexpected');
        });
      });

      it('should resolve token from backend', function () {
        // given
        csrfService.clearToken();

        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/csrf').respond(200, {
          token: 'foo'
        });

        // when
        var promise = csrfService.getToken();

        $httpBackend.flush();

        // then
        promise.then(function (token) {
          expect(token).toEqual('foo');
        }).catch(function () {
          throw Error('Unexpected');
        });
      });
    });
  });
})();
