(function () {
  'use strict';

  var moduleName = 'commons.resource';
  var targetName = 'tempUploadService';

  describe('module: ' + moduleName, function () {
    var tempUploadService;
    var Upload;

    beforeEach(function () {
      module(moduleName);

      inject(function (_tempUploadService_, _Upload_) {
        tempUploadService = _tempUploadService_;
        Upload = _Upload_;
      });
    });

    it('should be registered', function () {
      expect(tempUploadService).not.toBeUndefined();
    });

    describe('service: ' + targetName, function () {

      it('should upload file', function () {
        // given
        var file = {name: '1.png'};
        var forSeconds = 10;
        var blob = {uid: '123', contentType: 'image/png'};

        var uploadCb;
        spyOn(Upload, 'upload').and.returnValue({
          then: function (callback) {
            uploadCb = callback;
          }
        });

        // when
        var promise = tempUploadService.upload(file, forSeconds);

        // then
        expect(file.uploading).toBeTruthy();
        expect(file.progress).toBeDefined();

        // when resolving
        uploadCb(blob);

        promise.then(function (result) {
          expect(result).toEqual(blob);
        });

        expect(file.uploading).toBeFalsy();
        expect(file.progress).toBe(100);
      });

    });
  });
})();
