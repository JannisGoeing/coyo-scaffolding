(function () {
  'use strict';

  var moduleName = 'commons.terms';

  describe('module: ' + moduleName, function () {
    var SettingsModel, authService, $scope, $q, termsService, TermsModel;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);
        authService = jasmine.createSpyObj('authService', ['getUser']);
        TermsModel = jasmine.createSpyObj('TermsModel', ['getBestSuitable']);
        $provide.value('SettingsModel', SettingsModel);
        $provide.value('authService', authService);
        $provide.value('TermsModel', TermsModel);
      });

      inject(function (_$rootScope_, _$q_, _termsService_) {
        $scope = _$rootScope_.$new();
        $q = _$q_;
        termsService = _termsService_;
      });
    });

    function mockSettingsResponse(required) {
      SettingsModel.retrieve.and.returnValue($q.resolve({termsRequired: '' + required}));
    }

    function mockUserResponse(acceptance) {
      authService.getUser.and.returnValue($q.resolve({termsAcceptance: acceptance}));
    }

    describe('service: termsService', function () {

      it('should not need terms confirmation', function () {
        // given
        mockSettingsResponse(false);
        mockUserResponse('PENDING');

        // when
        var response;
        termsService.userNeedsToAcceptTerms().then(function (resp) {
          response = resp;
        });
        $scope.$apply();

        // then
        expect(response).toBe(false);
        expect(SettingsModel.retrieve).toHaveBeenCalled();
      });

      it('should need terms confirmation, user already confirmed', function () {
        // given
        mockSettingsResponse(true);
        mockUserResponse('ACCEPTED');

        // when
        var response;
        termsService.userNeedsToAcceptTerms().then(function (resp) {
          response = resp;
        });
        $scope.$apply();

        // then
        expect(response).toBe(false);
        expect(SettingsModel.retrieve).toHaveBeenCalled();
      });

      it('should need terms confirmation', function () {
        // given
        mockSettingsResponse(true);
        mockUserResponse('PENDING');

        // when
        var response;
        termsService.userNeedsToAcceptTerms().then(function (resp) {
          response = resp;
        });
        $scope.$apply();

        // then
        expect(response).toBe(true);
        expect(SettingsModel.retrieve).toHaveBeenCalled();
      });
    });
  });
})();
