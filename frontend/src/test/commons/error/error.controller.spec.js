(function () {
  'use strict';

  var moduleName = 'commons.error';

  describe('module: ' + moduleName, function () {

    var $controller, $stateParams = {}, $state;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_) {
      $controller = _$controller_;
      $state = jasmine.createSpyObj('$state', ['go']);
    }));

    var controllerName = 'ErrorController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $stateParams: $stateParams,
          $state: $state
        });
      }

      it('should init predefined retry button', function () {
        // given
        $stateParams.buttons = ['RETRY'];

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.buttons).toBeArrayOfSize(1);
        expect(ctrl.buttons[0].title).toBe('ERRORS.BUTTON.RETRY');
        var fn = ctrl.buttons[0].action[1];
        expect(angular.isFunction(fn)).toBeTrue();
        var $window = {location: jasmine.createSpyObj('location', ['reload'])};
        fn($window);
        expect($window.location.reload).toHaveBeenCalled();
      });

      it('should init default button', function () {
        // given
        $stateParams.buttons = null;

        // when
        var ctrl = buildController();

        // then
        expect(ctrl.buttons).toBeArrayOfSize(1);
        expect(ctrl.buttons[0].action).toBeUndefined();
        expect(ctrl.buttons[0].title).toBe('ERRORS.BACK');
      });

      it('should call button action', function () {
        // given
        var button = jasmine.createSpyObj('action', ['action']);
        var ctrl = buildController();

        // when
        ctrl.buttonAction(button);

        // then
        expect(button.action).toHaveBeenCalled();
      });

      it('should call default button action', function () {
        // given
        var button = {};
        var ctrl = buildController();

        // when
        ctrl.buttonAction(button);

        // then
        expect($state.go).toHaveBeenCalledWith('main', {}, {reload: true});
      });
    });
  });

})();
