(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'userGuideService';

  describe('module: ' + moduleName, function () {

    var modalService, backendUrlService, $httpBackend, userGuideService, $translate;

    beforeEach(function () {
      module(moduleName, function ($provide) {
        $translate = jasmine.createSpyObj('$translate', ['use', 'storageKey', 'storage', 'preferredLanguage', 'onReady']);
        $translate.use.and.returnValue('en');
        $provide.value('$translate', $translate);
      });

      inject(function (_modalService_, _backendUrlService_, _$httpBackend_, _userGuideService_) {
        modalService = _modalService_;
        backendUrlService = _backendUrlService_;
        $httpBackend = _$httpBackend_;
        userGuideService = _userGuideService_;

        $httpBackend.whenGET('app/commons/ui/components/user-guide/user-guide.modal.html').respond('');
      });
    });

    describe('service: ' + targetName, function () {

      it('should be defined', function () {
        expect(userGuideService).not.toBeUndefined();
      });

      it('should open guide', function () {
        // given
        mockGuidesRequest({en: {'my-guide': 'My Guide'}});
        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user/en/my-guide.md')
            .respond('## My Guide');

        spyOn(modalService, 'open').and.callThrough();

        // when
        userGuideService.open('my-guide');
        $httpBackend.flush();

        // then
        expect(modalService.open).toHaveBeenCalledTimes(1);
      });

      it('should build specific user guide url with the users language', function () {
        // given
        var language = 'de';
        mockGuidesRequest({de: {'my-guide': 'My Guide'}});
        $translate.use.and.returnValue(language);

        // when
        var guideUrl = userGuideService.getGuideUrl('my-guide');

        // then
        expect(guideUrl).toContain(language);
      });

      it('should build main user guide url with the users language', function () {
        // given
        var language = 'de';
        mockGuidesRequest({de: {'my-guide': 'My Guide'}});
        $translate.use.and.returnValue(language);

        // when
        var guideUrl = userGuideService.getUserGuideMainUrl();

        // then
        expect(guideUrl).toContain(language);
      });

      it('should use "en" as default language, if there is no documentation for the users language', function () {
        // given
        var language = 'xx';
        mockGuidesRequest({en: {'my-guide': 'My Guide'}});
        $translate.use.and.returnValue(language);

        // when
        var guideUrl = userGuideService.getUserGuideMainUrl();

        // then
        expect(guideUrl).toContain('en');
      });

      function mockGuidesRequest(guides) {
        $httpBackend.expectGET(backendUrlService.getUrl() + '/docs/guides/user-guides.json').respond(guides);
      }
    });
  });
})();
