(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $uibModalInstance, $q, ExternalUserModel;
    var workspaceId = '123';
    var takenInternalEmails = [];
    var activeInvitationEmails = [];

    beforeEach(module('ui.bootstrap'));
    beforeEach(module('coyo.domain'));
    beforeEach(module(moduleName));

    beforeEach(inject(function (_$q_, _$controller_, $rootScope) {
      $scope = $rootScope.$new();
      $controller = _$controller_;
      $q = _$q_;
      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
      ExternalUserModel = jasmine.createSpyObj('ExternalUserModel', ['checkEmail']);
    }));

    describe('directive: userChooserExternal', function () {

      function buildController() {
        return $controller('UserChooserExternalModalController', {
          $uibModalInstance: $uibModalInstance,
          workspaceId: workspaceId,
          ExternalUserModel: ExternalUserModel,
          activeInvitationEmails: activeInvitationEmails,
          takenInternalEmails: takenInternalEmails
        });
      }

      it('should init with default values', function () {
        // when
        var ctrl = buildController();

        // then
        expect(ctrl.workspaceId).toBe(workspaceId);
        expect(ctrl.takenInternalEmails).toBeEmptyArray();
        expect(ctrl.activeInvitationEmails).toBeEmptyArray();
      });

      it('should handle adding email tag for existing email', function () {
        //given
        var tag = createTag('robert.lang@coyo4.com');
        var response = {
          data: {
            usable: false,
            infoMessage: 'TAKEN_BY_INTERNAL'
          }
        };
        ExternalUserModel.checkEmail.and.returnValue($q.resolve(response));
        var ctrl = buildController();

        //when
        ctrl.validateEmail(tag);
        $scope.$apply();

        //then
        expect(ExternalUserModel.checkEmail).toHaveBeenCalled();
        expect(ctrl.takenInternalEmails.length).toBe(1);
        expect(ctrl.takenInternalEmails[0]).toBe(tag.text);
        expect(ctrl.activeInvitationEmails).toBeEmptyArray();
      });

      it('should handle adding email tag for existing invitation', function () {
        //given
        var tag = createTag('lise@lotte.com');
        var response = {
          data: {
            usable: false,
            infoMessage: 'ACTIVE_INVITATION'
          }
        };
        ExternalUserModel.checkEmail.and.returnValue($q.resolve(response));
        var ctrl = buildController();

        //when
        ctrl.validateEmail(tag);
        $scope.$apply();

        //then
        expect(ExternalUserModel.checkEmail).toHaveBeenCalled();
        expect(ctrl.takenInternalEmails.length).toBe(0);
        expect(ctrl.activeInvitationEmails.length).toBe(1);
        expect(ctrl.activeInvitationEmails[0]).toBe(tag.text);
        expect(ctrl.takenInternalEmails).toBeEmptyArray();
      });

      it('should handle removing existing email', function () {
        //given
        var tag = createTag('lise@lotte.com');
        var ctrl = buildController();
        ctrl.activeInvitationEmails.push(tag.text);

        //when
        ctrl.removeTag(tag);
        $scope.$apply();

        //then
        expect(ctrl.activeInvitationEmails).toBeEmptyArray();
        expect(ctrl.takenInternalEmails).toBeEmptyArray();
      });

      it('should handle removing email tag for existing invitation', function () {
        //given
        var tag = createTag('lise@lotte.com');
        var ctrl = buildController();
        ctrl.takenInternalEmails.push(tag.text);

        //when
        ctrl.removeTag(tag);
        $scope.$apply();

        //then
        expect(ctrl.activeInvitationEmails).toBeEmptyArray();
        expect(ctrl.takenInternalEmails).toBeEmptyArray();
      });

      it('should determine active invitation emails as valid form field', function () {
        //given
        var activeInvitationEmail = 'lise@lotte.com';
        var ctrl = buildController();
        ctrl.emailAddresses.push(activeInvitationEmail);
        ctrl.activeInvitationEmails.push(activeInvitationEmail);

        //when
        var result = ctrl.isFormValid();

        //then
        expect(result).toBeTrue();
      });

      it('should determine taken emails as invalid form field', function () {
        //given
        var alreadyTakenEmail = 'robert.lang@coyo4.com';
        var ctrl = buildController();
        ctrl.emailAddresses.push(alreadyTakenEmail);
        ctrl.takenInternalEmails.push(alreadyTakenEmail);

        //when
        var result = ctrl.isFormValid();

        //then
        expect(result).toBeFalse();
      });

      it('should determine leftover-text as valid form field (the tags-input will display this itself)', function () {
        //given
        var leftoverText = 'notAnEmail';
        var ctrl = buildController();
        ctrl.emailAddresses.push('lise@lotte.com');
        ctrl.leftoverText = leftoverText;

        //when
        var result = ctrl.isFormValid();

        //then
        expect(result).toBeTrue();
      });

      it('should close dialog with active invitation email', function () {
        //given
        var activeInvitationEmail = 'lise@lotte.com';
        var ctrl = buildController();
        ctrl.emailAddresses.push(activeInvitationEmail);
        ctrl.activeInvitationEmails.push(activeInvitationEmail);

        //when
        ctrl.save();
        $scope.$apply();

        //then
        expect($uibModalInstance.close).toHaveBeenCalledWith([activeInvitationEmail]);
      });

      it('should not close dialog with already taken email', function () {
        //given
        var alreadyTakenEmail = 'robert.lang@coyo4.com';
        var ctrl = buildController();
        ctrl.emailAddresses.push(alreadyTakenEmail);
        ctrl.takenInternalEmails.push(alreadyTakenEmail);

        //when
        ctrl.save();
        $scope.$apply();

        //then
        expect($uibModalInstance.close).not.toHaveBeenCalled();
      });

      it('should not close dialog with leftover text', function () {
        //given
        var validEmail = 'lise@lotte.com';
        var leftoverText = 'notAnEmail';
        var ctrl = buildController();
        ctrl.emailAddresses.push(validEmail);
        ctrl.leftoverText = leftoverText;

        //when
        ctrl.save();
        $scope.$apply();

        //then
        expect($uibModalInstance.close).not.toHaveBeenCalled();
      });

      it('should validate leftover text if it is a valid email when closing dialog', function () {
        //given
        var validEmail = 'lise@lotte.com';
        var tag = createTag(validEmail);
        var response = {
          data: {
            usable: true,
            infoMessage: 'FREE_TO_USE'
          }
        };
        ExternalUserModel.checkEmail.and.returnValue($q.resolve(response));
        var ctrl = buildController();
        ctrl.emailAddresses.push(validEmail);

        //when
        ctrl.validateEmail(tag);
        ctrl.save();
        $scope.$apply();

        //then
        expect(ExternalUserModel.checkEmail).toHaveBeenCalledWith(workspaceId, validEmail);
        expect($uibModalInstance.close).toHaveBeenCalledWith([validEmail]);
      });
    });

    function createTag(email) {
      return {text: email};
    }
  });
})();
