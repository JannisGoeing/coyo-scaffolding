(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoMessageTimestamp';

  describe('module: ' + moduleName, function () {

    var $controller, $scope;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {

        inject(function (_$controller_, $rootScope) {
          $controller = _$controller_;
          $scope = $rootScope.$new();
        });
      });

      var controllerName = 'MessageTimestampController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          var controller = $controller(controllerName, {
            $scope: $scope
          });
          return controller;
        }

        it('should initialize the controller', function () {
          // given
          var ctrl = buildController();

          // when
          $scope.$apply();

          // then
          expect(ctrl.isCurrentDay).toBeFunction();
          expect(ctrl.isYesterday).toBeFunction();
          expect(ctrl.isWithinAWeek).toBeFunction();
          expect(ctrl.isAWeekAgo).toBeFunction();
        });

        it('should evaluate the current day', function () {
          // given
          var ctrl = buildController();
          var testDate = new Date();

          // when
          var result1 = ctrl.isCurrentDay(testDate);
          var result2 = ctrl.isYesterday(testDate);
          var result3 = ctrl.isWithinAWeek(testDate);
          var result4 = ctrl.isAWeekAgo(testDate);

          // then
          expect(result1).toBeTruthy();
          expect(result2).toBeFalsy();
          expect(result3).toBeFalsy();
          expect(result4).toBeFalsy();
        });

        it('should evaluate the yesterday time', function () {
          // given
          var ctrl = buildController();
          var day = 86400000;
          var testMinDate = new Date(new Date() - day).setHours(0, 0, 0, 0);
          var testMaxDate = new Date(new Date() - day).setHours(23, 59, 59, 999);

          // when
          var resultMin1 = ctrl.isCurrentDay(testMinDate);
          var resultMin2 = ctrl.isYesterday(testMinDate);
          var resultMin3 = ctrl.isWithinAWeek(testMinDate);
          var resultMin4 = ctrl.isAWeekAgo(testMinDate);

          var resultMax1 = ctrl.isCurrentDay(testMaxDate);
          var resultMax2 = ctrl.isYesterday(testMaxDate);
          var resultMax3 = ctrl.isWithinAWeek(testMaxDate);
          var resultMax4 = ctrl.isAWeekAgo(testMaxDate);

          // then
          expect(resultMin1).toBeFalsy();
          expect(resultMin2).toBeTruthy();
          expect(resultMin3).toBeFalsy();
          expect(resultMin4).toBeFalsy();

          expect(resultMax1).toBeFalsy();
          expect(resultMax2).toBeTruthy();
          expect(resultMax3).toBeFalsy();
          expect(resultMax4).toBeFalsy();
        });

        it('should evaluate the within a week time', function () {
          // given
          var ctrl = buildController();
          var day = 86400000;
          var weekStart = day * 2;
          var weekEnd = day * 7;
          var testMinDate = new Date(new Date() - weekStart).setHours(0, 0, 0, 0);
          var testMaxDate = new Date(new Date() - weekEnd).setHours(23, 59, 59, 999);

          // when
          var resultMin1 = ctrl.isCurrentDay(testMinDate);
          var resultMin2 = ctrl.isYesterday(testMinDate);
          var resultMin3 = ctrl.isWithinAWeek(testMinDate);
          var resultMin4 = ctrl.isAWeekAgo(testMinDate);

          var resultMax1 = ctrl.isCurrentDay(testMaxDate);
          var resultMax2 = ctrl.isYesterday(testMaxDate);
          var resultMax3 = ctrl.isWithinAWeek(testMaxDate);
          var resultMax4 = ctrl.isAWeekAgo(testMaxDate);

          // then
          expect(resultMin1).toBeFalsy();
          expect(resultMin2).toBeFalsy();
          expect(resultMin3).toBeTruthy();
          expect(resultMin4).toBeFalsy();

          expect(resultMax1).toBeFalsy();
          expect(resultMax2).toBeFalsy();
          expect(resultMax3).toBeTruthy();
          expect(resultMax4).toBeFalsy();
        });

        it('should evaluate the week ago time', function () {
          // given
          var ctrl = buildController();
          var day = 86400000;
          var weekAgo = day * 8;
          var testDate = new Date(new Date() - weekAgo).setHours(0, 0, 0, 0);

          // when
          var result1 = ctrl.isCurrentDay(testDate);
          var result2 = ctrl.isYesterday(testDate);
          var result3 = ctrl.isWithinAWeek(testDate);
          var result4 = ctrl.isAWeekAgo(testDate);

          // then
          expect(result1).toBeFalsy();
          expect(result2).toBeFalsy();
          expect(result3).toBeFalsy();
          expect(result4).toBeTruthy();
        });

      });

    });

  });

})();
