(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'fileSize';

  describe('module: ' + moduleName, function () {
    var filter;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$filter_) {
        filter = _$filter_(targetName);
      });
    });

    describe('filter: ' + targetName, function () {

      it('should be registered', function () {
        // then
        expect(filter).toBeDefined();
      });

      it('should handle different file sizes', function () {
        // then
        expect(filter(Math.pow(1024, 0))).toEqual('1.0 B');
        expect(filter(Math.pow(1024, 1))).toEqual('1.0 KB');
        expect(filter(Math.pow(1024, 2))).toEqual('1.0 MB');
        expect(filter(Math.pow(1024, 3))).toEqual('1.0 GB');
        expect(filter(Math.pow(1024, 4))).toEqual('1.0 TB');
        expect(filter(Math.pow(1024, 5))).toEqual('1.0 PB');
      });

      it('should handle different precisions', function () {
        // then
        expect(filter(1, 0)).toEqual('1 B');
        expect(filter(1, 1)).toEqual('1.0 B');
        expect(filter(1, 2)).toEqual('1.00 B');
        expect(filter(1, 3)).toEqual('1.000 B');
        expect(filter(1, 4)).toEqual('1.0000 B');
        expect(filter(1, 5)).toEqual('1.00000 B');
      });

      it('should handle invalid file sizes', function () {
        // then
        expect(filter('')).toEqual('-');
        expect(filter(null)).toEqual('-');
        expect(filter(undefined)).toEqual('-');
        expect(filter(NaN)).toEqual('-');
        expect(filter(Infinity)).toEqual('-');
        expect(filter(-Infinity)).toEqual('-');
      });

      it('should handle empty file sizes', function () {
        // then
        expect(filter(0)).toEqual('0.0 B');
      });
    });
  });

})();
