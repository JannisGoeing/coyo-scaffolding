(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoImagePicker';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, fileLibraryModalService, $element, ngModelController;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        fileLibraryModalService = jasmine.createSpyObj('fileLibraryModalService', ['open']);
        $element = jasmine.createSpyObj('$element', ['controller']);
        ngModelController = jasmine.createSpyObj('ngModelController', ['$setViewValue']);
        $element.controller.and.callFake(function (controllerName) {
          return controllerName === 'ngModel' ? ngModelController : undefined;
        });

        inject(function (_$controller_, _$q_, $rootScope) {
          $q = _$q_;
          $scope = $rootScope.$new();
          $controller = _$controller_;
        });
      });

      var controllerName = 'ImagePickerController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          return $controller(controllerName, {
            fileLibraryModalService: fileLibraryModalService,
            $element: $element
          });
        }

        it('should select single image', function () {
          // given
          var oldFile = [{fileId: 'OLD-FILE-ID'}];
          var newFile = [{id: 'FILE-ID'}];
          var options = {selectMode: 'single'};

          var ctrl = buildController();
          ctrl.selectedImages = oldFile;
          ctrl.options = options;
          fileLibraryModalService.open.and.returnValue($q.resolve(newFile));

          // when
          ctrl.openFileLibrary();
          $scope.$apply();

          // then
          expect(ngModelController.$setViewValue).toHaveBeenCalledWith(newFile);
        });

        it('should select multiple images', function () {
          // given
          var selectedFiles = [{fileId: 'FILE-ID-1'}, {fileId: 'FILE-ID-2'}];
          var addedFiles = [{id: 'FILE-ID-1'}, {id: 'FILE-ID-3'}];
          var options = {selectMode: 'multiple'};

          var ctrl = buildController();
          ctrl.selectedImages = selectedFiles;
          ctrl.options = options;
          fileLibraryModalService.open.and.returnValue($q.resolve(addedFiles));

          // when
          ctrl.openFileLibrary();
          $scope.$apply();

          // then
          expect(ngModelController.$setViewValue).toHaveBeenCalled();
          var updatedFiles = ngModelController.$setViewValue.calls.mostRecent().args[0];
          expect(updatedFiles).toBeArrayOfSize(3);
          expect(updatedFiles[0].fileId).toBe('FILE-ID-1');
          expect(updatedFiles[1].fileId).toBe('FILE-ID-2');
          expect(updatedFiles[2].fileId).toBe('FILE-ID-3');
        });

        it('should remove a selected image', function () {
          // given
          var selectedFiles = [{fileId: 'FILE-ID-1'}, {fileId: 'FILE-ID-2'}];

          var ctrl = buildController();
          ngModelController.$viewValue = selectedFiles;
          ctrl.options = {selectMode: 'multiple'};

          // when
          ctrl.removeFile(selectedFiles[0]);

          // then
          expect(ngModelController.$setViewValue).toHaveBeenCalled();
          var updatedFiles = ngModelController.$setViewValue.calls.mostRecent().args[0];
          expect(updatedFiles).toBeArrayOfSize(1);
          expect(updatedFiles[0].fileId).toBe('FILE-ID-2');
        });

      });

    });

  });
})();
