(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var directiveName = 'coyoOptionsfield';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, utilService, ngModel;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {

        ngModel = jasmine.createSpyObj('ngModel', ['$setViewValue']);
        ngModel.$viewValue = [];
        ngModel.$validators = [];

        inject(function (_$controller_, $rootScope, _utilService_) {
          $controller = _$controller_;
          $scope = $rootScope.$new();
          utilService = _utilService_;
        });
      });

      var controllerName = 'OptionsFieldController';

      describe('controller: ' + controllerName, function () {

        function buildController() {
          var controller = $controller(controllerName, {
            $scope: $scope,
            utilService: utilService
          }, {
            ngModel: ngModel
          });
          return controller;
        }

        it('should initialize the model of the options field', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          ngModel.$render();
          $scope.$apply();

          // then
          expect(ctrl.options).toBeArrayOfSize(1);
          expect(ctrl.options[0].id).toBeNonEmptyString();
          expect(ctrl.options[0].name).toBeEmptyString();
          expect(ctrl.focusedOption).toBeEmptyString();
          expect(ngModel.$validators.noOptions).toBeFunction();
        });

        it('should add a new option and focus it', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.addOption();

          // then
          expect(ctrl.options).toBeArrayOfSize(1);
          expect(ctrl.options[0].id).toBeNonEmptyString();
          expect(ctrl.options[0].name).toBeEmptyString();
          expect(ctrl.focusedOption).toEqual(ctrl.options[0].id);
        });

        it('should delete first option', function () {
          // given
          var optionOne = {
            id: utilService.uuid(),
            name: 'Test Option I'
          };
          var optionTwo = {
            id: utilService.uuid(),
            name: 'Test Option II'
          };
          var ctrl = buildController();
          ctrl.options = [optionOne, optionTwo];

          // when
          ctrl.deleteOption(0);

          // then
          expect(ctrl.options).toBeArrayOfSize(1);
          expect(ctrl.options[0]).toEqual(optionTwo);
        });

        it('should fail when trying to delete last option', function () {
          // given
          var option = {
            id: utilService.uuid(),
            name: 'Test Option'
          };
          var ctrl = buildController();
          ctrl.options = [option];

          // when
          ctrl.deleteOption(0);

          // then
          expect(ctrl.options).toBeArrayOfSize(1);
          expect(ctrl.options[0]).toEqual(option);
        });

      });

    });

  });
})();
