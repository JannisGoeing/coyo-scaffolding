(function () {
  'use strict';

  var moduleName = 'commons.ui';
  var targetName = 'imageUploadModal';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $log, $q, $uibModalInstance, Upload, $httpBackend, coyoEndpoints, backendUrlService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, $rootScope, _$log_, _$q_, _$httpBackend_, _coyoEndpoints_,
                                _backendUrlService_) {
      $scope = $rootScope.$new();
      $log = _$log_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      $controller = _$controller_;
      coyoEndpoints = _coyoEndpoints_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

      $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close', 'dismiss']);
      Upload = jasmine.createSpyObj('Upload', ['upload', 'dataUrltoBlob']);
    }));

    describe('service: ' + targetName, function () {

      var controllerName = 'ImageUploadModalController';

      describe('controller: ' + controllerName, function () {

        function buildController(options) {
          return $controller(controllerName, {
            $log: $log,
            $uibModalInstance: $uibModalInstance,
            Upload: Upload,
            options: options
          });
        }

        describe('controller: init', function () {

          it('should init with default values', function () {
            // when
            var ctrl = buildController({});

            // then
            expect(ctrl.title).toBe('MODULE.PROFILE.MODALS.IMAGE_UPLOAD.TITLE');
            expect(ctrl.status).toBeNonEmptyObject();
            expect(ctrl.status.uploading).toBeFalsy();
            expect(ctrl.status.selecting).toBeFalsy();
            expect(ctrl.status.deleting).toBeFalsy();
          });

          it('should trigger upload correctly', function () {
            // given
            Upload.upload.and.callFake(function () {
              var deferred = $q.defer();
              deferred.resolve({
                data: {}
              });
              return deferred.promise;
            });

            // when
            var ctrl = buildController({});
            ctrl.upload({});

            // then
            expect(ctrl.status.uploading).toBeTruthy();
            $scope.$apply();

            expect(Upload.upload).toHaveBeenCalled();
            expect($uibModalInstance.close).toHaveBeenCalled();
            expect(ctrl.status.uploading).toBeFalsy();
          });

          it('should handle error on upload with log message', function () {
            // given
            Upload.upload.and.callFake(function () {
              var deferred = $q.defer();
              deferred.reject({
                error: {}
              });
              return deferred.promise;
            });
            spyOn($log, 'error');

            // when
            var ctrl = buildController({});
            ctrl.upload({});

            // then
            expect(ctrl.status.uploading).toBeTruthy();
            $scope.$apply();

            expect(Upload.upload).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(ctrl.status.uploading).toBeFalsy();
          });

          it('should trigger avatar deletion correctly', function () {
            // given
            var url = backendUrlService.getUrl() + coyoEndpoints.sender.avatar.replace('{id}', 0);
            var ctrl = buildController({url: url, canDelete: true});
            $httpBackend.expectDELETE(url).respond(200);

            // when
            ctrl.deleteImage();

            // then
            expect(ctrl.status.deleting).toBeTruthy();
            $httpBackend.flush();

            expect($uibModalInstance.close).toHaveBeenCalled();
            expect(ctrl.status.deleting).toBeFalsy();
          });

          it('should trigger cover deletion correctly', function () {
            // given
            var url = backendUrlService.getUrl() + coyoEndpoints.sender.cover.replace('{id}', 0);
            var ctrl = buildController({url: url, canDelete: true});
            $httpBackend.expectDELETE(url).respond(200);

            // when
            ctrl.deleteImage();

            // then
            expect(ctrl.status.deleting).toBeTruthy();
            $httpBackend.flush();

            expect($uibModalInstance.close).toHaveBeenCalled();
            expect(ctrl.status.deleting).toBeFalsy();
          });

        });

      });
    });
  });
})();
