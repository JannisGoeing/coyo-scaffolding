(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var $compile, $scope, SettingsModel;

    beforeEach(function () {
      module(moduleName);

      module(function ($provide) {
        SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieveByKey']);
        SettingsModel.retrieveByKey.and.returnValue({
          then: function (callback) {
            callback('(?=.*[a-z]).{6,}');
          }
        });
        $provide.value('SettingsModel', SettingsModel);
      });

      inject(function (_$compile_, $rootScope) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
      });
    });

    describe('directive: coyo-check-password', function () {
      var form;

      beforeEach(function () {
        var html = '<form name="form"><input ng-model="model.password" name="password" coyo-check-password></form>';
        $scope.model = {password: null};
        $compile(angular.element(html))($scope);
        $scope.$digest();
        form = $scope.form;
      });

      it('should not accept invalid password', function () {
        // when
        form.password.$setViewValue('abc');
        $scope.$digest();

        // then
        expect(form.password.$valid).toBe(false);
        expect(form.password.$error.password).toBe(true);
      });

      it('should not accept valid password', function () {
        // when
        form.password.$setViewValue('Abcdef123');
        $scope.$digest();

        // then
        expect(form.password.$valid).toBe(true);
        expect(form.password.$error.password).not.toBeDefined();
      });
    });
  });
})();
