(function () {
  'use strict';

  var moduleName = 'commons.ui';

  describe('module: ' + moduleName, function () {

    var scrollBehaviourService, $rootScope;
    beforeEach(function () {

      module(moduleName);

      inject(function (_scrollBehaviourService_, _$rootScope_) {
        scrollBehaviourService = _scrollBehaviourService_;
        $rootScope = _$rootScope_;
      });
    });

    var serviceName = 'scrollBehaviourService';

    describe('service: ' + serviceName, function () {
      it('should not change body css property when its not a "xs" screen size', function () {
        //given
        $rootScope.screenSize = {
          isXs: false
        };

        // when
        scrollBehaviourService.disableBodyScrollingOnXsScreen();

        // then
        expect(angular.element(document).find('html').hasClass('no-scroll')).toBeFalsy();
      });

      it('should change body css property to hidden', function () {
        $rootScope.screenSize = {
          isXs: true
        };

        // when
        scrollBehaviourService.disableBodyScrolling();

        // then
        expect(angular.element(document).find('html').hasClass('no-scroll')).toBeTruthy();
      });

      it('should change body css property to scroll', function () {
        //given
        $rootScope.screenSize = {
          isXs: false
        };
        angular.element(document).find('html').addClass('no-scroll');

        // when
        scrollBehaviourService.enableBodyScrolling();

        // then
        expect(angular.element(document).find('html').hasClass('no-scroll')).toBeFalsy();
      });
    });
  });

})();
