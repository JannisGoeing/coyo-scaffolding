(function () {
  'use strict';

  describe('domain: ForumThreadModel', function () {

    beforeEach(module('commons.target'));
    beforeEach(module('coyo.apps.forum'));

    var $httpBackend, ForumThreadModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _ForumThreadModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      ForumThreadModel = _ForumThreadModel_;
      backendUrlService = _backendUrlService_;
      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('should load all threads', function () {
        // given
        var response = null;
        var threads = [
          {title: 'title1'},
          {title: 'title2'}
        ];
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads')
            .respond(200, threads);

        // when
        model.get().then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(2);
      });

      it('should load a specific forum thread', function () {
        // given
        var thread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          publishDate: '0'
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/forumThreadId')
            .respond(200, thread);

        // when
        model.get('forumThreadId').then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(thread.title);
      });

      it('should save a new forum thread', function () {
        // given
        var newThread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          publishDate: '0'
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectPOST(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads')
            .respond(200, newThread);

        // when
        model.create(true).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(newThread.title);
      });

      it('should close a forum thread', function () {
        // given
        var newThread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          closed: true
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/close')
            .respond(200, newThread);

        // when
        model.setClosed(newThread.closed).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.closed).toEqual(true);
      });

      it('should reopen a forum thread', function () {
        // given
        var newThread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          closed: false
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/close')
            .respond(200, newThread);

        // when
        model.setClosed(newThread.closed).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.closed).toEqual(false);
      });

      it('should pin a forum thread', function () {
        // given
        var newThread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          pinned: true
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/pin')
            .respond(200, newThread);

        // when
        model.setPinned(newThread.pinned).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.pinned).toEqual(true);
      });

      it('should unpin a forum thread', function () {
        // given
        var newThread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          pinned: false
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/pin')
            .respond(200, newThread);

        // when
        model.setPinned(newThread.pinned).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.pinned).toEqual(false);
      });

      it('should delete a forum thread', function () {
        // given
        var thread = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          pinned: false
        };
        var response = null;
        var model = new ForumThreadModel({senderId: 'senderId', appId: 'appId', id: 'threadId'});
        $httpBackend
            .expectDELETE(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/forum/threads/threadId')
            .respond(200, thread);

        // when
        model.delete().then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(thread.title);
      });

    });
  });
}());
