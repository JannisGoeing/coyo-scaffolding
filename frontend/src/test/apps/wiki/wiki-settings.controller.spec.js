(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';
  var ctrlName = 'WikiSettingsController';

  describe('module: ' + moduleName, function () {

    beforeEach(module('commons.target'));
    beforeEach(module(moduleName));

    describe('controller: ' + ctrlName, function () {
      var $controller, $scope;

      beforeEach(inject(function ($rootScope, _$controller_) {
        $scope = $rootScope.$new();
        $controller = _$controller_;

        $scope.model = {
          settings: {}
        };

      }));

      function buildController() {
        return $controller(ctrlName, {
          $scope: $scope
        });
      }

      describe('controller init', function () {

        it('should initialize with default values for editorType and commentsAllowed', function () {
          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.app.settings.editorType).toBe('VIEWER');
          expect(ctrl.app.settings.commentsAllowed).toBe(false);
          expect(ctrl.app.settings.home).toBeUndefined();
          expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('VIEWER');
        });

        it('should initialize editorType and commentsAllowed if set', function () {
          // given
          $scope.model.settings.editorType = 'ADMIN';
          $scope.model.settings.commentsAllowed = true;
          $scope.model.settings.home = 'ARTICLE-ID';

          // when
          var ctrl = buildController();
          ctrl.$onInit();

          // then
          expect(ctrl.app.settings.editorType).toBe('ADMIN');
          expect(ctrl.app.settings.commentsAllowed).toBe(true);
          expect(ctrl.app.settings.home).toBe('ARTICLE-ID');
          expect(ctrl.app.settings.folderPermissions.modifyRole).toBe('ADMIN');
        });

      });

    });
  });
})();
