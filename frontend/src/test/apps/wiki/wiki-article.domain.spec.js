(function () {
  'use strict';

  describe('domain: WikiArticleModel', function () {

    beforeEach(module('commons.target'));
    beforeEach(module('coyo.apps.wiki'));

    var $httpBackend, WikiArticleModel, backendUrlService, WidgetLayoutModel;

    beforeEach(inject(function (_$httpBackend_, _WikiArticleModel_, _backendUrlService_, _WidgetLayoutModel_) {
      $httpBackend = _$httpBackend_;
      WikiArticleModel = _WikiArticleModel_;
      backendUrlService = _backendUrlService_;
      WidgetLayoutModel = _WidgetLayoutModel_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {

      it('should count wiki articles', function () {
        // given
        var app = {senderId: 'senderId', id: 'appId'};
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/count')
            .respond(200, 2);

        // when
        var response = null;
        WikiArticleModel.count(app, true, false, true).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response).toEqual(2);
      });

      it('should get wiki articles of parent recursively', function () {
        // given
        var app = {senderId: 'senderId', id: 'appId'};
        var articles = [
          {id: 'PARENT-ID'},
          {id: 'CHILD-ID1'},
          {id: 'CHILD-ID2'}
        ];

        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/children-recursive?parentId=PARENT-ID&includeWidgets=true')
            .respond(200, articles);

        // when
        var response = null;
        WikiArticleModel.getSubArticlesRecursiveWithWidgets(app, articles[0].id).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(3);
        expect(response[0].id).toEqual(articles[0].id);
      });

      it('should cache recursively loaded wiki articles with widgets', function () {
        // given
        var app = {senderId: 'senderId', id: 'appId'};
        var articles = [
          {id: 'PARENT-ID', widgetLayout: {name: 'PARENT-LAYOUT'}},
          {id: 'CHILD-ID1', widgetLayout: {name: 'CHILD1-LAYOUT'}},
          {id: 'CHILD-ID2', widgetLayout: {name: 'CHILD2-LAYOUT'}}
        ];

        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/children-recursive?parentId=PARENT-ID&includeWidgets=true')
            .respond(200, articles);

        spyOn(WidgetLayoutModel.prototype, 'cacheWithWidgets');

        // when
        var response = null;
        WikiArticleModel.getSubArticlesRecursiveWithWidgets(app, articles[0].id).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(WidgetLayoutModel.prototype.cacheWithWidgets).toHaveBeenCalledTimes(3);
        expect(response.length).toEqual(3);
        expect(response[0].id).toEqual(articles[0].id);
      });

    });

    describe('instance members', function () {

      it('should load all articles', function () {
        // given
        var response = null;
        var articles = [
          {title: 'title1'},
          {title: 'title2'}
        ];
        var model = new WikiArticleModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles')
            .respond(200, articles);

        // when
        model.get().then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(2);
      });

      it('should load sub articles of a parent article (not set) with permissions', function () {
        // given
        var response = null;
        var articles = [
          {title: 'title1'},
          {title: 'title2'}
        ];
        $httpBackend
            .expectGET(backendUrlService.getUrl()
          + '/web/senders/senderId/apps/appId/wiki/articles/children?_permissions=edit,delete')
            .respond(200, articles);

        // when
        WikiArticleModel.getSubArticlesWithPermissions({senderId: 'senderId', id: 'appId'}, '')
            .then(function (data) {
              response = data;
            });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(2);
      });

      it('should load sub articles of a parent article (set) with permissions', function () {
        // given
        var response = null;
        var articles = [
          {title: 'title1'},
          {title: 'title2'}
        ];
        $httpBackend
            .expectGET(backendUrlService.getUrl()
          + '/web/senders/senderId/apps/appId/wiki/articles/children?_permissions=edit,delete&parentId=parentId')
            .respond(200, articles);

        // when
        WikiArticleModel
            .getSubArticlesWithPermissions({senderId: 'senderId', id: 'appId'}, 'parentId')
            .then(function (data) {
              response = data;
            });
        $httpBackend.flush();

        // then
        expect(response.length).toEqual(2);
      });

      it('should load a specific wiki article', function () {
        // given
        var article = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          publishDate: '0'
        };
        var response = null;
        var model = new WikiArticleModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectGET(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/wikiArticleId')
            .respond(200, article);

        // when
        model.get('wikiArticleId').then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(article.title);
      });

      it('should save a new wiki article', function () {
        // given
        var newArticle = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          publishDate: '0'
        };
        var response = null;
        var model = new WikiArticleModel({senderId: 'senderId', appId: 'appId'});
        $httpBackend
            .expectPOST(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles')
            .respond(200, newArticle);

        // when
        model.create(true).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(newArticle.title);
      });

      it('should update an already existing wiki article', function () {
        // given
        var article = {
          title: 'title1',
          text: 'text1',
          teaserText: 'teaserText1',
          publishDate: '0'
        };
        var response = null;
        var model = new WikiArticleModel({senderId: 'senderId', appId: 'appId', id: 'wikiArticleId'});
        $httpBackend
            .expectPUT(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/wikiArticleId')
            .respond(200, article);

        // when
        model.update(true).then(function (data) {
          response = data;
        });
        $httpBackend.flush();

        // then
        expect(response.title).toEqual(article.title);
      });

      it('should delete an existing wiki article', function () {
        // given
        var model = new WikiArticleModel({senderId: 'senderId', appId: 'appId', id: 'wikiArticleId'});
        $httpBackend
            .expectDELETE(backendUrlService.getUrl() + '/web/senders/senderId/apps/appId/wiki/articles/wikiArticleId')
            .respond(200);

        // when
        model.delete();
        $httpBackend.flush();

        // then
        // nothing to do here...
      });

    });
  });
}());
