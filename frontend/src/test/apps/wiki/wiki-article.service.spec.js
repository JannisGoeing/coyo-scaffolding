(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';

  describe('module: ' + moduleName, function () {

    // the service's name
    var $scope, $q, $interval, wikiExportConfig, WikiArticleModel, wikiArticleService, modalService, titleService, app, article, user;

    beforeEach(function () {
      user = {id: 'USER-ID'};

      article = jasmine.createSpyObj('ArticleModel', ['delete', 'lock', 'unlock']);
      article.id = 'ARTICLE-ID';
      article.title = 'ARTICLE-TITLE';
      article.wikiArticles = 0;

      app = jasmine.createSpyObj('AppModel', ['save']);
      app.id = 'APP_ID';
      app.settings = {};

      modalService = jasmine.createSpyObj('modalService', ['confirmDelete', 'confirm', 'open']);
      titleService = jasmine.createSpyObj('titleService', ['getTitle', 'setTitle']);
      WikiArticleModel = jasmine.createSpyObj('WikiArticleModel', ['getSubArticlesRecursiveWithWidgets']);

      module(moduleName, function ($provide) {
        // provide dependencies for service
        $provide.value('modalService', modalService);
        $provide.value('ngxPageTitleService', titleService);
        $provide.value('WikiArticleModel', WikiArticleModel);
      });

      inject(function (_wikiArticleService_, _wikiExportConfig_, _$q_, _$interval_, $rootScope) {
        // inject dependencies for test
        wikiArticleService = _wikiArticleService_;
        wikiExportConfig = _wikiExportConfig_;
        $q = _$q_;
        $scope = $rootScope.$new();
        $interval = _$interval_;
      });

    });

    describe('Service: wikiArticleService', function () {

      it('should delete an article', function () {
        // given
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        article.delete.and.returnValue($q.resolve());

        // when
        wikiArticleService.deleteArticle(app, article);
        $scope.$apply();

        // then
        expect(modalService.confirmDelete).toHaveBeenCalled();
        var modalOptions = modalService.confirmDelete.calls.mostRecent().args[0];
        expect(modalOptions.alerts).toBeEmptyArray();
        expect(modalOptions.translationContext).toEqual({title: article.title});
        expect(article.delete).toHaveBeenCalled();

      });

      it('should remove the article as home when deleting and show warning', function () {
        // given
        app.settings.home = article.id;
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        article.delete.and.returnValue($q.resolve());

        // when
        wikiArticleService.deleteArticle(app, article);
        $scope.$apply();

        // then
        var modalOptions = modalService.confirmDelete.calls.mostRecent().args[0];
        expect(modalOptions.alerts).toEqual([{
          level: 'danger',
          title: 'APP.WIKI.ARTICLE.DELETE.WARNING.TITLE',
          text: 'APP.WIKI.ARTICLE.DELETE.HOME.TEXT'
        }]);
        expect(modalOptions.translationContext).toEqual({title: article.title});
        expect(article.delete).toHaveBeenCalled();
        expect(app.settings.home).toBe('');
        expect(app.save).toHaveBeenCalled();
      });

      it('should show a warning if article has subarticles', function () {
        // given
        article.wikiArticles = 2;
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        article.delete.and.returnValue($q.resolve());

        // when
        wikiArticleService.deleteArticle(app, article);
        $scope.$apply();

        // then
        var modalOptions = modalService.confirmDelete.calls.mostRecent().args[0];
        expect(modalOptions.alerts).toEqual([{
          level: 'danger',
          title: 'APP.WIKI.ARTICLE.DELETE.WARNING.TITLE',
          text: 'APP.WIKI.ARTICLE.DELETE.MULTIPLE.TEXT'
        }]);
        expect(modalOptions.translationContext).toEqual({
          noOfArticles: article.wikiArticles,
          title: article.title
        });
        expect(article.delete).toHaveBeenCalled();
      });

      it('should show a warning if article has shares', function () {
        // given
        article.shareCount = 1;
        modalService.confirmDelete.and.returnValue({result: $q.resolve()});
        article.delete.and.returnValue($q.resolve());

        // when
        wikiArticleService.deleteArticle(app, article);
        $scope.$apply();

        // then
        var modalOptions = modalService.confirmDelete.calls.mostRecent().args[0];
        expect(modalOptions.alerts).toEqual([{
          level: 'danger',
          title: 'APP.WIKI.ARTICLE.DELETE.WARNING.TITLE',
          text: 'APP.WIKI.ARTICLE.DELETE.MULTIPLE.SHARE.TEXT'
        }]);
        expect(modalOptions.translationContext).toEqual({
          noOfShares: article.shareCount,
          title: article.title
        });
        expect(article.delete).toHaveBeenCalled();
      });

      it('should open confirm modal when number of articles to be exported exceeds config const', function () {
        // given
        modalService.confirm.and.returnValue({
          result: $q.resolve()
        });

        // when
        wikiArticleService.confirmWikiExport(wikiExportConfig.exportWarningArticlesCount + 1);
        $scope.$apply();

        // then
        expect(modalService.confirm).toHaveBeenCalled();
      });

      it('should not open confirm modal when number of articles to be exported is smaller than config const', function () {
        // given
        modalService.confirm.and.returnValue({
          result: $q.resolve()
        });

        // when
        wikiArticleService.confirmWikiExport(wikiExportConfig.exportWarningArticlesCount - 1);
        $scope.$apply();

        // then
        expect(modalService.confirm).not.toHaveBeenCalled();
      });

      it('should open export preview of articles in a modal', function () {
        // given
        var opened = $q.defer();
        var closed = $q.defer();
        modalService.open.and.returnValue({
          result: $q.resolve(),
          opened: opened.promise,
          closed: closed.promise
        });

        var exportContent = {appId: app.id, articles: [article], title: article.title, selectedLanguage: 'EN'};

        // when
        wikiArticleService.exportPreview(exportContent);

        // then
        var modalOptions = modalService.open.calls.mostRecent().args[0];
        expect(modalOptions.resolve.exportPreviewContent()).toEqual(exportContent);
        expect(modalOptions.windowTopClass).toEqual('export-preview-modal');
      });

      it('should open export preview of article with sub-articles in a modal', function () {
        // given
        var parentArticle = {
          id: 'PARENT-ARTICLE-ID',
          title: 'PARENT_ARTICLE-TITLE'
        };

        var opened = $q.defer();
        var closed = $q.defer();
        modalService.open.and.returnValue({
          result: $q.resolve(),
          opened: opened.promise,
          closed: closed.promise
        });

        // when
        wikiArticleService.exportPreviewWithSubArticles(app, parentArticle);

        // then
        var modalOptions = modalService.open.calls.mostRecent().args[0];
        expect(modalOptions.resolve.exportPreviewContent()).toEqual({
          appId: app.id,
          articles: WikiArticleModel.getSubArticlesRecursiveWithWidgets(app, parentArticle.id),
          modalTitle: parentArticle.title,
          showLoadingBar: true,
          selectedLanguage: undefined
        });
        expect(modalOptions.windowTopClass).toEqual('export-preview-modal');
      });

      it('should change site title to article title when opening export preview', function () {
        // given
        var siteTitle = 'Site Title';
        titleService.getTitle.and.returnValue(siteTitle);

        var opened = $q.defer();
        var closed = $q.defer();
        modalService.open.and.returnValue({
          result: $q.resolve(),
          opened: opened.promise,
          closed: closed.promise
        });
        var exportContent = {appId: app.id, articles: [article], modalTitle: article.title, selectedLanguage: 'EN'};
        wikiArticleService.exportPreview(exportContent);

        // when
        opened.resolve();
        $scope.$apply();

        // then
        expect(titleService.setTitle).toHaveBeenCalledWith(article.title);
      });

      it('should change site title back to default when export preview was closed', function () {
        // given
        var siteTitle = 'Site Title';
        titleService.getTitle.and.returnValue(siteTitle);

        var opened = $q.defer();
        var closed = $q.defer();
        modalService.open.and.returnValue({
          result: $q.resolve(),
          opened: opened.promise,
          closed: closed.promise
        });
        wikiArticleService.exportPreview(app.id, article, article.title, 'EN');

        // when
        closed.resolve();
        $scope.$apply();

        // then
        expect(titleService.setTitle).toHaveBeenCalledWith(siteTitle);
      });

      it('should lock an article', function () {
        // given
        var lockedArticle = angular.copy(article);
        lockedArticle.locked = true;
        lockedArticle.lockHolder = user;
        article.lock.and.returnValue($q.resolve(lockedArticle));

        // when
        var promise = wikiArticleService.lock(article, user);

        // then
        expect(article.lock).toHaveBeenCalledTimes(1);
        promise.then(function (lockedArticle) {
          expect(lockedArticle.locked).toBe(true);
          expect(lockedArticle.lockHolder).toBe(user);
        });

        // when
        $scope.$apply();
        $interval.flush(10 * 60 * 1000);

        // then
        expect(article.lock).toHaveBeenCalledTimes(3);
      });

      it('should unlock an locked article', function () {
        // given
        article.locked = true;
        article.lockHolder = {id: 'ANOTHER-USER'};
        article.unlock.and.returnValue($q.resolve({locked: false, lockHolder: undefined}));
        spyOn($interval, 'cancel');

        // when
        wikiArticleService.unlock(article).then(function (unlockedArticle) {
          expect(unlockedArticle.locked).toBe(false);
          expect(unlockedArticle.lockHolder).toBeUndefined();
        });
        $scope.$apply();

        // then
        expect(article.unlock).toHaveBeenCalledTimes(1);
        expect($interval.cancel).toHaveBeenCalledTimes(1);
      });

      it('should release a locked article', function () {
        // given
        article.locked = true;
        article.lockHolder = user;
        article.unlock.and.returnValue($q.resolve({locked: false, lockHolder: undefined}));
        spyOn($interval, 'cancel');

        // when
        wikiArticleService.releaseLock(article, user, true).then(function (unlockedArticle) {
          expect(unlockedArticle.locked).toBe(false);
          expect(unlockedArticle.lockHolder).toBeUndefined();
        });
        $scope.$apply();

        // then
        expect(article.unlock).toHaveBeenCalledTimes(1);
        expect($interval.cancel).toHaveBeenCalledTimes(1);
      });

      it('should not release the lock if user is not the lock holder', function () {
        // given
        article.locked = true;
        article.lockHolder = {id: 'ANOTHER-USER'};
        spyOn($interval, 'cancel');

        // when
        wikiArticleService.releaseLock(article, user, true).catch(function () {
          expect(article.locked).toBe(true);
          expect(article.lockHolder).toEqual({id: 'ANOTHER-USER'});
        });
        $scope.$apply();

        // then
        expect(article.unlock).not.toHaveBeenCalled();
        expect($interval.cancel).not.toHaveBeenCalled();
      });

    });

  });
})();
