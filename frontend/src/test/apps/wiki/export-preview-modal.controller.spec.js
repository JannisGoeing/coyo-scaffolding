(function () {
  'use strict';

  var moduleName = 'coyo.apps.wiki';

  describe('module: ' + moduleName, function () {

    var widgetLayoutService;

    beforeEach(function () {

      widgetLayoutService = jasmine.createSpyObj('widgetLayoutService', ['onload']);

      module(moduleName, function ($provide) {
        $provide.value('widgetLayoutService', widgetLayoutService);
      });

    });

    var controllerName = 'ExportPreviewModalController';

    describe('controller: ' + controllerName, function () {

      var $rootScope, $scope, $controller, $uibModalInstance, $window, $document, $q, wikiExportConfig;
      var app, mainArticle, articles, exportPreviewContent;

      beforeEach(function () {

        mainArticle = {
          id: 'MAIN-ARTICLE-ID',
          title: 'MAIN-ARTICLE-TITLE'
        };

        articles = [mainArticle];

        app = {
          id: 'APP_ID'
        };

        exportPreviewContent = {
          appId: app.id,
          articles: articles,
          modalTitle: mainArticle.title,
          articleTitle: mainArticle.title,
          selectedLanguage: 'EN'
        };

        inject(function (_$rootScope_, _$controller_, _$document_, _$q_) {
          $rootScope = _$rootScope_;
          $scope = $rootScope.$new();
          $controller = _$controller_;
          $document = _$document_;
          $q = _$q_;
        });

        widgetLayoutService.onload.and.returnValue($q.resolve());
        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['dismiss']);
        $uibModalInstance.rendered = $q.resolve();
        $uibModalInstance.closed = $q.resolve();
        $window = jasmine.createSpyObj('$window', ['print']);

        wikiExportConfig = {
          defaultScale: 1,
          pageWidth: 860,
          previewRenderingBatchSize: 3
        };

      });

      function buildController(exportPreviewContent) {
        return $controller(controllerName, {
          $uibModalInstance: $uibModalInstance,
          $rootScope: $rootScope,
          $scope: $scope,
          $window: $window,
          $document: $document,
          widgetLayoutService: widgetLayoutService,
          exportPreviewContent: exportPreviewContent,
          wikiExportConfig: wikiExportConfig
        });
      }

      it('should init with widget layout for single article', function () {
        // when
        var ctrl = buildController(exportPreviewContent);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(widgetLayoutService.onload).toHaveBeenCalled();
        expect(ctrl.loading).toBe(false);
      });

      it('should init with batch loading articles', function () {
        // when
        var article1, article2;
        article1 = {
          id: 'ARTICLE-ID1',
          title: 'ARTICLE-TITLE1'
        };

        article2 = {
          id: 'ARTICLE-ID2',
          title: 'ARTICLE-TITLE2'
        };

        articles = [mainArticle, article1, article2];

        exportPreviewContent = {
          appId: app.id,
          articles: $q.when(articles),
          modalTitle: mainArticle.title,
          articleTitle: undefined,
          selectedLanguage: undefined
        };

        // when
        var ctrl = buildController(exportPreviewContent);
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.articlesLoadingBatchSize).toBe(wikiExportConfig.previewRenderingBatchSize);
        expect(ctrl.showLoadingBar).toBe(true);
        expect(ctrl.loading).toBe(true);
      });

      it('should load next batch when no pending requests', function () {
        // given
        var article1, article2;
        article1 = {
          id: 'ARTICLE-ID1',
          title: 'ARTICLE-TITLE1'
        };

        article2 = {
          id: 'ARTICLE-ID2',
          title: 'ARTICLE-TITLE2'
        };

        articles = [mainArticle, article1, article2];

        exportPreviewContent = {
          appId: app.id,
          articles: $q.when(articles),
          modalTitle: mainArticle.title,
          articleTitle: undefined,
          selectedLanguage: undefined
        };

        var ctrl = buildController(exportPreviewContent);
        ctrl.$onInit();
        $scope.$apply();

        // when
        for (var i = 0; i < ctrl.articlesLoadingBatchSize; i++) {
          ctrl.onImagesComplete();
        }
        $scope.$apply();

        // then
        expect(ctrl.articlesLoadingBatchSize).toBe(articles.length);
        expect(ctrl.showLoadingBar).toBe(false);
        expect(ctrl.loading).toBe(false);
      });

      it('should adapt scaling to element width', function () {
        // given
        var rendered = $q.defer();
        var closed = $q.defer();
        $uibModalInstance.rendered = rendered.promise;
        $uibModalInstance.closed = closed.promise;

        $document = angular.element(document);
        $document.find('body').append('<div id="wiki-article-export-modal"'
          + 'style="width: 430px; height: 500px;"></div>');

        var ctrl = buildController(exportPreviewContent);
        ctrl.$onInit();
        $scope.$apply();

        // when
        rendered.resolve();
        $scope.$apply();

        // then
        expect(ctrl.scale).toBe(0.5);
      });

      it('should print', function () {
        // given
        var ctrl = buildController(exportPreviewContent);
        ctrl.$onInit();
        $scope.$apply();

        // when
        ctrl.print();

        // then
        expect($window.print).toHaveBeenCalled();
      });

      it('should close', function () {
        // given
        var ctrl = buildController(exportPreviewContent);
        ctrl.$onInit();
        $scope.$apply();

        // when
        ctrl.close();

        // then
        expect($uibModalInstance.dismiss).toHaveBeenCalled();
      });

    });
  });

})();
