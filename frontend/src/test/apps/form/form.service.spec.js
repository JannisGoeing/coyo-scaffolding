(function () {
  'use strict';

  var moduleName = 'coyo.apps.form';

  describe('module: ' + moduleName, function () {

    // the service's name
    var formService,
        FormEntryModel,
        fields,
        fieldValues;

    beforeEach(function () {

      module(moduleName);

      inject(function (_formService_, _FormEntryModel_) {
        formService = _formService_;
        FormEntryModel = _FormEntryModel_;
      });

      fields = [{
        id: 'field-test-1'
      }, {
        id: 'field-test-2'
      }];

      fieldValues = [{
        fieldId: 'field-test-1',
        value: undefined
      }, {
        fieldId: 'field-test-2',
        value: undefined
      }];
    });

    describe('Service: formService', function () {

      it('should create new entry with empty values array', function () {
        // given
        var expectedEntry = new FormEntryModel({
          values: []
        });

        // when
        var entry = formService.createEntry();

        // then
        expect(entry).toEqual(expectedEntry);
      });

      it('should initialize entry with values', function () {
        // given
        var entry = new FormEntryModel({
          values: [fieldValues[0]]
        });
        expect(entry.values).toEqual([fieldValues[0]]);

        // when
        var result = formService.initEntry(entry, fields);

        // then
        expect(result.values).toEqual(fieldValues);
      });

      it('should initialize multiple entries with values', function () {
        // given
        var entries = [new FormEntryModel({values: []}), new FormEntryModel({values: []})];

        // when
        var result = formService.initEntries(entries, fields);

        // then
        expect(result[0].values).toEqual(fieldValues);
        expect(result[1].values).toEqual(fieldValues);
      });

      it('should return field value from form entry', function () {
        // given
        fieldValues[1].value = 'Test';
        var entry = new FormEntryModel({
          values: fieldValues
        });

        // when
        var fieldValue = formService.getFieldValue(entry, 'field-test-2');

        // then
        expect(fieldValue).toBe(fieldValue);
      });

    });

  });
})();
