(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';
  var directiveName = 'oyoc-app-chooser';

  describe('module: ' + moduleName, function () {

    var $scope, $compile, authService, template, AppConfigurationModel, appRegistry, AppModel;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {
      AppConfigurationModel = jasmine.createSpyObj('AppConfigurationModel', ['getActiveConfigsForSenderType']);
      appRegistry = jasmine.createSpyObj('appRegistry', ['getAll']);
      authService = jasmine.createSpyObj('authService',
          ['getUser', 'isAuthenticated', 'logout', 'getCurrentUserId', 'subscribeToUserUpdate']);
      AppModel = jasmine.createSpyObj('AppModel', ['fromConfig']);

      beforeEach(function () {
        module(function ($provide) {

          var app1 = {key: 'test-app-one', moderatorsOnly: false};
          var app2 = {key: 'test-app-two', moderatorsOnly: false};
          var app3 = {key: 'test-app-three', moderatorsOnly: false};
          var app4 = {key: 'test-app-four', moderatorsOnly: true};

          /* returns all keys that are enabled for a specific sender */
          AppConfigurationModel.getActiveConfigsForSenderType.and.returnValue({
            then: function (callback) {
              callback([
                app1,
                app2,
                app4
              ]);
              return {
                finally: function (callback) {
                  return callback();
                }
              };
            }
          });

          var user = {
            id: 'userId',
            hasGlobalPermissions: function () { return false; },
            moderatorMode: false
          };
          authService.isAuthenticated.and.returnValue(true);
          authService.getCurrentUserId.and.returnValue(user.id);
          authService.getUser.and.returnValue({
            then: function (callback) {
              callback(user);
            }
          });

          /* all registered apps */
          appRegistry.getAll.and.returnValue([app1, app2, app3, app4]);

          $provide.value('AppModel', AppModel);
          $provide.value('AppConfigurationModel', AppConfigurationModel);
          $provide.value('authService', authService);
          $provide.value('appRegistry', appRegistry);
        });
      });

      beforeEach(inject(function ($rootScope, _$compile_, $templateCache) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $templateCache.put('app/apps/api/components/app-chooser/app-chooser.html', '<div></div>');
        template = '<oyoc-app-chooser sender-type="testSender" ng-model="app"></oyoc-app-chooser>';
      }));

      it('should fetch all enabled unrestricted apps for a given sender on init', function () {
        // given

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();

        // then
        expect(element.isolateScope().loading).toBe(false);
        var configs = element.isolateScope().appConfigs;
        expect(configs).toBeArrayOfSize(2);
        expect(configs[0].key).toBe('test-app-one');
        expect(configs[1].key).toBe('test-app-two');
      });

      it('should select an app and set the model', function () {
        // given
        var config = {
          key: 'test-app'
        };
        $scope.app = {};
        AppModel.fromConfig.and.returnValue(config);

        // when
        var element = $compile(angular.element(template))($scope);
        $scope.$digest();
        element.isolateScope().selectConfig(config);

        // then
        expect($scope.app).toEqual(config);
      });
    });
  });

})();
