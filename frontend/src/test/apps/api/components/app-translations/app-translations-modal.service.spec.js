(function () {
  'use strict';

  var moduleName = 'coyo.apps.api';

  describe('module: ' + moduleName, function () {

    var $scope, appTranslationsModalService, modalService, sender;

    beforeEach(function () {

      sender = {
        id: 'TEST-ID'
      };

      modalService = jasmine.createSpyObj('modalService', ['open']);
      modalService.open.and.returnValue({
        result: {
          then: function (callback) {
            return callback(sender);
          }
        }
      });

      module(moduleName, function ($provide) {
        $provide.value('modalService', modalService);
      });

      inject(function (_appTranslationsModalService_, $rootScope) {
        $scope = $rootScope.$new();
        appTranslationsModalService = _appTranslationsModalService_;
      });

    });

    describe('service: appTranslationsModalService', function () {

      it('should open app navigation edit/translation modal and retrieve updated navigation as result', function () {
        // given - sender

        // when
        appTranslationsModalService.open(sender);
        $scope.$apply();

        // then
        expect(modalService.open).toHaveBeenCalled();

        var args = modalService.open.calls.mostRecent().args;
        // check that a copy is created but not the actual object is returned
        expect(args[0].resolve.sender()).toEqual(sender);
        expect(args[0].resolve.sender()).not.toBe(sender);
      });

    });
  });

})();
