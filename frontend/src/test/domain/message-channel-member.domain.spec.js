(function () {
  'use strict';

  describe('domain: MessageChannelMemberModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, MessageChannelMemberModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _MessageChannelMemberModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      MessageChannelMemberModel = _MessageChannelMemberModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('should mute', function () {
        // given
        $httpBackend.expectPUT(backendUrlService.getUrl() + '/web/message-channels/test/members/123/mute')
            .respond(200, {});

        // when
        var model = new MessageChannelMemberModel({id: 123, channelId: 'test'});
        model.mute();
        $httpBackend.flush();
      });
    });
  });
}());
