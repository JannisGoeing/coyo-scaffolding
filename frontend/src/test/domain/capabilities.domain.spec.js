(function () {
  'use strict';

  describe('domain: CapabilitiesModel', function () {
    beforeEach(module('coyo.domain'));

    var $httpBackend;
    var CapabilitiesModel, backendUrlService;

    var capabilities = {
      'preview': [
        {'image/jpeg': [
          {'details': 'image/jpeg'}
        ]},
        {'image/gif': [
          {'details': 'image/png'}
        ]},
        {'application/vnd.oasis.opendocument.text': [
          {'details': 'application/pdf'},
          {'details': 'image/jpeg'}
        ]}, {'application/pdf': [
          {'details': 'application/pdf'}
        ]}
      ],
      'authProvider': [
        {'name': 'coyo'},
        {'name': 'saml'}
      ],
      'featureToggles': {
        'x-ray-vision': true
      }
    };

    beforeEach(inject(function (_$httpBackend_, _CapabilitiesModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      CapabilitiesModel = _CapabilitiesModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('class members', function () {
      it('should get all capabilities', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/capabilities')
            .respond(200, {
              content: capabilities
            });

        // when
        var result = null;
        CapabilitiesModel.getAll().then(function (data) {
          result = data.content;
        });

        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result.preview[0]).toEqual(capabilities.preview[0]);
        expect(result.preview[1]).toEqual(capabilities.preview[1]);
        expect(result.preview[2]).toEqual(capabilities.preview[2]);
        expect(result.authProvider[0]).toEqual(capabilities.authProvider[0]);
        expect(result.authProvider[1]).toEqual(capabilities.authProvider[1]);
      });

      it('should get a specific capability', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/capabilities')
            .respond(200, capabilities);

        // when
        var result = null;
        CapabilitiesModel.get('preview').then(function (data) {
          result = data;
        });

        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result[0]).toEqual(capabilities.preview[0]);
        expect(result[1]).toEqual(capabilities.preview[1]);
      });

      it('should get mime types of available preview images', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/capabilities')
            .respond(200, capabilities);

        // when
        var result = null;
        CapabilitiesModel.previewImageFormat('image/jpeg').then(function (data) {
          result = data;
        });

        $httpBackend.flush();

        // then
        expect(result).toBeDefined();
        expect(result).toEqual(capabilities.preview[0]['image/jpeg'][0].details);
      });

      it('should check if pdf preview is available', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/capabilities')
            .respond(200, capabilities);

        // when
        var result1 = null;
        CapabilitiesModel.pdfAvailable('image/jpeg').then(function (data) {
          result1 = data;
        });

        var result2 = null;
        CapabilitiesModel.pdfAvailable('application/vnd.oasis.opendocument.text').then(function (data) {
          result2 = data;
        });

        $httpBackend.flush();

        // then
        expect(result1).toBeDefined();
        expect(result2).toBeDefined();
        expect(result1).toBeFalsy();
        expect(result2).toBeTruthy();
      });

      it('should check if image preview is available', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/capabilities')
            .respond(200, capabilities);

        // when
        var result1 = null;
        CapabilitiesModel.imgAvailable('image/jpeg').then(function (data) {
          result1 = data;
        });

        var result2 = null;
        CapabilitiesModel.imgAvailable('application/pdf').then(function (data) {
          result2 = data;
        });

        $httpBackend.flush();

        // then
        expect(result1).toBeDefined();
        expect(result2).toBeDefined();
        expect(result1).toBeTruthy();
        expect(result2).toBeFalsy();
      });

      it('should check if feature toggle is enabled', function () {
        // given
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/capabilities')
            .respond(200, capabilities);

        // when
        var isXRayVisionEnabled = null;
        CapabilitiesModel.isFeatureToggleEnabled('x-ray-vision').then(function (isEnabled) {
          isXRayVisionEnabled = isEnabled;
        });

        var isMindReadingEnabled = null;
        CapabilitiesModel.isFeatureToggleEnabled('mind-reading').then(function (isEnabled) {
          isMindReadingEnabled = isEnabled;
        });

        $httpBackend.flush();

        // then
        expect(isXRayVisionEnabled).toBeDefined();
        expect(isMindReadingEnabled).toBeDefined();
        expect(isXRayVisionEnabled).toBeTruthy();
        expect(isMindReadingEnabled).toBeFalsy();
      });
    });
  });
}());
