(function () {
  'use strict';

  describe('domain: FileAuthorModel', function () {
    beforeEach(module('coyo.domain'));

    var $httpBackend, FileAuthorModel, backendUrlService, fileAuthors, $rootScope;

    beforeEach(inject(function (_$httpBackend_, _FileAuthorModel_, _backendUrlService_, _$rootScope_) {
      $httpBackend = _$httpBackend_;
      FileAuthorModel = _FileAuthorModel_;
      backendUrlService = _backendUrlService_;
      $rootScope = _$rootScope_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});

      fileAuthors = {
        'any': {anyKey: 'anyContent'}, 'anotherId': {anotherKey: 'anotherContent'}
      };
    }));

    afterEach(function () {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should retrieve file authors', function () {
      //given
      var result = null;
      $httpBackend
          .expectGET(backendUrlService.getUrl() + '/web/senders/anySenderId/file/authors/anyAppId?ids=anyId&ids=anotherId')
          .respond(200, {content: fileAuthors});

      // when
      FileAuthorModel.getFileAuthors('anySenderId', 'anyAppId', ['anyId', 'anotherId'])
          .then(function (response) {
            result = response;
          });
      $rootScope.$apply();
      $httpBackend.flush();

      // then
      expect(result.content).toEqual(fileAuthors);
    });

    it('should retrieve version authors', function () {
      //given
      var result = null;
      $httpBackend
          .expectGET(backendUrlService.getUrl()
              + '/web/senders/anySenderId/documents/anyFileId/version/authors/anyAppIdOrSlug')
          .respond(200, {
            content: fileAuthors
          });

      // when
      FileAuthorModel.getVersionAuthors('anySenderId', 'anyAppIdOrSlug', 'anyFileId')
          .then(function (response) {
            result = response;
          });

      $rootScope.$apply();
      $httpBackend.flush();

      // then
      expect(result.content).toEqual(fileAuthors);
    });
  });
}());
