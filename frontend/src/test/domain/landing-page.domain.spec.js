(function () {
  'use strict';

  describe('domain: LandingPageModel', function () {

    beforeEach(module('coyo.domain'));

    var $httpBackend, LandingPageModel, backendUrlService;

    beforeEach(inject(function (_$httpBackend_, _LandingPageModel_, _backendUrlService_) {
      $httpBackend = _$httpBackend_;
      LandingPageModel = _LandingPageModel_;
      backendUrlService = _backendUrlService_;

      $httpBackend.whenGET(backendUrlService.getUrl() + '/web/csrf').respond({token: 'csrfToken'});
    }));

    describe('instance members', function () {

      it('queryWithPermissions finds all landing pages', function () {
        // given
        var pages = [{id: 'PAGE-1'}, {id: 'PAGE-1'}];
        $httpBackend.expectGET(backendUrlService.getUrl() + '/web/landing-pages?_permissions=manage')
            .respond(200, {content: pages});

        // when
        var result = null;
        LandingPageModel.queryWithPermissions({}, {}, ['manage'])
            .then(function (response) {
              result = response;
            });
        $httpBackend.flush();

        // then
        expect(result.content).toEqual(pages);
      });
    });
  });
}());
