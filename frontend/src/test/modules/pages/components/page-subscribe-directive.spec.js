(function () {
  'use strict';

  var moduleName = 'coyo.pages';
  var directiveName = 'coyoPageSubscribe';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, $rootScope, authService, subscriptionsService, user, page;

    beforeEach(module(moduleName));

    describe('directive: ' + directiveName, function () {

      beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $controller = _$controller_;
        $q = _$q_;

        user = {id: 'user-1'};
        page = {id: 'page-id'};
        authService = jasmine.createSpyObj('authService', ['getUser']);
        authService.getUser.and.returnValue($q.resolve(user));
        subscriptionsService = jasmine.createSpyObj('subscriptionsService', ['subscribe', 'unsubscribe', 'onSubscriptionChange']);
      }));

      var controllerName = 'PageSubscribeController';

      describe('controller: ' + controllerName, function () {

        function buildController(subscription) {
          subscriptionsService.onSubscriptionChange.and.callFake(function (userId, targetId, callback) {
            callback(subscription);
            $rootScope.$on('currentUser:updated', function (event, payload) {
              callback(payload);
            });
          });

          return $controller(controllerName, {
            $rootScope: $rootScope,
            $scope: $scope,
            authService: authService,
            subscriptionsService: subscriptionsService
          }, {
            page: page
          });
        }

        it('should init current user and show', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.currentUser).toBe(user);
          expect(ctrl.show).toBe(true);
          expect(ctrl.loading).toBe(false);
          expect(subscriptionsService.onSubscriptionChange).toHaveBeenCalled();
        });

        it('should isSubscribed return true if user has subscription', function () {
          // given
          var subscription = {userId: user.id, targetId: page.id};
          var ctrl = buildController(subscription);

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.subscribed).toBe(true);
          expect(subscriptionsService.onSubscriptionChange).toHaveBeenCalled();
        });

        it('should isSubscribed return false if user has no subscription', function () {
          // given
          subscriptionsService.onSubscriptionChange.and.callFake(function (userId, targetId, callback) {
            callback();
          });
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $rootScope.$apply();

          // then
          expect(ctrl.subscribed).toBe(false);
          expect(subscriptionsService.onSubscriptionChange).toHaveBeenCalled();
        });

        it('should toggle do nothing while loading', function () {
          // given
          var ctrl = buildController();
          ctrl.loading = true;

          // when
          ctrl.toggle();

          // then
          expect(subscriptionsService.subscribe).not.toHaveBeenCalled();
          expect(subscriptionsService.unsubscribe).not.toHaveBeenCalled();
        });

        it('should toggle subscribe if unsubscribed', function () {
          // given
          var ctrl = buildController();
          var event = jasmine.createSpyObj('event', ['stopPropagation']);
          page.userSubscriptionsCount = 1;
          subscriptionsService.subscribe.and.returnValue($q.resolve());
          spyOn($rootScope, '$emit').and.callThrough();

          // when
          ctrl.$onInit();
          $rootScope.$apply();
          ctrl.toggle(event);
          $rootScope.$emit('currentUser:updated', {});
          $rootScope.$apply();

          // then
          expect(subscriptionsService.subscribe).toHaveBeenCalledWith(user.id, page.id, 'page', page.id);
          expect(subscriptionsService.unsubscribe).not.toHaveBeenCalled();
          expect(page.userSubscriptionsCount).toBe(2);
          expect(ctrl.loading).toBe(false);
          expect(event.stopPropagation).toHaveBeenCalled();
          expect($rootScope.$emit).toHaveBeenCalledWith('page:subscribed', page);
        });

        it('should toggle unsubscribe if subscribed', function () {
          // given
          var subscription = {userId: user.id, targetId: page.id};
          var ctrl = buildController(subscription);
          var event = jasmine.createSpyObj('event', ['stopPropagation']);
          page.userSubscriptionsCount = 2;
          subscriptionsService.unsubscribe.and.returnValue($q.resolve());
          spyOn($rootScope, '$emit').and.callThrough();

          // when
          ctrl.$onInit();
          $rootScope.$apply();
          ctrl.toggle(event);
          $rootScope.$emit('currentUser:updated');
          $rootScope.$apply();

          // then
          expect(subscriptionsService.unsubscribe).toHaveBeenCalledWith(user.id, page.id);
          expect(subscriptionsService.subscribe).not.toHaveBeenCalled();
          expect(page.userSubscriptionsCount).toBe(1);
          expect(ctrl.loading).toBe(false);
          expect(event.stopPropagation).toHaveBeenCalled();
          expect($rootScope.$emit).toHaveBeenCalledWith('page:unSubscribed', page.id);
        });
      });
    });
  });
})();
