(function () {
  'use strict';

  var moduleName = 'coyo.colleagues';
  var targetName = 'ColleaguesMainController';

  describe('module: ' + moduleName, function () {
    var $controller, $scope, $q;
    var page;
    var UserModel;
    var currentUser = {};
    var $sessionStorage = {};
    var $translate;

    var profileGroups = [
      {
        name: 'contact',
        fields: [
          {name: 'phone', type: 'PHONE', order: 0, overview: true},
          {name: 'mobile', type: 'PHONE', order: 1},
          {
            name: 'twitter', type: 'LINK', order: 2, searchAggregation: true,
            searchAggregationOrder: 2
          }]
      }, {
        name: 'work',
        fields: [
          {name: 'jobTitle', type: 'TEXT', order: 0, searchAggregation: true},
          {name: 'company', type: 'TEXT', order: 1, searchAggregation: true, searchAggregationOrder: 1},
          {
            name: 'department', type: 'TEXT', order: 2, overview: true, searchAggregation: true,
            searchAggregationOrder: 2
          }]
      }
    ];

    function buildController() {
      return $controller(targetName, {
        $scope: $scope,
        $sessionStorage: $sessionStorage,
        UserModel: UserModel,
        currentUser: currentUser,
        profileFieldGroups: profileGroups
      });
    }

    beforeEach(function () {
      module('commons.ui');

      module(moduleName, function ($provide) {
        $provide.value('socketService', jasmine.createSpyObj('socketService', ['subscribe']));
      });

      inject(function (_$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $q = _$q_;

        page = {
          number: 0,
          last: false,
          content: [{id: 1, properties: {}}, {id: 2, properties: {}}, {id: 3, properties: {}}],
          aggregations: {
            department: [],
            subscribedTo: [{active: false, count: 2}],
            subscribedBy: [{active: false, count: 3}],
            subscribedNot: [{active: false, count: 3}]
          }
        };

        currentUser.properties = {};
        UserModel = jasmine.createSpyObj('UserModel', ['searchWithFilter']);
        UserModel.searchWithFilter.and.returnValue($q.resolve(page));

        $translate = jasmine.createSpyObj('$translate', ['instant']);
        $translate.instant.and.callFake(function (argument) {
          return argument;
        });

        $sessionStorage = {};
      });
    });

    describe('controller: ' + targetName, function () {

      it('should init', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        expect(ctrl.currentPage).toEqual(page);
        expect(_.keys(ctrl.aggregations)).toEqual(['company', 'department', 'twitter', 'jobTitle']);
        expect(ctrl.aggregations.company.filterAllActive).toBeTruthy();
        expect(ctrl.aggregations.company.translation).toBe('USER.COMPANY');
        expect(ctrl.subscriptionFilterModel.getItem('subscribedBy'))
            .toEqual(jasmine.objectContaining({active: false, count: 3}));
        expect(ctrl.subscriptionFilterModel.getItem('subscribedTo'))
            .toEqual(jasmine.objectContaining({active: false, count: 2}));
        expect(ctrl.profileGroups).toBe(profileGroups);
      });

      it('should search with defaults', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.loading = false;
        UserModel.searchWithFilter.calls.reset();

        // when
        ctrl.search();

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        var args = UserModel.searchWithFilter.calls.mostRecent().args;
        expect(args[0]).toBeUndefined();
        expect(args[1].getSort()).toBe('lastname.sort,firstname.sort');
        expect(args[3]).toEqual(['displayName', 'email', 'properties.department', 'properties.jobTitle']);
      });

      it('should search with term', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.loading = false;
        UserModel.searchWithFilter.calls.reset();

        // when
        ctrl.search('searchTerm');

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        var args = UserModel.searchWithFilter.calls.mostRecent().args;
        expect(args[0]).toBe('searchTerm');
        expect(args[1].getSort()).toEqual(['_score,DESC', 'lastname.sort,firstname.sort']);
        expect(args[3]).toEqual(['displayName', 'email', 'properties.department', 'properties.jobTitle']);
      });

      it('should reset filters on search', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.loading = false;

        ctrl.params.department = ['Dep1', 'Dep2'];
        ctrl.params.subscribedTo = [1, 2, 3];
        ctrl.params.subscribedBy = [1, 2, 3];
        ctrl.params.availability = ['ONLINE'];

        // when
        ctrl.search('searchTerm').then(function () {
          // then
          expect(ctrl.params.searchTerm).toEqual('searchTerm');
          expect(ctrl.params.department).toEqual([]);
          expect(ctrl.params.jobTitle).toEqual([]);
          expect(ctrl.params.twitter).toEqual([]);
          expect(ctrl.params.company).toEqual([]);
          expect(ctrl.params.subscribedTo).toEqual([]);
          expect(ctrl.params.subscribedBy).toEqual([]);
          expect(ctrl.params.availability).toEqual([]);
        });
        $scope.$apply();
      });

      it('should set aggregations after search', function () {
        // given
        currentUser.properties.department = 'User department';
        var page = {
          number: 0,
          last: true,
          content: [],
          aggregations: {
            department: [
              {key: 'Dep B', count: 3},
              {key: 'Dep C', count: 2},
              {key: 'User department', count: 2},
              {key: 'Dep A', count: 1}
            ],
            subscribedBy: [{key: 'subscribedBy', count: 1}],
            subscribedNot: [{key: 'subscribedNot', count: 1}],
            subscribedTo: [{key: 'subscribedTo', count: 1}],
            availability: [{key: 'availability', count: 0}]
          }
        };
        UserModel.searchWithFilter.and.returnValue($q.resolve(page));
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        $scope.$apply();

        // then
        expect(ctrl.aggregations.department.values).toEqual([
          {key: 'User department', count: 2},
          {key: 'Dep B', count: 3},
          {key: 'Dep C', count: 2},
          {key: 'Dep A', count: 1}
        ]);
      });

      it('should not change department aggregation order when no user department set', function () {
        // given
        currentUser.properties.department = undefined;
        var page = {
          number: 0,
          last: true,
          content: [],
          aggregations: {
            department: [
              {key: 'Dep B', count: 3},
              {key: 'Dep C', count: 2},
              {key: 'User department', count: 2},
              {key: 'Dep A', count: 1}
            ],
            subscribedBy: [{key: 'subscribedBy', count: 1}],
            subscribedNot: [{key: 'subscribedNot', count: 1}],
            subscribedTo: [{key: 'subscribedTo', count: 1}],
            availability: [{key: 'availability', count: 0}]
          }
        };
        UserModel.searchWithFilter.and.returnValue($q.resolve(page));
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        $scope.$apply();

        // then
        expect(ctrl.aggregations.department.values).toEqual([
          {key: 'Dep B', count: 3},
          {key: 'Dep C', count: 2},
          {key: 'User department', count: 2},
          {key: 'Dep A', count: 1}
        ]);
      });

      it('should not change department aggregation order when user department is not in aggregation list', function () {
        // given
        currentUser.properties.department = 'User department';
        var page = {
          number: 0,
          last: true,
          content: [],
          aggregations: {
            department: [
              {key: 'Dep B', count: 3},
              {key: 'Dep C', count: 2},
              {key: 'Dep A', count: 1}
            ],
            subscribedBy: [{key: 'subscribedBy', count: 1}],
            subscribedNot: [{key: 'subscribedNot', count: 1}],
            subscribedTo: [{key: 'subscribedTo', count: 1}],
            availability: [{key: 'availability', count: 0}]
          }
        };
        UserModel.searchWithFilter.and.returnValue($q.resolve(page));
        var ctrl = buildController();
        ctrl.$onInit();

        // when
        $scope.$apply();

        // then
        expect(ctrl.aggregations.department.values).toEqual([
          {key: 'Dep B', count: 3},
          {key: 'Dep C', count: 2},
          {key: 'Dep A', count: 1}
        ]);
      });

      it('should expand aggregation list', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        expect(ctrl.aggregations.department.max).toBe(5);

        // when
        ctrl.moreAggregations(ctrl.aggregations.department);

        // then
        expect(ctrl.aggregations.department.max).toBe(10);
      });

      it('should set aggregation filter', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // when
        ctrl.setAggregationFilter(ctrl.aggregations.department.name, ['dep1']);

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        expect(UserModel.searchWithFilter.calls.mostRecent().args[2].department).toEqual(['dep1']);

        // given
        $scope.$apply();

        // when
        ctrl.setAggregationFilter(ctrl.aggregations.department.name, []);

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        expect(UserModel.searchWithFilter.calls.mostRecent().args[2].department).toEqual([]);
      });

      it('should set subscription filter', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.subscriptionFilterModel.selectItem('subscribedTo');
        $scope.$apply();

        // when
        ctrl.setSubscriptionFilter();

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        var filterArg = UserModel.searchWithFilter.calls.mostRecent().args[2];
        expect(filterArg.subscribedTo).toEqual([true]);
        expect(filterArg.subscribedNot).toEqual([]);
        expect(filterArg.subscribedBy).toEqual([]);
      });

      it('should override default params with session storage', function () {
        // given
        $sessionStorage.colleagueList = {
          department: ['test'],
          subscribedTo: [true]
        };
        var ctrl = buildController();

        // when
        ctrl.$onInit();

        // then
        expect(ctrl.params.subscribedTo).toEqual([true]);
        expect(ctrl.params.department).toEqual(['test']);
      });

      it('should set availability filter', function () {
        // given
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // when
        ctrl.setAvailabilityFilter(['ONLINE']);

        // then
        expect(UserModel.searchWithFilter).toHaveBeenCalled();
        expect(UserModel.searchWithFilter.calls.mostRecent().args[2].availability).toEqual(['ONLINE']);
      });
    });
  });

})();
