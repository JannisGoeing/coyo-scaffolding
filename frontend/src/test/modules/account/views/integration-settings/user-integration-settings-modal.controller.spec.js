(function () {
  'use strict';

  var moduleName = 'coyo.account';
  var controllerName = 'UserIntegrationSettingsModalController';

  describe('module: ' + moduleName, function () {

    describe('controller: ' + controllerName, function () {

      var $controller, $rootScope, $scope, $q;
      var $injector, $uibModalInstance, ngxUserIntegrationSettingsService;

      beforeEach(function () {
        module(moduleName);

        inject(function (_$controller_, _$rootScope_, _$q_) {
          $controller = _$controller_;
          $rootScope = _$rootScope_;
          $scope = $rootScope.$new();
          $q = _$q_;
        });

        ngxUserIntegrationSettingsService = jasmine.createSpyObj('ngxUserIntegrationSettingsService',
            ['isCalendarSyncEnabled', 'disableCalendarSync', 'enableCalendarSync']);

        $uibModalInstance = jasmine.createSpyObj('$uibModalInstance', ['close']);
        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(ngxUserIntegrationSettingsService);

        var isCalendarSyncEnabledResult = jasmine.createSpyObj('result', ['toPromise']);
        isCalendarSyncEnabledResult.toPromise.and.returnValue($q.resolve(true));
        ngxUserIntegrationSettingsService.isCalendarSyncEnabled.and.returnValue(isCalendarSyncEnabledResult);
      });

      function buildController() {
        return $controller(controllerName, {
          $rootScope: $rootScope,
          $scope: $scope,
          $injector: $injector,
          $uibModalInstance: $uibModalInstance
        });
      }

      it('should init controller', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ngxUserIntegrationSettingsService.isCalendarSyncEnabled).toHaveBeenCalled();
        expect(ctrl.isCalendarSyncEnabled).toBe(true);
      });

      it('should enable calendar sync and close modal', function () {
        // given
        var enableResult = jasmine.createSpyObj('result', ['toPromise']);
        enableResult.toPromise.and.returnValue($q.resolve());
        ngxUserIntegrationSettingsService.enableCalendarSync.and.returnValue(enableResult);

        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.isCalendarSyncEnabled = true;

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(ngxUserIntegrationSettingsService.disableCalendarSync).not.toHaveBeenCalled();
        expect(ngxUserIntegrationSettingsService.enableCalendarSync).toHaveBeenCalled();
        expect($uibModalInstance.close).toHaveBeenCalled();
      });

      it('should disable calendar sync and close modal', function () {
        // given
        var disableResult = jasmine.createSpyObj('result', ['toPromise']);
        disableResult.toPromise.and.returnValue($q.resolve());
        ngxUserIntegrationSettingsService.disableCalendarSync.and.returnValue(disableResult);

        var ctrl = buildController();
        ctrl.$onInit();
        ctrl.isCalendarSyncEnabled = false;

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(ngxUserIntegrationSettingsService.enableCalendarSync).not.toHaveBeenCalled();
        expect(ngxUserIntegrationSettingsService.disableCalendarSync).toHaveBeenCalled();
        expect($uibModalInstance.close).toHaveBeenCalled();
      });

    });
  });
})();
