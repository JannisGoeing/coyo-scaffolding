(function () {
  'use strict';

  var moduleName = 'coyo.messaging';
  var targetName = 'MessagingPresenceStatusController';

  describe('module: ' + moduleName, function () {
    var $controller;
    var $scope;

    var userMock = {
      id: 123,
      updatePresenceStatus: function () {
        return {
          'finally': function (callback) {
            callback();
          }
        };
      }
    };

    var presenceStatusMock = {
      state: 'ONLINE',
      label: 'I am here'
    };

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
      });
    });

    describe('controller: ' + targetName, function () {

      function buildCtrl() {
        return $controller(targetName, {
          $scope: $scope
        }, {
          presenceStatus: presenceStatusMock,
          currentUser: userMock
        });
      }

      it('should init controller', function () {
        // when
        var ctrl = buildCtrl();
        ctrl.$onInit();

        // then
        expect(ctrl.presenceStatus).toBe(presenceStatusMock);
        expect(ctrl.currentUser).toBe(userMock);
        expect(ctrl.availableStates).toEqual(['ONLINE', 'AWAY', 'BUSY', 'GONE', 'OFFLINE']);
      });

      it('should update status when state changes', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        spyOn(userMock, 'updatePresenceStatus').and.callThrough();

        // when
        ctrl.updateState('AWAY');

        // then
        expect(userMock.updatePresenceStatus).toHaveBeenCalledWith({
          state: 'AWAY',
          label: presenceStatusMock.label
        });
      });

      it('should update status when label changes', function () {
        // given
        var ctrl = buildCtrl();
        ctrl.$onInit();
        spyOn(userMock, 'updatePresenceStatus').and.callThrough();

        // when
        ctrl.updateLabel('New label');

        // then
        expect(userMock.updatePresenceStatus).toHaveBeenCalledWith({
          state: presenceStatusMock.state,
          label: 'New label'
        });
      });
    });
  });
})();
