(function () {
  'use strict';

  var moduleName = 'coyo.admin.terms';
  var controllerName = 'AdminTermsGeneralController';

  describe('module: ' + moduleName, function () {

    var $controller, $scope, $q, TermsModel, modalService, languages, SettingsModel;

    beforeEach(function () {

      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _$q_) {
        $controller = _$controller_;
        $scope = _$rootScope_.$new();
        $q = _$q_;
      });

      modalService = jasmine.createSpyObj('modalService', ['confirm']);
      SettingsModel = jasmine.createSpyObj('SettingsModel', ['retrieve']);

      TermsModel = function TermsModel() {
      };
      TermsModel.prototype.save = function () {
      };
      TermsModel.reset = function () {
      };
      TermsModel.activate = function () {
      };
      TermsModel.deactivate = function () {
      };
      spyOn(TermsModel.prototype, 'save');
      spyOn(TermsModel, 'reset');
      spyOn(TermsModel, 'activate');
      spyOn(TermsModel, 'deactivate');

      languages = [
        {language: 'DE', active: true, defaultLanguage: true},
        {language: 'EN', active: true, defaultLanguage: false}
      ];

    });

    describe('controller: ' + controllerName, function () {

      function buildController(settings, translations) {
        return $controller(controllerName, {
          TermsModel: TermsModel,
          modalService: modalService,
          settings: settings,
          translations: translations,
          languages: languages,
          SettingsModel: SettingsModel
        });
      }

      it('should initialize controller', function () {
        // given
        var german = {'language': 'DE', 'title': 'test', 'text': 'de-content', url: 'url'};
        var english = {'language': 'EN', 'title': 'test2', 'text': 'en-content'};
        var translations = [english, german];

        // when
        var ctrl = buildController({defaultLanguage: 'DE', multiLanguageActive: true}, translations);
        ctrl.$onInit();

        // then
        expect(ctrl.defaultLanguage).toEqual('DE');
        expect(ctrl.isMultiLanguageActive).toBeTrue();
        expect(ctrl.translations).toContain(german);
        expect(ctrl.translations).toContain(english);
        expect(ctrl.languages.DE.translations.title).toBe('test');
        expect(ctrl.languages.DE.translations.text).toBe('de-content');
        expect(ctrl.languages.DE.translations.url).toBe('url');
        expect(ctrl.languages.EN.translations.title).toBe('test2');
        expect(ctrl.languages.EN.translations.text).toBe('en-content');
      });

      it('should check active flag in settings', function () {
        // given
        var german = {'language': 'DE', 'title': 'test'};
        var english = {'language': 'EN', 'title': 'test2'};
        var ctrlActive = buildController({termsRequired: 'true'}, [english, german], ['DE', 'EN']);
        var ctrlInactive = buildController({termsRequired: 'false'}, [english, german], ['DE', 'EN']);

        // when
        var active = ctrlActive.isActive();
        var inactive = ctrlInactive.isActive();

        // then
        expect(active).toBeTrue();
        expect(inactive).toBeFalse();
      });

      it('should check if terms can be activated', function () {
        // given
        var germanValid = {'language': 'DE', 'title': 'test', 'text': 'test'};
        var germanInvalid = {'language': 'DE', 'title': 'test'};
        var english = {'language': 'EN', 'title': 'test2'};
        var ctrlActiveValid = buildController({defaultLanguage: 'DE', termsRequired: 'true'}, [english, germanValid],
            ['DE', 'EN']);
        var ctrlInactiveValid = buildController({defaultLanguage: 'DE', termsRequired: 'false'}, [english, germanValid],
            ['DE', 'EN']);
        var ctrlActiveInvalid = buildController({defaultLanguage: 'DE', termsRequired: 'true'},
            [english, germanInvalid], ['DE', 'EN']);
        var ctrlInactiveInvalid = buildController({defaultLanguage: 'DE', termsRequired: 'false'},
            [english, germanInvalid], ['DE', 'EN']);

        // when
        var activeValid = ctrlActiveValid.isActive();
        var inactiveValid = ctrlInactiveValid.isActive();
        var activeInvalid = ctrlActiveInvalid.isActive();
        var inactiveInvalid = ctrlInactiveInvalid.isActive();

        // then
        expect(activeValid).toBeTrue();
        expect(inactiveValid).toBeFalse();
        expect(activeInvalid).toBeTrue();
        expect(inactiveInvalid).toBeFalse();
      });

      it('should save terms with language removal, update and creation', function () {
        // given
        var german = {id: 1, 'language': 'DE', 'title': 'title-de', 'text': 'text-de'};
        var english = {id: 2, 'language': 'EN', 'title': 'title-en', 'text': 'text-en'};

        german.save = jasmine.createSpy('save').and
            .returnValue($q.resolve({id: 1, 'language': 'DE', 'title': 'title-de', 'text': 'text-de-modified'}));
        english.delete = jasmine.createSpy('delete').and.returnValue($q.resolve(english));

        TermsModel.prototype.save.and.returnValue($q.resolve({id: 3, 'title': 'title-fr', 'text': 'text-fr'}));

        var ctrl = buildController({defaultLanguage: 'DE'}, [english, german]);
        ctrl.$onInit();

        // when
        ctrl.languages.DE.translations.text = 'text-de-modified'; // update german
        ctrl.languages.EN.translations = {}; // remove english
        ctrl.languages.FR = {translations: {'title': 'title-fr', 'text': 'text-fr'}, active: true}; // create french
        ctrl.save();
        $scope.$apply();

        // then
        expect(german.save).toHaveBeenCalled();
        expect(english.delete).toHaveBeenCalled();
        expect(TermsModel.prototype.save).toHaveBeenCalled();

        expect(ctrl.translations).toBeArrayOfSize(2);
        expect(ctrl.translations).toContain({id: 1, language: 'DE', title: 'title-de', text: 'text-de-modified'});
        expect(ctrl.translations).toContain({id: 3, title: 'title-fr', text: 'text-fr'});
      });

      it('should save activation', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.active = true;
        ctrl.hiddenUsers = true;
        TermsModel.prototype.save.and.returnValue($q.resolve({}));
        TermsModel.activate.and.returnValue($q.resolve({}));

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(TermsModel.activate).toHaveBeenCalledWith(true);
      });

      it('should save deactivation', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE', hiddenUsers: false}, []);
        ctrl.$onInit();
        ctrl.hiddenUsers = true;
        TermsModel.prototype.save.and.returnValue($q.resolve({}));
        TermsModel.deactivate.and.returnValue($q.resolve({}));

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(TermsModel.deactivate).toHaveBeenCalled();
      });

      it('should retrieve and update settings after save', function () {
        // given
        var settings = {anySetting: 'foo', termsRequired: true, hiddenUsers: true};
        var ctrl = buildController({defaultLanguage: 'DE', hiddenUsers: false}, []);
        ctrl.$onInit();
        ctrl.hiddenUsers = true;
        TermsModel.prototype.save.and.returnValue($q.resolve({}));
        TermsModel.deactivate.and.returnValue($q.resolve({}));
        SettingsModel.retrieve.and.returnValue($q.resolve(settings));

        // when
        ctrl.save();
        $scope.$apply();

        // then
        expect(SettingsModel.retrieve).toHaveBeenCalled();
        expect(ctrl.settings).toBe(settings);
      });

      it('should reset', function () {
        // given
        var german = {'language': 'DE', 'title': 'test'};
        var english = {'language': 'EN', 'title': 'test2'};
        var ctrl = buildController({defaultLanguage: 'DE'}, [english, german]);
        modalService.confirm.and.returnValue({result: $q.resolve()});

        // when
        ctrl.reset();
        $scope.$apply();

        // then
        expect(TermsModel.reset).toHaveBeenCalled();
      });

      it('should resolve that translation is required when translations exist for given language', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'EN';

        // when
        var actualResponse = ctrl.isTranslationRequired('DE');

        // then
        expect(actualResponse).toBeTrue();
      });

      it('should resolve that translation is required when current language is equal to given one', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'EN';

        // when
        var actualResponse = ctrl.isTranslationRequired('EN');

        // then
        expect(actualResponse).toBeTrue();
      });

      it('should resolve that translation is not required when current language is not equal to given one',
          function () {
            // given
            var ctrl = buildController({defaultLanguage: 'DE'}, []);
            ctrl.$onInit();
            ctrl.currentLanguage = 'DE';

            // when
            var actualResponse = ctrl.isTranslationRequired('EN');

            // then
            expect(actualResponse).toBeFalse();
          });

      it('should update validity', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();

        // when
        ctrl.updateValidity('DE', true);

        // then
        expect(ctrl.languages.DE.valid).toBeTrue();
      });

      it('should set content validity to invalid for empty title', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'DE';

        // when
        ctrl.updateContentValidity('', 'some content');

        // then
        expect(ctrl.languages.DE.valid).toBeFalse();
      });

      it('should set content validity to invalid for empty content', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'DE';

        // when
        ctrl.updateContentValidity('some title', '');

        // then
        expect(ctrl.languages.DE.valid).toBeFalse();
      });

      it('should set content validity to invalid for empty <p> tags', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'DE';

        // when
        ctrl.updateContentValidity('a title', '<p></p>');

        // then
        expect(ctrl.languages.DE.valid).toBeFalse();
      });

      it('should set content validity to invalid for empty <p><br></p> tags', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'DE';

        // when
        ctrl.updateContentValidity('a title', '<p></p>');

        // then
        expect(ctrl.languages.DE.valid).toBeFalse();
      });

      it('should set content validity to valid for valid content', function () {
        // given
        var ctrl = buildController({defaultLanguage: 'DE'}, []);
        ctrl.$onInit();
        ctrl.currentLanguage = 'DE';

        // when
        ctrl.updateContentValidity('a title', '<p>any content</p>');

        // then
        expect(ctrl.languages.DE.valid).toBeTrue();
      });

      it('should remove url from translations when the translations were applied from the default language',
          function () {
            // given
            var ctrl = buildController({defaultLanguage: 'DE'}, []);
            ctrl.$onInit();
            ctrl.currentLanguage = 'DE';

            // when
            ctrl.onTranslationsAdded(true);

            // then
            expect(ctrl.languages.DE.translations.url).toBeUndefined();
          });

    });
  });

})();
