(function () {
  'use strict';

  var moduleName = 'coyo.admin.userManagement';

  describe('module: ' + moduleName, function () {

    var controllerName = 'DetectedExternalRoleController';
    var $scope, $controller;

    beforeEach(function () {
      module(moduleName);
    });

    beforeEach(inject(function (_$rootScope_, _$controller_) {
      $scope = _$rootScope_.$new();
      $controller = _$controller_;
    }));

    describe('service: DetectedExternalRoleController', function () {
      function buildController() {
        return $controller(controllerName, {
          $scope: $scope
        }, {
        });
      }

      it('should change the detection flag to false', function () {
        // when
        var ctrl = buildController();
        ctrl.roles = [{systemRoleName: 'admin'}, {systemRoleName: 'user'}];

        // given
        ctrl.isExternalWorkspaceMemberIncluded = false;
        ctrl.superadmin = false;
        ctrl.detectExternalRole();

        // then
        expect(ctrl.isExternalWorkspaceMemberIncluded).toBe(false);
        expect(ctrl.isExternalSuperadmin).toBe(false);
      });

      it('should change the detection flag to true', function () {
        // given
        var ctrl = buildController();
        ctrl.roles = [{systemRoleName: 'admin'}, {systemRoleName: 'user'}, {systemRoleName: 'EXTERNAL_WS'}];

        // when
        ctrl.isExternalWorkspaceMemberIncluded = false;
        ctrl.superadmin = false;
        ctrl.detectExternalRole();

        // then
        expect(ctrl.isExternalWorkspaceMemberIncluded).toBe(true);
        expect(ctrl.isExternalSuperadmin).toBe(false);
      });

      it('should change the superadmin detection flag to true', function () {
        // given
        var ctrl = buildController();
        ctrl.roles = [{systemRoleName: 'admin'}, {systemRoleName: 'user'}, {systemRoleName: 'EXTERNAL_WS'}];

        // when
        ctrl.isExternalWorkspaceMemberIncluded = false;
        ctrl.superadmin = true;
        ctrl.detectExternalRole();

        // then
        expect(ctrl.isExternalWorkspaceMemberIncluded).toBe(true);
        expect(ctrl.isExternalSuperadmin).toBe(true);
      });
    });
  });
})();
