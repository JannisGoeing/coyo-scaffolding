(function () {
  'use strict';

  var moduleName = 'coyo.admin.settings';
  var controllerName = 'AdminMaintenanceSettingsController';

  describe('module: ' + moduleName, function () {

    var $scope, $q, $controller;
    var MaintenanceModel, maintenanceModel, tenantMaintenance, updatedTenantMaintenance, $injector;

    beforeEach(function () {
      module(moduleName);

      tenantMaintenance = {
        active: true,
        id: 'MAINTENANCE-ID',
        headline: 'MAINTENANCE-HEADLINE',
        message: 'MAINTENANCE-MESSAGE',
        type: 'TENANT_MAINTENANCE'
      };

      updatedTenantMaintenance = angular.merge({}, tenantMaintenance, {
        headline: 'UPDATED-MAINTENANCE-HEADLINE',
        message: 'UPDATED-MAINTENANCE-MESSAGE'
      });

      inject(function ($rootScope, _$q_, _$controller_) {
        $scope = $rootScope.$new();
        $q = _$q_;
        $controller = _$controller_;

        MaintenanceModel = function (args) {
          maintenanceModel = jasmine.createSpyObj('MaintenanceModel', ['save', '$url']);
          maintenanceModel.save.and.returnValue($q.resolve(updatedTenantMaintenance));
          maintenanceModel.$url.and.returnValue('SOME-URL');
          return angular.merge(maintenanceModel, args);
        };
        angular.extend(MaintenanceModel, jasmine.createSpyObj('MaintenanceModel', ['getLatest']));
        MaintenanceModel.getLatest.and.returnValue($q.resolve(tenantMaintenance));

        $injector = jasmine.createSpyObj('$injector', ['get']);
        $injector.get.and.returnValue(jasmine.createSpyObj('ngxNotificationService', ['success']));
      });
    });

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $injector: $injector,
          MaintenanceModel: MaintenanceModel
        });
      }

      describe('controller', function () {
        it('should init', function () {
          // given
          var ctrl = buildController();

          // when
          ctrl.$onInit();
          $scope.$apply();

          // then
          expect(MaintenanceModel.getLatest).toHaveBeenCalled();
          expect(ctrl.maintenance).toBeDefined();
        });

        it('should save settings', function () {
          // given
          var ctrl = buildController();
          ctrl.$onInit();
          $scope.$apply();

          // when
          ctrl.submit();
          $scope.$apply();

          // then
          expect(maintenanceModel.save).toHaveBeenCalled();
          expect(ctrl.maintenance.id).toBe(tenantMaintenance.id);
          expect(ctrl.maintenance.headline).toBe(updatedTenantMaintenance.headline);
          expect(ctrl.maintenance.message).toBe(updatedTenantMaintenance.message);
        });
      });
    });
  });
})();
