(function () {
  'use strict';

  var moduleName = 'coyo.admin.userDirectories.ldap';
  var controllerName = 'CsvSettingsController';

  describe('module: ' + moduleName, function () {
    var $controller, $stateParams, $state;
    var $rootScope, $scope, $q, modalService, UserDirectoryModel;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_, _modalService_, _$state_, _$stateParams_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $controller = _$controller_;
      $state = _$state_;
      $stateParams = _$stateParams_;
      modalService = _modalService_;

      $state = jasmine.createSpyObj('$state', ['go']);
      UserDirectoryModel = jasmine.createSpyObj('UserDirectoryModel', ['getOrphanPolicies']);
      UserDirectoryModel.getOrphanPolicies.and.returnValue($q.resolve(['POLICY1', 'POLICY2']));
    }));

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $state: $state,
          $stateParams: $stateParams,
          modalService: modalService,
          UserDirectoryModel: UserDirectoryModel
        }, {
          ngModel: {}
        });
      }

      it('should init', function () {
        // when
        var ctrl = buildController();
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(ctrl.ngModel.settings.userId).toBe('UNUSED_FIELD');
        expect(ctrl.ngModel.settings.uusername).toBe('UNUSED_FIELD');
        expect(ctrl.ngModel.settings.userFirstName).toBe('UNUSED_FIELD');
        expect(ctrl.ngModel.settings.userLastName).toBe('UNUSED_FIELD');
        expect(ctrl.availableOrphanedUsersPolicies).toEqual(['POLICY1', 'POLICY2']);
      });

    });

  });

})();
