(function () {
  'use strict';

  var moduleName = 'coyo.admin.userDirectories';
  var controllerName = 'AdminUserDirectoriesDetailsController';

  describe('module: ' + moduleName, function () {
    var $controller, $state;
    var $rootScope, $scope, $q, userDirectory, errorService, Upload, disabledDirectoryType;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$rootScope_, _$q_, _$controller_) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $controller = _$controller_;

      $state = jasmine.createSpyObj('$state', ['go']);
      userDirectory = jasmine.createSpyObj('userDirectory', ['save']);
      errorService = jasmine.createSpyObj('errorService', ['getValidationErrors']);
      Upload = jasmine.createSpyObj('Upload', ['upload']);
      disabledDirectoryType = [];
    }));

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          $state: $state,
          userDirectory: userDirectory,
          errorService: errorService,
          Upload: Upload,
          disabledDirectoryType: disabledDirectoryType
        });
      }

      it('should save none csv user directory', function () {
        // given
        var response = {};
        userDirectory.save.and.returnValue($q.resolve(response));

        // when
        var ctrl = buildController();
        ctrl.save();
        $scope.$apply();

        // then
        expect($state.go).toHaveBeenCalledWith('^.list');
      });

      it('should save csv user directory', function () {
        // given
        var response = {};
        Upload.upload.and.returnValue($q.resolve(response));

        userDirectory.type = 'csv';
        userDirectory.name = 'CSVSync';
        userDirectory.active = true;
        userDirectory.settings = {};
        userDirectory.settings.activation = true;
        userDirectory.settings.orphanedUsersPolicy = 'DELETE';
        userDirectory.settings.recoverDeletedUsers = true;
        userDirectory.settings.fileUid = 'file-uid';
        userDirectory.csvFile = 'file';

        var expectedData = {
          name: userDirectory.name,
          active: userDirectory.active,
          activation: userDirectory.settings.activation,
          orphanedUsersPolicy: userDirectory.settings.orphanedUsersPolicy,
          recoverDeletedUsers: userDirectory.settings.recoverDeletedUsers,
          fileUid: userDirectory.settings.fileUid,
          file: userDirectory.csvFile
        };

        // when
        var ctrl = buildController();
        ctrl.save();
        $scope.$apply();

        // then
        expect(Upload.upload).toHaveBeenCalledWith(jasmine.objectContaining({
          url: '/web/users/import',
          data: expectedData
        }));
        expect($state.go).toHaveBeenCalledWith('^.list');
      });

    });

  });

})();
