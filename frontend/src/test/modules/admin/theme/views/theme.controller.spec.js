(function () {
  'use strict';

  var moduleName = 'coyo.admin.themes';

  describe('module: ' + moduleName, function () {

    var injector, $controller, themeService, tempUploadService, Upload, errorService, $q, $rootScope, adminThemeConfig;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_, _adminThemeConfig_) {
      $controller = _$controller_;
      $q = _$q_;
      $rootScope = _$rootScope_;
      themeService = jasmine.createSpyObj('themeService', ['applyTheme']);
      errorService = jasmine.createSpyObj('errorService', ['suppressNotification']);
      tempUploadService = jasmine.createSpyObj('tempUploadService', ['upload']);
      Upload = jasmine.createSpyObj('Upload', ['resize', 'imageDimensions']);
      adminThemeConfig = _adminThemeConfig_;
      injector = jasmine.createSpyObj('$injector', ['get']);
      injector.get.and.returnValue(themeService);
    }));

    var controllerName = 'AdminThemeDetailsController';

    describe('controller: ' + controllerName, function () {

      function buildController(themeVariables, customScss) {
        return $controller(controllerName, {
          $injector: injector,
          themeService: themeService,
          tempUploadService: tempUploadService,
          Upload: Upload,
          errorService: errorService,
          backendUrl: '',
          themeVariables: themeVariables,
          customScss: customScss,
          themes: [],
          theme: {isNew: function () {
            return true;
          }},
          adminThemeConfig: adminThemeConfig
        });
      }

      it('should save', function () {
        // given
        var ctrl = buildController({});
        ctrl.theme.save = jasmine.createSpy('themeSave');
        ctrl.theme.save.and.returnValue($q.resolve({}));

        ctrl.theme.colors = {'color-primary': '#123456', 'color-primary-dark': '#ffffff'};

        // when
        ctrl.save();
        $rootScope.$apply();

        // then
        expect(ctrl.theme.colors['color-primary']).toBe('#123456');
        expect(ctrl.theme.colors['color-primary-dark']).toBeFalsy();
        expect(ctrl.theme.save).toHaveBeenCalled();
        expect(themeService.applyTheme).toHaveBeenCalled();
      });

      it('should upload image', function () {
        // given
        var ctrl = buildController({});
        ctrl.theme.images = {};
        // var fileToUpload = {};
        var resizedFile = {};
        var blob = {uid: 'blob-id'};
        var retinaBlob = {uid: 'retina-blob-id'};
        // _.find(ctrl.imageConfig, {key: 'image-coyo-front'}).data.fileToUpload = fileToUpload;
        ctrl.images['image-coyo-front'] = {fileToUpload: {}};
        Upload.resize.and.returnValue($q.resolve(resizedFile));
        tempUploadService.upload.and.returnValues($q.resolve(blob), $q.resolve(retinaBlob));
        Upload.imageDimensions.and.returnValue($q.resolve({height: 100}));

        // when
        ctrl.uploadImage('image-coyo-front');
        $rootScope.$apply();

        // then
        expect(Upload.resize).toHaveBeenCalled();
        expect(tempUploadService.upload).toHaveBeenCalled();
        expect(ctrl.theme.images['image-coyo-front']).toBe('\'/web/themes/public/files/blob-id\'');
        expect(ctrl.theme.images['image-coyo-front-hd']).toBe('\'/web/themes/public/files/retina-blob-id\'');
        expect(ctrl.theme.images['height-image-front']).toBe('100px');
      });
    });
  });

})();
