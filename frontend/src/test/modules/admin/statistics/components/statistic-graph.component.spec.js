(function () {
  'use strict';

  var moduleName = 'coyo.admin.statistics';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, StatisticsModel, $rootScope, dateRangePickerModalService;

    beforeEach(module(moduleName));

    beforeEach(inject(function (_$controller_, _$q_, _$rootScope_) {
      $controller = _$controller_;
      $q = _$q_;
      $rootScope = _$rootScope_.$new();
      $scope = $rootScope.$new();
      StatisticsModel = jasmine.createSpyObj('StatisticsModel', ['get']);
      dateRangePickerModalService = jasmine.createSpyObj('dateRangePickerModalServiceProvider', [open]);

      $rootScope.screenSize = {isXs: false};

      StatisticsModel.get.and.returnValue($q.resolve({
        name: 'test-statistic',
        dataSets: [{
          name: 'test-1',
          labels: ['1', '2'],
          dataPoints: [{
            x: '1',
            y: 100
          }, {
            x: '2',
            y: 200
          }]
        }, {
          name: 'test-2',
          labels: ['1', '2'],
          dataPoints: [{
            x: '1',
            y: 101
          }, {
            x: '2',
            y: 201
          }]
        }]
      }));
    }));

    var controllerName = 'AdminStatisticGraphController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        var ctrl = $controller(controllerName, {
          StatisticsModel: StatisticsModel,
          dateRangePickerModalService: dateRangePickerModalService,
          $scope: $scope,
          $rootScope: $rootScope,
          statisticsConfig: {
            'test-statistic': {
              dataSets: ['test-1', 'test-2', 'test-3'],
              showFilters: true,
              showCount: true
            }
          }
        });
        ctrl.statisticName = 'test-statistic';
        ctrl.$onInit();
        return ctrl;
      }

      it('should get data when initialized', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.$onInit();
        $scope.$apply();

        // then
        expect(StatisticsModel.get).toHaveBeenCalled();
        expect(ctrl.statistic.name).toBe('test-statistic');
        expect(ctrl.labels.length).toBe(2);
        expect(ctrl.data[0]).toEqual([100, 200]);
        expect(ctrl.data[1]).toEqual([101, 201]);
      });

      it('should get data when changed filter', function () {
        // given
        var ctrl = buildController();

        // when
        ctrl.loading = false;
        ctrl.series = ['test-1', 'test-2', 'test-3'];
        ctrl.data = [[1, 2], [2, 3], [4, 5]];
        ctrl.onClickFilter('test-3');

        // then
        expect(ctrl.data[2]).toEqual(undefined);
      });

    });
  });

})();
