(function () {
  'use strict';

  var moduleName = 'commons.tour';
  var directiveName = 'coyo-tour-step';

  describe('module: ' + moduleName, function () {

    var $rootScope, $scope, $compile, $q, $document, $transitions, tourService, step;

    beforeEach(module(moduleName));
    beforeEach(module('commons.templates'));

    describe('directive: ' + directiveName, function () {

      beforeEach(function () {
        step = {
          topic: 'test',
          order: 5,
          title: 'My Title',
          content: 'Tour Step Content'
        };

        tourService = jasmine.createSpyObj('tourService', ['init', 'isEnabled', 'isStepRegistered', 'registerStep', 'unregisterStep']);
        $transitions = jasmine.createSpyObj('$transitions', ['onStart', 'onSuccess', 'onError']);

        module(function ($provide) {
          $provide.value('tourService', tourService);
          $provide.value('$transitions', $transitions);
        });
      });

      beforeEach(inject(function (_$rootScope_, _$compile_, _$q_, _$document_, _$transitions_) {
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $q = _$q_;
        $document = _$document_;
        $transitions = _$transitions_;

        $transitions.onSuccess.and.callFake(function (criteria, callback) {
          $rootScope.$on('transition-success-mock', callback);
        });
      }));

      function _createTemplate(tourStepTemplate) {
        return '<div ui-tour="desktopTour">' + tourStepTemplate + '</div>';
      }

      it('should render enabled step with minimal arguments', function () {
        // given
        var template = '<coyo-tour-step style="display:block; padding:5px" topic="{{step.topic}}" step-id="stepTestId" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}">Hello world</coyo-tour-step>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(true));
        tourService.isStepRegistered.and.returnValue(false);

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $document[0].body.appendChild(element[0]);
        $scope.$digest();

        // then
        var span = element.querySelectorAll('span.tour-placeholder-highlight');

        expect(span.attr('tour-step-placement')).toBe('auto');
        expect(span.attr('tour-step-belongs-to')).toBe('desktopTour');
        expect(span.attr('tour-step')).toBe(step.topic);
        expect(span.attr('tour-step-title')).toBe(step.title);
        expect(span.attr('tour-step-content')).toBe(step.content);
        expect(span.attr('tour-step-order')).toBe('' + step.order);

        // cleanup
        $document[0].body.removeChild(element[0]);
      });

      it('should render enabled step with all arguments', function () {
        // given
        var template = '<coyo-tour-step style="display:block; padding:5px" topic="{{step.topic}}" step-id="test" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}" image-url="{{step.imageUrl}}" placement="{{step.placement}}" ' +
            'popup-class="{{step.popupClass}}" append-to-body="true" no-highlight="step.noHighlight" fixed="step.fixed">Hello World</coyo-tour-step>';

        $scope.step = angular.extend(step, {
          fixed: true,
          imageUrl: 'http://coyotest.com/image.png',
          placement: 'bottom',
          noHighlight: true,
          popupClass: 'additional-class',
          appendToBody: true
        });

        tourService.isEnabled.and.returnValue($q.resolve(true));
        tourService.isStepRegistered.and.returnValue(false);

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $document[0].body.appendChild(element[0]);
        $scope.$digest();

        // then
        var tourStep = element.querySelectorAll('coyo-tour-step');
        expect(tourStep.hasClass('tour-placeholder')).toBe(true);

        var span = tourStep.querySelectorAll('span.tour-placeholder');
        expect(span.attr('tour-step-placement')).toBe('bottom');
        expect(span.attr('tour-step-popup-class')).toBe(step.popupClass);
        expect(span.attr('tour-step-fixed')).toBe('' + step.fixed);
        expect(span.attr('tour-step-scroll-into-view')).toBe('false');
        expect(span.attr('tour-step-content').indexOf('src="' + step.imageUrl + '"')).toBeGreaterThan(-1);
        expect(span.attr('tour-step-append-to-body')).toBe('true');

        // cleanup
        $document[0].body.removeChild(element[0]);
      });

      it('should update the enabled status on state change', function () {
        // given
        var template = '<coyo-tour-step style="display:block; padding:5px" topic="{{step.topic}}" step-id="test" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}">Hello World</coyo-tour-step>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(true));
        tourService.isStepRegistered.and.returnValue(false);

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $document[0].body.appendChild(element[0]);
        $scope.$digest();

        // then
        expect(element.querySelectorAll('span.tour-placeholder-highlight').attr('tour-step')).toBe('test');

        // when
        tourService.isEnabled.and.returnValue($q.resolve(false));
        $rootScope.$emit('transition-success-mock');
        $scope.$apply();

        // then
        expect(element.querySelectorAll('span.tour-placeholder-highlight').length).toBe(0);

        // cleanup
        $document[0].body.removeChild(element[0]);
      });

      it('should unregister on destroy', function () {
        var template = '<coyo-tour-step topic="{{step.topic}}" step-id="stepTestId" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}"></coyo-tour-step>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(true));

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $scope.$digest();
        var elementScope = element.find('coyo-tour-step').eq(0).scope();
        elementScope.$destroy();

        // then
        expect(tourService.unregisterStep).toHaveBeenCalledTimes(1);
        expect(tourService.unregisterStep).toHaveBeenCalledWith('stepTestId');
      });

      it('should be disabled on tour end', function () {
        var template = '<coyo-tour-step topic="{{step.topic}}" step-id="stepTestId" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}"></coyo-tour-step>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(true));

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $scope.$digest();
        var ctrl = element.find('coyo-tour-step').eq(0).controller('coyoTourStep');

        // then
        expect(ctrl.enabled).toBe(true);

        // when
        $rootScope.$emit('tour.ended');
        $scope.$apply();

        // then
        expect(ctrl.enabled).toBe(false);
      });

      it('should enable the step on manual restart', function () {
        // given
        var template = '<coyo-tour-step topic="{{step.topic}}" step-id="stepTestId" order="{{step.order}}" title="{{step.title}}" ' +
            'content="{{step.content}}"></coyo-tour-step>';
        $scope.step = step;
        tourService.isEnabled.and.returnValue($q.resolve(false));

        // when
        var element = $compile(angular.element(_createTemplate(template)))($scope);
        $scope.$digest();
        var ctrl = element.find('coyo-tour-step').eq(0).controller('coyoTourStep');

        // then
        expect(ctrl.enabled).toBe(false);

        // when
        $rootScope.$emit('tour.restart', step.topic);
        $scope.$apply();

        // then
        expect(ctrl.enabled).toBe(true);

      });

    });

  });
})();
