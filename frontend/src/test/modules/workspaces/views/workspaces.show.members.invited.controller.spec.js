(function () {
  'use strict';

  var moduleName = 'coyo.workspaces';

  describe('module: ' + moduleName, function () {

    var $controller, $q, $scope, currentUser, workspace, ExternalUserModel, invitations, event;

    beforeEach(module(moduleName));

    beforeEach(function () {
      currentUser = {
        id: 'USER-ID-ONE'
      };
      invitations = [{
        user: {
          id: 'USER-ID-ONE'
        }
      }, {
        email: 'USER-EMAIL-TWO'
      }];
      workspace = jasmine.createSpyObj('WorkspaceModel', ['removeMember', 'getInvited', 'getInvitedExternalIncluded']);
      ExternalUserModel = jasmine.createSpyObj('ExternalUserModel', ['declineExternalMember']);
      event = jasmine.createSpyObj('$event', ['stopPropagation']);

      inject(function (_$controller_, _$q_, $rootScope) {
        $controller = _$controller_;
        $q = _$q_;
        $scope = $rootScope.$new();
      });
    });

    var controllerName = 'WorkspaceMemberInvitedController';

    describe('controller: ' + controllerName, function () {

      function buildController() {
        return $controller(controllerName, {
          currentUser: currentUser,
          workspace: workspace,
          ExternalUserModel: ExternalUserModel
        });
      }

      it('should load invited members', function () {
        // given
        var response = {
          content: invitations
        };
        workspace.getInvitedExternalIncluded.and.returnValue($q.resolve(response));
        var ctrl = buildController();

        // when
        $scope.$apply();

        // then
        expect(workspace.getInvitedExternalIncluded).toHaveBeenCalled();
        expect(ctrl.currentPage).toBe(response);
        expect(ctrl.loading).toBe(false);
      });

      it('should decline an invitation', function () {
        // given
        var response = {
          content: invitations
        };
        workspace.getInvitedExternalIncluded.and.returnValue($q.resolve(response));
        workspace.removeMember.and.returnValue($q.resolve());

        var ctrl = buildController();
        $scope.$apply();

        // when
        ctrl.decline('USER-ID-ONE', event);
        $scope.$apply();

        // then
        expect(workspace.removeMember).toHaveBeenCalled();
        expect(ctrl.currentPage.content).toBeArrayOfSize(1);
        expect(ctrl.currentPage.content[0].email).toBe('USER-EMAIL-TWO');
        expect(event.stopPropagation).toHaveBeenCalled();
      });

      it('should decline an external invitation', function () {
        // given
        var response = {
          content: invitations
        };
        workspace.getInvitedExternalIncluded.and.returnValue($q.resolve(response));
        ExternalUserModel.declineExternalMember.and.returnValue($q.resolve());

        var ctrl = buildController();
        $scope.$apply();

        // when
        ctrl.declineExternal('USER-EMAIL-TWO', event);
        $scope.$apply();

        // then
        expect(ExternalUserModel.declineExternalMember).toHaveBeenCalled();
        expect(ctrl.currentPage.content).toBeArrayOfSize(1);
        expect(ctrl.currentPage.content[0].user.id).toBe('USER-ID-ONE');
        expect(event.stopPropagation).toHaveBeenCalled();
      });

    });
  });

})();
