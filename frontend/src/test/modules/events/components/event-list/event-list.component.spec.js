(function () {
  'use strict';

  var moduleName = 'coyo.events';
  var controllerName = 'EventListComponentController';

  describe('module: ' + moduleName, function () {
    var $controller, $rootScope, $scope, moment;

    beforeEach(function () {
      module(moduleName);

      inject(function (_$controller_, _$rootScope_, _moment_) {
        $controller = _$controller_;
        $rootScope = _$rootScope_;
        $scope = $rootScope.$new();
        moment = _moment_;
      });
    });

    function buildController(page) {
      return $controller(controllerName, {
        $scope: $scope,
        moment: moment
      }, {
        page: page,
        loading: false
      });
    }

    function date2Str(date) {
      return date.format('YYYY-MM-DDTHH:mm:ss');
    }

    describe('controller: ' + controllerName, function () {

      it('should check if given date is today', function () {
        // given
        var today = date2Str(moment());
        var gone = date2Str(moment().subtract(7, 'days'));

        // when
        var ctrl = buildController({});

        // then
        expect(ctrl.isToday(today)).toBe(true);
        expect(ctrl.isToday(gone)).toBe(false);
      });

      it('should check if given date is ongoing', function () {
        // given
        var start = date2Str(moment().subtract(1, 'days'));
        var end = date2Str(moment().add(1, 'days'));
        var startGone = date2Str(moment().subtract(2, 'days'));
        var endGone = date2Str(moment().subtract(1, 'days'));

        // when
        var ctrl = buildController({});

        // then
        expect(ctrl.isOngoing(start, end)).toBe(true);
        expect(ctrl.isOngoing(startGone, endGone)).toBe(false);
      });

      it('should check if given start date has passed', function () {
        // given
        var startFuture = date2Str(moment().add(1, 'minutes'));
        var startGone = date2Str(moment().subtract(1, 'minutes'));

        // when
        var ctrl = buildController({});

        // then
        expect(ctrl.hasStarted(startFuture)).toBe(false);
        expect(ctrl.hasStarted(startGone)).toBe(true);
      });

      it('should check if divider should be displayed', function () {
        // given
        var currentPage = {
          content: [
            {startDate: '2017-06-01T10:00:00'},
            {startDate: '2017-06-01T10:00:00'},
            {startDate: '2017-06-02T10:00:00'},
            {startDate: '2017-06-03T10:00:00'},
            {startDate: '2017-06-03T10:00:00'}
          ]
        };

        // when
        var ctrl = buildController(currentPage);

        // then
        expect(ctrl.showDivider(0)).toBe(true);
        expect(ctrl.showDivider(1)).toBe(false);
        expect(ctrl.showDivider(2)).toBe(true);
        expect(ctrl.showDivider(3)).toBe(true);
        expect(ctrl.showDivider(4)).toBe(false);
      });

      it('should increase the attendingCount if participation status was changed from other to ATTENDING', function () {
        // given
        var event = {attendingCount: 1};
        var currentPage = {
          content: [event]
        };
        var ctrl = buildController(currentPage);

        // when
        ctrl.onParticipationStatusChanged({newStatus: 'ATTENDING', oldStatus: 'NOT_ATTENDING'}, event);

        // then
        expect(event.attendingCount).toBe(2);
      });

      it('should decrease the attendingCount if participation status was changed from ATTENDING to other', function () {
        // given
        var event = {attendingCount: 1};
        var currentPage = {
          content: [event]
        };
        var ctrl = buildController(currentPage);

        // when
        ctrl.onParticipationStatusChanged({newStatus: 'NOT_ATTENDING', oldStatus: 'ATTENDING'}, event);

        // then
        expect(event.attendingCount).toBe(0);
      });

      it('should not decrease the attendingCount if participation status was changed from MAYBE_ATTENDING to NOT_ATTENDING', function () {
        // given
        var event = {attendingCount: 1};
        var currentPage = {
          content: [event]
        };
        var ctrl = buildController(currentPage);

        // when
        ctrl.onParticipationStatusChanged({newStatus: 'NOT_ATTENDING', oldStatus: 'MAYBE_ATTENDING'}, event);

        // then
        expect(event.attendingCount).toBe(1);
      });
    });
  });

})();
