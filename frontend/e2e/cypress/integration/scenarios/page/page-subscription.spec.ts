describe('Page creation', function () {
    const users = require('../../../fixtures/users/users.json');

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin(users.RL.loginName, users.RL.pass) //'Robert Lang'
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('subscribed user should see a private page without auto-subscription if has access', function () {
        const visibility = 'private';
        const subscription = 'off';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        const subscribedUser = users.IB.displayName; //'Ian Bold';
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('POST', '/web/pages').as('createPage');
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription, subscribedUser);
        cy.wait('@createPage').its('status').should('eq', 201);

        cy.apiLogout();
        cy.clearLocalStorage().apiLogin(users.IB.loginName, users.IB.pass).apiDismissTour();

        cy.visit('/home/timeline');
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card]').within(() => {
            cy.get('[data-test=button-page-subscribe]')
                .should('be.visible')
                .contains('Subscribe');
        })
    });

    it('any user should see and be automatically subscribed to a public page with auto-subscription for everyone', function () {
        const visibility = 'public';
        const subscription = 'everyone';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('POST', '/web/pages').as('createPage');
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription);
        cy.wait('@createPage').its('status').should('eq', 201);
        cy.apiLogout();

        cy.clearLocalStorage().apiLogin(users.IB.loginName, users.IB.pass).apiDismissTour(); //'Ian Bold'
        cy.visit('/home/timeline');
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card]').within(() => {
            cy.get('[data-test=autosubscription-hint]')
                .should('be.visible')
                .contains('automatically subscribed');
        });

        cy.apiLogout();

        cy.clearLocalStorage().apiLogin(users.NF.loginName, users.NF.pass).apiDismissTour(); //'Nancy Fork'
        cy.visit('/home/timeline');
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card]').within(() => {
            cy.get('[data-test=autosubscription-hint]')
                .should('be.visible')
                .contains('automatically subscribed');
        })
    });

    it('selected user should be automatically subscribed to a private page with auto-subscription for selected users if has access', function () {
        const visibility = 'private';
        const subscription = 'selected';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        const subscribedUser = users.IB.displayName; //'Ian Bold';
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('POST', '/web/pages').as('createPage');
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription, subscribedUser);
        cy.wait('@createPage').its('status').should('eq', 201);
        cy.apiLogout();

        cy.clearLocalStorage().apiLogin(users.IB.loginName, users.IB.pass).apiDismissTour();
        cy.visit('/home/timeline');
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card]').within(() => {
            cy.get('[data-test=autosubscription-hint]')
                .should('be.visible')
                .contains('automatically subscribed');
        })
    });
});
