describe('Page creation', function () {
    const users = require('../../../fixtures/users/users.json');

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin(users.RL.loginName, users.RL.pass) //'Robert Lang'
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should create a private page without auto-subscription', function () {
        const subscribedUser = users.IB.displayName; //'Ian Bold';
        const visibility = 'private';
        const visibilityU = visibility.toUpperCase();
        const subscription = 'off';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        cy.createPage(pageName, visibility, subscription, subscribedUser);
        cy.get('[data-test=text-page-title]').contains(pageName);
        cy.get('[data-test=page-followers-count] > coyo-page-visibility')
            .should('have.attr', 'ng-reflect-visibility', visibilityU)
    });

    it('should create a page with description and category', function () {
        const subscribedUser = users.IB.displayName; //'Ian Bold';
        const visibility = 'public';
        const subscription = 'off';
        const description = Cypress.faker.lorem.sentences();
        const category = 'Company';
        const pageName = 'Qa ' + visibility + ' page with description, category and subscription ' + subscription + ' - ' + Date.now();
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription, subscribedUser, description, category);
        cy.get('[data-test=text-page-title]').contains(pageName);
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card]').within(() => {
            cy.get('[data-test=page-description]')
                .should('contain', description);
            cy.get('[data-test=page-tile-category]')
                .should('contain', category);
        })
    });

    it('should create a public page with auto-subscription for everyone', function () {
        const visibility = 'public';
        const visibilityU = visibility.toUpperCase();
        const subscription = 'everyone';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription);
        cy.get('[data-test=text-page-title]')
            .contains(pageName);
        cy.get('[data-test=page-followers-count] > coyo-page-visibility')
            .should('have.attr', 'ng-reflect-visibility', visibilityU);
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card]').within(() => {
            cy.get('[data-test=autosubscription-hint]')
                .should('be.visible')
                .should('contain', 'automatically subscribed');
        })
    });

    it('should create a private page with auto-subscription for selected users', function () {
        const subscribedUser = users.IB.displayName; //'Ian Bold';
        const visibility = 'private';
        const visibilityU = visibility.toUpperCase();
        const subscription = 'selected';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription, subscribedUser);
        cy.get('[data-test=text-page-title]')
            .contains(pageName);
        cy.get('[data-test=page-followers-count] > coyo-page-visibility')
            .should('have.attr', 'ng-reflect-visibility', visibilityU);
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card-footer]').within(() => {
            cy.get('[data-test=page-tile-user-subscriptions-count]')
                .should('be.visible')
                .should('contain', '2');
        })
    });

    it('should create a public page with auto-subscription for selected users', function () {
        const subscribedUser = users.IB.displayName; //'Ian Bold';
        const visibility = 'public';
        const visibilityU = visibility.toUpperCase();
        const subscription = 'selected';
        const pageName = 'Qa ' + visibility + ' page with subscription ' + subscription + ' - ' + Date.now();
        const pageNameURL = pageName.replace(/\s/g, "+");

        cy.server();
        cy.route('GET', `/web/pages?**&term=${pageNameURL}`).as('getPage');

        cy.createPage(pageName, visibility, subscription, subscribedUser);
        cy.get('[data-test=text-page-title]')
            .contains(pageName);
        cy.get('[data-test=page-followers-count] > coyo-page-visibility')
            .should('have.attr', 'ng-reflect-visibility', visibilityU);
        cy.get('[data-test=navigation-pages]')
            .click();
        cy.get('[data-test=user-search-filter]').type(pageName);
        cy.wait('@getPage');
        cy.get('[data-test=page-card-footer]').within(() => {
            cy.get('[data-test=page-tile-user-subscriptions-count]')
                .should('be.visible')
                .should('contain', '2');
        })
    });
});
