describe('Workspace creation', function () {

    beforeEach(function () {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        cy.clearLocalStorage().apiLogin('rl', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should create a new public workspace', function () {
        const category = 'Ideas';
        const visibility = 'public';
        const workspaceName = `qa-ws-${visibility}-${category}-${Date.now()}`;
        cy.createWorkspace(workspaceName, category, visibility);
        // Assert that the workspace was created
        cy.get('[data-test=text-workspace-title]')
            .should('contain', workspaceName)
            .should('contain', visibility)
            .should('contain', category);
        //TODO add verification that workspace is public and has right category
    });

    it('should create a new private workspace', function () {
        const category = 'Projects';
        const visibility = 'private';
        const workspaceName = `qa-ws-${visibility}-${category}-${Date.now()}`;
        cy.createWorkspace(workspaceName, category, visibility);
        // Assert that the workspace was created
        cy.get('[data-test=text-workspace-title]')
            .should('contain', workspaceName)
            .should('contain', visibility)
            .should('contain', category);
        //TODO add verification that workspace is private and has right category
    });

    it('should create a new protected workspace', function () {
        const category = 'Products';
        const visibility = 'protected';
        const workspaceName = `qa-ws-${visibility}-${category}-${Date.now()}`;
        cy.createWorkspace(workspaceName, category, visibility);
        // Assert that the workspace was created
        cy.get('[data-test=text-workspace-title]')
            .should('contain', workspaceName)
            .should('contain', visibility)
            .should('contain', category);
        //TODO add verification that workspace is protected and has right category
    });
});
