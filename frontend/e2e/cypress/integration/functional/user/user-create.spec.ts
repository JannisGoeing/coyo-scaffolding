describe('User creation', function () {

  beforeEach(function () {
    Cypress.Cookies.defaults({
      whitelist: 'COYOSESSION'
    });
    cy.clearLocalStorage().apiLogin('rl', 'demo')
        .apiLanguage('EN')
        .apiDismissTour();
    cy.visit('/home/timeline');
  });

  afterEach(function () {
    cy.apiLogout();
  });

  it('should create a new admin', function () {
    cy.adminAreaNavigation();
    cy.createUser('Admins', 'Admin');

  });

  it('should create a new user', function () {
    cy.adminAreaNavigation();
    cy.createUser('Users', 'User');
  });

  it('should create a new external workspace member', function () {
    cy.adminAreaNavigation();
    cy.createUser('Users', 'External Workspace Member');
  });
});
