describe('Timeline comments', () => {
    let timelinePostMessage = '';
    let targetId = '';
    let targetType = '';

    beforeEach(() => {
        Cypress.Cookies.defaults({
            whitelist: 'COYOSESSION'
        });
        timelinePostMessage = 'Qa timeline-post for comment test ' + Date.now();
        cy.clearLocalStorage().apiLogin('ib', 'demo')
            .apiLanguage('EN')
            .apiDismissTour();
        cy.apiCreateTimelinePost(timelinePostMessage, false).then((response) => {
            targetId = response.id;
            targetType = response.typeName;
            cy.log('Set targetId', targetId);
            cy.log('Set targetType', targetType);
        }).apiShowNewTimelinePosts();
        cy.visit('/home/timeline');
    });

    afterEach(function () {
        cy.apiLogout();
    });

    it('should be possible to comment on a timeline entry of non-restricted post', () => {
        const comment = 'little comment for timeline comment test';

        cy.server();
        cy.route('POST', '/web/comments**').as('postTimelineComment');

        cy.get('[data-test=timeline-item]').contains(timelinePostMessage).parents('[data-test=timeline-item]').as('post').within(() => {
            cy.get('[data-test=timeline-comment-message-edit-mode]')
                .type(comment + '{enter}');
            cy.wait('@postTimelineComment').its('status').should('eq', 201);
            cy.get('@post')
                .should('contain', comment);
        });
    });

    it('should contain submit and cancel options when create a comment on a timeline post', () => {
        const comment = 'little comment for timeline comment test';

        cy.server();
        cy.route('POST', '/web/comments**').as('postTimelineComment');

        cy.get('[data-test=timeline-item]').contains(timelinePostMessage).parents('[data-test=timeline-item]').as('post').within(() => {
            cy.get('[data-test=timeline-comment-message-edit-mode]')
                .type(comment)
                .get('[data-test=comment-form-submit]').should('be.visible')
                .get('[data-test=comment-form-reset]').should('be.visible')
                .get('[data-test=comment-form-submit]').click();
            cy.wait('@postTimelineComment').its('status').should('eq', 201);
        });
    });

    it('should edit the comment on a timeline post', () => {
        const comment = 'little comment for timeline comment test';
        cy.apiCreateTimelineComment(comment, targetId, targetType);

        cy.server();
        cy.route('POST', '/web/comments**').as('postTimelineComment');

        cy.get('[data-test=timeline-stream]').contains('coyo-timeline-item', timelinePostMessage).within(() => {
            cy.get('[data-test=comment-item]').within(() => {
                cy.get('button[data-test=comment-context-menu]').click({force: true}); //TODO neither invoke('show') nor trigger('mouseover') work here
            });
        });
        cy.get('button[data-test=edit-comment-button]').click();
        cy.get('[data-test=timeline-stream]').contains('coyo-timeline-item', timelinePostMessage).within(() => {
                cy.get('[data-test=timeline-comment-message-edit-mode]').click().type(' - edited {enter}');
            cy.get('[data-test=comment-message]').should('contain', comment + ' - edited');
        });
    });

  xit('should be possible to comment with emoji on a timeline entry of non-restricted post', () => {
  });

  xit('should be possible to comment with a file attachment on a timeline entry of non-restricted post', () => {
  });

  xit('should like the comment on a timeline post', () => {
  });

  xit('should unlike the comment on a timeline post', () => {
  });

  xit('should delete the comment on a timeline post', () => {
  });

  });
