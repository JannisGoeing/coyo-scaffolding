// ***********************************************************
// Custom commands related to Landing Page
//
// Commands defined in this file may utilize Mocha’s
// shared context object: that is, aliases using `this.*`.
// Ideally, these commands utilize only variables defined in
// authentication commands at `./commands-auth.ts`,
// ***********************************************************

Cypress.Commands.add('apiCreateLandingPage', function (name: string, force: boolean = false) {
  return !force ? cy.end() : cy.requestAuth('GET', '/web/landing-pages?admin=true').then(function (response) {
    const landingPage = Cypress._.find(response.body, { name: name }) as any;
    return landingPage ? cy.requestAuth('DELETE', '/web/landing-pages/' + landingPage.id) : cy.end();
  }).then(function () {
    cy.requestAuth('POST', '/web/landing-pages', {
      name: name,
      visibility: 'PUBLIC',
      active: true,
      adminIds: [this.user.id],
      adminGroupIds: [],
      userIds: [this.user.id],
      userGroupIds: [],
    }).its('body');
  });
});
