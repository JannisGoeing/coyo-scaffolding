// ***********************************************************
// Custom commands related to coyo pages
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Command for creating a page via API
     * @param {string} name describes the name of the page
     * @param {string} visibility can be 'PUBLIC' or 'PRIVATE', case matters!
     *
     */

    apiCreatePage(name: string, visibility?: string): Chainable<any>;

    /**
     * Command for creating a page via UI
     * @param {string} pageName is a custom name for the page
     * @param {string} visibility can be 'public' or 'private'
     * @param {string} subscription can be 'off', 'everyone', or 'selected'
     * @param {string} subscribedUser is the user name like 'Ian Bold' used to select user for 'selected' subscription
     * @param {string} description is the description of the page, which is an optional field
     * @param {string} category is a page category optional field, which can be selected from the existing categories from the dropdown
     */

    createPage(pageName: string, visibility: string, subscription: string, subscribedUser?:string, description?:string, category?:string): Chainable<any>;
  }
}