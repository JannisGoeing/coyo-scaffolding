// ***********************************************************
// Custom commands for general use not related to specific components
// ***********************************************************
/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {

        /**
         * Navigate to Admin area via UI
         *
         */

        adminAreaNavigation(): Chainable<void>;

        /**
         * Command for searching for up to two words using the global search
         * @param {string} firstWord iss the first word should be a single word
         * @param {string} secondWord is the second word should be a single word
         *
         */

        search(firstWord: string, secondWord: string): Chainable<any>;

        /**
         * Command for creating an app
         * @param {string} app can be 'Blog', 'Content', 'Events', 'Documents', 'Form', 'Forum', 'List', 'Tasks', 'Timeline', 'Wiki', or 'Championship'
         *
         */

        createApp(app: string): Chainable<any>;
    }
}