// ***********************************************************
// Custom commands related to login via UI
// ***********************************************************

declare namespace Cypress {

  interface Chainable<Subject = any> {

    /**
     * Logs in the user into COYO via UI.
     *
     * @param {string} username is the login name of the user.
     * @param {string} password is the password of the user.
     * @example
     *    cy.login('rl', 'password');
     */

    login(username:string, password:string): Chainable<any>;

    /**
     * Resets the password of COYO user through the reset password email link via UI.
     *
     * @param {string} user is the login name of the user.
     * @param {string} password is the password of the user.
     * @example
     *    cy.login('rl', 'password');
     */
    resetPassword(user:string): Chainable<any>;

  }
}
