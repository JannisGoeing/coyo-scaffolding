// ***********************************************************
// Custom commands done via API
// ***********************************************************

declare namespace Cypress {
  interface Chainable {
    /**
     * Get client secret via API
     *
     */
    getClientSecret(): Cypress.Chainable<any>;

    /**
     * Command for setting a users language via API
     * @param {string} should be "DE" or "EN" or any language available for the instance
     *
     */
    apiLanguage(language: string): Cypress.Chainable<Response>;

    /**
     * Dismisses the COYO tour for the given topics.
     *
     * @param {string[]} [topics] The list of tour topics to be dismissed.
     * If left empty all possible tours will be dismissed
     * @example
     *    cy.dismissTour();
     *    cy.dismissTour(['timeline', 'pages', 'workspaces']);
     */
    apiDismissTour(topics?: string[]): Chainable<Response>;

    /**
     * Command for enabling the tour via API
     * All possible tours will be enabled
     *
     */
    apiEnableTour():Chainable<Response>;

  }
}